﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Globalization;
namespace HotelBAL
{
    public class HotelSendMail_Log
    {
        HotelDAL.HotelDA objDa = new HotelDAL.HotelDA();
        public string GetID(string preChar)
        {
            string rnmd = GetRandomNumber("1,2,3,4,5,6,7,8,9,0,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");
            return preChar + rnmd;
        }
        private string GetRandomNumber(string allowedChars1)
        {
            char[] sep = { ',' };
            string[] arr = allowedChars1.Split(sep);
            string rndString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= 9; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                rndString += temp;
            }
            return rndString;
        }

        public void SendMailCopy(HotelShared.HotelBooking SearchDetails, string mailBody, string HtlName, string BaseURL)
        {
            try
            {
                string Agent_EmailId = GetAgentEmailID(SearchDetails.AgentID, SearchDetails.PGEmail);
                DataSet MailDS = new DataSet();
                MailDS = objDa.GetMailingDetails("HOTEL_BOOKING", Agent_EmailId);

                string strHTML = "", strFileName = "", strMailMsg = "";
                string strTiketStyle = ".HtltktRjeference { background-color: #F0F0F0; color: #006600; font-size: 13px; font-weight: bold;height: 25px; padding-left: 10px;border: thin solid #000000; } .TktAgencyHeading { font-size: 13px;\tcolor: #000000;\theight: 20px; padding-left: 20px; font-weight: bold; } ";
                strTiketStyle += " .TktAgency{ float: right;\tfont-family: arial, Helvetica, sans-serif;\tfont-size: 11px; color: #000000; height: 20px;\tpadding-left: 20px;} .TktHeding{ font-size: 11px; color: #000000; height: 29px;\twidth: 92px; font-weight: bold;} .TktData{\tfont-size: 11px; color: #000000; height: 29px;\twidth: 245px;} ";
                strTiketStyle += " .TktData1{ font-size: 11px; color: #000000; height: 29px;} .HtlTkthead{ background-color: #223e53; color: #ffffff; font-size: 13px; font-weight: bold; height: 25px;\tpadding-left: 10px;} .TktHtlHeading { font-size: 11px;\tcolor: #000000;\theight: 29px;\tfont-weight: bold;\twidth: 92px; padding-left: 10px;} ";
                strTiketStyle += "  .TktHtl{font-size: 11px;\tcolor: #000000;\theight: 29px; padding-left: 10px;} .HtlRoomHeading{\tfont-size: 11px;\tfont-weight: bold;\tcolor: #000000;\theight: 29px; width: 74px;\tpadding-left: 10px;} .HtlRoom {\tfont-size: 11px; color: #000000; height: 29px;\twidth: 20px;} .htlAdtCount { font-size: 11px; color: #000000; height: 29px;\twidth: 40px;}";
                bool rightHTML = false;
                string strscrpt = "<script src='" + BaseURL + "/Hotel/JS/jquery-1.3.2.min.js' type='text/javascript'></script><script src='" + BaseURL + "/Hotel/JS/jquery-barcode.js' type='text/javascript'></script>";
                strscrpt += "<script type='text/javascript'>var btype = 'code128';var renderer = 'css';var quietZone = false;if ($('#quietzone').is(':checked') || $('#quietzone').attr('checked')) {quietZone = true;}var settings = {output: renderer,bgColor: '#FFFFFF',color: '#000000',barWidth: '1',barHeight: '110',moduleSize: '5',posX: '10',posY: '20',addQuietZone: '1'};$('#canvasTarget').hide();$('#barcodeTarget').html('').show().barcode('" + SearchDetails.Orderid + "', btype, settings);</script>";

                strFileName = ConfigurationManager.AppSettings["HotelBookingCopyPath"] + SearchDetails.BookingID + "-" + HtlName + ".html";
                strFileName = strFileName.Replace("/", "-");
                strHTML = "<html><head><title>Hotel Booking Details</title><style type='text/css'> .maindiv{border: #223e53 1px solid; margin: 10px auto 10px auto; width: 650px; font-size:12px; font-family:tahoma,Arial;}\t .text1{color:#333333; font-weight:bold;}\t .pnrdtls{font-size:12px; color:#333333; text-align:left;font-weight:bold;}\t .pnrdtls1{font-size:12px; color:#333333; text-align:left;}\t .bookdate{font-size:11px; color:#CC6600; text-align:left}\t .flthdr{font-size:11px; color:#CC6600; text-align:left; font-weight:bold}\t .fltdtls{font-size:11px; color:#333333; text-align:left;}\t.text3{font-size:11px; padding:5px;color:#333333; text-align:right}\t .hdrtext{padding-left:5px; font-size:14px; font-weight:bold; color:#FFFFFF;}\t .hdrtd{background-color:#333333;}\t  .lnk{color:#333333;text-decoration:underline;}\t  .lnk:hover{color:#333333;text-decoration:none;}\t  .contdtls{font-size:12px; padding-top:8px; padding-bottom:3px; color:#333333; font-weight:bold}\t  .hrcss{color:#CC6600; height:1px; text-align:left; width:450px;}\t </style><style type='text/css'>" + strTiketStyle + "</style></head><body>" + mailBody + strscrpt + "</body></html>";
                rightHTML = SaveTextToFile(strHTML, strFileName);

                strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />";
                strMailMsg += "Greetings of the day. <br /><br /><br /><br />";
                strMailMsg += "Your Hotel booking reference no is <b>" + SearchDetails.Orderid + "</b><br />";
                strMailMsg += "Please find an attachment for your Booked Hotel Details.<br /> <br /> <b>Holet Name:</b> " + HtlName + "<br /><b> Room Name:</b> " + SearchDetails.RoomName;
                strMailMsg += "<br /><b>CheckIn Date:</b> " + Convert.ToDateTime(SearchDetails.CheckInDate).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString();
                strMailMsg += "<br /><b>CheckOut Date:</b> " + Convert.ToDateTime(SearchDetails.CheckOutDate).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString();
                strMailMsg += "<br /><b>Total Price:</b> Rs. " + SearchDetails.TotalRoomrate.ToString() + "<br /><br />Please find an attachment of your Booked Hotel Details.";
                strMailMsg += "<br /><br /><br /><br />Best Regards,<br /><br />" + MailDS.Tables[0].Rows[0]["REGARDS"].ToString() + "</p>";


                //Sending mail
                System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
                System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
                msgMail.To.Clear();
                msgMail.To.Add(new System.Net.Mail.MailAddress(SearchDetails.PGEmail));
                msgMail.CC.Add(new System.Net.Mail.MailAddress(Agent_EmailId));

                //if (SearchDetails.HtlType == HotelShared.HotelStatus.International.ToString())
                //{
                //    MailDS = objDa.GetMailingDetails("HOTEL_BOOKING_Intl", Agent_EmailId);
                //    msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["BCC"].ToString()));
                //}
                //else
                //{
                //    MailDS = objDa.GetMailingDetails("HOTEL_BOOKING_Dom", Agent_EmailId);
                //    msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["BCC"].ToString()));
                //}
                //msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["CC"].ToString()));

                msgMail.From = new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["MAILFROM"].ToString());
                msgMail.Subject = MailDS.Tables[0].Rows[0]["SUBJECT"].ToString();
                msgMail.IsBodyHtml = true;
                msgMail.Body = strMailMsg;
                if (rightHTML)
                    msgMail.Attachments.Add(new System.Net.Mail.Attachment(strFileName));
                try
                {
                    objMail.Credentials = new System.Net.NetworkCredential(MailDS.Tables[0].Rows[0]["UserId"].ToString(), MailDS.Tables[0].Rows[0]["Pass"].ToString());
                    objMail.Host = MailDS.Tables[0].Rows[0]["SMTPCLIENT"].ToString();
                    objMail.Send(msgMail);
                }
                catch (Exception ex)
                {
                    HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SendMailCopy");
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SendMailCopy");
            }
        }
        public int SendEmail(string EmailId, string mailBody, string AgentID, string BaseURL)
        {
            try
            {
                string strTiketStyle = ".HtltktRjeference { background-color: #F0F0F0; color: #006600; font-size: 13px; font-weight: bold;height: 25px; padding-left: 10px;border: thin solid #000000; } .TktAgencyHeading { font-size: 13px;\tcolor: #000000;\theight: 20px; padding-left: 20px; font-weight: bold; } ";
                strTiketStyle += " .TktAgency{ float: right;\tfont-family: arial, Helvetica, sans-serif;\tfont-size: 11px; color: #000000; height: 20px;\tpadding-left: 20px;} .TktHeding{ font-size: 11px; color: #000000; height: 29px;\twidth: 92px; font-weight: bold;} .TktData{\tfont-size: 11px; color: #000000; height: 29px;\twidth: 245px;} ";
                strTiketStyle += " .TktData1{ font-size: 11px; color: #000000; height: 29px;} .HtlTkthead{ background-color: #223e53; color: #ffffff; font-size: 13px; font-weight: bold; height: 25px;\tpadding-left: 10px;} .TktHtlHeading { font-size: 11px;\tcolor: #000000;\theight: 29px;\tfont-weight: bold;\twidth: 92px; padding-left: 10px;} ";
                strTiketStyle += "  .TktHtl{font-size: 11px;\tcolor: #000000;\theight: 29px; padding-left: 10px;} .HtlRoomHeading{\tfont-size: 11px;\tfont-weight: bold;\tcolor: #000000;\theight: 29px; width: 74px;\tpadding-left: 10px;} .HtlRoom {\tfont-size: 11px; color: #000000; height: 29px;\twidth: 20px;} .htlAdtCount { font-size: 11px; color: #000000; height: 29px;\twidth: 40px;}";

                string Orderid = "A435456ADF";
                string strscrpt = "<script src='" + BaseURL + "/Hotel/JS/jquery-1.3.2.min.js' type='text/javascript'></script><script src='" + BaseURL + "/Hotel/JS/jquery-barcode.js' type='text/javascript'></script>";
                strscrpt += "<script type='text/javascript'>var btype = 'code128';var renderer = 'css';var quietZone = false;if ($('#quietzone').is(':checked') || $('#quietzone').attr('checked')) {quietZone = true;}var settings = {output: renderer,bgColor: '#FFFFFF',color: '#000000',barWidth: '1',barHeight: '110',moduleSize: '5',posX: '10',posY: '20',addQuietZone: '1'};$('#canvasTarget').hide();$('#barcodeTarget').html('').show().barcode('" + Orderid + "', btype, settings);</script>";
                strscrpt += "<html><head><title>Hotel Booking Details</title><style type='text/css'> .maindiv{border: #223e53 1px solid; margin: 10px auto 10px auto; width: 650px; font-size:12px; font-family:tahoma,Arial;}\t .text1{color:#333333; font-weight:bold;}\t .pnrdtls{font-size:12px; color:#333333; text-align:left;font-weight:bold;}\t .pnrdtls1{font-size:12px; color:#333333; text-align:left;}\t .bookdate{font-size:11px; color:#CC6600; text-align:left}\t .flthdr{font-size:11px; color:#CC6600; text-align:left; font-weight:bold}\t .fltdtls{font-size:11px; color:#333333; text-align:left;}\t.text3{font-size:11px; padding:5px;color:#333333; text-align:right}\t .hdrtext{padding-left:5px; font-size:14px; font-weight:bold; color:#FFFFFF;}\t .hdrtd{background-color:#333333;}\t  .lnk{color:#333333;text-decoration:underline;}\t  .lnk:hover{color:#333333;text-decoration:none;}\t  .contdtls{font-size:12px; padding-top:8px; padding-bottom:3px; color:#333333; font-weight:bold}\t  .hrcss{color:#CC6600; height:1px; text-align:left; width:450px;}\t </style><style type='text/css'>" + strTiketStyle + "</style></head><body>" + mailBody + strscrpt + "</body></html>";

                DataSet MailDS = new DataSet();
                string Agent_EmailId = GetAgentEmailID(AgentID, EmailId);
                MailDS = objDa.GetMailingDetails("HOTEL_BOOKING", AgentID);
                MailMessage msgobj = new MailMessage();

                msgobj.Subject = MailDS.Tables[0].Rows[0]["SUBJECT"].ToString();
                msgobj.Body = strscrpt;
                msgobj.IsBodyHtml = true;
                msgobj.From = new MailAddress("Hotel Booked(Ticket Copy)<" + Agent_EmailId + ">");
                msgobj.To.Add(EmailId);
                msgobj.Bcc.Add(Agent_EmailId);

                SmtpClient smtp = new SmtpClient(MailDS.Tables[0].Rows[0]["SMTPCLIENT"].ToString());
                smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(MailDS.Tables[0].Rows[0]["UserId"].ToString(), MailDS.Tables[0].Rows[0]["Pass"].ToString());
                smtp.Send(msgobj);
                return 1;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SendEmail");
                return 0;
            }
        }
        public int SendMails(string toEMail, string from, string bcc, string cc, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile)
        {
            System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
            msgMail.To.Clear();
            msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
            msgMail.From = new System.Net.Mail.MailAddress(from);
            if (!string.IsNullOrEmpty(bcc))
            {
                msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
            }
            if (!string.IsNullOrEmpty(AttachmentFile))
            {
                msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
            }

            msgMail.Subject = subject;
            msgMail.IsBodyHtml = true;
            msgMail.Body = body;
            try
            {
                objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
                objMail.Host = smtpClient;
                objMail.Send(msgMail);
                return 1;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SendMails");
                return 0;

            }
        }
        protected string GetAgentEmailID(string AgentID, string ToEmailID)
        {
            string Agent_EmailId = ToEmailID;
            try
            {
                DataSet AgencyDS = new DataSet();
                AgencyDS = objDa.GetAgencyDetails(AgentID);
                if (AgencyDS.Tables.Count > 0)
                {
                    if (AgencyDS.Tables[0].Rows.Count > 0)
                    {
                        Agent_EmailId = AgencyDS.Tables[0].Rows[0]["Email"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "GetAgentEmailID");
            }
            return Agent_EmailId;
        }
        public bool SaveTextToFile(string strData, string FullPath)
        {
            bool Saved = false;
            System.IO.StreamWriter objReader = null;
            try
            {
                objReader = new System.IO.StreamWriter(FullPath);
                objReader.Write(strData);
                objReader.Close();
                Saved = true;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SaveTextToFile");
            }
            return Saved;
        }
        public int ExceptionEmail(string mailBody)
        {
            try
            {
                DataSet MailDS = new DataSet();
                MailDS = objDa.GetMailingDetails("HOTEL_BOOKING_Err", "");
                MailMessage msgobj = new MailMessage();
                msgobj.Subject = "Exception in Flywidus Hotel Module";
                msgobj.Body = mailBody;
                msgobj.IsBodyHtml = true;
                msgobj.From = new MailAddress(MailDS.Tables[0].Rows[0]["MAILFROM"].ToString());
                msgobj.To.Add(MailDS.Tables[0].Rows[0]["MAILTO"].ToString());
                SmtpClient smtp = new SmtpClient(MailDS.Tables[0].Rows[0]["SMTPCLIENT"].ToString());
                smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(MailDS.Tables[0].Rows[0]["UserId"].ToString(), MailDS.Tables[0].Rows[0]["Pass"].ToString());
                smtp.Send(msgobj);
                return 1;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "ExceptionEmail");
                return 0;
            }
        }

        public int HotelEnquiryEmail(HotelShared.HotelSearch HotelDetails)
        {
            try
            {
                string strMailMsg = "<div style='font-family:verdana; font-size:12px'>Dear Team,<br /><br />";
                strMailMsg += "Flywidus <br /><br />";
                strMailMsg += "Please provide me lowest fare for <b>" + HotelDetails.HotelName + " </b><br />";
                strMailMsg += "Please find below details.<br /> <br />";
                strMailMsg += "<br /><b>City Name:</b> " + HotelDetails.SearchCity;
                strMailMsg += "<br /><b>CheckIn Date:</b> " + Convert.ToDateTime(HotelDetails.CheckInDate).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString();
                strMailMsg += "<br /><b>CheckOut Date:</b> " + Convert.ToDateTime(HotelDetails.CheckOutDate).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString();
                strMailMsg += "<br /><b>No of Room:</b> " + HotelDetails.NoofRoom.ToString();
                strMailMsg += "<br /><b>No of Adult:</b> " + HotelDetails.TotAdt.ToString();
                if (HotelDetails.TotChd > 0)
                    strMailMsg += "<br /><b>No of Child:</b> Rs. " + HotelDetails.TotChd.ToString();

                DataTable agencytd = objDa.GetAgencyDetails(HotelDetails.AgentID).Tables[0];
                strMailMsg += "<br /><br /><br /><br />Best Regards,<br />" + agencytd.Rows[0]["Agency_Name"].ToString() + " (" + HotelDetails.AgentID + ")<br />";
                strMailMsg += agencytd.Rows[0]["Address"].ToString() + "<br />";
                strMailMsg += agencytd.Rows[0]["City"].ToString() + "<br />";
                strMailMsg += agencytd.Rows[0]["Mobile"].ToString() + "<br />";
                strMailMsg += agencytd.Rows[0]["Email"].ToString() + "</div>";

                DataSet MailDS = new DataSet(); MailMessage msgobj = new MailMessage();
                MailDS = objDa.GetMailingDetails("HOTEL_Enquiry", "");
                if (HotelDetails.HtlType == HotelShared.HotelStatus.International.ToString())
                    msgobj.To.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["CC"].ToString()));
                else
                    msgobj.To.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["BCC"].ToString()));

                msgobj.Subject = MailDS.Tables[0].Rows[0]["SUBJECT"].ToString();
                msgobj.Body = strMailMsg;
                msgobj.IsBodyHtml = true;
                msgobj.From = new MailAddress(MailDS.Tables[0].Rows[0]["MAILFROM"].ToString());
                SmtpClient smtp = new SmtpClient(MailDS.Tables[0].Rows[0]["SMTPCLIENT"].ToString());
                smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(MailDS.Tables[0].Rows[0]["UserId"].ToString(), MailDS.Tables[0].Rows[0]["Pass"].ToString());
                smtp.Send(msgobj);
                return 1;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelEnquiryEmail");
                return 0;
            }
        }

        public int SendEmailForHold_Reject(string orderid, string HotelName, string RoomName, string Agentid, string Emailid, string triptype)
        {
            int i = 0;
            string Agent_EmailId = GetAgentEmailID(Agentid, "");
            //Sending mail
            System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
            msgMail.To.Clear();
            msgMail.To.Add(new System.Net.Mail.MailAddress(Emailid));
            msgMail.CC.Add(new System.Net.Mail.MailAddress(Agent_EmailId));
            DataSet MailDS = new DataSet();
            //if (triptype == HotelShared.HotelStatus.International.ToString())
            //{
            //    MailDS = objDa.GetMailingDetails("HOTEL_BOOKING_Intl", Agent_EmailId);
            //    msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["BCC"].ToString()));
            //}
            //else
            //{
            //    MailDS = objDa.GetMailingDetails("HOTEL_BOOKING_Dom", Agent_EmailId);
            //    msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["BCC"].ToString()));
            //}
            string strMailMsg = "";
            msgMail.CC.Add(new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["CC"].ToString()));

            msgMail.From = new System.Net.Mail.MailAddress(MailDS.Tables[0].Rows[0]["MAILFROM"].ToString());
            msgMail.Subject = MailDS.Tables[0].Rows[0]["SUBJECT"].ToString();
            msgMail.IsBodyHtml = true;
            msgMail.Body = strMailMsg;

            try
            {
                objMail.Credentials = new System.Net.NetworkCredential(MailDS.Tables[0].Rows[0]["UserId"].ToString(), MailDS.Tables[0].Rows[0]["Pass"].ToString());
                objMail.Host = MailDS.Tables[0].Rows[0]["SMTPCLIENT"].ToString();
                objMail.Send(msgMail);
                i = 1;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SendMailCopy");
            }
            return i;
        }
        public string TicketSummury(string OrderID, string BaseURL)
        {
            StringBuilder TktStr = new StringBuilder();
            try
            {
                DataSet htldtlDs = new DataSet();
                htldtlDs = objDa.htlintsummary(OrderID, "Ticket");
                if (htldtlDs != null)
                {
                    DataTable dt = htldtlDs.Tables[0];
                    DataTable dtgv = htldtlDs.Tables[1];

                    DataTable dtag = objDa.GetAgencyDetails(dt.Rows[0]["LoginID"].ToString()).Tables[0];
                    TktStr.Append("<table width='820px' border='0'  cellspacing='0'  cellpadding='0' align='center' style='font-family: arial, Helvetica, sans-serif;'>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td colspan='2' align='center' style='color: #FFFFFF;font-size: 13px;font-weight: bold;height: 25px;padding-left: 10px;border: thin solid #000000;background-color:#223E53;'>Hotel Voucher</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='border: thin solid #999999;'>");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' >");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td valign='top' background='" + BaseURL + "/images/nologo.png' style='background-repeat: no-repeat;width:137px;height:137px;'><img style='width:137px;height:137px;' src='" + BaseURL + "/AgentLogo/" + dtag.Rows[0]["User_Id"].ToString() + ".jpg' /> </td>");
                    //for agency logo
                    TktStr.Append("<td style='padding:10px;width:60%;'>");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' style='float: right;width:100%;text-align:left;' >");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 13px;color: #000000;height: 20px;padding-left: 20px;font-weight: bold;'>Agency Information</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-family: arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 20px;padding-left: 20px;'>" + dtag.Rows[0]["Agency_Name"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-family: arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 20px;padding-left: 20px;'>" + dtag.Rows[0]["Address"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-family: arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 20px;padding-left: 20px;'>" + dtag.Rows[0]["City"].ToString() + ", " + dtag.Rows[0]["State"].ToString() + ", " + dtag.Rows[0]["Country"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-family: arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 20px;padding-left: 20px;'>" + dtag.Rows[0]["Mobile"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-family: arial, Helvetica, sans-serif;font-size: 11px;color: #000000;height: 20px;padding-left: 20px;'>" + dtag.Rows[0]["Email"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("<td style='padding:10px; float:right'><div id='barcodeTarget' class='barcodeTarget'></div>");
                    TktStr.Append("<canvas id='canvasTarget' style='width:150px; height:150px;'></canvas> </td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");

                    //booking information
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='padding: 10px;border: thin solid #999999'>");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td colspan='6' style='font-size: 13px;font-weight: bold;height: 25px;padding-left: 10px;background-color:#223E53;color: #FFFFFF;border: thin solid #000000;'>Booking Details</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 85px;font-weight: bold;'>Booking ID: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 200px;'>" + dt.Rows[0]["BookingID"].ToString().ToString() + "</td>");
                    if (dt.Rows[0]["Provider"].ToString() == "TG" || dt.Rows[0]["Provider"].ToString() == "RZ")
                    {
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 92px;font-weight: bold;'></td>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 200px;'> </td>");
                    }
                    else
                    {
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 110px;font-weight: bold;'>Reservation No: </td>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 200px;'>" + dt.Rows[0]["ConfirmationNo"].ToString() + "</td>");
                    }

                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 92px;font-weight: bold;'>Status: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;'>" + dt.Rows[0]["Status"].ToString() + "</td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 85px;font-weight: bold;'>Booking Date: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 200px;'>" + dt.Rows[0]["BookingDate"].ToString() + "</td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 110px;font-weight: bold;'>Checkin Date: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 200px;'>" + Convert.ToDateTime(dt.Rows[0]["CheckIN"]).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString() + "</td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 92px;font-weight: bold;'>CheckOut Date:</td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;'>" + Convert.ToDateTime(dt.Rows[0]["CheckOut"]).ToString("D", CultureInfo.GetCultureInfo("en-GB")).ToString() + "</td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");

                    //hotel information start
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='border: thin solid #999999;' >");
                    TktStr.Append("<table width='100%' border='0' cellpadding='0'>");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td>");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td colspan='4' style='font-size: 13px;font-weight: bold;height: 25px;padding-left: 10px;background-color:#223E53;color: #FFFFFF;border: thin solid #000000;'>Hotel Details</td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 83px;padding-left: 10px;'>Hotel Name: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["HotelName"].ToString() + " ");
                    TktStr.Append(SetStars(dt.Rows[0]["StarRating"].ToString(), BaseURL));
                    TktStr.Append("</td>");
                    //TktStr.Append( "<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 83px;padding-left: 10px;'>Hotel Class: </td>");
                    //TktStr.Append( "<td style='font-size: 11px;color: #000000;height: 29px;'>" + dt.Rows[0]["StarRating"] + " Star</td>");
                    TktStr.Append("</tr>");

                    //TktStr.Append( "<tr>");
                    //TktStr.Append( "<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 83px;padding-left: 10px;'>Room Type: </td>");
                    //TktStr.Append( "<td colspan='3' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["RoomName"].ToString() + "</td>");
                    //TktStr.Append( "</tr>");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 83px;padding-left: 10px;'>Address: </td>");
                    TktStr.Append("<td colspan='3' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["Address"].ToString() + "</td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("<tr>");
                    if (dt.Rows[0]["ContactNo"].ToString() != "")
                    {
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 92px;padding-left: 10px;'>Hotel Phone No: </td>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["ContactNo"].ToString() + "</td>");
                    }
                    else
                        TktStr.Append("<td colspan='2'></td>");
                    TktStr.Append("<td colspan='2'></td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("<tr>");
                    TktStr.Append("<td colspan='4'>");
                    TktStr.Append("<table>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 11px;font-weight: bold;color: #000000;\theight: 29px;width: 74px;padding-left: 10px;'>No. of Room: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 20px;'>" + dt.Rows[0]["RoomCount"].ToString() + "</td>");
                    TktStr.Append("<td style='font-size: 11px;font-weight: bold;color: #000000;\theight: 29px;width: 74px;padding-left: 10px;'>No. of Night: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 20px;'>" + dt.Rows[0]["NightCount"].ToString() + "</td>");
                    TktStr.Append("<td style='font-size: 11px;font-weight: bold;color: #000000;\theight: 29px;width: 74px;padding-left: 10px;'>No. of Adult: </td>");
                    TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 40px;'>" + dt.Rows[0]["AdultCount"].ToString() + "</td>");
                    if (Convert.ToInt32(dt.Rows[0]["ChildCount"]) > 0)
                    {
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 92px;font-weight: bold;'>No. of Children: </td>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;width: 20px;'>" + dt.Rows[0]["ChildCount"].ToString() + "</td>");
                    }
                    else
                        TktStr.Append("<td></td><td></td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");

                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    // Hotel(Image)
                    //TktStr.Append( "<td width='20%'><img src=" + dt.Rows[0]["HtlImage"].ToString() + " width='164' height='173' /></td>");
                    TktStr.Append("</tr>");

                    //Note for Child Share bed start
                    if (dt.Rows[0]["SharingBedding"].ToString() != "")
                    {
                        TktStr.Append("<tr>");
                        TktStr.Append("<td  style='font-size: 11px;color: #000000;height: 29px;padding-left: 10px;'>");
                        if (dt.Rows[0]["Provider"].ToString() == "GTA")
                        {
                            if ((dt.Rows[0]["SharingBedding"].ToString() == "true"))
                                TktStr.Append("<strong>Note:</strong> ** Child share existing bedding **");
                            else
                                TktStr.Append("<strong>Note:</strong> ** Child provided with extra bed **");
                        }
                        else
                            if ((dt.Rows[0]["SharingBedding"].ToString() == "true"))
                                TktStr.Append("<strong>Note:</strong> ** Non Smoking Room **");

                        TktStr.Append("</td></tr>");
                    }
                    //Note for Child Share bed End

                    //Essential Information start
                    if (!string.IsNullOrEmpty(dt.Rows[0]["EssentialInfo"].ToString()))
                    {
                        TktStr.Append("<tr>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left: 10px;'>");
                        TktStr.Append(dt.Rows[0]["EssentialInfo"].ToString() + "</td></tr>");
                    }
                    //Essential Information end

                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");
                    //hotel information End

                    //room information start
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='border: thin solid #999999;' >");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >");

                    TktStr.Append("<tr style='height: 25px;background-color:#223E53;'>");
                    TktStr.Append("<td style='font-size: 13px;font-weight: bold;padding-left: 10px;border: thin solid #000000;color: #FFFFFF;'>Room Type</td>");
                    TktStr.Append("<td style='font-size: 13px;font-weight: bold;padding-left: 10px;border: thin solid #000000;color: #FFFFFF;'>Included</td>");
                    TktStr.Append("<td style='font-size: 13px;font-weight: bold;padding-left: 10px;border: thin solid #000000;color: #FFFFFF;'>No of Guest</td>");
                    TktStr.Append("</tr>");

                    if (dt.Rows[0]["Provider"].ToString() != "RoomXML")
                    {

                        for (int r = 0; r < Convert.ToInt32(dt.Rows[0]["RoomCount"]); r++)
                        {
                            TktStr.Append("<tr>");
                            TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["RoomName"].ToString() + "</td>");
                            TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>");
                            //Included In room start
                            if (dt.Rows[0]["Inclusion"].ToString() != "")
                            {
                                if (dt.Rows[0]["Provider"].ToString() == "GTA")
                                {
                                    string[] inclu = dt.Rows[0]["Inclusion"].ToString().Split('#');
                                    if (inclu.Length > 1)
                                    {
                                        TktStr.Append(" Meals (" + inclu[0]);
                                        for (int l = 1; l <= inclu.Length - 1; l++)
                                        {
                                            TktStr.Append(" and " + inclu[l]);
                                        }
                                        TktStr.Append(")");
                                    }
                                    else
                                        TktStr.Append(inclu[0]);
                                }
                                else
                                {
                                    if (dt.Rows[0]["Inclusion"].ToString() != "")
                                        TktStr.Append(dt.Rows[0]["Inclusion"].ToString().Replace(",", ", &nbsp;&nbsp;"));
                                }
                            }
                            TktStr.Append("</td>");
                            //Included In Room End

                            TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + htldtlDs.Tables[2].Rows[r]["AdultCount"].ToString() + " Adult");
                            if (Convert.ToInt32(htldtlDs.Tables[2].Rows[r]["ChildCount"]) > 0)
                            {
                                TktStr.Append(" and " + htldtlDs.Tables[2].Rows[r]["ChildCount"].ToString() + " Child");
                            }
                            TktStr.Append("</td>");

                            TktStr.Append("</tr>");
                        }
                    }
                    else
                    {
                        TktStr.Append("<tr>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dt.Rows[0]["RoomName"].ToString() + "</td>");
                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left: 10px;'>");
                        for (int r = 0; r < Convert.ToInt32(dt.Rows[0]["RoomCount"]); r++)
                        {
                            //Included In room start
                            if (dt.Rows[0]["Inclusion"].ToString() != "")
                            {
                                if (dt.Rows[0]["Provider"].ToString() == "GTA")
                                {
                                    string[] inclu = dt.Rows[0]["Inclusion"].ToString().Split('#');
                                    if (inclu.Length > 1)
                                    {
                                        TktStr.Append(" Meals (" + inclu[0]);
                                        for (int l = 1; l <= inclu.Length - 1; l++)
                                        {
                                            TktStr.Append(" and " + inclu[l]);
                                        }
                                        TktStr.Append(")");
                                    }
                                    else
                                        TktStr.Append(inclu[0]);
                                }
                                else
                                {
                                    if (dt.Rows[0]["Inclusion"].ToString() != "")
                                        TktStr.Append(dt.Rows[0]["Inclusion"].ToString().Replace(",", ", &nbsp;&nbsp;"));
                                }
                            }
                            TktStr.Append(" <div class='clear1'></div>");
                        }
                        TktStr.Append("</td>");
                        //Included In Room End

                        TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>");
                        for (int r = 0; r < Convert.ToInt32(dt.Rows[0]["RoomCount"]); r++)
                        {
                            TktStr.Append(htldtlDs.Tables[2].Rows[r]["AdultCount"].ToString() + " Adult");
                            if (Convert.ToInt32(htldtlDs.Tables[2].Rows[r]["ChildCount"]) > 0)
                            {
                                TktStr.Append(" and " + htldtlDs.Tables[2].Rows[r]["ChildCount"].ToString() + " Child");
                            }
                            TktStr.Append(" <div class='clear1'></div>");
                        }
                        TktStr.Append("</td>");
                        TktStr.Append("</tr>");
                    }

                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");
                    //room information End

                    //guest information start
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='border: thin solid #999999;' >");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td colspan='3' style='font-size: 13px;font-weight: bold;height: 25px;padding-left: 10px;background-color:#223E53;color: #FFFFFF;border: thin solid #000000;'>Guest Details</td>");
                    TktStr.Append("</tr>");

                    //code for repeat Guest Name on by one
                    //DataTable dtgv = objDa.htlintsummary(OrderID, "GetGuest").Tables[0];

                    for (int i = 0; i < dtgv.Rows.Count; i++)
                    {
                        TktStr.Append("<tr>");
                        if (dtgv.Rows[i]["GType"].ToString() == "Child")
                        {
                            TktStr.Append("<td colspan='3' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dtgv.Rows[i]["GTitle"].ToString() + " " + dtgv.Rows[i]["GFName"].ToString() + " " + dtgv.Rows[i]["GLName"].ToString());
                            TktStr.Append(" &nbsp;&nbsp;&nbsp;&nbsp; <strong> Age: </strong> " + dtgv.Rows[i]["GPhoneNo"].ToString() + "</td>");
                        }
                        else
                            TktStr.Append("<td colspan='3' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dtgv.Rows[i]["GTitle"].ToString() + " " + dtgv.Rows[i]["GFName"].ToString() + " " + dtgv.Rows[i]["GLName"].ToString() + "</td>");
                        TktStr.Append("</tr>");

                        if (i == dtgv.Rows.Count - 1)
                        {
                            TktStr.Append("<tr>");
                            TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 56px;padding-left: 10px;'>Address:<td>");
                            TktStr.Append("<td colspan='2' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dtgv.Rows[0]["Address"].ToString() + ", " + dtgv.Rows[0]["City"].ToString() + ", " + dtgv.Rows[0]["Country"].ToString() + "</td>");
                            TktStr.Append("</tr>");
                            TktStr.Append("<tr>");
                            TktStr.Append("<td style='font-size: 11px;color: #000000;height: 29px;font-weight: bold;width: 65px;padding-left: 10px;'>Phone No.:</td><td colspan='2' style='font-size: 11px;color: #000000;height: 29px;padding-left:10px;'>" + dtgv.Rows[0]["GPhoneNo"].ToString() + "</td>");
                            TktStr.Append("</tr>");
                        }
                    }

                    TktStr.Append("</table>");
                    TktStr.Append("</td>");
                    TktStr.Append("</tr>");
                    //guest information End

                    //cancellation policy Start
                    TktStr.Append("<tr class='CacncelSelection'>");
                    TktStr.Append("<td style='border: thin solid #999999;color: #000000;' >");
                    TktStr.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='font-size: 13px;font-weight: bold;height: 22px;padding-left: 11px;color: #000000;'>CANCELLATION POLICIES</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("<tr>");
                    TktStr.Append("<td style='padding: 0 11px 11px 11px; text-align: justify; font-size: 11px; line-height:20px;color: #000000;'>" + dt.Rows[0]["CancellationPoli"].ToString() + "</td>");
                    TktStr.Append("</tr>");
                    TktStr.Append("</table></td>");
                    TktStr.Append("</tr>");
                    //cancellation policy end

                    //Note for Provider start
                    if (dt.Rows[0]["Provider"].ToString() == "GTA")
                    {
                        TktStr.Append("<tr>");
                        TktStr.Append("<td style='border: thin solid #999999;background-color: #223E53;color: #FFFFFF;font-size: 11px;font-weight: bold;height: 22px;padding-left: 10px;' >");
                        TktStr.Append("FOR SUPPLIER USE ONLY: THIS SERVICE IS PAYABLE BY GTA LONDON. ONLY PAYMENT FOR EXTRAS TO BE COLLECTED FROM THE CLIENT.");
                        TktStr.Append("</td></tr>");
                    }
                    //Note for Provider end

                    TktStr.Append("</table>");
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "TicketSummury");
            }
            return TktStr.ToString();
        }

        protected string SetHotelService_GTA(string Services)
        {
            string InclImg = ""; int j = 0, k = 0, l = 0, m = 0;
            try
            {
                string[] incluions = Services.Split('#');
                for (int i = 0; i < incluions.Length; i++)
                {
                    switch (incluions[i])
                    {
                        case "*CP":
                        case "*HP":
                            if (k == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' style='margin:2px 2px; 0 2px' />";
                                k = 1;
                            }
                            break;
                        case "*PT":
                        case "*RS":
                            if (j == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/lounge.png' title='Room Service' style='margin:2px 2px; 0 2px' />";
                                j = 1;
                            }
                            break;
                        case "*DD":
                            InclImg += "<img src='../Hotel/Images/Facility/Phone.png' title='Direct dial phone' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*OP":
                            InclImg += "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*IP":
                            InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*TA":
                            InclImg += "<img src='../Hotel/Images/Facility/travel_desk.png' title='Travel agency facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*LY":
                            InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*CR":
                            InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Car rental facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*BC":
                            InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*DF":
                            InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*GY":
                            InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gymnasium' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*LF":
                            InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lifts' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*IN":
                            InclImg += "<img src='../Hotel/Images/Facility/Internet.png' title='Internet' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*MB":
                            InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*BS":
                            InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby sitting' style='margin:2px;' />";
                            break;
                        case "*BP":
                            InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*SA":
                            InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Sauna' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*LS":
                            InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*TE":
                            InclImg += "<img src='../Hotel/Images/Facility/tennis.png' title='Tennis' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*GF":
                            InclImg += "<img src='../Hotel/Images/Facility/golf.png' title='Golf' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*SV":
                        case "*TV":
                            if (l == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' style='margin:2px 2px; 0 2px' />";
                                l = 1;
                            }
                            break;
                        case "*AC":
                            InclImg += "<img src='../Hotel/Images/Facility/AC.png' title='AC' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "*HD":
                            InclImg += "<img src='../Hotel/Images/Facility/bath.png' title='Hair Dryer' style='margin:2px 2px; 0 2px' />";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelService_GTA");
            }
            return InclImg;
        }

        protected string SetHotelService_TG(DataTable HtlServices)
        {
            string InclImg = ""; int i = 0, j = 0, k = 0, l = 0, m = 0, n = 0;
            try
            {
                foreach (DataRow Services in HtlServices.Rows)
                {
                    switch (Services["Amenity_id"].ToString().Trim())
                    {
                        case "01":
                        case "344":
                            if (n == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/travel_desk.png' title='Tagency Help Desk' style='margin:2px 2px; 0 2px' />";
                                n = 0;
                            }
                            break;
                        case "03":
                        case "04":
                            if (i == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Car rental facilities' style='margin:2px 2px; 0 2px' />";
                                i = 1;
                            }
                            break;
                        case "08":
                            InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Banquet_hall' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "09":
                            InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "11":
                            InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "22":
                            InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby sitting' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "24":
                            InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "26":
                            InclImg += "<img src='../Hotel/Images/Facility/Phone.png' title='Direct dial phone' style='margin:0 2px' />";
                            break;
                        case "30":
                        case "32":
                            InclImg += "<img src='../Hotel/Images/Facility/breakfast.png' title='Tea/Coffee' style='margin:0 2px' />";
                            break;
                        case "31":
                        case "44":
                        case "65":
                            if (j == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' style='margin:2px 2px; 0 2px' />";
                                j = 1;
                            }
                            break;
                        case "40":
                            InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lifts' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "52":
                            InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gymnasium' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "54":
                        case "55":
                        case "56":
                        case "57":
                        case "58":
                            if (k == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Internet.png' title='Internet' style='margin:2px 2px; 0 2px' />";
                                k = 1;
                            }
                            break;
                        case "59":
                            InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "71":
                        case "72":
                        case "73":
                        case "74":
                        case "75":
                            if (l == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' style='margin:2px;' />";
                                l = 1;
                            }
                            break;
                        case "88":
                            InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Sauna' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "122":
                        case "360":
                            InclImg += "<img src='../Hotel/Images/Facility/AC.png' title='AC' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "126":
                        case "325":
                        case "131":
                            if (m == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' style='margin:2px 2px; 0 2px' />";
                                m = 1;
                            }
                            break;
                        case "334":
                            InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "338":
                            InclImg += "<img src='../Hotel/Images/Facility/golf.png' title='Golf' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "345":
                            InclImg = "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                            break;
                        case "355":
                            InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' style='margin:2px 2px; 0 2px' />";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelService_TG");
            }
            return InclImg;
        }

        public string HotelServices(string Provider, string HotelCode)
        {
            string Hotelinclution = "";
            try
            {
                if (Provider == "TG")
                    Hotelinclution = SetHotelService_TG(objDa.GetHotelServices(HotelCode, "property"));
                else
                    Hotelinclution = SetHotelService_GTA(HotelCode);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelServices");
            }
            return Hotelinclution;
        }

        public string SetStars(string star, string BaseURL)
        {
            string starImg = "";
            try
            {
                string[] starratings = star.Split('.');
                for (int i = 1; i <= Convert.ToInt32(starratings[0]); i++)
                {
                    starImg += "<img src='" + BaseURL + "/Hotel/Images/star.png' title='Hotel Rating' />";
                }
                if (starratings.Length > 1)
                {
                    if (starratings[1] == "5")
                        starImg += "<img src='Images/star_cut.png' alt='Hotel Rating'/>";
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetStars " + star);
            }
            return starImg;
        }
        protected string setStarratings(int stars, string BaseURL)
        {
            string starImg = "";
            for (int m = 1; m <= stars; m++)
            {
                starImg += "<img src='" + BaseURL + "/Hotel/Images/star.png' alt='Hotel Rating' />";
            }
            return starImg;
        }
    }
}

