﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL
{
   public class G8CancelPNR
    {

        public string LoginID { get; set; }
        public string LoginPass { get; set; }
        public string Iatanumber { get; set; }
        public string UserID { get; set; }
        public string UserPass { get; set; }
        public string SecurityGUID { get; set; }
        public string IP { get; set; }

        public G8CancelPNR(string loginId, string loginPass, string iatanumber, string userId, string userPass, string ip)
        {
            LoginID = loginId;
            LoginPass = loginPass;
            Iatanumber = iatanumber;
            UserID = userId;
            UserPass = userPass;
            IP = ip;

        }


        public void CancelPNR(string pnr) 
        {
            Dictionary<string, string> objDict = new Dictionary<string, string>();
            string exep = "";
            try
            {
                G8SecurityToken objSecToken = new G8SecurityToken(LoginID, LoginPass);
                G8SvcAndMethodUrls SMUrl = new G8SvcAndMethodUrls();

                SecurityGUID = objSecToken.GetSecurityToken(SMUrl.RetrieveSecurityTokenUrl, SMUrl.SecurityTokenSvcUrl, ref exep);
                if (!string.IsNullOrEmpty(SecurityGUID))
                {
                    G8TravelAgent objTa = new G8TravelAgent(SecurityGUID, Iatanumber, UserID, UserPass, IP);

                    if (objTa.LoginTA(SMUrl.LoginTAUrl, SMUrl.TASvcUrl, ref exep))
                    {
                        decimal transfee = objTa.TranFeeTA(SMUrl.TransFeeTAUrl, SMUrl.TASvcUrl, ref exep);

                        if (transfee >= 0)
                        {
                            G8Reservation objReservation = new G8Reservation(SecurityGUID, Iatanumber, UserID, UserPass, IP);

                            objReservation.RetrievePNR(SMUrl.RetrievePNRUrl, SMUrl.ReservationSvcUrl, pnr, ref objDict, ref exep);
                            objReservation.CancelPNR(SMUrl.CancelPNRUrl, SMUrl.ReservationSvcUrl, pnr, ref objDict, ref exep);
                            objReservation.SaveReservation(SMUrl.CreatePNRUrl, SMUrl.ReservationSvcUrl,pnr, ref objDict, ref exep);


                        }

                    }


                }



            }
            catch (Exception ex)
            {
                throw ex;
            }



            //return resultList;
        }



    }
}
