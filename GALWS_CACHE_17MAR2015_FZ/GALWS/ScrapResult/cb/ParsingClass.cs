﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net.Mail;
using Microsoft.VisualBasic;
using System.Threading;

namespace scrap.cb
{
    public class ParsingClass
    {
        // Methods


        // Nested Types
        public class faredetails
        {
            // Fields
            private string _avlStatus;
            private string _cabin;
            private string _farebasis;
            private string _faretype;
            private string _passengertype;
            private string _rbd;
            private string _segmentref;

            // Methods
            // public faredetails();

            // Properties
            public string avlStatus { get; set; }
            public string cabin { get; set; }
            public string farebasis { get; set; }
            public string faretype { get; set; }
            public string passengertype { get; set; }
            public string rbd { get; set; }
            public string segmentref { get; set; }
        }

        public class Flightsinformation
        {
            // Fields
            private string _companyidmc;
            private string _companyidoc;
            private string _dateofarr;
            private string _dateofdep;
            private string _deslocation;
            private string _desterminal;
            private string _electicketing;
            private string _equiptype;
            private string _flightClass;
            private string _flightFareBasis;
            private string _flightno;
            private string _flightrefno;
            private string _flightreftime;
            private List<ParsingClass.Flightsinformation> _fligtinfolst;
            private List<ParsingClass.Flightsinformation> _fligtinfolst1;
            private string _productdetailqualifier;
            private string _sourceterminal;
            private string _sourclocation;
            private string _timeofarr;
            private string _timeofdep;

            // Methods
            // public Flightsinformation();

            // Properties
            public string companyidmc { get; set; }
            public string companyidoc { get; set; }
            public string dateofarr { get; set; }
            public string dateofdep { get; set; }
            public string deslocation { get; set; }
            public string desterminal { get; set; }
            public string electicketing { get; set; }
            public string equiptype { get; set; }
            public string flightClass { get; set; }
            public string flightFareBasis { get; set; }
            public string flightno { get; set; }
            public string flightrefno { get; set; }
            public string flightreftime { get; set; }
            public List<ParsingClass.Flightsinformation> fligtinfolst { get; set; }
            public List<ParsingClass.Flightsinformation> fligtinfolst1 { get; set; }
            public string productdetailqualifier { get; set; }
            public string sourceterminal { get; set; }
            public string sourclocation { get; set; }
            public string timeofarr { get; set; }
            public string timeofdep { get; set; }
        }

        public class PaxFairDetails
        {
            // Fields
            private string _Additional;
            private string _commission;
            private string _company;
            private string _distcommission;
            private string _fareamount;
            private List<ParsingClass.faredetails> _faredetaillist;
            private List<ParsingClass.PaxFare> _paxfarelist;
            private string _paxfareno;
            private string _ptc;
            private string _serviceTax;
            private string _taxamount;
            private string _transportStageQualifier;
            private string _travellerref;
            private string _txnFee;
            private string _YQ;

            // Methods
            //public PaxFairDetails();

            // Properties
            public string Additional { get; set; }
            public string commission { get; set; }
            public string company { get; set; }
            public string distcommission { get; set; }
            public string fareamount { get; set; }
            public List<ParsingClass.faredetails> faredetaillist { get; set; }
            public List<ParsingClass.PaxFare> paxfarelist { get; set; }
            public string paxfareno { get; set; }
            public string ptc { get; set; }
            public string serviceTax { get; set; }
            public string taxamount { get; set; }
            public string transportStageQualifier { get; set; }
            public string travellerref { get; set; }
            public string txnFee { get; set; }
            public string YQ { get; set; }
        }

        public class PaxFare
        {
            // Fields
            private string _messageCode;
            private string _messageQualifier;
            private string _textdescription;

            // Methods
            // public PaxFare();

            // Properties
            public string messageCode { get; set; }
            public string messageQualifier { get; set; }
            public string textdescription { get; set; }
        }

        public class Recommendation
        {
            // Fields
            private string _currencycode;
            private string _itemno;
            private ParsingClass.PaxFairDetails _PaxFare;
            private List<ParsingClass.PaxFairDetails> _PaxFareList;
            private string _Price;
            private List<ParsingClass.SegmentFlightRef> _SegmentFlightRefList;

            // Methods
            //Recommendation;

            // Properties
            public string currencycode { get; set; }
            public string itemno { get; set; }
            public ParsingClass.PaxFairDetails PaxFare { get; set; }
            public List<ParsingClass.PaxFairDetails> PaxFareList { get; set; }
            public string Price { get; set; }
            public List<ParsingClass.SegmentFlightRef> SegmentFlightRefList { get; set; }
        }

        public class SegmentFlightRef
        {
            // Fields
            private string _flighttype;
            private List<ParsingClass.Flightsinformation> _fligtinfolist;
            private List<ParsingClass.Flightsinformation> _fligtinfolist1;
            private ParsingClass.PaxFairDetails _PaxFare;
            private List<ParsingClass.PaxFairDetails> _PaxFareList;
            private string _refQualifier;
            private string _segrefno;

            // Methods
            //public SegmentFlightRef();

            // Properties
            public string flighttype { get; set; }
            public List<ParsingClass.Flightsinformation> fligtinfolist { get; set; }
            public List<ParsingClass.Flightsinformation> fligtinfolist1 { get; set; }
            public ParsingClass.PaxFairDetails PaxFare { get; set; }
            public List<ParsingClass.PaxFairDetails> PaxFareList { get; set; }
            public string refQualifier { get; set; }
            public string segrefno { get; set; }
        }
        public string getValue(string obj)
        {
            string str = "";
            try
            {
                if (obj != null)
                {
                    if (string.IsNullOrEmpty(obj))
                    {
                        return "";
                    }
                    return obj;
                }
                str = "";
            }
            catch (Exception exception)
            {
                return exception.Message.ToString();
            }
            return str;
        }
        public string ParsingSpice(string searchparam, Hashtable[] Flttable, Hashtable[] Faretable)
        {
            string xMLData = "";
            string[] strArray = searchparam.Split(new char[] { ',', ';' });
            int num = 0;
            int num2 = Convert.ToInt32(strArray[5]);
            int num3 = Convert.ToInt32(strArray[6]);
            int num4 = Convert.ToInt32(strArray[7]);
            if (num2 != 0)
            {
                num = 1;
            }
            if (num3 != 0)
            {
                num++;
            }
            if (num4 != 0)
            {
                num++;
            }
            string datafilename = "INR";
            try
            {
                List<SegmentFlightRef> list = new List<SegmentFlightRef>();
                List<SegmentFlightRef> list2 = new List<SegmentFlightRef>();
                int length = Flttable.Length;
                for (int i = 0; i < length; i++)
                {
                    foreach (string str3 in Faretable[i].Keys)
                    {
                        SegmentFlightRef ref2 = new SegmentFlightRef();
                        foreach (string str5 in Faretable[i][str3].ToString().Split(new char[] { '^' }))
                        {
                            ref2 = new SegmentFlightRef();
                            List<PaxFairDetails> list3 = new List<PaxFairDetails>();
                            decimal num7 = 0M;
                            int num8 = 0;
                            for (int j = 0; j < 3; j++)
                            {
                                string str6 = "0";
                                string str7 = "";
                                if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "ADT";
                                }
                                else if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "CHD";
                                }
                                else if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "INF";
                                }
                                ParsingClass.faredetails faredetails = new ParsingClass.faredetails
                                {
                                    avlStatus = "",
                                    cabin = str5.Split(new char[] { '~' })[5],
                                    farebasis = str5.Split(new char[] { '~' })[6],
                                    faretype = str5.Split(new char[] { '~' })[5],
                                    passengertype = str7,
                                    segmentref = "1",
                                    rbd = str5.Split(new char[] { '~' })[6]
                                };
                                List<ParsingClass.faredetails> list4 = new List<ParsingClass.faredetails> {
                            faredetails
                        };
                                PaxFare fare = new PaxFare
                                {
                                    messageQualifier = "LCCAPI",
                                    textdescription = "",
                                    messageCode = ""
                                };
                                List<PaxFare> list5 = new List<PaxFare> {
                            fare
                        };
                                PaxFairDetails details = new PaxFairDetails
                                {
                                    paxfarelist = list5
                                };
                                num7 = Convert.ToDecimal(str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1]);
                                details.fareamount = Convert.ToString(num7);
                                if ((str7 == "ADT") && (num8 == 0))
                                {
                                    details.taxamount = str5.Split(new char[] { '~' })[4];
                                    num8++;
                                }
                                else
                                {
                                    details.taxamount = "0";
                                }
                                details.YQ = str6;
                                details.ptc = str7;
                                details.paxfareno = "1";
                                details.faredetaillist = list4;
                                if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    for (int k = 0; k < num2; k++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num3 != 0))
                                {
                                    for (int m = 0; m < num3; m++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num4 != 0))
                                {
                                    for (int n = 0; n < num4; n++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if ((str5.Split(new char[] { '~' }).Length == 8) && (str5.Split(new char[] { '~' })[7] != ""))
                                {
                                    datafilename = str5.Split(new char[] { '~' })[7];
                                }
                            }
                            new List<Flightsinformation>();
                            if (Flttable[i].ContainsKey(str3))
                            {
                                List<Flightsinformation> list6 = new List<Flightsinformation>();
                                string[] strArray2 = Flttable[i][str3].ToString().Split(new char[] { '^' });
                                for (int num13 = 0; num13 < strArray2.Length; num13++)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            {
                                                Flightsinformation flightsinformation = new Flightsinformation
                                                {
                                                    flightFareBasis = "",
                                                    companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                                    dateofdep = strArray2[num13].Split(new char[] { '~' })[3],
                                                    deslocation = strArray2[num13].Split(new char[] { '~' })[2],
                                                    desterminal = "",
                                                    electicketing = "Y",
                                                    equiptype = "",
                                                    flightClass = "",
                                                    flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]),
                                                    flightrefno = "1",
                                                    sourceterminal = "",
                                                    sourclocation = strArray2[num13].Split(new char[] { '~' })[1],
                                                    timeofarr = strArray2[num13].Split(new char[] { '~' })[6],
                                                    timeofdep = strArray2[num13].Split(new char[] { '~' })[4]
                                                };
                                                flightsinformation.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation.timeofarr, flightsinformation.timeofdep));
                                                list6.Add(flightsinformation);
                                                flightsinformation.fligtinfolst = list6;
                                                ref2.fligtinfolist = list6;
                                                break;
                                            }
                                        case 1:
                                            {
                                                Flightsinformation flightsinformation2 = new Flightsinformation
                                                {
                                                    flightFareBasis = "",
                                                    companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                                    dateofdep = strArray2[num13].Split(new char[] { '~' })[3],
                                                    deslocation = strArray2[num13].Split(new char[] { '~' })[2],
                                                    desterminal = "",
                                                    electicketing = "Y",
                                                    equiptype = "",
                                                    flightClass = "",
                                                    flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]),
                                                    flightrefno = "1",
                                                    sourceterminal = "",
                                                    sourclocation = strArray2[num13].Split(new char[] { '~' })[1],
                                                    timeofarr = strArray2[num13].Split(new char[] { '~' })[6],
                                                    timeofdep = strArray2[num13].Split(new char[] { '~' })[4]
                                                };
                                                flightsinformation2.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation2.timeofarr, flightsinformation2.timeofdep));
                                                list6.Add(flightsinformation2);
                                                flightsinformation2.fligtinfolst = list6;
                                                ref2.fligtinfolist = list6;
                                                break;
                                            }
                                    }
                                }
                            }
                            ref2.PaxFareList = list3;
                            switch (i)
                            {
                                case 0:
                                    list.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;

                                case 1:
                                    list2.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;
                            }
                        }
                    }
                }
                List<Recommendation> recommendationlst = new List<Recommendation>();
                Recommendation item = new Recommendation
                {
                    SegmentFlightRefList = list
                };
                recommendationlst.Add(item);
                xMLData = this.parseObjectToXML(recommendationlst, "LCC", "", "INR", "", "", "0");
                xMLData = "<InflAirResResponse>" + xMLData + "</InflAirResResponse>";
                xMLData = this.XMLToStringForm(xMLData, "", "-LCC", datafilename, searchparam, "");
                if (list2.Count > 0)
                {
                    recommendationlst = new List<Recommendation>();
                    item = new Recommendation
                    {
                        SegmentFlightRefList = list2
                    };
                    recommendationlst.Add(item);
                    string str8 = "";
                    str8 = this.parseObjectToXML(recommendationlst, "LCC", "", datafilename, "", "", "0");
                    str8 = "<InflAirResResponse>" + str8 + "</InflAirResResponse>";
                    str8 = this.XMLToStringForm(str8, "", "-LCC", datafilename, searchparam, "");
                    xMLData = xMLData + "()" + str8;
                }
            }
            catch (Exception)
            {
            }
            return xMLData;
        }
        public string parseObjectToXML(List<Recommendation> recommendationlst, string Supplier, string MerchantID, string CurrencyCode, string donecarduser, string MerchantURL, string JSONflag)
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                string[] strArray = donecarduser.Split(new char[] { '$' });
                string[,] markupComm = null;
                try
                {
                    markupComm = CommonFunction.GetMarkupComm(strArray[0], MerchantID, MerchantURL, strArray[3]);
                }
                catch (Exception exception)
                {
                    exception.ToString();
                }
                foreach (Recommendation recommendation in recommendationlst)
                {
                    if (recommendation.SegmentFlightRefList != null)
                    {
                        foreach (SegmentFlightRef ref2 in recommendation.SegmentFlightRefList)
                        {
                            builder.Append("<SegmentFlightRef>");
                            builder.Append("<segrefno>" + this.getValue(ref2.segrefno) + "</segrefno>");
                            builder.Append("<refQualifier>" + this.getValue(ref2.refQualifier) + "</refQualifier>");
                            string str = "";
                            int num = 0;
                            int num2 = 0;
                            string str2 = "";
                            string str3 = "";
                            foreach (Flightsinformation flightsinformation in ref2.fligtinfolist)
                            {
                                if (num != 0)
                                {
                                    break;
                                }
                                builder.Append("<Outbound>");
                                if (flightsinformation.fligtinfolst != null)
                                {
                                    foreach (Flightsinformation flightsinformation2 in flightsinformation.fligtinfolst)
                                    {
                                        builder.Append("<Flightinfo>");
                                        builder.Append("<refno>" + this.getValue(flightsinformation2.flightrefno) + "</refno>");
                                        builder.Append("<flighttime>" + this.getValue(flightsinformation2.flightreftime) + "</flighttime>");
                                        builder.Append("<DOD>" + this.getValue(flightsinformation2.dateofdep) + "</DOD>");
                                        builder.Append("<TOD>" + this.getValue(flightsinformation2.timeofdep) + "</TOD>");
                                        builder.Append("<DOA>" + this.getValue(flightsinformation2.dateofarr) + "</DOA>");
                                        builder.Append("<TOA>" + this.getValue(flightsinformation2.timeofarr) + "</TOA>");
                                        builder.Append("<SrcLocation>");
                                        builder.Append("<Source>" + this.getValue(flightsinformation2.sourclocation) + "</Source>");
                                        builder.Append("<Terminal>" + this.getValue(flightsinformation2.sourceterminal) + "</Terminal>");
                                        builder.Append("</SrcLocation>");
                                        builder.Append("<DstLocation>");
                                        builder.Append("<Destination>" + this.getValue(flightsinformation2.deslocation) + "</Destination>");
                                        builder.Append("<Terminal>" + this.getValue(flightsinformation2.desterminal) + "</Terminal>");
                                        builder.Append("</DstLocation>");
                                        builder.Append("<marketingCarrier>" + this.getValue(flightsinformation2.companyidmc) + "</marketingCarrier>");
                                        this.getValue(flightsinformation2.companyidmc);
                                        builder.Append("<operatingCarrier>" + this.getValue(flightsinformation2.companyidoc) + "</operatingCarrier>");
                                        builder.Append("<flightno>" + this.getValue(flightsinformation2.flightno) + "</flightno>");
                                        builder.Append("<equiptype>" + this.getValue(flightsinformation2.equiptype) + "</equiptype>");
                                        builder.Append("<elecTicketing>" + this.getValue(flightsinformation2.electicketing) + "</elecTicketing>");
                                        builder.Append("<productdetailqualifier>" + this.getValue(flightsinformation2.productdetailqualifier) + "</productdetailqualifier>");
                                        if (JSONflag == "1")
                                        {
                                            builder.Append("<FlightClass>" + this.getValue(flightsinformation2.flightClass) + "</FlightClass>");
                                        }
                                        builder.Append("</Flightinfo>");
                                        if (string.IsNullOrEmpty(str2))
                                        {
                                            str2 = this.getValue(flightsinformation2.flightFareBasis);
                                        }
                                        else
                                        {
                                            str2 = str2 + "~" + this.getValue(flightsinformation2.flightFareBasis);
                                        }
                                        str = this.getValue(flightsinformation2.companyidoc);
                                    }
                                }
                                if (num == 0)
                                {
                                    builder.Append("</Outbound>");
                                }
                                num++;
                            }
                            if (ref2.fligtinfolist1 != null)
                            {
                                foreach (Flightsinformation flightsinformation3 in ref2.fligtinfolist1)
                                {
                                    if (num2 != 0)
                                    {
                                        break;
                                    }
                                    builder.Append("<Inbound>");
                                    if (flightsinformation3.fligtinfolst1 != null)
                                    {
                                        foreach (Flightsinformation flightsinformation4 in flightsinformation3.fligtinfolst1)
                                        {
                                            builder.Append("<Flightinfo>");
                                            builder.Append("<refno>" + this.getValue(flightsinformation4.flightrefno) + "</refno>");
                                            builder.Append("<flighttime>" + this.getValue(flightsinformation4.flightreftime) + "</flighttime>");
                                            builder.Append("<DOD>" + this.getValue(flightsinformation4.dateofdep) + "</DOD>");
                                            builder.Append("<TOD>" + this.getValue(flightsinformation4.timeofdep) + "</TOD>");
                                            builder.Append("<DOA>" + this.getValue(flightsinformation4.dateofarr) + "</DOA>");
                                            builder.Append("<TOA>" + this.getValue(flightsinformation4.timeofarr) + "</TOA>");
                                            builder.Append("<SrcLocation>");
                                            builder.Append("<Source>" + this.getValue(flightsinformation4.sourclocation) + "</Source>");
                                            builder.Append("<Terminal>" + this.getValue(flightsinformation4.sourceterminal) + "</Terminal>");
                                            builder.Append("</SrcLocation>");
                                            builder.Append("<DstLocation>");
                                            builder.Append("<Destination>" + this.getValue(flightsinformation4.deslocation) + "</Destination>");
                                            builder.Append("<Terminal>" + this.getValue(flightsinformation4.desterminal) + "</Terminal>");
                                            builder.Append("</DstLocation>");
                                            builder.Append("<marketingCarrier>" + this.getValue(flightsinformation4.companyidmc) + "</marketingCarrier>");
                                            this.getValue(flightsinformation4.companyidmc);
                                            builder.Append("<operatingCarrier>" + this.getValue(flightsinformation4.companyidoc) + "</operatingCarrier>");
                                            builder.Append("<flightno>" + this.getValue(flightsinformation4.flightno) + "</flightno>");
                                            builder.Append("<equiptype>" + this.getValue(flightsinformation4.equiptype) + "</equiptype>");
                                            builder.Append("<elecTicketing>" + this.getValue(flightsinformation4.electicketing) + "</elecTicketing>");
                                            builder.Append("<productdetailqualifier>" + this.getValue(flightsinformation4.productdetailqualifier) + "</productdetailqualifier>");
                                            if (JSONflag == "1")
                                            {
                                                builder.Append("<FlightClass>" + this.getValue(flightsinformation4.flightClass) + "</FlightClass>");
                                            }
                                            builder.Append("</Flightinfo>");
                                            if (string.IsNullOrEmpty(str3))
                                            {
                                                str3 = this.getValue(flightsinformation4.flightClass);
                                            }
                                            else
                                            {
                                                str3 = str3 + "~" + this.getValue(flightsinformation4.flightClass);
                                            }
                                            str = this.getValue(flightsinformation4.companyidoc);
                                        }
                                    }
                                    if (num2 == 0)
                                    {
                                        builder.Append("</Inbound>");
                                    }
                                    num2++;
                                }
                            }
                            builder.Append("<FareDetails>");
                            try
                            {
                                if (ref2.PaxFareList != null)
                                {
                                    string str4 = "";
                                    int index = 0;
                                    foreach (PaxFairDetails details in ref2.PaxFareList)
                                    {
                                        str4 = this.getValue(details.ptc);
                                        string[] strArray3 = new string[3];
                                        if (index == 0)
                                        {
                                            try
                                            {
                                                string str5 = string.Concat(new object[] { str, ",", Convert.ToDouble(this.getValue(details.fareamount)) - Convert.ToDouble(this.getValue(details.taxamount)), ",", this.getValue(details.taxamount) });
                                                string str6 = "";
                                                str6 = donecarduser;
                                                string[] strArray4 = (str6 + "$" + str5).Split(new char[] { '$' });
                                                string txnData = MerchantID + "()" + strArray4[0];
                                                strArray3 = CommonFunction.OfflineMarkupComm(txnData, strArray4[4], strArray4[1], strArray4[2], markupComm).Split(new char[] { '$' });
                                                builder.Append("<TxnFee>" + strArray3[0] + "</TxnFee>");
                                                builder.Append("<Commission>" + strArray3[1] + "</Commission>");
                                            }
                                            catch (Exception exception2)
                                            {
                                                exception2.ToString();
                                            }
                                        }
                                        if (str4 == "ADT")
                                        {
                                            builder.Append("<Adult>");
                                        }
                                        else if (((str4 == "CH") | (str4 == "CHD")) | (str4 == "CNN"))
                                        {
                                            builder.Append("<Child>");
                                        }
                                        else
                                        {
                                            builder.Append("<INF>");
                                        }
                                        builder.Append("<paxfareno>" + this.getValue(details.paxfareno) + "</paxfareno>");
                                        builder.Append("<fareamt>" + this.getValue(details.fareamount) + "</fareamt>");
                                        builder.Append("<YQ>" + this.getValue(details.YQ) + "</YQ>");
                                        builder.Append("<taxamt>" + this.getValue(details.taxamount) + "</taxamt>");
                                        builder.Append("<transStageQual>" + this.getValue(details.transportStageQualifier) + "</transStageQual>");
                                        builder.Append("<company>" + this.getValue(details.company) + "</company>");
                                        builder.Append("<ptc>" + this.getValue(details.ptc) + "</ptc>");
                                        builder.Append("<travellerref>" + this.getValue(details.travellerref) + "</travellerref>");
                                        if (details.paxfarelist != null)
                                        {
                                            foreach (PaxFare fare in details.paxfarelist)
                                            {
                                                builder.Append("<Fare>");
                                                builder.Append("<msgQual>" + this.getValue(fare.messageQualifier) + "</msgQual>");
                                                builder.Append("<msgcode>" + this.getValue(fare.messageCode) + "</msgcode>");
                                                builder.Append("<Description>" + this.getValue(fare.textdescription) + "</Description>");
                                                builder.Append("</Fare>");
                                            }
                                        }
                                        else
                                        {
                                            builder.Append("<Fare>");
                                            builder.Append("</Fare>");
                                        }
                                        if (details.faredetaillist != null)
                                        {
                                            foreach (ParsingClass.faredetails faredetails in details.faredetaillist)
                                            {
                                                try
                                                {
                                                    string[] strArray5 = this.getValue(faredetails.rbd).Split(new char[] { ':' });
                                                    string[] strArray6 = this.getValue(faredetails.farebasis).Split(new char[] { ':' });
                                                    string[] strArray7 = strArray6[0].Split(new char[] { '~' });
                                                    string[] strArray8 = strArray6[1].Split(new char[] { '~' });
                                                    strArray5[0].Split(new char[] { '~' });
                                                    strArray5[1].Split(new char[] { '~' });
                                                    string[] strArray9 = str2.Split(new char[] { '~' });
                                                    string[] strArray10 = str3.Split(new char[] { '~' });
                                                    for (int i = 0; i <= (strArray9.Length - 1); i++)
                                                    {
                                                        string str9 = "";
                                                        str9 = strArray9[i] + "-";
                                                        int length = str9.Length;
                                                        int startIndex = strArray5[0].IndexOf(str9) + length;
                                                        string str10 = strArray5[0].Substring(startIndex, strArray5[0].IndexOf("~", startIndex) - startIndex);
                                                        builder.Append("<FareDetail>");
                                                        builder.Append("<segref>" + this.getValue(faredetails.segmentref) + "</segref>");
                                                        builder.Append("<rbd>" + str10 + "</rbd>");
                                                        builder.Append("<AvlStatus>" + this.getValue(faredetails.avlStatus) + "</AvlStatus>");
                                                        builder.Append("<cabin>" + this.getValue(faredetails.cabin) + "</cabin>");
                                                        if (i < strArray6.Length)
                                                        {
                                                            builder.Append("<FareBasis>" + strArray7[index] + "</FareBasis>");
                                                        }
                                                        builder.Append("<passengertype>" + this.getValue(faredetails.passengertype) + "</passengertype>");
                                                        builder.Append("<faretype>" + this.getValue(faredetails.faretype) + "</faretype>");
                                                        builder.Append("</FareDetail>");
                                                    }
                                                    if (!string.IsNullOrEmpty(strArray10[0]))
                                                    {
                                                        for (int j = 0; j <= (strArray10.Length - 1); j++)
                                                        {
                                                            string str11 = "";
                                                            str11 = strArray10[j] + "-";
                                                            int num8 = str11.Length;
                                                            int num9 = strArray5[1].IndexOf(str11) + num8;
                                                            string str12 = strArray5[1].Substring(num9, strArray5[1].IndexOf("~", num9) - num9);
                                                            builder.Append("<FareDetail>");
                                                            builder.Append("<segref>" + this.getValue(faredetails.segmentref) + "</segref>");
                                                            builder.Append("<rbd>" + str12 + "</rbd>");
                                                            builder.Append("<AvlStatus>" + this.getValue(faredetails.avlStatus) + "</AvlStatus>");
                                                            builder.Append("<cabin>" + this.getValue(faredetails.cabin) + "</cabin>");
                                                            if (j < strArray6.Length)
                                                            {
                                                                builder.Append("<FareBasis>" + strArray8[index] + "</FareBasis>");
                                                            }
                                                            builder.Append("<passengertype>" + this.getValue(faredetails.passengertype) + "</passengertype>");
                                                            builder.Append("<faretype>" + this.getValue(faredetails.faretype) + "</faretype>");
                                                            builder.Append("</FareDetail>");
                                                        }
                                                    }
                                                }
                                                catch (Exception exception3)
                                                {
                                                    exception3.ToString();
                                                    builder.Append("<FareDetail>");
                                                    builder.Append("<segref>" + this.getValue(faredetails.segmentref) + "</segref>");
                                                    builder.Append("<rbd>" + this.getValue(faredetails.rbd) + "</rbd>");
                                                    builder.Append("<AvlStatus>" + this.getValue(faredetails.avlStatus) + "</AvlStatus>");
                                                    builder.Append("<cabin>" + this.getValue(faredetails.cabin) + "</cabin>");
                                                    builder.Append("<FareBasis>" + this.getValue(faredetails.farebasis) + "</FareBasis>");
                                                    builder.Append("<passengertype>" + this.getValue(faredetails.passengertype) + "</passengertype>");
                                                    builder.Append("<faretype>" + this.getValue(faredetails.faretype) + "</faretype>");
                                                    builder.Append("</FareDetail>");
                                                }
                                            }
                                        }
                                        if (str4 == "ADT")
                                        {
                                            builder.Append("</Adult>");
                                        }
                                        else if (((str4 == "CH") | (str4 == "CHD")) | (str4 == "CNN"))
                                        {
                                            builder.Append("</Child>");
                                        }
                                        else
                                        {
                                            builder.Append("</INF>");
                                        }
                                        index++;
                                    }
                                }
                            }
                            catch (Exception exception4)
                            {
                                exception4.ToString();
                            }
                            builder.Append("</FareDetails>");
                            builder.Append("</SegmentFlightRef>");
                        }
                    }
                }
            }
            catch (Exception exception5)
            {
                exception5.ToString();
            }
            return builder.ToString();
        }

        public string ParsingAirAsia(string searchparam, Hashtable[] Flttable, Hashtable[] Faretable)
        {
            string xMLData = "";
            string[] strArray = searchparam.Split(new char[] { ',', ';' });
            int num = 0;
            int num2 = Convert.ToInt32(strArray[5]);
            int num3 = Convert.ToInt32(strArray[6]);
            int num4 = Convert.ToInt32(strArray[7]);
            if (num2 != 0)
            {
                num = 1;
            }
            if (num3 != 0)
            {
                num++;
            }
            if (num4 != 0)
            {
                num++;
            }
            string datafilename = "INR";
            try
            {
                List<SegmentFlightRef> list = new List<SegmentFlightRef>();
                List<SegmentFlightRef> list2 = new List<SegmentFlightRef>();
                int length = Flttable.Length;
                for (int i = 0; i < length; i++)
                {
                    foreach (string str3 in Faretable[i].Keys)
                    {
                        SegmentFlightRef ref2 = new SegmentFlightRef();
                        foreach (string str5 in Faretable[i][str3].ToString().Split(new char[] { '^' }))
                        {
                            ref2 = new SegmentFlightRef();
                            List<PaxFairDetails> list3 = new List<PaxFairDetails>();
                            decimal num7 = 0M;
                            int num8 = 0;
                            for (int j = 0; j < 3; j++)
                            {
                                string str6 = "0";
                                string str7 = "";
                                if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "ADT";
                                }
                                else if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "CHD";
                                }
                                else if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str7 = "INF";
                                }
                                ParsingClass.faredetails faredetails = new ParsingClass.faredetails
                                {
                                    avlStatus = "",
                                    cabin = str5.Split(new char[] { '~' })[5],
                                    farebasis = str5.Split(new char[] { '~' })[6],
                                    faretype = str5.Split(new char[] { '~' })[5],
                                    passengertype = str7,
                                    segmentref = "1",
                                    rbd = str5.Split(new char[] { '~' })[6]
                                };
                                List<ParsingClass.faredetails> list4 = new List<ParsingClass.faredetails> {
                            faredetails
                        };
                                PaxFare fare = new PaxFare
                                {
                                    messageQualifier = "LCCAPI",
                                    textdescription = "",
                                    messageCode = ""
                                };
                                List<PaxFare> list5 = new List<PaxFare> {
                            fare
                        };
                                PaxFairDetails details = new PaxFairDetails
                                {
                                    paxfarelist = list5
                                };
                                num7 = Convert.ToDecimal(str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1]);
                                details.fareamount = Convert.ToString(num7);
                                if ((str7 == "ADT") && (num8 == 0))
                                {
                                    details.taxamount = str5.Split(new char[] { '~' })[4];
                                    num8++;
                                }
                                if ((str7 == "CHD") && (num8 == 1))
                                {
                                    details.taxamount = str5.Split(new char[] { '~' })[4];
                                    num8++;
                                }
                                if (str7 == "INF")
                                {
                                    details.taxamount = "0";
                                }
                                details.YQ = str6;
                                details.ptc = str7;
                                details.paxfareno = "1";
                                details.faredetaillist = list4;
                                if ((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    for (int k = 0; k < num2; k++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num3 != 0))
                                {
                                    for (int m = 0; m < num3; m++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str5.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num4 != 0))
                                {
                                    for (int n = 0; n < num4; n++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if ((str5.Split(new char[] { '~' }).Length == 8) && (str5.Split(new char[] { '~' })[7] != ""))
                                {
                                    datafilename = str5.Split(new char[] { '~' })[7];
                                }
                            }
                            new List<Flightsinformation>();
                            if (Flttable[i].ContainsKey(str3))
                            {
                                List<Flightsinformation> list6 = new List<Flightsinformation>();
                                string[] strArray2 = Flttable[i][str3].ToString().Split(new char[] { '^' });
                                for (int num13 = 0; num13 < strArray2.Length; num13++)
                                {
                                    switch (i)
                                    {
                                        case 0:
                                            {
                                                Flightsinformation flightsinformation = new Flightsinformation
                                                {
                                                    flightFareBasis = "",
                                                    companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                                    dateofdep = strArray2[num13].Split(new char[] { '~' })[3],
                                                    deslocation = strArray2[num13].Split(new char[] { '~' })[2],
                                                    desterminal = "",
                                                    electicketing = "Y",
                                                    equiptype = "",
                                                    flightClass = "",
                                                    flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]),
                                                    flightrefno = "1",
                                                    sourceterminal = "",
                                                    sourclocation = strArray2[num13].Split(new char[] { '~' })[1],
                                                    timeofarr = strArray2[num13].Split(new char[] { '~' })[6],
                                                    timeofdep = strArray2[num13].Split(new char[] { '~' })[4]
                                                };
                                                flightsinformation.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation.timeofarr, flightsinformation.timeofdep));
                                                list6.Add(flightsinformation);
                                                flightsinformation.fligtinfolst = list6;
                                                ref2.fligtinfolist = list6;
                                                break;
                                            }
                                        case 1:
                                            {
                                                Flightsinformation flightsinformation2 = new Flightsinformation
                                                {
                                                    flightFareBasis = "",
                                                    companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                                    dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                                    dateofdep = strArray2[num13].Split(new char[] { '~' })[3],
                                                    deslocation = strArray2[num13].Split(new char[] { '~' })[2],
                                                    desterminal = "",
                                                    electicketing = "Y",
                                                    equiptype = "",
                                                    flightClass = "",
                                                    flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]),
                                                    flightrefno = "1",
                                                    sourceterminal = "",
                                                    sourclocation = strArray2[num13].Split(new char[] { '~' })[1],
                                                    timeofarr = strArray2[num13].Split(new char[] { '~' })[6],
                                                    timeofdep = strArray2[num13].Split(new char[] { '~' })[4]
                                                };
                                                flightsinformation2.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation2.timeofarr, flightsinformation2.timeofdep));
                                                list6.Add(flightsinformation2);
                                                flightsinformation2.fligtinfolst = list6;
                                                ref2.fligtinfolist = list6;
                                                break;
                                            }
                                    }
                                }
                            }
                            ref2.PaxFareList = list3;
                            switch (i)
                            {
                                case 0:
                                    list.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;

                                case 1:
                                    list2.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;
                            }
                        }
                    }
                }
                List<Recommendation> recommendationlst = new List<Recommendation>();
                Recommendation item = new Recommendation
                {
                    SegmentFlightRefList = list
                };
                recommendationlst.Add(item);
                xMLData = this.parseObjectToXML(recommendationlst, "LCC", "", "INR", "", "", "0");
                xMLData = "<InflAirResResponse>" + xMLData + "</InflAirResResponse>";
                xMLData = this.XMLToStringForm(xMLData, "", "-LCC", datafilename, searchparam, "");
                if (list2.Count > 0)
                {
                    recommendationlst = new List<Recommendation>();
                    item = new Recommendation
                    {
                        SegmentFlightRefList = list2
                    };
                    recommendationlst.Add(item);
                    string str8 = "";
                    str8 = this.parseObjectToXML(recommendationlst, "LCC", "", datafilename, "", "", "0");
                    str8 = "<InflAirResResponse>" + str8 + "</InflAirResResponse>";
                    str8 = this.XMLToStringForm(str8, "", "-LCC", datafilename, searchparam, "");
                    xMLData = xMLData + "()" + str8;
                }
            }
            catch (Exception)
            {
            }
            return xMLData;
        }
        public string ParsingPegasus(string searchparam, Hashtable[] Flttable, Hashtable[] Faretable)
        {
            string xMLData = "";
            string[] strArray = searchparam.Split(new char[] { ',', ';' });
            int num = 0;
            int num2 = Convert.ToInt32(strArray[5]);
            int num3 = Convert.ToInt32(strArray[6]);
            int num4 = Convert.ToInt32(strArray[7]);
            if (num2 != 0)
            {
                num = 1;
            }
            if (num3 != 0)
            {
                num++;
            }
            if (num4 != 0)
            {
                num++;
            }
            string datafilename = "INR";
            DataSet set = new DataSet();
            string str3 = "";
            string str4 = "";
            set.ReadXml("http://resources.ibe4all.com/CommonIBE/App_Data/xml/airport.xml");
            foreach (DataRow row in set.Tables[0].Rows)
            {
                if (str3 == "")
                {
                    str3 = row.ItemArray[2] + "^" + row.ItemArray[0];
                }
                else
                {
                    str3 = string.Concat(new object[] { str3, "/", row.ItemArray[2], "^", row.ItemArray[0] });
                }
                str4 = str3.ToUpper();
            }
            try
            {
                List<SegmentFlightRef> list = new List<SegmentFlightRef>();
                List<SegmentFlightRef> list2 = new List<SegmentFlightRef>();
                int length = Flttable.Length;
                for (int i = 0; i < length; i++)
                {
                    foreach (string str5 in Faretable[i].Keys)
                    {
                        SegmentFlightRef ref2 = new SegmentFlightRef();
                        foreach (string str7 in Faretable[i][str5].ToString().Split(new char[] { '^' }))
                        {
                            ref2 = new SegmentFlightRef();
                            List<PaxFairDetails> list3 = new List<PaxFairDetails>();
                            decimal num7 = 0M;
                            int num8 = 0;
                            for (int j = 0; j < 3; j++)
                            {
                                string str8 = "0";
                                string str9 = "";
                                if ((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str9 = "ADT";
                                }
                                else if ((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str9 = "CHD";
                                }
                                else if ((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    str9 = "INF";
                                }
                                ParsingClass.faredetails faredetails = new ParsingClass.faredetails
                                {
                                    avlStatus = "",
                                    cabin = str7.Split(new char[] { '~' })[5],
                                    farebasis = str7.Split(new char[] { '~' })[6],
                                    faretype = str7.Split(new char[] { '~' })[5],
                                    passengertype = str9,
                                    segmentref = "1",
                                    rbd = str7.Split(new char[] { '~' })[6]
                                };
                                List<ParsingClass.faredetails> list4 = new List<ParsingClass.faredetails> {
                            faredetails
                        };
                                PaxFare fare = new PaxFare
                                {
                                    messageQualifier = "",
                                    textdescription = "",
                                    messageCode = ""
                                };
                                List<PaxFare> list5 = new List<PaxFare> {
                            fare
                        };
                                PaxFairDetails details = new PaxFairDetails
                                {
                                    paxfarelist = list5
                                };
                                num7 = Convert.ToDecimal(str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1]);
                                details.fareamount = Convert.ToString(num7);
                                if ((str9 == "ADT") && (num8 == 0))
                                {
                                    details.taxamount = str7.Split(new char[] { '~' })[4];
                                    num8++;
                                }
                                if ((str9 == "CHD") && (num8 == 1))
                                {
                                    details.taxamount = str7.Split(new char[] { '~' })[4];
                                    num8++;
                                }
                                if (str9 == "INF")
                                {
                                    details.taxamount = "0";
                                }
                                details.YQ = str8;
                                details.ptc = str9;
                                details.paxfareno = "1";
                                details.faredetaillist = list4;
                                if ((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "ADT") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0"))
                                {
                                    for (int k = 0; k < num2; k++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "CHD") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num3 != 0))
                                {
                                    for (int m = 0; m < num3; m++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if (((str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[0].ToUpper() == "INF") && (str7.Split(new char[] { '~' })[j].Split(new char[] { ',' })[1] != "0")) && (num4 != 0))
                                {
                                    for (int n = 0; n < num4; n++)
                                    {
                                        list3.Add(details);
                                    }
                                }
                                if ((str7.Split(new char[] { '~' }).Length == 8) && (str7.Split(new char[] { '~' })[7] != ""))
                                {
                                    datafilename = str7.Split(new char[] { '~' })[7];
                                }
                            }
                            new List<Flightsinformation>();
                            if (Flttable[i].ContainsKey(str5))
                            {
                                List<Flightsinformation> list6 = new List<Flightsinformation>();
                                string[] strArray2 = Flttable[i][str5].ToString().Split(new char[] { '^' });
                                for (int num13 = 0; num13 < strArray2.Length; num13++)
                                {
                                    if (i == 0)
                                    {
                                        Flightsinformation flightsinformation = new Flightsinformation
                                        {
                                            flightFareBasis = "",
                                            companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                            companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                            dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                            dateofdep = strArray2[num13].Split(new char[] { '~' })[3]
                                        };
                                        string str10 = strArray2[num13].Split(new char[] { '~' })[2];
                                        if (str4.Contains(str10))
                                        {
                                            string[] strArray4 = str4.Split(new string[] { strArray2[num13].Split(new char[] { '~' })[2] }, StringSplitOptions.None)[1].Split(new char[] { '/' });
                                            flightsinformation.deslocation = strArray4[0].Replace("^", "");
                                        }
                                        else
                                        {
                                            flightsinformation.deslocation = strArray2[num13].Split(new char[] { '~' })[2];
                                        }
                                        flightsinformation.desterminal = "";
                                        flightsinformation.electicketing = "Y";
                                        flightsinformation.equiptype = "";
                                        flightsinformation.flightClass = "";
                                        flightsinformation.flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]);
                                        flightsinformation.flightrefno = "1";
                                        flightsinformation.sourceterminal = "";
                                        string str11 = strArray2[num13].Split(new char[] { '~' })[1];
                                        if (str4.Contains(str11))
                                        {
                                            string[] strArray6 = str4.Split(new string[] { strArray2[num13].Split(new char[] { '~' })[1] }, StringSplitOptions.None)[1].Split(new char[] { '/' });
                                            flightsinformation.sourclocation = strArray6[0].Replace("^", "");
                                        }
                                        else
                                        {
                                            flightsinformation.sourclocation = strArray2[num13].Split(new char[] { '~' })[1];
                                        }
                                        flightsinformation.timeofarr = strArray2[num13].Split(new char[] { '~' })[6];
                                        flightsinformation.timeofdep = strArray2[num13].Split(new char[] { '~' })[4];
                                        flightsinformation.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation.timeofarr, flightsinformation.timeofdep));
                                        list6.Add(flightsinformation);
                                        flightsinformation.fligtinfolst = list6;
                                        ref2.fligtinfolist = list6;
                                    }
                                    if (i == 1)
                                    {
                                        Flightsinformation flightsinformation2 = new Flightsinformation
                                        {
                                            flightFareBasis = "",
                                            companyidmc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                            companyidoc = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[0].TrimEnd(new char[0]),
                                            dateofarr = strArray2[num13].Split(new char[] { '~' })[5],
                                            dateofdep = strArray2[num13].Split(new char[] { '~' })[3]
                                        };
                                        string str12 = strArray2[num13].Split(new char[] { '~' })[2];
                                        if (str4.Contains(str12))
                                        {
                                            string[] strArray8 = str4.Split(new string[] { strArray2[num13].Split(new char[] { '~' })[2] }, StringSplitOptions.None)[1].Split(new char[] { '/' });
                                            flightsinformation2.deslocation = strArray8[0].Replace("^", "");
                                        }
                                        else
                                        {
                                            flightsinformation2.deslocation = strArray2[num13].Split(new char[] { '~' })[2];
                                        }
                                        flightsinformation2.desterminal = "";
                                        flightsinformation2.electicketing = "Y";
                                        flightsinformation2.equiptype = "";
                                        flightsinformation2.flightClass = "";
                                        flightsinformation2.flightno = strArray2[num13].Split(new char[] { '~' })[0].Split(new char[] { ' ' })[1].TrimEnd(new char[0]);
                                        flightsinformation2.flightrefno = "1";
                                        flightsinformation2.sourceterminal = "";
                                        string str13 = strArray2[num13].Split(new char[] { '~' })[1];
                                        if (str4.Contains(str13))
                                        {
                                            string[] strArray10 = str4.Split(new string[] { strArray2[num13].Split(new char[] { '~' })[1] }, StringSplitOptions.None)[1].Split(new char[] { '/' });
                                            flightsinformation2.sourclocation = strArray10[0].Replace("^", "");
                                        }
                                        else
                                        {
                                            flightsinformation2.sourclocation = strArray2[num13].Split(new char[] { '~' })[1];
                                        }
                                        flightsinformation2.timeofarr = strArray2[num13].Split(new char[] { '~' })[6];
                                        flightsinformation2.timeofdep = strArray2[num13].Split(new char[] { '~' })[4];
                                        flightsinformation2.flightreftime = Convert.ToString(CommonFunction.calculatetime(flightsinformation2.timeofarr, flightsinformation2.timeofdep));
                                        list6.Add(flightsinformation2);
                                        flightsinformation2.fligtinfolst = list6;
                                        ref2.fligtinfolist = list6;
                                    }
                                }
                            }
                            ref2.PaxFareList = list3;
                            switch (i)
                            {
                                case 0:
                                    list.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;

                                case 1:
                                    list2.Add(ref2);
                                    ref2.segrefno = "1";
                                    break;
                            }
                        }
                    }
                }
                List<Recommendation> recommendationlst = new List<Recommendation>();
                Recommendation item = new Recommendation
                {
                    SegmentFlightRefList = list
                };
                recommendationlst.Add(item);
                xMLData = this.parseObjectToXML(recommendationlst, "LCC", "", "INR", "", "", "0");
                xMLData = "<InflAirResResponse>" + xMLData + "</InflAirResResponse>";
                xMLData = this.XMLToStringForm(xMLData, "", "-LCC", datafilename, searchparam, "");
                if (list2.Count > 0)
                {
                    recommendationlst = new List<Recommendation>();
                    item = new Recommendation
                    {
                        SegmentFlightRefList = list2
                    };
                    recommendationlst.Add(item);
                    string str14 = "";
                    str14 = this.parseObjectToXML(recommendationlst, "LCC", "", datafilename, "", "", "0");
                    str14 = "<InflAirResResponse>" + str14 + "</InflAirResResponse>";
                    str14 = this.XMLToStringForm(str14, "", "-LCC", datafilename, searchparam, "");
                    xMLData = xMLData + "()" + str14;
                }
            }
            catch (Exception)
            {
            }
            return xMLData;
        }




        public string XMLToStringForm(string XMLData, string MerchantID, string ParsedFlag, string Datafilename, string SearchParam, string goAirtoken)
        {
            string str = "";
            try
            {
                string[] strArray = Strings.Split(SearchParam, ",", -1, CompareMethod.Binary);
                string str2 = "False";
                if (strArray[0].ToUpper() == "TWO")
                {
                    str2 = "True";
                }
                string xml = XMLData;
                XmlDocument document = new XmlDocument();
                document.LoadXml(xml);
                XmlNodeList elementsByTagName = document.GetElementsByTagName("InflAirResResponse");
                int num = 1;
                foreach (XmlNode node in elementsByTagName)
                {
                    string str4 = "";
                    foreach (XmlNode node2 in node.ChildNodes)
                    {
                        string str5 = "";
                        string str6 = "";
                        string str7 = "";
                        if (ParsedFlag.Contains("-GOAIR"))
                        {
                            ParsedFlag = "-GOAIR" + node2["segrefno"].InnerText + "[" + goAirtoken;
                        }
                        if (ParsedFlag.Contains("-HER"))
                        {
                            ParsedFlag = "-HER" + node2["segrefno"].InnerText + "[" + goAirtoken;
                        }
                        if (ParsedFlag.Contains("-EZEEGO"))
                        {
                            ParsedFlag = "-EZEEGO" + node2["segrefno"].InnerText.Replace("-", "[d]");
                        }
                        if (ParsedFlag.Contains("-UAPI"))
                        {
                            ParsedFlag = "-UAPI" + node2["segrefno"].InnerText;
                        }
                        XmlNodeList childNodes = node2["Outbound"].ChildNodes;
                        int count = childNodes.Count;
                        foreach (XmlNode node3 in childNodes)
                        {
                            XmlNode node4 = node3["SrcLocation"];
                            XmlNode node5 = node3["DstLocation"];
                            if (string.IsNullOrEmpty(str5))
                            {
                                str5 = "@1;1;" + node3["flighttime"].InnerText + ";EFT" + ParsedFlag + ";" + node3["marketingCarrier"].InnerText + ";MCX;" + node3["DOD"].InnerText + ";" + node3["TOD"].InnerText + ";" + node3["DOA"].InnerText + ";" + node3["TOA"].InnerText + ";" + node4["Source"].InnerText + ";" + node5["Destination"].InnerText + ";" + node3["marketingCarrier"].InnerText + ";" + node3["operatingCarrier"].InnerText + ";" + node3["flightno"].InnerText + ";" + node3["flightno"].InnerText + ";" + node3["elecTicketing"].InnerText + ";LCA";
                            }
                            else
                            {
                                str5 = str5 + "^" + node3["DOD"].InnerText + ";" + node3["TOD"].InnerText + ";" + node3["DOA"].InnerText + ";" + node3["TOA"].InnerText + ";" + node4["Source"].InnerText + ";" + node5["Destination"].InnerText + ";" + node3["marketingCarrier"].InnerText + ";" + node3["operatingCarrier"].InnerText + ";" + node3["flightno"].InnerText + ";" + node3["flightno"].InnerText + ";" + node3["elecTicketing"].InnerText + ";LCA";
                            }
                        }
                        if (node2["Inbound"] != null)
                        {
                            XmlNodeList list4 = node2["Inbound"].ChildNodes;
                            count += list4.Count;
                            foreach (XmlNode node6 in list4)
                            {
                                XmlNode node7 = node6["SrcLocation"];
                                XmlNode node8 = node6["DstLocation"];
                                if (string.IsNullOrEmpty(str6))
                                {
                                    str6 = "@1;1;" + node6["flighttime"].InnerText + ";EFT" + ParsedFlag + ";" + node6["marketingCarrier"].InnerText + ";MCX;" + node6["DOD"].InnerText + ";" + node6["TOD"].InnerText + ";" + node6["DOA"].InnerText + ";" + node6["TOA"].InnerText + ";" + node7["Source"].InnerText + ";" + node8["Destination"].InnerText + ";" + node6["marketingCarrier"].InnerText + ";" + node6["operatingCarrier"].InnerText + ";" + node6["flightno"].InnerText + ";" + node6["flightno"].InnerText + ";" + node6["elecTicketing"].InnerText + ";LCA";
                                }
                                else
                                {
                                    str6 = str6 + "^" + node6["DOD"].InnerText + ";" + node6["TOD"].InnerText + ";" + node6["DOA"].InnerText + ";" + node6["TOA"].InnerText + ";" + node7["Source"].InnerText + ";" + node8["Destination"].InnerText + ";" + node6["marketingCarrier"].InnerText + ";" + node6["operatingCarrier"].InnerText + ";" + node6["flightno"].InnerText + ";" + node6["flightno"].InnerText + ";" + node6["elecTicketing"].InnerText + ";LCA";
                                }
                            }
                            str5 = str5 + str6;
                        }
                        decimal num3 = 0M;
                        XmlNodeList list5 = node2["FareDetails"].ChildNodes;
                        double num4 = 0.0;
                        double num5 = 0.0;
                        foreach (XmlNode node9 in list5)
                        {
                            string str8 = "";
                            num3 = Convert.ToDecimal(node9["YQ"].InnerText);
                            if (((node9.LocalName == "Adult") | (node9.LocalName == "Child")) | (node9.LocalName == "INF"))
                            {
                                string innerText = "";
                                try
                                {
                                    if (node9["Fare"]["msgQual"].InnerText != null)
                                    {
                                        innerText = node9["Fare"]["msgQual"].InnerText;
                                    }
                                }
                                catch (Exception)
                                {
                                }
                                if (string.IsNullOrEmpty(str7))
                                {
                                    XmlNode node10 = node9["FareDetail"];
                                    if (node10["faretype"].InnerText == "Non Refundable")
                                    {
                                        str7 = "~" + node9["paxfareno"].InnerText + ";" + node9["fareamt"].InnerText + ";" + node9["taxamt"].InnerText + ";" + node9["ptc"].InnerText + ";1;NRF;96;Non Refundable;LAST TKT DTE;" + innerText + ";" + node9["YQ"].InnerText + ";0;" + Datafilename + ";" + node2["segrefno"].InnerText.Replace("-", "");
                                    }
                                    else
                                    {
                                        str7 = "~" + node9["paxfareno"].InnerText + ";" + node9["fareamt"].InnerText + ";" + node9["taxamt"].InnerText + ";" + node9["ptc"].InnerText + ";1;PEN;96;LAST TKT DTE;" + innerText + ";" + node9["YQ"].InnerText + ";0;" + Datafilename + ";" + node2["segrefno"].InnerText.Replace("-", "");
                                    }
                                }
                                else
                                {
                                    str7 = str7 + "~" + node9["paxfareno"].InnerText + ";" + node9["fareamt"].InnerText + ";" + node9["taxamt"].InnerText + ";" + node9["ptc"].InnerText + ";1;PEN;96;LAST TKT DTE;" + innerText + ";" + node9["YQ"].InnerText + ";0;" + Datafilename + ";" + node2["segrefno"].InnerText.Replace("-", "");
                                }
                                num4 += Convert.ToDouble(node9["fareamt"].InnerText);
                                num5 += Convert.ToDouble(node9["taxamt"].InnerText);
                                foreach (XmlNode node11 in node9.ChildNodes)
                                {
                                    if ((node11.NodeType == XmlNodeType.Element) & (node11.LocalName == "FareDetail"))
                                    {
                                        for (int i = 1; i <= count; i++)
                                        {
                                            if (string.IsNullOrEmpty(str8))
                                            {
                                                str8 = node11["rbd"].InnerText + ";" + node11["AvlStatus"].InnerText + ";NoClass;" + node11["rbd"].InnerText + ";" + node9["ptc"].InnerText + ";RP";
                                            }
                                            else
                                            {
                                                str8 = str8 + ";" + node11["rbd"].InnerText + ";" + node11["AvlStatus"].InnerText + ";NoClass;" + node11["rbd"].InnerText + ";" + node9["ptc"].InnerText + ";RP";
                                            }
                                        }
                                    }
                                }
                                str7 = str7 + ";" + str8;
                            }
                        }
                        if (str2 == "True")
                        {
                            str7 = string.Concat(new object[] { num3, ";", num3, ";", num4, ";", num5, ";", str7 });
                        }
                        else
                        {
                            str7 = string.Concat(new object[] { num3, ";", num4, ";", num5, ";", str7 });
                        }
                        if (string.IsNullOrEmpty(str4))
                        {
                            str4 = str7 + str5;
                        }
                        else
                        {
                            str4 = str4 + "#" + str7 + str5;
                        }
                        num++;
                    }
                    if (string.IsNullOrEmpty(str))
                    {
                        str = str4;
                    }
                    else
                    {
                        str = str + "()" + str4;
                    }
                }
            }
            catch (Exception exception)
            {
                exception.ToString();
            }
            return str;
        }







    }
}