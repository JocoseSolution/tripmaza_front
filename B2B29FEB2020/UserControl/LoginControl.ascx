﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb" Inherits="UserControl_LoginControl" %>
<style type="text/css">
    .container-fluid {
        padding-right: 90px;
        padding-left: 90px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
    }

    .service__icon--heading {
        font-size: 25px;
        text-align: center;
        line-height: 1.3;
        padding: 25px 0;
        color: #808080;
        font-weight: 200;
    }

    .float-md-left {
        float: left !important;
    }

    .service__icon--list {
        display: inline-block;
        width: 49%;
        text-align: center;
    }

    .service__icon--label {
        font-size: 14px;
        color: #324253;
        padding-top: 10px;
    }

    .round-ico {
        border: 2px solid #e67817;
        border-radius: 50%;
        padding: 7px;
        color: #e25b18;
    }

    .semiround {
        background: #24547d;
        padding: 10px;
        border-radius: 10px;
        color: #fff;
    }

    .business__image {
        height: 300px;
    }

    .pt-5, .py-5 {
        padding-top: 3rem !important;
    }

    .main__heading {
        font-size: 32px;
        color: #333;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    @media (min-width: 768px) {
        .col-md-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
    }

    .business__points--text {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .business__content {
        display: flex;
        align-items: center;
        padding: 10px 0;
    }

    .pl-2, .px-2 {
        padding-left: .5rem !important;
    }

    .business__details--heading {
        font-size: 20px;
        color: #324253;
        font-weight: 500;
        margin-bottom: 0px;
    }

    .business__details--text {
        font-size: 12px;
        color: #999999;
        font-weight: 300;
        padding-top: 5px;
    }

    .business {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .loginas__tab {
        padding: 25px 0 20px;
    }

    .loginas__tab--list.active, .loginas__tab--list:hover {
        color: #ff1228;
        border-bottom: 1px solid #ff1228;
    }

    .loginas__tab--list {
        display: inline-block;
        margin-right: 15px;
        font-size: 12px;
        font-family: "TJ_font-700";
        font-weight: 500;
        cursor: pointer;
        padding-bottom: 8px;
        color: #999;
        font-family: 'Quicksand', sans-serif !important;
    }

    .form__field--text {
        font-size: 14px;
        padding: 10px 0;
        color: #999;
    }

    .login__form--reset--link {
        color: #ff1228;
    }

    .trav-b2b {
        font-size: 20px;
        color: #000000;
        width: 800px;
        margin: 0 auto;
        text-align: center;
        font-weight: 600;
        padding: 0px 0 60px 0;
    }

    .b2b-product-tab {
        display: flex;
        justify-content: center;
        align-items: center;
        padding-bottom: 150px;
        margin: 0 auto;
        margin-right: 50%;
        margin-left: 55%;
    }

    .product-type {
        height: 100px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-end;
        padding-right: 93px;
    }

    .b2b-static-cont .b2b-product-tab .product-type img {
        display: block;
    }

    .product-type span {
        display: block;
        font-size: 22px;
        color: #ee3157;
        padding-top: 17px;
        font-weight: 600;
    }

    .easier-management-types-cont {
        background: #fafafa;
        margin-bottom: 90px;
    }

        .easier-management-types-cont .perfect-heading {
            padding-top: 100px;
            padding-bottom: 60px;
        }

    .aertrip-color {
        color: #ffbe07;
    }

    .easier-management-types {
        display: flex;
        justify-content: center;
        text-align: center;
    }

        .easier-management-types .management-lists {
            margin-right: 50px;
            width: 162px;
            color: #000;
            font-size: 16px;
            font-weight: 600;
        }

            .easier-management-types .management-lists .image {
                padding: 0;
                width: 93px;
                margin: 0 auto;
            }

            .easier-management-types .management-lists .name {
                padding-top: 16px;
                color: #000;
            }

    element.style {
    }

    .easier-management-para {
        font-size: 24px;
        padding: 85px 0 100px 0;
        width: 916px;
        margin: 0 auto;
        text-align: center;
        color: #000000;
    }

    .perfect-heading {
        font-size: 55px;
        text-align: center;
        margin: 0;
        padding: 0 0 50px 0;
        font-weight: bold;
        color: #2c3a4e;
    }

    @media only screen and (max-width: 500px) {
        .banner {
            display: none;
        }

        .banner, .business, .deal, .theme-copyright {
            display: none;
        }

        .navbar-theme.navbar-theme-abs {
            display: none !important;
        }

        .theme-login-header {
            text-align: center;
        }
    }

    input[type="radio"] {
        visibility: hidden;
        height: 0;
        width: 0;
    }

    label {
        display: flex;
        vertical-align: middle;
        align-items: center;
        justify-content: center;
        text-align: center;
        cursor: pointer;
        background-color: var(--color-gray);
        color: #828282;
        padding: 5px 10px;
        border-radius: 6px;
        transition: color --transition-fast ease-out, background-color --transition-fast ease-in;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        margin-right: 8px;
    }

        label:last-of-type {
            margin-right: 0;
        }

    input[type="radio"]:checked + label {
        color: #e14e18;
        border-bottom: 2px solid #0672b7;
    }

    input[type="radio"]:hover:not(:checked) + label {
        background-color: none;
        color: #000;
    }

    .InputGroup {
        display: flex;
    }
</style>




<div class="theme-hero-area" style="margin-top: 50px;">

    <section class="why_choose" style="position: relative;padding: 80px 0 60px;">			
    <div class="container">	
        <div class="row text-left align-items-center">
           
            <div class="col-md-12 pl-30">	
                <div class="row">
                    <div class="col-md-6">	
                        <div class="why-choose-img">
                            <img src="../Advance_CSS/Images/why-choose-us.jpg" alt="Big image">
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="section-title text-left why_title">
                            <span>Why Choose Us?</span>
                            <h2>Every Time We Provide <br> best Service</h2>
                            <p>Ut et justo invidunt et sit takimata dolor accusam erat.
                                 Duo amet consetetur diam stet vero dolor no est accusam,
                                  aliquyam dolor vero sit sea. No et justo consetetur at lorem clita sit ea.
                                   Dolor sed clita dolor ea elitr sed elitr sea. At est ut lorem voluptua lorem elitr.
                                   Amet erat nonumy amet sed takimata clita, invidunt lorem ut no sit aliquyam justo.</p>
                             <p>Tempor dolor vero dolor sadipscing eos erat dolor sanctus aliquyam et,
                                  et justo labore lorem kasd clita sed, sanctus sit consetetur no no ut erat dolor, 
                                  nonumy sanctus labore takimata erat amet lorem magna nonumy justo.</p> <br><br>
                                  <div class="about-content1"> <a href="#" class="read-btn">Read More</a>  </div>   							
                        </div>
                    </div><!-- END COL -->
                </div><!-- END ROW -->	
                <div class="row">
                    <div class="col-lg-3 col-xs-12">
                        <div class="single-choose mb-20">
                            <i class="icofont-man-in-glasses"></i>
                            <div class="choose-content">
                                <h4>Best Travel Guide</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing typesetting</p>
                            </div>
                        </div>
                    </div><!-- END COL -->
                    
                    <div class="col-lg-3 col-xs-12 pr-0">
                        <div class="single-choose mb-20">
                            <i class="icofont-world"></i>
                            <div class="choose-content">
                                <h4>World Class Service</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing typesetting</p>
                            </div>
                        </div>
                    </div><!-- END COL -->
                    
                    <div class="col-lg-3 col-xs-12">
                        <div class="single-choose">									
                            <i class="icofont-ui-wifi"></i>
                            <div class="choose-content">
                                <h4>Free Wifi</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing typesetting</p>
                            </div>
                        </div>
                    </div><!-- END COL -->		

                    <div class="col-lg-3 col-xs-12 pr-0">
                        <div class="single-choose">									
                            <i class="icofont-live-support"></i>
                            <div class="choose-content">
                                <h4>24/7 Support</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing typesetting</p>
                            </div>
                        </div>
                    </div><!-- END COL -->			
                </div>
            </div><!-- END COL -->	

        </div><!-- END ROW -->
    </div><!-- END CONTAINER -->			
</section>


  
    <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-strong" style="background: #fff !important;"></div>
    </div>
    <div class="theme-hero-area-body">
        <div class="theme-page-section _pt-100 theme-page-section-xl" style="padding-top: 6px !important;">
            <div class="container" style="margin-top: 20px; border-radius: 10px; width: 97%; background: #e5e5e5; height: 435px; box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);">
                <div class="row" style="">
                    <div class="col-md-8 banner" style="padding: 0; height: 435px; border-radius: 10px 0px 0px 10px;">
                        <img src="../Advance_CSS/banner-pln.jpg" style="width: 100%;" />
                    </div>
                    <div class="col-md-4" style="padding: 40px 15px 0 30px;">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" aria-hidden="true" id="login-pop">
        <div class="modal-dialog modal-sm" style="width: 30%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icofont-close-line-circled"></i></button>
                </div>
                <div class="modal-body">
                    <div class="theme-login theme-login-white">
                            <div class="theme-login-header">
                                <h4 class="theme-login-title">Welcome to Tripmaza</h4>
                                <p style="font-size: 14px; color: #999;">Login here to your account as</p>
                                <asp:Label ID="lblerror" Visible="false" Font-Size="10px" runat="server" Style="font-weight: 500; font-size: 13px; color: rgb(229 0 0); background-color: rgb(238 207 207); padding: 8px; margin-bottom: 6px; border-radius: 6px; display: block;"></asp:Label>
                                <div class="loginas__tab">
                                    <div class="InputGroup">
                                        <asp:RadioButton ID="rdoAgent" runat="server" Text="AGENT" GroupName="LoginType" Checked="true" />
                                        <asp:RadioButton ID="rdoSupplier" runat="server" Text="SUPPLIER" GroupName="LoginType" />
                                    </div>
                                </div>
                            </div>
                            <div class="">
                               
                                    <form class="theme-login-form">
                                        <asp:Login ID="UserLogin" runat="server" style="width:100%;">
                                            <TextBoxStyle />
                                            <LoginButtonStyle />
                                            <LayoutTemplate>
                                                <div class="form-group theme-login-form-group">
                                                    <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                        <div class="">
                                                            <i class="theme-search-area-section-icon icofont-user-alt-7" style="line-height: 44px !important;"></i>
                                                            <asp:TextBox runat="server" class="theme-search-area-section-input" placeholder="User Id" ID="UserName" Style="height: 45px !important; border-radius: 5px; border: 1px solid #949494 !important;"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group theme-login-form-group">
                                                    <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                        <div class="">
                                                            <i class="theme-search-area-section-icon icofont-key" style="line-height: 44px !important;"></i>
                                                            <asp:TextBox runat="server" class="theme-search-area-section-input" TextMode="Password" placeholder="Password" ID="Password" Style="height: 45px !important; border-radius: 5px; border: 1px solid #949494 !important;"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-uc btn-dark btn-block btn-lg-custom" Style="height: 50px;" />
                                                    </div>
                                                </div>
                                                <div>
                                                    <p class="form__field--text" style="margin-bottom: -5px;">
                                                        Forgot your password? <a href="../ForgotPassword.aspx" class="login__form--reset--link">Reset Here </a>
                                                    </p>
                                                    <p class="form__field--text">
                                                        Don’t have any account? <a href="regs_new.aspx" class="login__form--reset--link">Sign up here</a>
                                                    </p>
                                                </div>
                                            </LayoutTemplate>
                                            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                                            <TitleTextStyle />
                                        </asp:Login>
                                    </form>
                                
                            </div>
                        </div>

                </div>
                <div class="modal-footer hidden">
                    <button type="button" class="btn btn-danger" id="pax-confirm" style="float: left; background: #ff0000">Cancel</button>
                   

                </div>
            </div>
        </div>
    </div>



<section class="business pt-5">
    <div class="container">
        <h1 class="main__heading mb-3" style="font-weight: 500;">Easier Management
        </h1>
        <div class="row">
            <div class="col-md-6 p-0 " style="margin-top: 16px;">
                <img src="https://www.crownmgt.com.au/assets/img/10.jpg" class="business__image" alt="Tripmaza B2B">
            </div>
            <div class="col-md-6 business__points">
                <p class="business__points--text">And that’s not all! A lot more features like Timeline view, Filter Graphs, Group Booking tools are available online. This with unparalleled Security &amp; Privacy and 24/7 Online &amp; Offline Support makes Tripmaza the best B2B travel platform out there.</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="business__content">
                            <i class="icofont-dashboard-web icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">Your very own CRM</h4>
                                <p class="business__details--text">Years of business experience</p>
                            </div>
                        </div>
                        <div class="business__content">
                            <i class="icofont-pay icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">Payment options</h4>
                                <p class="business__details--text">Get awesome opportunities</p>
                            </div>
                        </div>
                        <div class="business__content">
                            <i class="icofont-air-ticket icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">Booking management</h4>
                                <p class="business__details--text">Get Interactive exclusive portal</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="business__content">
                            <i class="icofont-live-support icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">24/7</h4>
                                <p class="business__details--text">Online after sales</p>
                            </div>
                        </div>
                        <div class="business__content">
                            <i class="icofont-list icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">Online Accounting</h4>
                                <p class="business__details--text">Earn good deals and commissions</p>
                            </div>
                        </div>
                        <div class="business__content">
                            <i class="icofont-sale-discount icofont-2x round-ico"></i>
                            <div class="business__details pl-2">
                                <h4 class="business__details--heading">Latest Deals</h4>
                                <p class="business__details--text">Access to the great deals</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="theme-hero-area">
    <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg ws-action" style="background-image: url(../Advance_CSS/img/xgxku6um9jy_1500x800.jpeg); transform: translate3d(0px, -101.896px, 0px); height: 612px;" data-parallax="true"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-strong"></div>
    </div>
    <div class="theme-hero-area-body">
        <div class="theme-page-section theme-page-section-xxl">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="theme-hero-text theme-hero-text-white theme-hero-text-center">
                            <div class="theme-hero-text-header">
                                <h2 class="theme-hero-text-title">Travel Smart</h2>
                                <p class="theme-hero-text-subtitle">Subscribe now and unlock our secret deals. Save up to 70% by getting access to our special offers for hotels, flights, cars, vacation rentals and travel experiences.</p>
                            </div>
                            <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" href="#">Sign Up Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="large-12 medium-12 small-12" style="display: none;">
    <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
    <div class="lft f16" style="display: none;">
        Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
    </div>
    <div class="clear1">
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
        </div>
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="font-size: 23px;"></i></span>
        </div>
    </div>
    <div class="large-4 medium-4 small-12 columns">
        <div class="clear1">
        </div>
        <br />
        <a href="../ForgotPassword.aspx" style="color: #ff0000">Forgot Password</a>
    </div>
    <div class="clear">
    </div>
    <div class="clear">
    </div>
</div>
