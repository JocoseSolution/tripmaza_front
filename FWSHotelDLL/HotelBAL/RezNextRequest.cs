﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelShared;
namespace HotelBAL
{
   public class RezNextRequest
   {
       public string RZHotelSearchRequest(HotelSearch HtlSearchQuery)
       {
           StringBuilder ReqXml = new StringBuilder("");
           try
           {
               ReqXml.Append(HtlSearchQuery.RZXmlHeader);
               ReqXml.Append("<Header><From>");
               ReqXml.Append("<SystemId>" + HtlSearchQuery.RezNextSystemId + "</SystemId>");
               ReqXml.Append("<Credential><UserName>" + HtlSearchQuery.RezNextUserName + "</UserName>");
               ReqXml.Append("<Password>" + HtlSearchQuery.RezNextPassword + "</Password></Credential> </From>");
               ReqXml.Append("<Action>OTA_HotelAvailRQ</Action></Header>");
               ReqXml.Append("<Body><OTA_HotelAvailRQ Version=\"4.000\" TransactionIdentifier=\"" + HtlSearchQuery.SearchID + "\">");
               ReqXml.Append("<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria>");
               ReqXml.Append("<Criterion Type=\"2\">");
                if (HtlSearchQuery.HotelName == "")
                    ReqXml.Append("<CityInfo CityName=\"" + HtlSearchQuery.SearchCity + "\" />");
               else
                    ReqXml.Append(" <HotelRef HotelCode=\"" + HtlSearchQuery.HotelName + "\" />");

                ReqXml.Append("<StayDateRange Start=\"" + HtlSearchQuery.CheckInDate + "\" End=\"" + HtlSearchQuery.CheckOutDate + "\" />");
                ReqXml.Append("<RoomInfo NumberOfRooms=\"" + HtlSearchQuery.NoofRoom + "\">");
               for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
               {
                   ReqXml.Append("<Rooms Room=\"" + (i + 1).ToString() + "\" Adult=\"" + HtlSearchQuery.AdtPerRoom[i].ToString() + "\" Child=\"" + HtlSearchQuery.ChdPerRoom[i].ToString() + "\" />");
                   //if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                   //{
                   //    for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                   //    {
                   //        ReqXml.Append(" ChildAge='" + HtlSearchQuery.ChdAge[i, j] + "' ");
                   //    }
                   //}
                   //ReqXml.Append(" OccupancyType='1'></Rooms>");
               }
               ReqXml.Append("</RoomInfo>");

               ReqXml.Append("<CurrencyInfo CurrencyCode=\"INR\" />");
               ReqXml.Append("</Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ></Body></Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZHotelSearchRequest");
           }
           //HtlSearchQuery.RZ_HotelSearchReq = ReqXml.ToString();
           return ReqXml.ToString();
       }

       public string RZTaxAmountRequest(HotelSearch HtlSearchQuery)
       {
           StringBuilder ReqXml = new StringBuilder("");
           try
           {
               ReqXml.Append("<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">");
               ReqXml.Append("<Header><From>");
               ReqXml.Append("<SystemId>" + HtlSearchQuery.RezNextSystemId + "</SystemId>");
               ReqXml.Append("<Credential><UserName>" + HtlSearchQuery.RezNextUserName + "</UserName>");
               ReqXml.Append("<Password>" + HtlSearchQuery.RezNextPassword + "</Password></Credential> </From>");
               ReqXml.Append("<Action>Hotel_TaxInfo</Action></Header>");
               ReqXml.Append("<Body><HotelTaxRequest Version=\"4.000\" TransactionIdentifier=\"" + HtlSearchQuery.SearchID + "\">");
               ReqXml.Append("<TaxRequestSegments><TaxRequestSegment><HotelSearchCriteria>");
               ReqXml.Append("<Criterion>");
               ReqXml.Append(" <HotelRef HotelCode=\"" + HtlSearchQuery.HtlCode + "\" />");
               ReqXml.Append("<RateRef RackRate=\"" + HtlSearchQuery.PGCity + "\" ChargeRate=\"" + HtlSearchQuery.PGState + "\"  TaxType=\"" + HtlSearchQuery.PGCountry + "\"/>");
               ReqXml.Append("<CurrencyInfo CurrencyCode=\"INR\" />");
               ReqXml.Append("</Criterion></HotelSearchCriteria></TaxRequestSegment></TaxRequestSegments></HotelTaxRequest></Body></Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZTaxAmountRequest");
           }
           return ReqXml.ToString();
       }

       public string RZPreBookingRequest(HotelBooking HtlSearchQuery)
       {
           StringBuilder ReqXml = new StringBuilder("");
           try
           {
               ReqXml.Append("<Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">");
               ReqXml.Append("<Header><From>");
               ReqXml.Append("<SystemId>" + HtlSearchQuery.RezNextSystemId + "</SystemId>");
               ReqXml.Append("<Credential><UserName>" + HtlSearchQuery.RezNextUserName + "</UserName>");
               ReqXml.Append("<Password>" + HtlSearchQuery.RezNextPassword + "</Password></Credential> </From>");
               ReqXml.Append("<Action>OTA_HotelAvailRQ</Action></Header>");
               ReqXml.Append("<Body><OTA_HotelAvailRQ Version=\"4.000\" TransactionIdentifier=\"" + HtlSearchQuery.Orderid + "\">");
               ReqXml.Append("<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria>");
               ReqXml.Append("<Criterion Type=\"1\">");
               ReqXml.Append(" <HotelRef HotelCode=\"" + HtlSearchQuery.HtlCode + "\" />");
               ReqXml.Append("<StayDateRange Start=\"" + HtlSearchQuery.CheckInDate + "\" End=\"" + HtlSearchQuery.CheckOutDate + "\" />");
               ReqXml.Append("<RoomInfo NumberOfRooms=\"" + HtlSearchQuery.NoofRoom + "\">");
               for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
               {
                   ReqXml.Append("<Rooms Room=\"" + (i + 1).ToString() + "\" Adult=\"" + HtlSearchQuery.AdtPerRoom[i].ToString() + "\" Child=\"" + HtlSearchQuery.ChdPerRoom[i].ToString() + "\" />");
               }
               ReqXml.Append("</RoomInfo>");
               ReqXml.Append("<CurrencyInfo CurrencyCode=\"INR\" />");
               ReqXml.Append("</Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ></Body></Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZPreBookingRequest");
           }
           return ReqXml.ToString();
       }

       public string RZPreBookingTaxAmountRequest(HotelBooking HtlSearchQuery, decimal Rackrate, decimal AmountBeforeTax)
       {
           StringBuilder ReqXml = new StringBuilder("");
           try
           {
               ReqXml.Append("<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">");
               ReqXml.Append("<Header><From>");
               ReqXml.Append("<SystemId>" + HtlSearchQuery.RezNextSystemId + "</SystemId>");
               ReqXml.Append("<Credential><UserName>" + HtlSearchQuery.RezNextUserName + "</UserName>");
               ReqXml.Append("<Password>" + HtlSearchQuery.RezNextPassword + "</Password></Credential> </From>");
               ReqXml.Append("<Action>Hotel_TaxInfo</Action></Header>");
               ReqXml.Append("<Body><HotelTaxRequest Version=\"4.000\" TransactionIdentifier=\"" + HtlSearchQuery.Orderid + "\">");
               ReqXml.Append("<TaxRequestSegments><TaxRequestSegment><HotelSearchCriteria>");
               ReqXml.Append("<Criterion>");
               ReqXml.Append(" <HotelRef HotelCode=\"" + HtlSearchQuery.HtlCode + "\" />");
               ReqXml.Append("<RateRef RackRate=\"" + Rackrate.ToString() + "\" ChargeRate=\"" + AmountBeforeTax.ToString() + "\"  TaxType=\"" + HtlSearchQuery.TaxType + "\"/>");
               ReqXml.Append("<CurrencyInfo CurrencyCode=\"INR\" />");
               ReqXml.Append("</Criterion></HotelSearchCriteria></TaxRequestSegment></TaxRequestSegments></HotelTaxRequest></Body></Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZPreBookingTaxAmountRequest");
           }
           return ReqXml.ToString();
       }

       public HotelBooking RZBookingRequest(HotelBooking BookingDetals)
       {
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               string[] roomtype=BookingDetals.RoomTypeCode.Split('|');;
               ReqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:htn=\"http://pms.ihotelier.com/HTNGService/services/HTNG2011BService\">");
               ReqXml.Append("<soapenv:Header> <wsa:MessageID>" + BookingDetals.Orderid + "</wsa:MessageID><wsa:To>http://pmcoutllm01-t5.tcprod.local:8080/HtngSimulator/PMSInterfaceSimulator</wsa:To>");
               ReqXml.Append("<wsa:UserName>" + BookingDetals.RezNextUserName + "</wsa:UserName>");
               ReqXml.Append("<wsa:PassWord>" + BookingDetals.RezNextPassword + "</wsa:PassWord>>");
               ReqXml.Append("<wsa:ReplyTo><wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address></wsa:ReplyTo> <wsa:Action>http://htng.org/2011B/HTNG2011B_SubmitResult</wsa:Action></soapenv:Header>");
               ReqXml.Append("<soapenv:Body><OTA_HotelResNotifRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd\" ");
               ReqXml.Append("Version=\"4.0\" ResStatus=\"Book\" TimeStamp=\"" + DateTime.Now.ToString("yyy-MM-dd hh:mm:ss") + "\">");
               ReqXml.Append("<POS><Source><RequestorID Type=\"14\" ID=\"" + BookingDetals.RezNextSystemId + "\" /><BookingChannel Type=\"5\" Primary=\"1\"> <CompanyName Code=\"WEB\">WEB</CompanyName></BookingChannel></Source></POS>");
               ReqXml.Append("<HotelReservations>");
               ReqXml.Append("<HotelReservation CreatorID=\"" + BookingDetals.RezNextSystemId + "\" CreateDateTime=\"" + DateTime.Today.ToString("yyy-MM-dd hh:mm:ss") + "\" ResStatus=\"Reserved\">");
               ReqXml.Append("<UniqueID ID=\"" + BookingDetals.Orderid + "\" Type=\"14\" />");
               
               string[] roomrates=BookingDetals.EXRateKey.Split('#');
               string[] roomrateAmts = roomrates[0].Split('/');
               string[] Taxses = roomrates[1].Split('/');
               ReqXml.Append("<RoomStays>");
               for (int i = 0; i < BookingDetals.NoofRoom; i++)
               {
                   ReqXml.Append("<RoomStay SourceOfBusiness=\" \" MarketCode=\" \">");
                   ReqXml.Append("<RatePlans><RatePlan RatePlanCode=\"" + BookingDetals.RoomPlanCode + "\"><MealsIncluded MealPlanCodes=\"" + roomtype[1] + "\" MealPlanIndicator=\"True\" /></RatePlan></RatePlans>");
                   ReqXml.Append("<RoomRates><RoomRate RoomTypeCode=\"" + roomtype[0] + "\" NumberOfUnits=\"1\" RatePlanCode=\"" + BookingDetals.RoomPlanCode + "\" RatePlanCategory=\"" + roomtype[2] + "\">");
                   ReqXml.Append("<Rates><Rate EffectiveDate=\"" + BookingDetals.CheckInDate + "\" ExpireDate=\"" + BookingDetals.CheckOutDate + "\" RateTimeUnit=\"DAY\" UnitMultiplier=\"" + BookingDetals.NoofNight + "\">");
                   ReqXml.Append("<Base AmountBeforeTax=\"" + roomrateAmts[i] + "\" AmountAfterTax=\"" + (Convert.ToDecimal(roomrateAmts[i]) + Convert.ToDecimal(Taxses[i])).ToString() + "\" CurrencyCode=\"INR\">");
                   ReqXml.Append("<Taxes Amount=\"" + Taxses[i] + "\" CurrencyCode=\"INR\" />");
                   ReqXml.Append("</Base></Rate></Rates></RoomRate></RoomRates>");

                   ReqXml.Append("<GuestCounts IsPerRoom=\"1\">");
                   ReqXml.Append("<GuestCount AgeQualifyingCode=\"10\" Count=\"" + BookingDetals.AdtPerRoom[i].ToString() + "\"/>");
                   ReqXml.Append("<GuestCount AgeQualifyingCode=\"8\" Count=\"" + BookingDetals.ChdPerRoom[i].ToString() + "\"/>");
                   ReqXml.Append("</GuestCounts>");

                   ReqXml.Append("<TimeSpan Start=\"" + BookingDetals.CheckInDate + "\" End=\"" + BookingDetals.CheckOutDate + "\" />");
                   ReqXml.Append("<Guarantee GuaranteeType=\"PrePay\"></Guarantee>");
                   ReqXml.Append("<Total AmountBeforeTax=\"" + roomrateAmts[i] + "\" AmountAfterTax=\"" + (Convert.ToDecimal(roomrateAmts[i]) + Convert.ToDecimal(Taxses[i])).ToString() + "\" CurrencyCode=\"INR\">");
                   ReqXml.Append("<Taxes Amount=\"" + Taxses[i] + "\" CurrencyCode=\"INR\" TaxType=\"" + BookingDetals.TaxType + "\" />");
                   ReqXml.Append("</Total><BasicPropertyInfo HotelCode=\"" + BookingDetals.HtlCode + "\" /></RoomStay>");
               }
               ReqXml.Append("</RoomStays>");

               ReqXml.Append("<ResGuests><ResGuest ResGuestRPH=\"0\" PrimaryIndicator=\"1\"><Profiles><ProfileInfo>");
               ReqXml.Append("<UniqueID Type=\"1\" ID_Context=\"Customer\" />");
               ReqXml.Append("<Profile ProfileType=\"1\">");
               ReqXml.Append("<Customer><PersonName>");
               ReqXml.Append("<NamePrefix>" + BookingDetals.PGTitle + "</NamePrefix>");
               ReqXml.Append("<GivenName>" + BookingDetals.PGFirstName + "</GivenName>");
               ReqXml.Append("<Surname>" + BookingDetals.PGLastName + "</Surname>");
               ReqXml.Append("</PersonName>");
               ReqXml.Append("<Telephone PhoneLocationType=\"6\"  FormattedInd=\"0\" PhoneNumber=\"" + BookingDetals.PGContact + "\" PhoneTechType=\"1\" />");
               ReqXml.Append("<Email>" + BookingDetals.PGEmail + "</Email>");
               ReqXml.Append("<Address Type=\"1\">");
               ReqXml.Append("<AddressLine>" + BookingDetals.PGAddress + "</AddressLine>");
               ReqXml.Append("<CityName>" + BookingDetals.PGCity + "</CityName>");
               ReqXml.Append("<PostalCode>" + BookingDetals.PGPin + "</PostalCode>");
               ReqXml.Append("<StateProv>" + BookingDetals.PGState + "</StateProv>");
               ReqXml.Append("<CountryName>" + BookingDetals.PGCountry + "</CountryName>");
               ReqXml.Append("</Address></Customer></Profile></ProfileInfo></Profiles></ResGuest></ResGuests>");
               ReqXml.Append("<ResGlobalInfo><HotelReservationIDs><HotelReservationID ResID_Value=\"" + BookingDetals.Orderid + "\" ResID_Type=\"14\" ResID_Source=\"" + BookingDetals.RezNextSystemId + "\" /></HotelReservationIDs></ResGlobalInfo>");
               ReqXml.Append("</HotelReservation></HotelReservations></OTA_HotelResNotifRQ></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZBookingRequest");
           }
           BookingDetals.BookingConfReq = ReqXml.ToString();
           return BookingDetals;
       }
       public HotelBooking RZBookingRequestOld(HotelBooking BookingDetals)
       {
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               string[] roomtype=BookingDetals.RoomTypeCode.Split('|');;
               ReqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:htn=\"http://pms.ihotelier.com/HTNGService/services/HTNG2011BService\">");
               ReqXml.Append("<soapenv:Header>");
               ReqXml.Append("<wsa:UserName>" + BookingDetals.RezNextUserName + "</wsa:UserName>");
               ReqXml.Append("<wsa:PassWord>" + BookingDetals.RezNextPassword + "</wsa:PassWord>>");
               ReqXml.Append("</soapenv:Header><soapenv:Body>");
               ReqXml.Append("<OTA_HotelResNotifRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd\" Version=\"4.0\" ResStatus=\"Book\">");
               ReqXml.Append("<POS><Source><RequestorID Type='14' ID='Looknbook' /><BookingChannel Type='5' Primary='1'></BookingChannel></Source></POS>");
               ReqXml.Append("<HotelReservations><UniqueID ID= "+ BookingDetals.Orderid + " Type='14' />");
               ReqXml.Append("<HotelReservation CreatorID='Looknbook' CreateDateTime="+ DateTime.Today.ToString("MM/dd/yyyy")+" ResStatus='Reserved'>");
               ReqXml.Append("<RoomStays><RoomStay SourceOfBusiness='WEB'>");
               ReqXml.Append("<RoomRates><RoomRate RoomTypeCode=" + roomtype[0] + " NumberOfUnits=" + BookingDetals.NoofRoom + " RatePlanCode=" + BookingDetals .RoomPlanCode + " RatePlanCategory=" + roomtype[0] + ">");
               ReqXml.Append("<Rates><Rate EffectiveDate=" + BookingDetals.CheckInDate + " ExpireDate=" + BookingDetals.CheckOutDate + " UnitMultiplier=" + BookingDetals.NoofNight + ">");
               ReqXml.Append(" <Base AmountBeforeTax=" + BookingDetals.AmountBeforeTax + " AmountAfterTax=" + (BookingDetals.AmountBeforeTax + BookingDetals.Taxes).ToString() + " CurrencyCode='INR'>");
               ReqXml.Append("<Taxes Amount=" + BookingDetals.Taxes  + " CurrencyCode='INR' />");
               ReqXml.Append("</Base></Rates></RoomRate>");
               
               for (int i = 0; i > BookingDetals.NoofRoom;i++)
               {
                   ReqXml.Append("<GuestCounts IsPerRoom='0'>");
                   ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count=" +  BookingDetals.AdtPerRoom[i].ToString() + ">");
                   if (Convert.ToInt32(BookingDetals.ChdPerRoom[i]) > 0)
                       ReqXml.Append("<GuestCounts AgeQualifyingCode='8' Count=" + BookingDetals.ChdPerRoom[i].ToString() + ">");
                   ReqXml.Append("</GuestCounts");
               }
               ReqXml.Append("<TimeSpan Start=" + BookingDetals.CheckInDate + " End=" + BookingDetals.CheckOutDate + " />");
               ReqXml.Append("<Guarantee GuaranteeType='PrePay'></Guarantee>");
               ReqXml.Append("<Total AmountBeforeTax=" + BookingDetals.AmountBeforeTax + " CurrencyCode='INR'>");
               ReqXml.Append("<Taxes Amount=" + BookingDetals.Taxes + " CurrencyCode='INR' TaxType=" + BookingDetals .TaxType + " />");
               ReqXml.Append("</Total><BasicPropertyInfo HotelCode=" + BookingDetals.HtlCode + " /></RoomStay></RoomStays>");

               ReqXml.Append("<ResGuests><ResGuest ResGuestRPH='0'><Profiles><ProfileInfo>");
               ReqXml.Append("<UniqueID Type='1' ID_Context='Customer' />");
               ReqXml.Append("<Profile ProfileType='1'>");
               ReqXml.Append("<Customer><PersonName>");
               ReqXml.Append("<NamePrefix>" + BookingDetals.PGTitle + "</NamePrefix>");
               ReqXml.Append("<GivenName>" + BookingDetals.PGFirstName + "</GivenName>");
               ReqXml.Append("<MiddleName></MiddleName>");
               ReqXml.Append("<Surname>" + BookingDetals.PGLastName + "</Surname>");
               ReqXml.Append("</PersonName>");

               ReqXml.Append("<Telephone PhoneLocationType='6'  FormattedInd='0' PhoneNumber='" + BookingDetals.PGContact + "' PhoneTechType='1' />");
               ReqXml.Append("<Email>" + BookingDetals.PGEmail + "</Email>");
               ReqXml.Append("<Address Type='1'>");
               ReqXml.Append("<AddressLine>" + BookingDetals.PGAddress + "</AddressLine>");
               ReqXml.Append("<CityName>" + BookingDetals.PGCity + "</CityName>");
               ReqXml.Append("<PostalCode>" + BookingDetals.PGPin + "</PostalCode>");
               ReqXml.Append("<StateProv>" + BookingDetals.PGState + "</StateProv>");
               ReqXml.Append("<CountryName>" + BookingDetals.PGCountry + "</CountryName>");
               ReqXml.Append("</Address></Customer></Profile></ProfileInfo></Profiles></ResGuest></ResGuests>");
               ReqXml.Append("<ResGlobalInfo><HotelReservationIDs><HotelReservationID ResID_Value=" + BookingDetals.Orderid + " ResID_Type='14' ResID_Source='MMT' /></HotelReservationIDs></ResGlobalInfo>");
               ReqXml.Append("</HotelReservation></HotelReservations></OTA_HotelResNotifRQ></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "RZBookingRequest");
           }
           BookingDetals.BookingConfReq = ReqXml.ToString();
           return BookingDetals;
       }

       public HotelCancellation RZCancellationRequest(HotelCancellation BookingDetals)
       {
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               ReqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:htn=\"http://pms.ihotelier.com/HTNGService/services/HTNG2011BService\">");
               ReqXml.Append("<soapenv:Header><wsa:MessageID>" + BookingDetals.Orderid + "</wsa:MessageID><wsa:To>http://pmcoutllm01-t5.tcprod.local:8080/HtngSimulator/PMSInterfaceSimulator</wsa:To>");
               ReqXml.Append("<wsa:UserName>" + BookingDetals.Can_UserID + "</wsa:UserName> <wsa:PassWord>" + BookingDetals.Can_Password + "</wsa:PassWord>");
               ReqXml.Append("<wsa:ReplyTo> <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address> </wsa:ReplyTo>");
               ReqXml.Append("<wsa:Action>http://htng.org/2011B/HTNG2011B_SubmitResult</wsa:Action> </soapenv:Header>");
               ReqXml.Append("<soapenv:Body><OTA_HotelResNotifRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRQ.xsd\" Version=\"4.0\" ResStatus=\"Cancel\" TimeStamp=\"" + DateTime.Today.ToString("yyy-MM-dd hh:mm:ss") + "\">");
               ReqXml.Append("<POS><Source><RequestorID Type=\"14\" ID=\"" + BookingDetals.Can_PropertyId + "\" /><BookingChannel Type=\"5\" Primary=\"1\"> <CompanyName Code=\"WEB\">WEB</CompanyName> </BookingChannel> </Source> </POS>");
               ReqXml.Append("<HotelReservations><HotelReservation CreatorID=\"" + BookingDetals.Can_PropertyId + "\">");
               ReqXml.Append("<UniqueID ID=\"" + BookingDetals.Orderid + "\" Type=\"14\" />");
               ReqXml.Append("<BasicPropertyInfo HotelCode=\"" + BookingDetals.HotelCode + "\" />");
               ReqXml.Append("<ResGlobalInfo> <HotelReservationIDs> <HotelReservationID ResID_Type=\"14\" ResID_Value=\"" + BookingDetals.Orderid + "\" ResID_Source=\"" + BookingDetals.Can_PropertyId + "\" />");
               ReqXml.Append("</HotelReservationIDs> </ResGlobalInfo> </HotelReservation> </HotelReservations> </OTA_HotelResNotifRQ> </soapenv:Body> </soapenv:Envelope>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CancellationRequest");
           }
           BookingDetals.BookingCancelReq = ReqXml.ToString();
           return BookingDetals;
       }

    }
}
