﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CookComputing.XmlRpc;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Xml;
using System.Threading;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections;
namespace BS_BAL
{
    #region[XmlRpc Request Interface]
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface IgetSourceList : IXmlRpcProxy
    {
        [XmlRpcMethod("index.getSourceList")]
        XmlRpcStruct[] getSourceList();
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Igetdestinationlist : IXmlRpcProxy
    {
        [XmlRpcMethod("index.getDestinationList")]
        XmlRpcStruct[] Destinationlist(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Igetbustojourney : IXmlRpcProxy
    {
        [XmlRpcMethod("select.bustojurney")]
        XmlRpcStruct[] getbustojourney(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Igetbusseating : IXmlRpcProxy
    {
        [XmlRpcMethod("index.busseating")]
        XmlRpcStruct[] getBusSeating(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Igetseatselection : IXmlRpcProxy
    {
        [XmlRpcMethod("index.seatselection")]
        XmlRpcStruct[] getseatselection(XmlRpcStruct param);
    }

    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface IgetCheckGender : IXmlRpcProxy
    {
        [XmlRpcMethod("index.checkgender")]
        XmlRpcStruct[] getcheckgender(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Igetseatbooking : IXmlRpcProxy
    {
        [XmlRpcMethod("index.seatbooking")]
        XmlRpcStruct[] getseatbooking(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface Icancelpolicy : IXmlRpcProxy
    {
        [XmlRpcMethod("index.cancelpolicy")]
        XmlRpcStruct[] cancelpolicy(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface igetcancellationconfirmation : IXmlRpcProxy
    {
        [XmlRpcMethod("index.cancellationconfirmation")]
        XmlRpcStruct[] getcancellationconfirmation(XmlRpcStruct param);
    }
    [XmlRpcUrl("http://api.abhibus.com/abhibus/springtravels/server.php?SecurityKey=TENKSDfkADSKYJFKDS")] //live
    //[XmlRpcUrl("http://abhibus.net/abhibus/pay/server.php")]//test
    public interface igetticketcancellation : IXmlRpcProxy
    {
        [XmlRpcMethod("index.ticketcancellation")]
        XmlRpcStruct[] getticketcancellation(XmlRpcStruct param);
    }
    #endregion
    public class AbhiBusService
    {
        BS_DAL.SharedDAL shareddal; string msg = "";
        SharedBAL sharedbal; string resXml = "";
        string reqXml = "";
        EXCEPTION_LOG.ErrorLog erlog;
        #region[GET AbhiBus source List]
        public List<BS_SHARED.SHARED> getABSourceList()
        {
            List<BS_SHARED.SHARED> srclist = new List<BS_SHARED.SHARED>();
            try
            {
                IgetSourceList proxy = XmlRpcProxyGen.Create<IgetSourceList>();
                XmlRpcStruct[] res = proxy.getSourceList();
                foreach (XmlRpcStruct st in res)
                {
                    srclist.Add(new BS_SHARED.SHARED { src = st["source"].ToString().Trim(), srcID = st["source_num"].ToString().Trim(),provider_name="AB" });
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return srclist;
        }
        #endregion
        #region[GET AbhiBus destination List]
        public List<BS_SHARED.SHARED> getABDestList(string srcID, string src, string providerName)
        {
            List<BS_SHARED.SHARED> destlist = new List<BS_SHARED.SHARED>();
            try
            {
                Igetdestinationlist proxy = XmlRpcProxyGen.Create<Igetdestinationlist>();
                XmlRpcStruct structData = new XmlRpcStruct();
                structData.Add("sourceid", srcID.Trim());
                XmlRpcStruct[] res = proxy.Destinationlist(structData);
                foreach (XmlRpcStruct st in res)
                {
                    destlist.Add(new BS_SHARED.SHARED { dest = st["destination"].ToString().Trim(), destID = st["destination_num"].ToString().Trim(), src = src, srcID = srcID, provider_name = providerName });
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return destlist;
        }
        //public List<BS_SHARED.SHARED> getABDestList(string srcID)
        //{
        //    List<BS_SHARED.SHARED> destlist = new List<BS_SHARED.SHARED>();
        //    try
        //    {
        //        Igetdestinationlist proxy = XmlRpcProxyGen.Create<Igetdestinationlist>();
        //        XmlRpcStruct structData = new XmlRpcStruct();
        //        structData.Add("sourceid", srcID.Trim());
        //        XmlRpcStruct[] res = proxy.Destinationlist(structData);
        //        foreach (XmlRpcStruct st in res)
        //        {
        //            destlist.Add(new BS_SHARED.SHARED { dest = st["destination"].ToString().Trim(), destID = st["destination_num"].ToString().Trim() });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        erlog = new EXCEPTION_LOG.ErrorLog();
        //        erlog.writeErrorLog(ex, "SHARED_BAL");
        //    }
        //    return destlist;
        //}
        #endregion
        #region[Abhi Bus Result]
        public List<BS_SHARED.SHARED> getABJourneyList(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> resultList = new List<BS_SHARED.SHARED>();
            string[] hr = null; string arrhour = ""; string dephour = "";
            List<BS_SHARED.SHARED> farelist = new List<BS_SHARED.SHARED>();
            XmlRpcStruct[] res = null; XmlRpcStruct structData;
            XmlRpcStruct[] req = new XmlRpcStruct[1];

            sharedbal = new SharedBAL();
            try
            {
                Igetbustojourney proxy = XmlRpcProxyGen.Create<Igetbustojourney>();
                structData = new XmlRpcStruct();
                structData.Add("sourceid", shared.srcID);
                structData.Add("destinationid", shared.destID);
                structData.Add("jdate", shared.journeyDate);
                structData.Add("psgr_count", shared.NoOfPax);
                structData.Add("seat_sleeper", shared.SeatType);
                req[0] = structData;
                //---------------------request xml------------------//
                reqXml = getSerializeXML(req, "SearchRequest");
                res = proxy.getbustojourney(structData);
                resXml = getSerializeXML(res, "SearchResponse");
               
                for (int i = 0; i <= res.Count() - 1; i++)
                { var varDP=res[i]["droppingpoints"] as string[];
                var varBP = res[i]["boardingpoints"] as string[];


                if (varDP !=null && varBP!=null)
                    {
                        BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
                        sharedd.berthfare = res[i]["berth_fare"].ToString();
                        sharedd.travelerID = res[i]["traveler_id"].ToString();
                        //-------------------------get cancellation policy-----------------//
                        string[] can = getCanPolicy(res[i]["traveler_id"].ToString().Trim());
                        //--------------------------end--------------------------------//
                        sharedd.canPolicy_AB = can;
                        sharedd.berthfarewithTaxes = res[i]["berth_fare_with_taxes"].ToString();
                        sharedd.seatfare = res[i]["seat_fare"].ToString();
                        if (res[i]["berth_fare"].ToString() == res[i]["seat_fare"].ToString())
                        {
                            sharedd.seat_Originalfare = new string[1];
                            sharedd.seat_farewithMarkp = new string[1];
                            farelist = sharedbal.getMarkUp(shared.agentID.Trim(), res[i]["seat_fare"].ToString());
                            sharedd.seat_farewithMarkp[0] = Convert.ToString(Math.Round(farelist[0].mrkpFare, 0));
                            sharedd.seat_Originalfare[0] = res[i]["seat_fare"].ToString();
                        }
                        else
                        {
                            string arrfare = res[i]["seat_fare"].ToString() + "," + res[i]["berth_fare"].ToString();

                            farelist = sharedbal.getMarkUp(shared.agentID.Trim(), arrfare);
                            sharedd.seat_Originalfare = new string[farelist.Count];
                            sharedd.seat_farewithMarkp = new string[farelist.Count];
                            sharedd.seat_Originalfare[0] = res[i]["seat_fare"].ToString();
                            sharedd.seat_Originalfare[1] = res[i]["berth_fare"].ToString();
                            for (int f = 0; f <= farelist.Count - 1; f++)
                            {
                                sharedd.seat_farewithMarkp[f] = Convert.ToString(Math.Round(farelist[f].mrkpFare, 0));
                            }
                        }
                        sharedd.seatfarewithTaxes = res[i]["seat_fare_with_taxes"].ToString();
                        sharedd.arrTime = res[i]["arrival_time"].ToString().Trim();
                        sharedd.departTime = res[i]["departure_time"].ToString().Trim();
                        sharedd.serviceName = res[i]["service_name"].ToString();
                        sharedd.remainingSeat = Convert.ToInt32(res[i]["remaining_seats"].ToString());
                        sharedd.serviceNumber = res[i]["service_number"].ToString();
                        sharedd.serviceType = res[i]["service_type"].ToString();
                        sharedd.busType = res[i]["bus_type"].ToString();
                        sharedd.traveler = res[i]["traveler_agent"].ToString();
                        sharedd.totalSeat = Convert.ToInt32(res[i]["total_seats"].ToString());
                        System.Object DR = res[i]["droppingpoints"];
                        System.Object BD = res[i]["boardingpoints"];
                        object[] Drop = (object[])DR;
                        object[] Board = (object[])BD;
                        sharedd.bdPoint = Board.Cast<string>().ToArray();
                        sharedd.drPoint = Drop.Cast<string>().ToArray();
                        sharedd.serviceID = res[i]["service_id"].ToString();
                        sharedd.SeatType = res[i]["seat_type"].ToString();
                        //sharedd.startDate = res[i]["service_start_date"].ToString();
                        sharedd.provider_name = "AB";
                        sharedd.idproofReq = "false";
                        sharedd.partialCanAllowed = "false";
                        DateTime d1 = new DateTime();
                        d1 = Convert.ToDateTime(sharedd.arrTime); DateTime d2 = new DateTime();
                        d2 = Convert.ToDateTime(sharedd.departTime);
                        TimeSpan ts = d1.Subtract(d2).Duration();
                        sharedd.Dur_Time = ts.ToString();
                    
                        if (sharedd.drPoint != null && sharedd.bdPoint != null)
                            resultList.Add(sharedd);
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return resultList;
        }
        #endregion
        #region[AbhiBus Seat layout]
        public string ABSeatLayOut(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>(); string markupfare = "0";
            sharedbal = new SharedBAL(); XmlRpcStruct[] reqstruct = new XmlRpcStruct[1];
            string layout_lower = "<table cellpadding='0' cellspacing='0'  id='seatTbllower_AB' class='tbl' border='0'>";
            string layout_upper = "<table cellpadding='0' cellspacing='0'  id='seatTblupper_AB' class='tbl' border='0'>";
            string seatLayout = "<table width='100%' cellpadding='0' cellspacing='0'>";
            seatLayout += "<tr>";
            seatLayout += "<td style='font-weight:bold; width:30%; color:#20313F; font-size:18px;'>Select Your Seat:</td>";
            seatLayout += "</tr>";
            seatLayout += "<tr>";
            DateTime date = Convert.ToDateTime(shared.journeyDate.Trim().Replace("-", "/"));
            seatLayout += "<td style='font-style:italic;'>" + shared.src.Trim() + " To " + shared.dest.Trim() + " on, " + date.DayOfWeek + " " + date.Day + " " + date.ToString("MMM") + " " + date.Year + " (" + shared.traveler + ")</td>";
            seatLayout += "</tr>";
            try
            {
                Igetbusseating proxy = XmlRpcProxyGen.Create<Igetbusseating>();
                XmlRpcStruct structData = new XmlRpcStruct();
                structData.Add("jdate", shared.journeyDate);
                structData.Add("sourceid", shared.srcID.Trim());
                structData.Add("destinationid", shared.destID);
                structData.Add("serviceid", shared.serviceID.Trim());
                structData.Add("seat_sleeper", shared.SeatType);
                XmlRpcStruct[] resData = proxy.getBusSeating(structData);
                //------------------------save seat layout request and response-------------------//
                reqstruct[0] = structData;
                reqXml = getSerializeXML(reqstruct, "SeatLayoutRequest");
                resXml = getSerializeXML(resData, "SeatLayoutResponse");
                //--------------------------------end---------------------------------------------//
                string str = "";
                if (resData[0]["gendertype"].ToString() == "F")
                {
                    str = "gfsgdf";
                }


                #region[Lower deck]
                if (resData[0]["decks"].ToString() == "1")
                {
                    for (int row = 1; row <= Convert.ToInt32(resData[0]["tot_rows"].ToString()); row++)
                    {
                        layout_lower += "<tr>";
                        layout_upper += "<tr>";
                        for (int col = 1; col <= Convert.ToInt32(resData[0]["tot_cols"].ToString()); col++)
                        {
                            string[] deck = (string[])resData[0]["lowerdeck_seat_nos"];
                            for (int d = 0; d <= deck.Length - 1; d++)
                            {
                                string[] array = deck[d].Trim().Replace("#*#", "@").Split('@');
                                if ((row + "-" + col) == array[1].Trim())
                                {
                                    if (resData[0]["seat_fare"].ToString() == resData[0]["berth_fare"].ToString())
                                    {
                                        markupfare = resData[0]["seat_fare"].ToString();
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), resData[0]["seat_fare"].ToString());
                                        if (resData[0]["div_row"].ToString() == row.ToString())
                                        {
                                            if (array[2].Trim() == "S" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "S" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }
                                        }
                                        else
                                        {
                                            if (array[2].Trim() == "S" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "S" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        markupfare = resData[0]["seat_fare"].ToString() + "," + resData[0]["berth_fare"].ToString();
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), markupfare);
                                        string[] exec = (string[])resData[0]["executive_seat_nos"];

                                        if (resData[0]["div_row"].ToString() == row.ToString())
                                        {
                                            if (array[2].Trim() == "S" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }

                                            if (array[2].Trim() == "S" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {

                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'></td>";

                                            }

                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'></td>";
                                            }

                                        }
                                        else
                                        {
                                            if (array[2].Trim() == "S" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "A" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "S" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "B" && array[3].Trim() == "B" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                else
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                            }
                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                            {

                                                layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }

                                            if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                            {
                                                layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                            }

                                        }


                                    }

                                }
                            }
                        }
                        layout_lower += "</tr>";
                        layout_upper += "</tr>";
                    }
                    layout_lower += "</table>";
                    seatLayout += "</td>";
                    seatLayout += "</tr>";
                    seatLayout += "<tr>";
                    seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'></span>";
                    seatLayout += layout_lower;
                    seatLayout += "</td>";
                    seatLayout += "</tr>";
                    seatLayout += "</table>";
                }
#endregion
                #region[Upper and Lower Deck]
                else if (resData[0]["decks"].ToString() == "2")
                {
                    for (int row = 1; row <= Convert.ToInt32(resData[0]["upper_tot_rows"].ToString()); row++)
                    {
                        layout_upper += "<tr>";
                        for (int col = Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 1; col <= Convert.ToInt32(resData[0]["total_seat_cols"].ToString()); col++)
                        {
                            string[] deck = (string[])resData[0]["upperdeck_seat_nos"];
                            for (int d = 0; d <= deck.Length - 1; d++)
                            {
                                string[] array = deck[d].Trim().Replace("#*#", "@").Split('@');
                                if ((row + "-" + col) == array[1].Trim())
                                {
                                    if (resData[0]["seat_fare"].ToString() == resData[0]["berth_fare"].ToString())
                                    {
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), resData[0]["seat_fare"].ToString());
                                        if (resData[0]["upper_div_row"].ToString() == row.ToString())
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    layout_upper += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    layout_upper += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_upper += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        markupfare = resData[0]["seat_fare"].ToString() + "," + resData[0]["berth_fare"].ToString();
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), markupfare);
                                        string[] exec = (string[])resData[0]["executive_seat_nos"];

                                        if (resData[0]["upper_div_row"].ToString() == row.ToString())
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {

                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {

                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {

                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";

                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["upper_tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {

                                                        layout_upper += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_upper += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_upper += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_upper += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        layout_upper += "</tr>";
                    }
                    layout_upper += "</table>";
                    for (int row = 1; row <= Convert.ToInt32(resData[0]["tot_rows"].ToString()); row++)
                    {
                        layout_lower += "<tr>";
                        for (int col = 1; col <= Convert.ToInt32(resData[0]["tot_cols"].ToString()); col++)
                        {
                            string[] deck = (string[])resData[0]["lowerdeck_seat_nos"];
                            for (int d = 0; d <= deck.Length - 1; d++)
                            {
                                string[] array = deck[d].Trim().Replace("#*#", "@").Split('@');
                                if ((row + "-" + col) == array[1].Trim())
                                {
                                    if (resData[0]["seat_fare"].ToString() == resData[0]["berth_fare"].ToString())
                                    {
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), resData[0]["seat_fare"].ToString());
                                        if (resData[0]["div_row"].ToString() == row.ToString())
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        markupfare = resData[0]["seat_fare"].ToString() + "," + resData[0]["berth_fare"].ToString();
                                        list = sharedbal.getMarkUp(shared.agentID.Trim(), markupfare);
                                        string[] exec = (string[])resData[0]["executive_seat_nos"];

                                        if (resData[0]["div_row"].ToString() == row.ToString())
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {

                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }

                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='" + (Convert.ToInt32(resData[0]["tot_cols"].ToString()) + 5) + "'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }

                                            }
                                        }
                                        else
                                        {
                                            if (array.Length > 2)
                                            {
                                                if (array[2].Trim() == "S" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right'><div class='divAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + " | Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "A")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperAval'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "S" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "B" && array[3].Trim() == "B")
                                                {
                                                    if (Array.IndexOf(exec, array[0].Trim()) > 0)
                                                    {
                                                        layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[1].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["berth_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                    else
                                                    {
                                                        layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[0].Trim() + " | Fare:" + list[0].mrkpFare + "' rel='Seat No:" + array[0].Trim() + " | Fare:" + resData[0]["seat_fare"].ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
                                                    }
                                                }
                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "S")
                                                {
                                                    layout_lower += "<td align='right'><div class='divBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }

                                                if (array[2].Trim() == "NA" && resData[0]["serv_layout_type"].ToString() == "B")
                                                {
                                                    layout_lower += "<td align='right' colspan='2'><div class='divHoriSleperBlock'  title='Seat No:" + array[1].Trim() + "'></div></td>";
                                                }

                                            }
                                        }

                                    }
                                }
                            }
                        }
                        layout_lower += "</tr>";

                    }
                    layout_lower += "</table>";
                    if (layout_upper.Length > 150)
                    {
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Upper</span>";
                        seatLayout += layout_upper;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Lower</span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                    else
                    {
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'></span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                }
#endregion
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return seatLayout;
        }
        #endregion
        #region[Gender Check]
        public object[] check_Gender(BS_SHARED.SHARED shared)
        {
            string gender = ""; string seat = ""; object[] chk = null;
            try
            {
                for (int g = 0; g <= shared.gender.Count - 1; g++)
                {
                    gender += shared.gender[g].Trim() + ",";
                }
                for (int s = 0; s <= shared.paxseat.Count - 1; s++)
                {
                    seat += shared.paxseat[s].Trim() + ",";
                }
                IgetCheckGender proxy = XmlRpcProxyGen.Create<IgetCheckGender>();
                XmlRpcStruct structdata = new XmlRpcStruct();
                structdata.Add("jdate", shared.journeyDate.Trim());
                structdata.Add("sourceid", shared.srcID.Trim());
                structdata.Add("destinationid", shared.srcID.Trim());
                structdata.Add("serviceid", shared.srcID.Trim());
                structdata.Add("selected_seats", seat.Remove(seat.LastIndexOf(",")));
                structdata.Add("psgr_gender_type", gender.Remove(gender.LastIndexOf(",")));
                XmlRpcStruct[] strChk = proxy.getcheckgender(structdata);
                for (int j = 0; j <= strChk.Count() - 1; j++)
                {
                    chk = (object[])strChk[j]["message"];
                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return chk;
        }
        #endregion
        #region[Block Seat]
        public string getBlockSeat_Details(BS_SHARED.SHARED shared)
        {
            string seat = ""; string resSeat = "";
            object[] seatnum = null;
            try
            {
                Igetseatselection proxy = XmlRpcProxyGen.Create<Igetseatselection>();
                XmlRpcStruct structData = new XmlRpcStruct();
                structData.Add("jdate", shared.journeyDate.Trim());
                structData.Add("sourceid", shared.srcID.Trim());
                structData.Add("destinationid", shared.destID.Trim());
                structData.Add("serviceid", shared.serviceID.Trim());
                for (int s = 0; s <= shared.paxseat.Count - 1; s++)
                {
                    seat += shared.paxseat[s].Trim() + ",";
                }
                seat = seat.Remove(seat.LastIndexOf(","));
                structData.Add("selected_seats", seat.Trim());
                XmlRpcStruct[] resStruct = proxy.getseatselection(structData);
                if (resStruct[0]["sel_seats"].ToString() == "Error")
                {
                    resSeat = "Error";
                }
                else
                {
                    for (int b = 0; b <= resStruct.Count() - 1; b++)
                    {
                        seatnum = (object[])resStruct[b]["sel_seats"];
                        for (int c = 0; c <= seatnum.Count() - 1; c++)
                        {
                            resSeat += seatnum[c].ToString().Trim() + ",";
                        }
                    }
                }
                resSeat = resSeat.Remove(resSeat.LastIndexOf(","));
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return resSeat;
        }
        #endregion
        #region[Book Seat]
        public List<BS_SHARED.SHARED> getFinalTicket(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> list_Ticket = new List<BS_SHARED.SHARED>();
            string seat = ""; string gender = ""; string paxname = "";
            BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED(); string[] n = null;
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                Igetseatbooking proxy = XmlRpcProxyGen.Create<Igetseatbooking>();
                XmlRpcStruct strutData = new XmlRpcStruct();
                strutData.Add("jdate", shared.journeyDate.Trim());
                strutData.Add("sourceid", shared.srcID.Trim());
                strutData.Add("destinationid", shared.destID.Trim());
                strutData.Add("serviceid", shared.serviceID.Trim());
                for (int s = 0; s <= shared.paxseat.Count - 1; s++)
                {
                    seat += shared.paxseat[s].Trim() + ",";
                    if (shared.paxname[s].IndexOf(",") >= 0)
                    {
                        n = shared.paxname[s].Split(',');
                        paxname += n[0].Trim() + ",";
                    }
                    else
                    {
                        paxname += shared.paxname[s].Trim() + ",";
                    }

                    gender += shared.gender[s].Trim() + ",";
                }
                seat = seat.Remove(seat.LastIndexOf(","));
                paxname = paxname.Remove(paxname.LastIndexOf(","));
                gender = gender.Remove(gender.LastIndexOf(","));
                strutData.Add("selected_seats", seat.Trim());
                strutData.Add("psgr_gender_type", gender.Trim());
                strutData.Add("psgr_name", paxname.Trim());
                strutData.Add("boardingpoint_id", shared.boardpointid.Trim());
                strutData.Add("customer_address", shared.paxaddress.Trim());
                strutData.Add("customer_name", n[0].Trim());
                strutData.Add("customer_phoneno", shared.paxmob.Trim());
                strutData.Add("customer_email", shared.paxemail.Trim());
                XmlRpcStruct[] resStruct = proxy.getseatbooking(strutData);
                //---------------------save request xml--------------------------//
                XmlRpcStruct[] reqstruct = new XmlRpcStruct[1];
                reqstruct[0] = strutData;
                reqXml = getSerializeXML(reqstruct, "BookRequest");
                //-----------------------end-----------------------------------//
                //---------------------save response xml-------------------------//
                resXml = getSerializeXML(resStruct, "BookResponse");
                //------------------------end-----------------------------------//

                //------------------------save---------------------------------//
                sharedd.orderID = shared.orderID.Trim();
                sharedd.agentID = shared.agentID.Trim();
                sharedd.bookreq = reqXml.Trim();
                sharedd.bookres = resXml.Trim();
                sharedd.blockKey = shared.blockKey;
                sharedd.tin = "";
                sharedd.pnr = "";
                sharedd.provider_name = shared.provider_name.Trim();
                shareddal.insertBooking_Request_Response(sharedd);
                //------------------------end---------------------------------//
                for (int t = 0; t <= resStruct.Count() - 1; t++)
                {
                    if (resStruct[t]["status"].ToString() == "Fail")
                    {
                        list_Ticket.Add(new BS_SHARED.SHARED { bookres = resStruct[t]["message"].ToString(), status = resStruct[t]["status"].ToString() });
                    }
                    else
                    {
                        list_Ticket.Add(new BS_SHARED.SHARED { bookres = resXml.Trim(), tin = resStruct[t]["ticket_num"].ToString(), status = resStruct[t]["status"].ToString(), totFare = Convert.ToDecimal(resStruct[t]["total_fare"].ToString()), serviceNumber = resStruct[t]["service_number"].ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list_Ticket;
        }
        #endregion
        #region[Serialize Request]
        private string getSerializeXML(XmlRpcStruct[] obj, string ParentNode)
        {
            string xml = "";
            try
            {
                var reult = (from c in obj select c).ToList();
                XDocument finalXDocument = new XDocument();
                XElement xElem1 = new XElement(ParentNode);
                foreach (var itm in reult)
                {
                    Dictionary<string, string> resultDict = new Dictionary<string, string>();
                    foreach (var i in itm)
                    {
                        DictionaryEntry dd = (DictionaryEntry)i;
                        resultDict.Add(dd.Key.ToString(), dd.Value.ToString());
                    }
                    XElement xElem = new XElement("items", resultDict.Select(x => new XElement("item", new XAttribute("id", x.Key), new XAttribute("value", x.Value))));
                    xElem1.Add(xElem);
                }
                finalXDocument.Add(xElem1);
                xml = finalXDocument.ToString();
                string folder = "D:\\XmlFile" + "(" + DateTime.Now.ToString("dd-MM-yyyy") + ")" + "";
                DirectoryInfo info = new DirectoryInfo(folder);
                if (!Directory.Exists(info.FullName))
                {
                    Directory.CreateDirectory(folder);
                    finalXDocument.Save(folder + "/" + ParentNode + DateTime.Now.ToString("hh:mm:ss").Replace(":", "") + ".xml");
                }
                else
                {
                    finalXDocument.Save(folder + "/" + ParentNode + DateTime.Now.ToString("hh:mm:ss").Replace(":", "") + ".xml");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
        #region[Get CancelPolicy]
        private string[] getCanPolicy(string travelerId)
        {
            string[] policy = new string[4];
            try
            {
                Icancelpolicy proxy = XmlRpcProxyGen.Create<Icancelpolicy>();
                XmlRpcStruct structdata = new XmlRpcStruct();
                structdata.Add("travel_partner_id", travelerId.Trim());
                XmlRpcStruct[] resstruct = proxy.cancelpolicy(structdata);

                policy[0] = resstruct[0]["traveler_agent_name"].ToString().Trim();
                policy[1] = resstruct[0]["cuttoff_time"].ToString().Trim();
                policy[2] = resstruct[0]["return_amount"].ToString().Trim();
                policy[3] = resstruct[0]["cancel_type"].ToString().Trim();

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return policy;
        }
        #endregion
        #region[Get Cancel Confrimation]
        public string[] getCancelConfrimation(BS_SHARED.SHARED shared)
        {
            string[] resCan = new string[6];
            try
            {
                igetcancellationconfirmation proxy = XmlRpcProxyGen.Create<igetcancellationconfirmation>();
                XmlRpcStruct structData = new XmlRpcStruct();
                structData.Add("ticketno", shared.tin.Trim());
                XmlRpcStruct[] resStruct = proxy.getcancellationconfirmation(structData);
                if (resStruct[0].Count > 3)
                {
                    resCan[0] = resStruct[0]["status"].ToString();
                    resCan[1] = resStruct[0]["message"].ToString();
                    resCan[2] = resStruct[0]["ticket_number"].ToString();
                    resCan[3] = resStruct[0]["total_ticket_fare"].ToString();
                    resCan[4] = resStruct[0]["total_refund_amount"].ToString();
                    resCan[5] = resStruct[0]["cancellation_parcentage"].ToString();
                }
                else
                {
                    resCan[0] = resStruct[0]["status"].ToString();
                    resCan[1] = resStruct[0]["message"].ToString();
                    resCan[2] = resStruct[0]["ticket_number"].ToString();
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");

            }
            return resCan;
        }
        #endregion
        #region[Cancel Ticket]
        private string[] cancelTicket(BS_SHARED.SHARED shared)
        {
            string[] canResponse = new string[6];
            BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
            XmlRpcStruct[] reqstruct = new XmlRpcStruct[1];
            try
            {
                igetticketcancellation proxy = XmlRpcProxyGen.Create<igetticketcancellation>();
                XmlRpcStruct structData = new XmlRpcStruct();
                structData.Add("ticketno", shared.tin.Trim());
                XmlRpcStruct[] resStruct = proxy.getticketcancellation(structData);
                reqstruct[0] = structData;
                //-------------------save cancel request and response xml------------------//
                sharedd.canrequest = getSerializeXML(reqstruct, "TicketCancelRequest");
                sharedd.canresponse = getSerializeXML(resStruct, "TicketCancelresponse");
                sharedd.orderID = shared.orderID.Trim();
                sharedd.agentID = shared.agentID.Trim();
                sharedd.provider_name = shared.provider_name;
                shareddal.InsertUpdateCanREQ(sharedd, "Insert");
                //------------------------end----------------------------------------------//
                canResponse[0] = resStruct[0]["status"].ToString();
                canResponse[1] = resStruct[0]["message"].ToString();
                canResponse[2] = resStruct[0]["ticket_number"].ToString();
                canResponse[3] = resStruct[0]["total_ticket_fare"].ToString();
                canResponse[4] = resStruct[0]["total_refund_amount"].ToString();
                canResponse[5] = resStruct[0]["cancellation_parcentage"].ToString();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return canResponse;
        }
        #endregion
        #region[Calculate Refund]
        public string[] getcancelTicketStatus(BS_SHARED.SHARED shared)
        {
            string[] cancelResponse = new string[5]; shareddal = new BS_DAL.SharedDAL();
            double busfare = 0; string[] canResp = null; int canPercent = 0; double totcanRhrg = 0;
            try
            {

                canResp = cancelTicket(shared);
                if (cancelResponse.Length < 0)
                {
                    return cancelResponse;
                }
                else
                {

                    if (shared.fare.IndexOf(",") >= 0)
                    {
                        string[] spltfare = shared.fare.Split(',');
                        string[] splitnetfare = shared.netfare.Split(',');
                        canPercent = Convert.ToInt32(canResp[5].Trim().Replace("%", "").Replace(" ", ""));
                        for (int b = 0; b <= spltfare.Length - 1; b++)
                        {
                            busfare = Convert.ToDouble((Convert.ToDouble(spltfare[b]) * (canPercent) / 100));
                            totcanRhrg += busfare;
                            shared.cancelRecharge += Convert.ToString(busfare) + ",";
                            shared.refundAmt += Convert.ToString(Convert.ToDouble(splitnetfare[b]) - busfare) + ",";
                        }
                    }
                    else
                    {
                        shared.fare = shared.fare;
                        canPercent = Convert.ToInt32(canResp[5].Trim().Replace("%", "").Replace(" ", ""));
                        busfare = Convert.ToDouble((Convert.ToDouble(shared.fare) * (canPercent) / 100));
                        totcanRhrg += busfare;
                        shared.cancelRecharge = Convert.ToString(busfare);
                        shared.refundAmt = Convert.ToString(Convert.ToDouble(shared.netfare) - busfare);
                    }
                    //-------------------------------insert into database----------------------------------//
                    shareddal.updateCanchrgandRefund(shared);
                    if (shared.refundAmt.IndexOf(",") >= 0)
                    {
                        shared.refundAmt = shared.refundAmt.Remove(shared.refundAmt.LastIndexOf(","));
                    }
                    string[] addfare = shared.refundAmt.Split(',');
                    for (int x = 0; x <= addfare.Length - 1; x++)
                    {
                        shared.addAmt += Convert.ToDecimal(addfare[x].Trim());
                    }
                    shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                    shareddal.insertLedgerDetails(shared, "Add");
                    shareddal.updateTicketandPnr(shared);
                    //--------------------------------------end--------------------------------------------//

                    //----------------------------------yatra credit note integratrion---------------------------//
                    //YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
                    //bool flag = bs.IntegrateYatra(shared.orderID, shared.agentID, shared.tin, "C");
                    //---------------------------------------end-------------------------------------------------//
                }
                cancelResponse[0] = canResp[0].Trim();//sucess/fail//
                cancelResponse[1] = Convert.ToString(totcanRhrg);//cancel charge//
                cancelResponse[2] = Convert.ToString(shared.addAmt);//refund amount//
                cancelResponse[3] = canResp[2].Trim();//ticket number//
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return cancelResponse;
        }
        #endregion
    
    }
}
