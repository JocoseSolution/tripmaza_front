﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="Terms_of_Service.aspx.vb" Inherits="privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">

            <div class="theme-hero-area-bg" style="background-image: url(https://boatrentalusa.com/wp-content/uploads/2021/06/terms.jpg);"></div>
            <%--<div class="theme-hero-area-mask theme-hero-area-mask-half"></div>--%>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Terms of Services</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">

            <h3>Terms Of Service</h3>
            <p style="text-align: justify">
                We welcome you to our website. References to the Site are deemed to include derivatives thereof, including but not necessarily limited to mobile websites and applications. The Terms & Conditions? As defined below- apply to our services, directly or indirectly made available to you through whatever means, including online reservations, customer service requests or otherwise. By accessing, browsing and using the Site and/or by making a reservation through
Our Website, you acknowledge and agree having read, understood and agreed to the Terms & Conditions. If you do not agree to the Terms & Conditions, please cease use of the Site immediately. The Terms & Conditions? As may be amended from time to time? Constitute the entire agreement, and supersede any other agreements or understandings (oral or written), between you and us with respect to their subject matters unless explicitly stated otherwise.
            </p>
            <p style="text-align: justify">
                This web site is published and maintained by Flyagainonline, a company incorporated and existing in accordance with the laws of the Republic of India. When you access, browse or use this Site you accept, without limitation or qualification, the terms and conditions set forth below. When you access any sub-site (whether belonging to an 'associate' of Flyagainonline or otherwise) through this site, then such sub-site may have its own terms and conditions of use which is specific to such sub-site. Sub-sites may contain such additional terms and conditions of use as may be set out in such sub-site.
            </p>
            <p style="text-align: justify">
                These Terms and Conditions of Use and any additional terms posted on this Site together constitute the entire agreement between Flyagainonline and you with respect to your use of this Site.
            </p>


            <h3>Ownership
            </h3>
            <p style="text-align: justify">
                All materials on this Site, including but not limited to audio, images, software, text, icons and such like (the "Content"), are protected by copyright under international conventions and copyright laws. You cannot use the Content, except as specified herein. You agree to follow all instructions on this Site limiting the way you may use the Content.
            </p>
            <p style="text-align: justify">
                There are a number of proprietary logos, service marks and trademarks found on this Site whether owned/used by Flyagainonline or otherwise. By displaying them on this Site, Flyagainonline is not granting you any license to utilize those proprietary logos, service marks, or trademarks. Any unauthorized use of the Content may violate copyright laws, trademark laws, the laws of privacy and publicity, and civil and criminal statutes.
            </p>
            <p style="text-align: justify">
                You may download such copy/copies of the Content to be used only by you for your personal use at home unless the subsite you are accessing states that you may not. If you download any Content from this Site, you shall not remove any copyright or trademark notices or other notices that go with it.
            </p>

            <h3>Use of the Site
            </h3>
            <p style="text-align: justify">Flyagainonline grants you a limited, restricted, personal, non-transferable, non-sublicense able, revocable license to access and use the Site only as expressly permitted in the Terms & Conditions. Except for this limited license, we do not grant you any other rights or license with respect to the Site; any rights or licenses not expressly granted herein are reserved. The content and information on the Site, as well as the software and infrastructure used to provide such content and information, is proprietary to Flyagainonline or its suppliers and providers, including the Hotels. You may only use the Site to make bona fide and legitimate enquiries or bookings and you hereby undertake not to make any speculative, false or fraudulent bookings or any reservations in anticipation of demand. You undertake that the payment details you provide us with in making a booking are fully correct. You also undertake to provide correct and accurate e-mail, postal and/or other contact details to Flyagainonline and acknowledge that Flyagainonline may use these details to contact you in the event that this should prove necessary.</p>
            <p style="text-align: justify">
                Accordingly, as a condition of using the Site, you agree not to use the Site or its contents or information for any commercial or non-personal purpose (direct or indirect) or for any purpose that is illegal, unlawful or prohibited by the Terms of Use. Except with our prior written authorization, you agree not to modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell or re-sell any information, software, products, or services obtained from the Site. In addition, you agree not to: use the Site or its contents for any non-authorized commercial purpose;
make any speculative, false, or fraudulent booking; access, monitor or copy any content or information of the Site using any robot, spider, scraper or other automated means or any manual process for any purpose without our express prior written permission;
violate the restrictions in any robot exclusion headers on the Site or bypass or circumvent other measures employed to prevent or limit access to the Site; take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on our infrastructure
deep-link to any portion of the Site (including, without limitation, purchase paths) for any purpose without our express prior written permission; re-sell, use, copy, monitor (e.g. spider, scrape), display, download or reproduce any content or information, software, products or services available on the Site for any commercial or competitive activity or purpose; or otherwise incorporate any part of the Site into any other website without our prior written authorization; deliver any unlawful (according to any and all applicable laws or regulations) postings to or through the Site, or any postings which advocate illegal activity; deliver, or provide links to, any postings containing material that could be considered harmful, obscene, pornographic, indecent, lewd, violent, abusive, profane, racist, discriminating, insulting, threatening, tortuous, harassing, hateful or otherwise objectionable; deliver or provide links to, any postings containing defamatory, slanderous, false or libellous material; deliver any posting that infringes or violates any intellectual property or other right of any entity or person, including, without limitation, copyrights, patents, trademarks, laws governing trade secrets, rights to privacy, or publicity; deliver any posting that you do not have a right to make available under law or contractual or fiduciary relationships; impersonate another person or entity or falsely state or otherwise misrepresent your affiliation with a person or entity, or adopt a false identity if the purpose of doing so is to mislead, deceive, or defraud another; manipulate identifiers, including by forging headers, in order to disguise the origin of any posting that you deliver; use the Site in any manner which could damage, disable, overburden, or impair or otherwise interfere with the use of the Site or other users' computer equipment, or cause damage, disruption or limit the functioning of any software, hardware, or telecommunications equipment; attempt to gain unauthorized access to the Site, any related website, other accounts, computer system, or networks connected to the Site, through hacking, password mining, or any other means; obtain or attempt to obtain any materials or information through any means not intentionally made available through the Site, including harvesting or otherwise collecting information about others such as email addresses;
engage in any deceptive practices intended to manipulate the organic Search Engine Results Page (SERP) or employ search engine optimization (?SEO?) techniques considered to be against common search engine guidelines. SEO practices considered unethical, or? Black hat? (aka?spamdexing?) Include, though are not limited to cloaking, Meta data and title tags, content scraping, link schemes, Google bombs, keyword stuffing, hidden text and links, doorway and cloaked pages, link farming or schemes, blog comment spam, etc.; or
do anything else which could cause damage to the Site, Flyagainonline employees, Flyagainonline reputation, or would otherwise have a negative impact. Unless otherwise expressly permitted, websites may not hyperlink to any page beyond the homepage of the Site or frame the Site or any web page or material herein, nor may any entity hyperlink any aspect of the Site in an email for commercial purposes without the express written permission of Flyagainonline. By making a reservation on the Site, you represent and warrant that you are not covered by any international sanction program
            </p>


            <h3>Disclaimer
            </h3>
            <p style="text-align: justify">
                The material in this Site could include technical inaccuracies or typographical errors. Flyagainonline may make changes or improvements at any time.
            </p>
            <p style="text-align: justify">
                The materials on this site are provided on an 'As Is' basis, without warranties of any kind either expressed or implied. To the fullest extent permissible pursuant to applicable law, Flyagainonline disclaims all warranties of merchantability and fitness for a particular purpose.
            </p>
            <p style="text-align: justify">
                Flyagainonline does not warrant that the functions contained in this site will be uninterrupted or error free, that defects will be corrected, or that this site or the servers that make it available are free of viruses or other harmful components, but shall endeavour to ensure your fullest satisfaction.
            </p>


            <p style="text-align: justify">
                Flyagainonline does not warrant or make any representations regarding the use of or the result of the use of the material on the site in terms of their correctness, accuracy, reliability, or otherwise, insofar as such material is derived from other service providers such as airlines, hotel owners and tour operator
            </p>
            <p style="text-align: justify">
                You acknowledge that this Website is provided only on the basis set out in these terms and conditions. Your uninterrupted access or use of this Website on this basis may be prevented by certain factors outside our reasonable control including, without limitation, the unavailability, inoperability or interruption of the Internet or other telecommunications services or as a result of any maintenance or other service work carried out on this Website. Flyagainonline does not accept any responsibility and will not be liable for any loss or damage whatsoever arising out of or in connection with any ability/inability to access or to use the Site.
            </p>
            <p style="text-align: justify">
                You also acknowledge that through this Site, Flyagainonline merely provides intermediary services in order to facilitate highest quality services to you. Flyagainonline is not the last-mile service provider to you and therefore, Flyagainonline shall not be or deemed to be responsible for any lack or deficiency of services provided by any person (airline, travel/tour operator, hotel, facility or similar agency) you shall engage or hire or appoint pursuant to or resulting from, the material available in this Site.
            </p>
            <p style="text-align: justify">
                Flyagainonline will not be liable to you or to any other person for any direct, indirect, incidental, punitive or consequential loss, damage, cost or expense of any kind whatsoever and howsoever caused from out of your usage of this Site.
            </p>


            <p style="text-align: justify">
                Notwithstanding anything else to the contrary contained elsewhere herein or otherwise at law, Flyagainonline liability (whether by way of indemnification to you or otherwise) shall be restricted to a maximum of INR 1000 (Indian Rupees One Thousand only).
            </p>

            <h3>Terms and Conditions of Booking</h3>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Rates and Price Variations:</span> We reserve the right to vary prices and rates in the event of changes in exchange rates or price rises made by wholesalers or other suppliers. If the cost of any service increases due to exchange rate fluctuations, price increases, tax changes or any other reason, you are required to pay the increase when notified by us or you may cancel the booking which may result in cancellation fees. We are not liable in any way if any increase occurs. Rates quoted are appropriate to the particular product at the time of quoting and these rates may change prior to the travel date. All prices are subject to availability and can be withdrawn or varied without notice.
            </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Final Payment:</span>Final payment must be paid immediately when requested prior to travel date. No vouchers will be issued until final payment is received in our office. Final payment conditions for certain air, Car, Hotel and tour products may vary, and these are shown on the individual pages. We will advise you in writing of these conditions at the time of booking. Please note the final payment may vary from the original booking price or quote if the product is subject to exchange rate fluctuations or price rises by wholesalers or other suppliers.
            </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Card Fees: </span>Please note that a card fee will be applied automatically to credit card payment amounts: This fee covers a range of costs associated with processing bookings paid for by credit card including the merchant fees of the various credit card companies, payment processing costs, administration costs, the cost of maintaining IT systems used for payment security to minimise credit card fraud, credit card chargebacks and associated fees.
            </p>
            <h5>Payments made by cheque or direct deposit do not attract any fees. 
            </h5>

            <h3>ADM POLICY: </h3>
            <p style="text-align: justify">
                Any ADM arising due to the below mentioned reasons will have to be borne by the Travel Partner 
            </p>
            <p style="text-align: justify">GDS Misuse </p>
            <p style="text-align: justify">
                Fictitious Names or Fake Name like Test Names / Wrong Names 
            </p>
            <p style="text-align: justify">
                Duplicate Bookings 
            </p>
            <p style="text-align: justify">
                Churning for same segment / Flight / Date 
            </p>
            <p style="text-align: justify">
                Hold Bookings must be released or issued before Time Limit to avoid Agent Debit Memo (ADM) 
            </p>
            <p style="text-align: justify">
                In Case the Booking is aborted at the time of Hold or Booking - Please don’t issue tickets multiple times, Please contact our call centre for further action - ADM's will be borne by the agent if not guided by this policy 
            </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Payment Processing Terms & Conditions: </span>By providing your credit card details and accepting our Terms & Conditions, you authorise Flyagainonline to arrange for funds to be debited from your nominated credit card, in accordance with the terms & conditions of the Direct Debit Request Service Agreement as amended from time to time.
            </p>
            <p style="text-align: justify">
                Your bank or credit card provider may apply currency conversion fees. Credit Cards are required to secure bookings if you are travelling within 14 days.
            </p>


            <h3>Standard Cancellation Policy: </h3>
            <ul>
                <li>Cancellation within 12 Hours of Scheduled Departure Time must be cancelled with the Airline directly, Flyagainonline will not be liable in any way if the passenger is No Show. 
                </li>
                <li>All Bookings cancelled may attract charges levied by airlines.
                </li>

            </ul>
            <p style="text-align: justify">
                Cancellations must be in the form of Amendment on the portal.
            </p>
            <p style="text-align: justify">
                If a credit has been approved it is valid for 6 months from the date the cancellation was made.
            </p>



            <h3>Flight Cancellation Policy:
            </h3>

            <ul>
                <li>Flights booked on this website are governed by the terms and conditions of the airfare you purchased and are determined by the Airline (not Flyagainonline).</li>
                <li>In most cases, airfares are fully non-refundable and non-transferable.
                </li>
                <li>Airline charges or part or full cancellation fees may apply to your particular airfare.
                </li>
                <li>A Travel Consultant will help you wherever possible within these terms and conditions.
                </li>
            </ul>



            <p style="text-align: justify">
                <span style="font-weight: 600;">Special Cancellation Conditions:</span> Certain air, car, hotel and tour products will apply additional cancellation charges. These cancellation conditions and costs are located under the pricing on the individual pages and will be clearly advised to you in writing at time of booking.
            </p>

            <p style="text-align: justify">
                <span style="font-weight: 600;">Amendment Fees:</span>Amendment Fees: Any amendments made to confirmed bookings will incur a fee; the fees are charged per amendment. This is in addition to any fees that may be levied by the supplier or airline.
            </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Credit Card Chargeback Fees:</span> Any fees charged to Flyagainonline by our credit card payment provider arising from a chargeback or a disputed charge on the cardholder's credit card will be charged to the cardholder. This fee is non-refundable.
            </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Change of Itinerary After booking Has Commenced:</span> Any alteration or cancellation of services after your booking has commenced can incur penalties. There is no refund for unused services.
            </p>
            <p style="text-align: justify"><span style="font-weight: 600;">Refunds:</span> All refund requests must be in writing, and made direct to us or through the Agent from whom the travel arrangements were purchased. Claims must be made prior to departure time. Refunds will not be made for bookings cancelled due to inclement weather or illness. These must be claimed through your travel Insurance. No refunds will be made for services once travel arrangements have commenced. No guarantee is provided or warranted that any refund will be available. if payment done twice for one transaction, the one transaction amount will be refunded as Deposit in Flyagainonline Account within 07 to 10 working days </p>
            <p style="text-align: justify">
                <span style="font-weight: 600;">Reporting of Incidents:</span>Reporting of Incidents: Any abnormal incidents including injuries, service problems, cancellation of a service or dissatisfaction must be reported to Flyagainonline during the event to allow us an opportunity to rectify the situation or provide assistance.
            </p>


            <h3>Currency Conversion:
            </h3>
            <p style="text-align: justify">Rates may be converted into different currencies for your convenience. Hotel rates provided in a currency other than the currency displayed on the Site are converted to the displayed currency at a rate determined by Flyagainonline, which may include a conversion and/or processing fee for certain currencies. The currency and amount you see displayed on the final booking page is the amount Flyagainonline will charge to your credit card. You may be given the option to pay in your credit card currency. Your bank may impose additional fees on the transaction, over which Flyagainonline has no control. Please note that exchange rates fluctuate daily</p>


        </div>
    </div>

</asp:Content>

