﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using System.Collections;

namespace STD.BAL
{


    /// <summary>
    /// Summary description for FltService
    /// </summary>
    public class FltService : IFlt
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchParam"></param>
        /// <returns></returns>
        public ArrayList FltSearchResult(FlightSearch searchParam)
        {
            Process src = new Process();
            src.connectionString = connectionString;
            ArrayList result = src.GetFltSearchResponse(searchParam);
            return result;
        }
        //public ArrayList FltSearchResultCoupon(FlightSearch searchParam)
        //{
        //    Process src = new Process();
        //    src.connectionString = connectionString;
        //    ArrayList result = src.GetFltSearchResponseCoupon(searchParam);
        //    return result;
        //}
    }
}