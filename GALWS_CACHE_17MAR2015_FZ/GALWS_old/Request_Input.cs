﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Request_Input
/// </summary>
public class Request_Input
{
    public Request_Input()
    {

    }
    public string Version { get; set; }
    public int NoOfStop { get; set; }
    public string TripType { get; set; }
    public string TimeStamp { get; set; }
    public string EchoToken { get; set; }
    
    
    public string[] DepartureDateTime { get; set; }
    public string[] ArrivalDateTime { get; set; }
    public string[] OriginLocation { get; set; }
    public string[] DestinationLocation { get; set; }
    public string[] RPH { get; set; }
    public string[] FlightNumber { get; set; }


    public string GivenName { get; set; }
    public string Surname { get; set; }
    public string NameTitle { get; set; }
    public string AreaCityCode { get; set; }
    public string CountryAccessCode { get; set; }
    public string PhoneNumber { get; set; }
    public string CountryNameCode { get; set; }
    public string DocHolderNationality { get; set; }
   
    public string BirthDate { get; set; }
 
    public string TRAVELREFERNCENO { get; set; }
    public string TransactionIdentifier { get; set; }
    public string journeyDuration { get; set; }
    public string dTotalNo_Adt { get; set; }
    public string dTotalNo_CHD { get; set; }
    public string dTotalNo_INF { get; set; }

    public string UID { get; set; }
    public string DistId { get; set; }
    public string UserType { get; set; }


    public string CorporateId { get; set; }
    public string Password { get; set; }
    public string ServerUrl { get; set; }
    public string JSessionId { get; set; }



}
