﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using STD.DAL;
using STD.Shared;
using System.Configuration;

namespace STD.BAL.TBO
{
    public class TBOFareRule
    {
        public TBOFareRule()
        {

        }


        public FareRuleResponse GetFareRule(string constr, string sno)
        {
            FareRuleResponse resp = new FareRuleResponse();

            TBOFareRuleRequest request = new TBOFareRuleRequest();
            constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            Credentials objCrd = new Credentials(constr);
            TBOAuthProcess objTbProcess = new TBOAuthProcess();
             string strJsonResponse="";
            List<CredentialList> CrdList = objCrd.GetServiceCredentials("");
            string url = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].AvailabilityURL;
            string userid = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].UserID;
            string Pwd = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].Password;
            string OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
            string ip = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].ServerIP;

            string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, OrgCode, userid, Pwd, ip);

            string[] alist = sno.Split(':');

            if (alist.Length>2)
            {
                request.ResultIndex = alist[0].Trim();
                request.TraceId = alist[1].Trim();
                request.TokenId = strTokenID;
                request.EndUserIp = ip;
            string strJsonData = JsonConvert.SerializeObject(request);
             strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.FareRuleUrl, strJsonData);
             resp = JsonConvert.DeserializeObject<FareRuleResponse>(strJsonResponse);

            }

            return resp;

        }
    }


    public class TBOFareRuleRequest
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string ResultIndex { get; set; }
    }


    public class Error
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FareRule1
    {
        public string Airline { get; set; }
        public string Destination { get; set; }
        public string FareBasisCode { get; set; }
        public object FareRestriction { get; set; }
        public string FareRuleDetail { get; set; }
        public string Origin { get; set; }
    }

    public class ResponseFareRule
    {
        public Error Error { get; set; }
        public List<FareRule1> FareRules { get; set; }
        public int ResponseStatus { get; set; }
        public string TraceId { get; set; }
    }

    public class FareRuleResponse
    {
        public ResponseFareRule Response { get; set; }
    }
}
