﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Security.Cryptography;

namespace STD.BAL
{
    public class RandomKeyGenerator
    {
        private const int LEN = 1;

        public string Generate()
        {
            string strRndKey = "";
            strRndKey = UsingGuid() + RNGCharacterMask();
            // & "-" & UsingTicks() & "-" & UsingDateTime()
            return strRndKey;
        }

        private string UsingGuid()
        {
            string result = Guid.NewGuid().ToString().GetHashCode().ToString("x");
            return result;
        }

        private string UsingTicks()
        {
            string val = DateTime.Now.Ticks.ToString("x");
            return val;
        }

        private string RNGCharacterMask()
        {
            int maxSize = 8;
            char[] chars = new char[62];
            string a = null;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        private string UsingDateTime()
        {
            return DateTime.Now.ToString().GetHashCode().ToString("x");
        }

        private string GetRandomNumber(string allowedChars1)
        {

            //char[] sep = { ',' };
            string sep = ",";

            string[] arr = Utility.Split(allowedChars1,sep);

            string rndString = "";

            int temp = 0;

            Random rand = new Random();

            for (int i = 0; i <= 4; i++)
            {
                temp = rand.Next(0, arr.Length);
                rndString += temp.ToString();
            }

            return rndString;

        }

        public string GetRndm()
        {

            string allowedChars = "";

            allowedChars = "1,2,3,4,5,6,7,8,9,0";

            string rnmd2 = GetRandomNumber(allowedChars);

            return rnmd2;

        }

   

    }
}