﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using STD.Shared;
using STD.BAL;

namespace AirArabia
{
    public class GetFinalResult
    {
        SqlConnection Conn;
        bool IsCorpp;
        string AgType;
        float srvCharge;
        public GetFinalResult()
        {
            Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

        }
        public GetFinalResult(bool IsCorp, string AgentType, float srvCharge1)
        {
            Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            IsCorpp = IsCorp;
            AgType = AgentType;
            srvCharge = srvCharge1;
        }
        FlightCommonBAL objFltComm = new FlightCommonBAL(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<FlightSearchResults> ObjFinalResult = new List<FlightSearchResults>();
        Request_Input ObjReqInput = new Request_Input();
        List<PriceQuoteRequest> PriceQuoteRequest;
        string ArrivalDate = "";
        string ArrivalTime = "";
        string DepartureDate = "";
        string DepartureTime = "";
        string DepartureAirport = "";
        string ArrivalAirport = "";
        string Sector = "";
        string FareBasisCode = "";

        decimal AdtTax = 0;

        string RPH = "";
        string FlightNo = "";

        decimal ChdTax = 0;

        decimal InfTax = 0;
        int LineNo = 0;
        int AdultCount = 0;
        int ChildCount = 0;
        int InfantCount = 0;
        int LineNumber = 0;
        int LineNumber1 = 0;
        int LineCount = 0;
        string OrgDestFrom = "";
        string OrgDestTo = "";



        DataTable dtFinalTable = new DataTable();
        FltSearch ObjFltSearch = new FltSearch();



        public List<FlightSearchResults> GetResponse(string ServerUrlorIP, string UserId, string Password, string Origin, string Dest, string DepDate, string RetDate, string Adt, string Chd, string Inf, string TripType, string UID, string DistId)
        {
            //goto jump;
            RequestResponse objsAirArabia = new RequestResponse();
            ObjFltSearch.OriginLocation = Origin;
            ObjFltSearch.DestinationLocation = Dest;
            ObjFltSearch.DepartureDateTime = DepDate + "T00:00:00";  //"2013-01-25T00:00:00";
            ObjFltSearch.ArrivalDateTime = RetDate + "T00:00:00";
            ObjFltSearch.dTotalNo_Adt = Adt;
            ObjFltSearch.dTotalNo_CHD = Chd;
            ObjFltSearch.dTotalNo_INF = Inf;
            ObjFltSearch.TripType = TripType;
            ObjFltSearch.ServerUrlorIp = ServerUrlorIP;
            ObjFltSearch.UserId = UserId;
            ObjFltSearch.Password = Password;

            string FltSearchResponse = objsAirArabia.getAvailabilty(ObjFltSearch);

            if (FltSearchResponse.IndexOf("No availability") > 0 || FltSearchResponse.IndexOf("Invalid") > 0 || FltSearchResponse == "" || FltSearchResponse == null)
            {

            }
            else
            {
                try
                {
                    string[] res=Utility.Split(FltSearchResponse,"~");
                    ObjReqInput.JSessionId = res[0].ToString();
                    XNamespace ns = "http://www.opentravel.org/OTA/2003/05";
                    XDocument xdoc = XDocument.Parse(res[1].ToString());

                    var TranInformation = from TI in xdoc.Descendants(ns + "OTA_AirAvailRS")
                                          select new
                                          {
                                              EchoToken = TI.Attribute("EchoToken").Value,
                                              TransactionIdentifier = TI.Attribute("TransactionIdentifier").Value,
                                              Version = TI.Attribute("Version").Value

                                          };


                    foreach (var ObjTransInform in TranInformation)
                    {
                        ObjReqInput.Version = ObjTransInform.Version;
                        ObjReqInput.EchoToken = ObjTransInform.EchoToken;
                        ObjReqInput.TransactionIdentifier = ObjTransInform.TransactionIdentifier;
                    }


                    var FlightSegment = from OD in xdoc.Descendants(ns + "OTA_AirAvailRS").Descendants(ns + "OriginDestinationInformation").Descendants(ns + "OriginDestinationOptions").Descendants(ns + "OriginDestinationOption")
                                        select new
                                        {
                                            ArrivalDateTime = OD.Element(ns + "FlightSegment").Attribute("ArrivalDateTime").Value,
                                            DepartureDateTime = OD.Element(ns + "FlightSegment").Attribute("DepartureDateTime").Value,
                                            FlightNumber = OD.Element(ns + "FlightSegment").Attribute("FlightNumber").Value,
                                            JourneyDuration = OD.Element(ns + "FlightSegment").Attribute("JourneyDuration").Value,
                                            RPH = OD.Element(ns + "FlightSegment").Attribute("RPH").Value,
                                            DepartureAirport = OD.Element(ns + "FlightSegment").Element(ns + "DepartureAirport").Attribute("LocationCode").Value,
                                            ArrivalAirport = OD.Element(ns + "FlightSegment").Element(ns + "ArrivalAirport").Attribute("LocationCode").Value

                                        };


                    #region[SET & GET LINE NO]

                    // --------------- GET LINE NUMBER START --------------------//


                    int k = 0;

                    int z = 1;
                    List<string> ResultStop = new List<string>();

                    List<string> FinalList = new List<string>();

                    // --------------- SET LINE NUMBER FOR ONE WAY START --------------------//

                    if (ObjFltSearch.TripType == "OneWay")
                    {
                        foreach (var ObjFlightSegment in FlightSegment)
                        {
                            if (ObjFlightSegment.ArrivalAirport == ObjFltSearch.DestinationLocation)
                            {
                                ResultStop.Add(FlightSegment.ElementAt(k).ToString().Replace('}', ',') + " " + "LineNo = " + z + " " + " }");
                                k = k + 1;
                                FinalList.AddRange(ResultStop);
                                ResultStop.Clear();
                                z = z + 1;

                            }
                            else
                            {
                                ResultStop.Add(FlightSegment.ElementAt(k).ToString().Replace('}', ',') + " " + "LineNo = " + z + " }");
                                k = k + 1;

                            }

                        }
                    }
                    // --------------- SET LINE NUMBER FOR ONE WAY END --------------------//

                    // --------------- SET LINE NUMBER FOR RETURN START --------------------//

                    if (ObjFltSearch.TripType == "Return")
                    {
                        foreach (var ObjFlightSegment in FlightSegment)
                        {
                            if (ObjFlightSegment.ArrivalAirport == ObjFltSearch.OriginLocation)
                            {
                                ResultStop.Add(FlightSegment.ElementAt(k).ToString().Replace('}', ',') + " " + "LineNo = " + z + " " + " }");
                                k = k + 1;
                                FinalList.AddRange(ResultStop);
                                ResultStop.Clear();
                                z = z + 1;

                            }
                            else
                            {
                                ResultStop.Add(FlightSegment.ElementAt(k).ToString().Replace('}', ',') + " " + "LineNo = " + z + " }");
                                k = k + 1;

                            }

                        }
                    }

                    // --------------- SET LINE NUMBER FOR RETURN END --------------------//

                    PriceQuoteRequest = new List<PriceQuoteRequest>();



                    for (int j = 0; j < FinalList.Count; j++)
                    {
                        string[] ArrayFinalList1 = FinalList[j].Split(',');

                        for (int l = 0; l <= ArrayFinalList1.Length - 1; l++)
                        {

                            string[] ArrayFinalList2 = ArrayFinalList1[l].Split('=');

                            for (int m = 0; m < ArrayFinalList2.Length - 1; m++)
                            {
                                if (ArrayFinalList2[m].Trim().Contains("ArrivalDateTime"))
                                {
                                    ArrivalDate = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("DepartureDateTime"))
                                {
                                    DepartureDate = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("FlightNumber"))
                                {
                                    FlightNo = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("RPH"))
                                {
                                    RPH = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("DepartureAirport"))
                                {
                                    DepartureAirport = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("ArrivalAirport"))
                                {
                                    ArrivalAirport = ArrayFinalList2[ArrayFinalList2.Length - 1];

                                }
                                else if (ArrayFinalList2[m].Trim().Contains("LineNo"))
                                {
                                    LineNo = Convert.ToInt32(ArrayFinalList2[ArrayFinalList2.Length - 1].Replace("}", "").Trim());

                                }

                            }

                        }
                        PriceQuoteRequest.Add(new PriceQuoteRequest { ArrivalDate = ArrivalDate, DepartureDate = DepartureDate, RPH = RPH, FlightNumber = FlightNo, ArrAirportCode = ArrivalAirport, DepAirportCode = DepartureAirport, LineNo = LineNo });

                    }


                    // --------------- GET LINE NUMBER END -------------------- //

                    #endregion

                    #region[SET INPUT FOR PRICEQUOTE]

                    // --------------- HERE INPUTS SEND FOR PRICEQUOTE START -------------------- //





                    int p = 0;
                    LineCount = PriceQuoteRequest.ElementAt(PriceQuoteRequest.Count - 1).LineNo;
                    DataTable dtAgentMarkup = new DataTable();
                    dtAgentMarkup = GetMarkUp(UID, DistId, "I", "TA");

                    DataTable dtAdminMarkup = new DataTable();
                    dtAdminMarkup = GetMarkUp(UID, DistId, "I", "AD");

                    for (int h = 1; h <= LineCount; h++)
                    {

                        var GetPriceInput = from ObjFR in PriceQuoteRequest
                                            where ObjFR.LineNo == h
                                            select new
                                            {
                                                ObjFR.DepAirportCode,
                                                ObjFR.ArrAirportCode,
                                                ObjFR.DepartureDate,
                                                ObjFR.ArrivalDate,
                                                ObjFR.FlightNumber,
                                                ObjFR.RPH,
                                                ObjFR.LineNo

                                            };

                        Request_Input ObjReqInput1 = new Request_Input();
                        int n = 0;
                        ObjReqInput1.ArrivalDateTime = new string[PriceQuoteRequest.Count];
                        ObjReqInput1.DepartureDateTime = new string[PriceQuoteRequest.Count];
                        ObjReqInput1.OriginLocation = new string[PriceQuoteRequest.Count];
                        ObjReqInput1.DestinationLocation = new string[PriceQuoteRequest.Count];
                        ObjReqInput1.FlightNumber = new string[PriceQuoteRequest.Count];
                        ObjReqInput1.RPH = new string[PriceQuoteRequest.Count];

                        foreach (var ObjGetPriceInput in GetPriceInput)
                        {

                            for (; p < GetPriceInput.Count() - (GetPriceInput.Count() - 1); p++)
                            {

                                ObjReqInput1.ArrivalDateTime[n] = ObjGetPriceInput.ArrivalDate.Trim();
                                ObjReqInput1.DepartureDateTime[n] = ObjGetPriceInput.DepartureDate.Trim();
                                ObjReqInput1.OriginLocation[n] = ObjGetPriceInput.DepAirportCode.Trim();
                                ObjReqInput1.DestinationLocation[n] = ObjGetPriceInput.ArrAirportCode.Trim();
                                ObjReqInput1.FlightNumber[n] = ObjGetPriceInput.FlightNumber.Trim();
                                ObjReqInput1.RPH[n] = ObjGetPriceInput.RPH.Trim();
                                LineNumber = ObjGetPriceInput.LineNo;
                                if (GetPriceInput.Count() > 1)
                                {
                                    n++;

                                }

                            }
                            p = 0;
                        }
                        ObjReqInput1.Version = ObjReqInput.Version;
                        ObjReqInput1.EchoToken = ObjReqInput.EchoToken;
                        ObjReqInput1.TransactionIdentifier = ObjReqInput.TransactionIdentifier;
                        ObjReqInput1.dTotalNo_Adt = ObjFltSearch.dTotalNo_Adt;
                        ObjReqInput1.dTotalNo_CHD = ObjFltSearch.dTotalNo_CHD;
                        ObjReqInput1.dTotalNo_INF = ObjFltSearch.dTotalNo_INF;
                        ObjReqInput1.TripType = ObjFltSearch.TripType;

                        ObjReqInput1.NoOfStop = GetPriceInput.Count() - 1;
                        ObjReqInput1.UID = UID;
                        ObjReqInput1.DistId = DistId;
                        ObjReqInput1.UserType = "TA";

                        ObjReqInput1.CorporateId = UserId;
                        ObjReqInput1.Password = Password;
                        ObjReqInput1.ServerUrl = ServerUrlorIP;
                        ObjReqInput1.JSessionId = ObjReqInput.JSessionId;
                        GetPriceQuote(ObjReqInput1, dtAgentMarkup, dtAdminMarkup);


                    }


                    // --------------- HERE INPUTS SEND FOR PRICEQUOTE END -------------------- //
                    #endregion


                    //if (LineNumber == LineCount)
                    //{

                    //    dtFinalTable = MyRecordListToDataTable(ObjFinalResult);


                    //}



                }
                catch (Exception ex)
                {
                    // throw ex;
                }
            }
            //jump:
            return ObjFinalResult;

        }


        public List<FlightSearchResults> GetPriceQuote(Request_Input Obj, DataTable dtAgentMarkup, DataTable dtAdminMarkup)
        {
            RequestResponse objsAirArabia = new RequestResponse();

            XNamespace Fns = "http://www.opentravel.org/OTA/2003/05";
            string TripType = "";
            string Stops = "";
            string SearchValue = "";
            int stop1 = 0;
            int stop2 = 0;
            int Flight = 0;
            int k = 0;
            int Leg = 0;
            string Trip = "";


            string FullDepDateTime = "";
            string FullArrDateTime = "";
            string TransactionIdentifier = "";
            string FlightIdentification = "";
            string Departure_Date = "";
            string Arrival_Date = "";
            string ArrivalCityName = "";
            string DepartureCityName = "";

            string PriceQuote = objsAirArabia.PriceQuote(Obj);
            if (PriceQuote.IndexOf("No availability") > 0 || PriceQuote.IndexOf("Invalid") > 0 || PriceQuote.IndexOf("[No seats/fares available]") > 0 || PriceQuote == "" || PriceQuote == null)
            {
                LineNumber1 += 1;
            }
            else
            {

                XDocument XPriceQuoteDoc = XDocument.Parse(PriceQuote);


                var TranInf = from TI in XPriceQuoteDoc.Descendants(Fns + "OTA_AirPriceRS")
                              select new
                              {
                                  EchoToken = TI.Attribute("EchoToken").Value,
                                  TransactionIdentifier = TI.Attribute("TransactionIdentifier").Value,
                                  Version = TI.Attribute("Version").Value

                              };

                TransactionIdentifier = TranInf.ElementAt(0).TransactionIdentifier;

                // ---------------------------------- FLIGHT SEGMENT START-----------------------------------------------//

                var FlightSegment = from OD in XPriceQuoteDoc.Descendants(Fns + "AirItinerary").Descendants(Fns + "OriginDestinationOptions").Descendants(Fns + "OriginDestinationOption").Descendants(Fns + "FlightSegment")
                                    select new
                                    {
                                        ArrivalDateTime = OD.Attribute("ArrivalDateTime").Value,
                                        DepartureDateTime = OD.Attribute("DepartureDateTime").Value,
                                        FlightNumber = OD.Attribute("FlightNumber").Value,
                                        RPH = OD.Attribute("RPH").Value,
                                        DepartureAirport = OD.Element(Fns + "DepartureAirport").Attribute("LocationCode").Value,
                                        ArrivalAirport = OD.Element(Fns + "ArrivalAirport").Attribute("LocationCode").Value

                                    };


                // ---------------------------------- FLIGHT SEGMENT END-----------------------------------------------//

                #region[Total Fare Section]

                // ---------------------------------- TOTAL FARE START-----------------------------------------------//

                decimal BaseFare = 0;
                decimal TotFare = 0;
                decimal TotalFareWithCcFee = 0;

                var TotalFare = from TF in XPriceQuoteDoc.Descendants(Fns + "AirItineraryPricingInfo")
                                select new
                                {
                                    BaseFare = TF.Element(Fns + "ItinTotalFare").Element(Fns + "BaseFare").Attribute("Amount").Value,// + " " + TF.Element(Fns + "ItinTotalFare").Element(Fns + "BaseFare").Attribute("CurrencyCode").Value,

                                    TotalFare = TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalFare").Attribute("Amount").Value,// + " " + TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalFare").Attribute("CurrencyCode").Value,

                                    TotalEquivFare = TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalEquivFare").Attribute("Amount").Value,// + " " + TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalEquivFare").Attribute("CurrencyCode").Value,

                                    TotalFareWithCCFee = TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalFareWithCCFee").Attribute("Amount").Value,// + " " + TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalFareWithCCFee").Attribute("CurrencyCode").Value,

                                    TotalEquivFareWithCCFee = TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalEquivFareWithCCFee").Attribute("Amount").Value// + " " + TF.Element(Fns + "ItinTotalFare").Element(Fns + "TotalEquivFareWithCCFee").Attribute("CurrencyCode").Value


                                };

                foreach (var ObjTotalFare in TotalFare)
                {
                    BaseFare = Convert.ToDecimal(ObjTotalFare.BaseFare);// Math.Round();
                    TotFare = Convert.ToDecimal(ObjTotalFare.TotalFare);// Math.Round();
                    TotalFareWithCcFee = Math.Round(Convert.ToDecimal(ObjTotalFare.TotalFareWithCCFee));

                }


                // ---------------------------------- TOTAL FARE END -----------------------------------------------//

                #endregion

                #region[Adult Fare Types]

                decimal AdtBaseFare = 0;
                decimal AdtTotalFare = 0;
                string AdtRefNumber = "";

                decimal A_YQ = 0;
                decimal A_OtherTax = 0;

                #endregion

                #region[Child Fare Types]

                decimal ChdBaseFare = 0;
                decimal ChdTotalFare = 0;
                string ChdRefNumber = "";

                decimal C_YQ = 0;

                decimal C_OtherTax = 0;


                #endregion

                #region[Infant Fare Types]

                decimal InfBaseFare = 0;
                decimal InfTotalFare = 0;
                string InfRefNumber = "";
                decimal I_YQ = 0;

                decimal I_OtherTax = 0;



                #endregion


                // ---------------------------------- FARE BREAKUP START -----------------------------------------------//

                var FareBreakUp = from AFB in XPriceQuoteDoc.Descendants(Fns + "PTC_FareBreakdowns")
                                  select new
                                  {
                                      FareBasis = AFB.Element(Fns + "PTC_FareBreakdown").Element(Fns + "FareBasisCodes").Element(Fns + "FareBasisCode").Value,
                                      adtFare = AFB.Elements(Fns + "PTC_FareBreakdown").Where(x => x.Element(Fns + "PassengerTypeQuantity").Attribute("Code").Value == "ADT"),
                                      chdFare = AFB.Elements(Fns + "PTC_FareBreakdown").Where(x => x.Element(Fns + "PassengerTypeQuantity").Attribute("Code").Value == "CHD"),
                                      infFare = AFB.Elements(Fns + "PTC_FareBreakdown").Where(x => x.Element(Fns + "PassengerTypeQuantity").Attribute("Code").Value == "INF")
                                  };


                foreach (var obj in FareBreakUp)
                {
                    #region[Adult Fare BreakUp]

                    // ---------------------------------- ADULT FARE BREAKUP START -----------------------------------------------//
                    if (obj.adtFare.Count() > 0)
                    {
                        XDocument AdultXDoc = XDocument.Parse(obj.adtFare.ElementAt(0).ToString());
                        AdultCount = Convert.ToInt32(AdultXDoc.Element(Fns + "PTC_FareBreakdown").Element(Fns + "PassengerTypeQuantity").Attribute("Quantity").Value);
                        FareBasisCode = AdultXDoc.Element(Fns + "PTC_FareBreakdown").Element(Fns + "FareBasisCodes").Value;
                        var AdultFare = from item in AdultXDoc.Descendants(Fns + "PTC_FareBreakdown")
                                        select new
                                        {
                                            BaseFare = item.Element(Fns + "PassengerFare").Element(Fns + "BaseFare").Attribute("Amount").Value,

                                            Taxes = from Tax in item.Element(Fns + "PassengerFare").Element(Fns + "Taxes").Elements(Fns + "Tax")
                                                    select (string)Tax.Attribute("TaxCode") + "-" + (string)Tax.Attribute("Amount"),

                                            Fees = from Fee in item.Element(Fns + "PassengerFare").Element(Fns + "Fees").Elements(Fns + "Fee")
                                                   select (string)Fee.Attribute("FeeCode").Value.Substring(0, 2) + "-" + (string)Fee.Attribute("Amount"),

                                            TotalFare = item.Element(Fns + "PassengerFare").Element(Fns + "TotalFare").Attribute("Amount").Value,

                                            TravelerRefNumber = item.Element(Fns + "TravelerRefNumber").Attribute("RPH").Value
                                        };

                        foreach (var ObjAdultFare in AdultFare)
                        {
                            AdtBaseFare = Math.Round(Convert.ToDecimal(ObjAdultFare.BaseFare));
                            AdtTotalFare = Math.Round(Convert.ToDecimal(ObjAdultFare.TotalFare));
                            AdtRefNumber = ObjAdultFare.TravelerRefNumber;

                        }

                        // ------------ ADULT TAXES & FEES ------------ //


                        for (int o = 0; o < AdultFare.ElementAt(k).Taxes.Count(); o++)
                        {

                            A_OtherTax = A_OtherTax + Convert.ToDecimal(AdultFare.ElementAt(k).Taxes.ElementAt(o).ToString().Split('-').ElementAt(1).ToString());

                        }
                        for (int p = 0; p < AdultFare.ElementAt(k).Fees.Count(); p++)
                        {
                            if (AdultFare.ElementAt(k).Fees.ElementAt(p).ToString().Substring(0, 2).ToString() == "YQ")
                            {
                                A_YQ += Convert.ToDecimal(AdultFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());

                            }
                            else
                            {
                                A_OtherTax = A_OtherTax + Convert.ToDecimal(AdultFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());
                            }
                        }

                        A_YQ = Math.Round(A_YQ, 0);
                        A_OtherTax = Math.Round(A_OtherTax, 0);
                        AdtTax = A_OtherTax + A_YQ;
                    }



                    // ---------------------------------- ADULT FARE BREAKUP END -----------------------------------------------//
                    #endregion

                    #region[Child Fare BreakUp]

                    // ---------------------------------- CHILD FARE BREAKUP START -----------------------------------------------//

                    if (obj.chdFare.Count() > 0)
                    {
                        XDocument ChildXDoc = XDocument.Parse(obj.chdFare.ElementAt(0).ToString());
                        ChildCount = Convert.ToInt32(ChildXDoc.Element(Fns + "PTC_FareBreakdown").Element(Fns + "PassengerTypeQuantity").Attribute("Quantity").Value);
                        var ChildFare = from item in ChildXDoc.Descendants(Fns + "PTC_FareBreakdown")
                                        select new
                                        {
                                            BaseFare = item.Element(Fns + "PassengerFare").Element(Fns + "BaseFare").Attribute("Amount").Value,

                                            Taxes = from Tax in item.Element(Fns + "PassengerFare").Element(Fns + "Taxes").Elements(Fns + "Tax")
                                                    select (string)Tax.Attribute("TaxCode") + "-" + (string)Tax.Attribute("Amount"),

                                            Fees = from Fee in item.Element(Fns + "PassengerFare").Element(Fns + "Fees").Elements(Fns + "Fee")
                                                   select (string)Fee.Attribute("FeeCode").Value.Substring(0, 2) + "-" + (string)Fee.Attribute("Amount"),

                                            TotalFare = item.Element(Fns + "PassengerFare").Element(Fns + "TotalFare").Attribute("Amount").Value,

                                            TravelerRefNumber = item.Element(Fns + "TravelerRefNumber").Attribute("RPH").Value

                                        };


                        foreach (var ObjChildFare in ChildFare)
                        {
                            ChdBaseFare = Math.Round(Convert.ToDecimal(ObjChildFare.BaseFare));
                            ChdTotalFare = Math.Round(Convert.ToDecimal(ObjChildFare.TotalFare));
                            ChdRefNumber = ObjChildFare.TravelerRefNumber;

                        }

                        // ------------ CHILD TAXES & FEES ------------ //

                        for (int o = 0; o < ChildFare.ElementAt(k).Taxes.Count(); o++)
                        {

                            C_OtherTax = C_OtherTax + Convert.ToDecimal(ChildFare.ElementAt(k).Taxes.ElementAt(o).ToString().Split('-').ElementAt(1).ToString());

                        }
                        for (int p = 0; p < ChildFare.ElementAt(k).Fees.Count(); p++)
                        {
                            if (ChildFare.ElementAt(k).Fees.ElementAt(p).ToString().Substring(0, 2).ToString() == "YQ")
                            {
                                C_YQ += Convert.ToDecimal(ChildFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());

                            }
                            else
                            {
                                C_OtherTax = C_OtherTax + Convert.ToDecimal(ChildFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());
                            }
                        }
                        C_YQ = Math.Round(C_YQ, 0);
                        C_OtherTax = Math.Round(C_OtherTax, 0);
                        ChdTax = C_OtherTax + C_YQ;


                    }

                    // ---------------------------------- CHILD FARE BREAKUP END -----------------------------------------------//

                    #endregion

                    #region[Infant Fare BreakUp]


                    // ---------------------------------- INFANT FARE BREAKUP START -----------------------------------------------//


                    if (obj.infFare.Count() > 0)
                    {
                        XDocument InfantXDoc = XDocument.Parse(obj.infFare.ElementAt(0).ToString());
                        InfantCount = Convert.ToInt32(InfantXDoc.Element(Fns + "PTC_FareBreakdown").Element(Fns + "PassengerTypeQuantity").Attribute("Quantity").Value);
                        var InfantFare = from item in InfantXDoc.Descendants(Fns + "PTC_FareBreakdown")
                                         select new
                                         {
                                             BaseFare = item.Element(Fns + "PassengerFare").Element(Fns + "BaseFare").Attribute("Amount").Value,

                                             Taxes = from Tax in item.Element(Fns + "PassengerFare").Element(Fns + "Taxes").Elements(Fns + "Tax")
                                                     select (string)Tax.Attribute("TaxCode") + "-" + (string)Tax.Attribute("Amount"),

                                             Fees = from Fee in item.Element(Fns + "PassengerFare").Element(Fns + "Fees").Elements(Fns + "Fee")
                                                    select (string)Fee.Attribute("FeeCode").Value.Substring(0, 2) + "-" + (string)Fee.Attribute("Amount"),

                                             TotalFare = item.Element(Fns + "PassengerFare").Element(Fns + "TotalFare").Attribute("Amount").Value,

                                             TravelerRefNumber = item.Element(Fns + "TravelerRefNumber").Attribute("RPH").Value
                                         };

                        foreach (var ObjInfantFare in InfantFare)
                        {
                            InfBaseFare = Math.Round(Convert.ToDecimal(ObjInfantFare.BaseFare));
                            InfTotalFare = Math.Round(Convert.ToDecimal(ObjInfantFare.TotalFare));
                            InfRefNumber = ObjInfantFare.TravelerRefNumber;

                        }

                        // ------------ INFANT TAXES & FEES ------------ //

                        for (int o = 0; o < InfantFare.ElementAt(k).Taxes.Count(); o++)
                        {

                            I_OtherTax = I_OtherTax + Convert.ToDecimal(InfantFare.ElementAt(k).Taxes.ElementAt(o).ToString().Split('-').ElementAt(1).ToString());

                        }
                        for (int p = 0; p < InfantFare.ElementAt(k).Fees.Count(); p++)
                        {
                            if (InfantFare.ElementAt(k).Fees.ElementAt(p).ToString().Substring(0, 2).ToString() == "YQ")
                            {
                                I_YQ += Convert.ToDecimal(InfantFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());

                            }
                            else
                            {
                                I_OtherTax = I_OtherTax + Convert.ToDecimal(InfantFare.ElementAt(k).Fees.ElementAt(p).ToString().Split('-').ElementAt(1).ToString());
                            }
                        }
                        I_YQ = Math.Round(I_YQ, 0);
                        I_OtherTax = Math.Round(I_OtherTax, 0);
                        InfTax = I_OtherTax + I_YQ;

                    }

                    // ---------------------------------- INFANT FARE BREAKUP END -----------------------------------------------//

                    #endregion
                }
                // ---------------------------------- FARE BREAKUP END -----------------------------------------------//

                // ---------------------------------- CREATE FINAL LIST START -----------------------------------------------//

                SearchValue = ObjFltSearch.OriginLocation + ObjFltSearch.DestinationLocation + DateTime.Now.ToString();

                int seg = 0;
                foreach (var ObjFlightSegment in FlightSegment)
                {

                    FullArrDateTime = ObjFlightSegment.ArrivalDateTime;
                    FullDepDateTime = ObjFlightSegment.DepartureDateTime;
                    ArrivalDate = ObjFlightSegment.ArrivalDateTime.Substring(0, 10).Split('-').ElementAt(2).ToString() + ObjFlightSegment.ArrivalDateTime.Substring(0, 10).Split('-').ElementAt(1).ToString() + ObjFlightSegment.ArrivalDateTime.Substring(0, 10).Split('-').ElementAt(0).Substring(2, 2).ToString();
                    Arrival_Date = ObjFlightSegment.ArrivalDateTime.Substring(0, 10).Split('-').ElementAt(2).ToString() + " " + datecon(ObjFlightSegment.ArrivalDateTime.Substring(0, 10).Split('-').ElementAt(1).ToString());
                    ArrivalTime = ObjFlightSegment.ArrivalDateTime.Replace(":", "").Substring(ObjFlightSegment.ArrivalDateTime.IndexOf("T") + 1, 4);
                    DepartureDate = ObjFlightSegment.DepartureDateTime.Substring(0, 10).Split('-').ElementAt(2).ToString() + ObjFlightSegment.DepartureDateTime.Substring(0, 10).Split('-').ElementAt(1).ToString() + ObjFlightSegment.DepartureDateTime.Substring(0, 10).Split('-').ElementAt(0).Substring(2, 2).ToString();
                    Departure_Date = ObjFlightSegment.DepartureDateTime.Substring(0, 10).Split('-').ElementAt(2).ToString() + " " + datecon(ObjFlightSegment.DepartureDateTime.Substring(0, 10).Split('-').ElementAt(1).ToString());
                    DepartureTime = ObjFlightSegment.DepartureDateTime.Replace(":", "").Substring(ObjFlightSegment.DepartureDateTime.IndexOf("T") + 1, 4);
                    FlightNo = ObjFlightSegment.FlightNumber;
                    RPH = ObjFlightSegment.RPH;
                    DepartureAirport = ObjFlightSegment.DepartureAirport;
                    ArrivalAirport = ObjFlightSegment.ArrivalAirport;
                    ArrivalCityName = GetCityName(ArrivalAirport);
                    DepartureCityName = GetCityName(DepartureAirport);

                    if (ObjFltSearch.OriginLocation != DepartureAirport && ObjFltSearch.DestinationLocation != ArrivalAirport)
                    {
                        TripType = "R";
                    }

                    else
                    {
                        TripType = "O";

                    }
                    if (ObjFltSearch.TripType == "OneWay")
                    {
                        Stops = Convert.ToInt32(FlightSegment.Count() - 1) + "-Stop";
                        Flight = 1;
                        Leg = Leg + 1;
                        Sector = ObjFltSearch.OriginLocation + ":" + ObjFltSearch.DestinationLocation;
                        OrgDestFrom = ObjFltSearch.OriginLocation;
                        OrgDestTo = ObjFltSearch.DestinationLocation;
                        Trip = "rdbOneWay";


                    }
                    if (ObjFltSearch.TripType == "Return")
                    {
                        Trip = "rdbRoundTrip";
                        if (ObjFltSearch.OriginLocation != DepartureAirport && ObjFltSearch.DestinationLocation != ArrivalAirport)
                        {
                            OrgDestFrom = ObjFltSearch.DestinationLocation;
                            OrgDestTo = ObjFltSearch.OriginLocation;

                        }
                        else
                        {
                            OrgDestFrom = ObjFltSearch.OriginLocation;
                            OrgDestTo = ObjFltSearch.DestinationLocation;



                        }

                        // ------------- Stop Count Start--------------  //
                        int i = 0; int j = 0;
                        while (i < FlightSegment.Count())
                        {
                            if (FlightSegment.ElementAt(i).ArrivalAirport == ObjFltSearch.DestinationLocation)
                            {
                                stop1 = i;
                                Stops = stop1 + "-Stop";
                                break;
                            }
                            else
                                i++;

                        }
                        while (j < FlightSegment.Count())
                        {
                            if (FlightSegment.ElementAt(i + 1).ArrivalAirport == ObjFltSearch.OriginLocation)
                            {
                                stop2 = j;
                                Stops = stop2 + "-Stop";
                                break;
                            }
                            else
                                j++;
                            i++;

                        }

                        // ------------- Stop Count End--------------  //

                        Sector = ObjFltSearch.OriginLocation + ":" + ObjFltSearch.DestinationLocation + ":" + ObjFltSearch.DestinationLocation + ":" + ObjFltSearch.OriginLocation;
                        Leg = Leg + 1;

                        while (FlightSegment.ElementAt(seg).DepartureAirport == ObjFltSearch.DestinationLocation)
                        {
                            Leg = 1;
                            break;
                        }

                        if (ObjFltSearch.OriginLocation != DepartureAirport && ObjFltSearch.DestinationLocation != ArrivalAirport)
                        {
                            Flight = 2;
                        }

                        else
                        {
                            Flight = 1;

                        }

                    }

                    FlightIdentification = FlightNo.Replace("G9", "");




                    #region[Push Markup, commision , ServiceTax]
                    // -------------------  Markup, commision , ServiceTax --------------------- //
                    //SMS charge calc
                   // float srvCharge = 0;
                   // srvCharge = objFltComm.GetMiscServiceCharge("I", "G9", Obj.UID);
                    //SMS charge calc end

                    double totBFWInf = 0;
                    double totBFWOInf = 0;
                    double totFS = 0;
                    decimal totTax = 0;
                    double ADTAgentMrk1 = 0;
                    double CHDAgentMrk1 = 0;
                    double ADTAdminMrk1 = 0;
                    double CHDAdminMrk1 = 0;
                    double FTotalFare = 0;
                    int AStax = 0;
                    int CStax = 0;
                    int IStax = 0;
                    int ATF = 0;
                    int CTF = 0;
                    int ITF = 0;
                    float AdtMgtFee = 0;
                    float ChdMgtFee = 0;
                    float InfMgtFee = 0;
                    float TotMgtFee = 0;
                    double Iatacommision = 0;



                    totBFWInf = Convert.ToDouble((AdtBaseFare * Convert.ToInt32(Obj.dTotalNo_Adt)) + (ChdBaseFare * Convert.ToInt32(Obj.dTotalNo_CHD)) + (InfBaseFare * Convert.ToInt32(Obj.dTotalNo_INF)));
                    totBFWOInf = Convert.ToDouble(AdtBaseFare * Convert.ToInt32(Obj.dTotalNo_Adt)) + (Convert.ToDouble(ChdBaseFare) * Convert.ToInt32(Obj.dTotalNo_CHD));
                    //SMS charge add 
                    AdtTotalFare = AdtTotalFare + Convert.ToDecimal(srvCharge.ToString());
                    AdtTax = AdtTax + Convert.ToDecimal(srvCharge.ToString());
                    //SMS charge add end

                    //SMS charge add 
                    ChdTotalFare = ChdTotalFare + Convert.ToDecimal(srvCharge.ToString());
                    ChdTax = ChdTax + Convert.ToDecimal(srvCharge.ToString());
                    //SMS charge add end

                    totTax = Convert.ToDecimal((AdtTax * Convert.ToInt32(Obj.dTotalNo_Adt)) + (ChdTax * Convert.ToInt32(Obj.dTotalNo_CHD)) + (InfTax * Convert.ToInt32(Obj.dTotalNo_INF)));

                    totFS = Convert.ToDouble((A_YQ * Convert.ToInt32(Obj.dTotalNo_Adt) + (C_YQ * Convert.ToInt32(Obj.dTotalNo_CHD)) + (I_YQ * Convert.ToInt32(Obj.dTotalNo_INF))));

                    Hashtable HsTblSTax = ServiceTax("G9", Convert.ToDouble(AdtBaseFare), Convert.ToDouble(AdtBaseFare), Convert.ToDouble(A_YQ), "I");
                    AStax = Convert.ToInt32(HsTblSTax["STax"]) * Convert.ToInt32(Obj.dTotalNo_Adt);
                    ATF = Convert.ToInt32(HsTblSTax["TF"]) * Convert.ToInt32(Obj.dTotalNo_Adt);
                    Iatacommision = Convert.ToDouble(HsTblSTax["IATAComm"]);
                    HsTblSTax.Clear();


                    HsTblSTax = ServiceTax("G9", Convert.ToDouble(ChdBaseFare), Convert.ToDouble(ChdBaseFare), Convert.ToDouble(C_YQ), "I");
                    CStax = Convert.ToInt32(HsTblSTax["STax"]) * Convert.ToInt32(Obj.dTotalNo_CHD);
                    CTF = Convert.ToInt32(HsTblSTax["TF"]) * Convert.ToInt32(Obj.dTotalNo_CHD);
                    HsTblSTax.Clear();

                    HsTblSTax = ServiceTax("G9", Convert.ToDouble(InfBaseFare), 0, 0, "I");
                    IStax = Convert.ToInt32(HsTblSTax["STax"]) * Convert.ToInt32(Obj.dTotalNo_INF);
                    ITF = Convert.ToInt32(HsTblSTax["TF"]) * Convert.ToInt32(Obj.dTotalNo_INF);

                    if (IsCorpp == true)
                    {
                        try
                        {
                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(AgType, "G9", AdtBaseFare, A_YQ, "I", AdtTotalFare);
                            AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            AStax = Int32.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            if (Convert.ToInt32(Obj.dTotalNo_CHD) > 0)
                            {
                                MGDT.Clear();
                                MGDT = objFltComm.clac_MgtFee(AgType, "G9", ChdBaseFare, C_YQ, "I", ChdTotalFare);
                                ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                CStax = Int32.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            if (Convert.ToInt32(Obj.dTotalNo_INF) > 0)
                            {
                                MGDT.Clear();
                                MGDT = objFltComm.clac_MgtFee(AgType, "G9", InfBaseFare, I_YQ, "I", InfTotalFare);
                                InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                IStax = Int32.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            TotMgtFee = (AdtMgtFee * Convert.ToInt32(Obj.dTotalNo_Adt)) + (ChdMgtFee * Convert.ToInt32(Obj.dTotalNo_CHD)) + (InfMgtFee * Convert.ToInt32(Obj.dTotalNo_INF));
                        }
                        catch { }
                    }


                    ADTAgentMrk1 = CalcMarkup(dtAgentMarkup, "G9", Convert.ToDouble(AdtTotalFare), "I");
                    if (Convert.ToInt32(Obj.dTotalNo_CHD) > 0)
                    {
                        CHDAgentMrk1 = CalcMarkup(dtAgentMarkup, "G9", Convert.ToDouble(ChdTotalFare), "I");
                    }
                    else
                    {
                        CHDAgentMrk1 = 0;
                    }
                    //if (Convert.ToInt32(Obj.dTotalNo_CHD) > 0)
                    //{
                    //    CHDAdminMrk1 = CalcMarkup(dtAdminMarkup, "G9", Convert.ToDouble(ChdTotalFare), "I");

                    //}
                    //else
                    //{
                    //    CHDAgentMrk1 = 0;
                    //}
                    ADTAdminMrk1 = CalcMarkup(dtAdminMarkup, "G9", Convert.ToDouble(AdtTotalFare), "I");
                    if (Convert.ToInt32(Obj.dTotalNo_CHD) > 0)
                    {
                        CHDAdminMrk1 = CalcMarkup(dtAdminMarkup, "G9", Convert.ToDouble(AdtTotalFare), "I");
                    }
                    else
                    {
                        CHDAdminMrk1 = 0;
                    }
                    double totMrk = 0;
                    totMrk = ADTAdminMrk1 * Convert.ToInt32(Obj.dTotalNo_Adt);
                    totMrk = totMrk + ADTAgentMrk1 * Convert.ToInt32(Obj.dTotalNo_Adt);
                    totMrk = totMrk + CHDAdminMrk1 * Convert.ToInt32(Obj.dTotalNo_CHD);
                    totMrk = totMrk + CHDAgentMrk1 * Convert.ToInt32(Obj.dTotalNo_CHD);

                    if ((Trip.ToString() == JourneyType.I.ToString()) && (IsCorpp == true))
                    {
                        AdtBaseFare = AdtBaseFare + Convert.ToDecimal(ADTAdminMrk1);
                        AdtTotalFare = AdtTotalFare + Convert.ToDecimal(ADTAdminMrk1);
                        if (Convert.ToInt32(Obj.dTotalNo_CHD) > 0)
                        {
                            ChdBaseFare = ChdBaseFare + Convert.ToDecimal(CHDAdminMrk1);
                            ChdTotalFare = ChdTotalFare + Convert.ToDecimal(CHDAdminMrk1);
                        }
                        totBFWInf = totBFWInf + (Convert.ToDouble(ADTAdminMrk1) * Convert.ToInt32(Obj.dTotalNo_Adt)) + (Convert.ToDouble(CHDAdminMrk1) * Convert.ToInt32(Obj.dTotalNo_CHD));
                        FTotalFare = totBFWInf + Convert.ToDouble(totTax) + AStax + CStax + IStax + ATF + CTF + ITF + (Convert.ToDouble(ADTAgentMrk1) * Convert.ToInt32(Obj.dTotalNo_Adt)) + (Convert.ToDouble(CHDAgentMrk1) * Convert.ToInt32(Obj.dTotalNo_CHD)) + TotMgtFee;
                    }
                    else
                    { FTotalFare = totBFWInf + Convert.ToDouble(totTax) + AStax + CStax + IStax + ATF + CTF + ITF + totMrk + TotMgtFee; }



                    // -------------------  End --------------------- //

                    #endregion



                    ObjFinalResult.Add(new FlightSearchResults
                    {
                        ArrAirportCode = ArrivalAirport,
                        DepAirportCode = DepartureAirport,
                        DepartureLocation = DepartureAirport,
                        ArrivalCityName = ArrivalCityName,
                        ArrivalLocation = ArrivalAirport,
                        ArrivalDate = ArrivalDate,
                        DepartureDate = DepartureDate,
                        Sector = Sector,
                        ArrivalTime = ArrivalTime,
                        DepartureTime = DepartureTime,
                        OrgDestFrom = OrgDestFrom,
                        OrgDestTo = OrgDestTo,
                        AirLineName = "Air Arabia",
                        Arrival_Date = Arrival_Date,
                        Stops = Stops,
                        AdtFareType = "Non Refundable",
                        AdtBfare = (float)AdtBaseFare,
                        AdtFare = (float)AdtBaseFare + (float)AdtTax,
                        AdtFSur = (float)A_YQ,
                        AdtRbd = "",
                        AdtOT = (float)AdtTax - (float)A_YQ,
                        AdtTax = (float)AdtTax,
                        AdtMgtFee = AdtMgtFee,
                        AdtSrvTax = (float)AStax,

                        DepartureCityName = DepartureCityName,

                        ChdBFare = (float)ChdBaseFare,
                        ChdFare = (float)ChdBaseFare + (float)ChdTax,
                        ChdFSur = (float)C_YQ,
                        ChdRbd = "NA",
                        ChdOT = (float)ChdTax - (float)C_YQ,
                        ChdTax = (float)ChdTax,
                        ChdMgtFee = ChdMgtFee,
                        ChdSrvTax = (float)CStax,

                        InfBfare = (float)InfBaseFare,
                        InfFare = (float)InfBaseFare + (float)InfTax,
                        InfFSur = (float)I_YQ,
                        InfOT = (float)InfTax - (float)I_YQ,
                        InfTax = (float)InfTax,
                        InfMgtFee = InfMgtFee,
                        InfSrvTax = (float)IStax,

                        TotBfare = (float)totBFWInf,//---------------Change
                        TotalFare = (float)FTotalFare,//-----------------Change
                        TotDur = "",
                        Adult = AdultCount,
                        Child = ChildCount,
                        Infant = InfantCount,
                        Departure_Date = Departure_Date,
                        TotPax = AdultCount + ChildCount + InfantCount,
                        TripType = TripType,
                        STax = AStax + CStax + IStax,
                        TFee = ATF + CTF + ITF,
                        TotDis = 0,
                        RBD = "NA",
                        EQ = "NA",
                        TotalTax = (float)totTax,
                        TotMgtFee = TotMgtFee,
                        TripCnt = "0",
                        MarketingCarrier = "G9",
                        FlightIdentification = FlightIdentification,
                        AvailableSeats = "0",
                        ValiDatingCarrier = "G9",
                        Searchvalue = SearchValue,
                        LineNumber = LineNumber - LineNumber1,
                        Flight = Flight.ToString(),
                        Leg = Leg,
                        Trip = "I",
                        //Currency = "INR",
                        //CS = "Rs.",
                        ADTAdminMrk = (float)ADTAdminMrk1,
                        ADTAgentMrk = (float)ADTAgentMrk1,
                        CHDAdminMrk = (float)CHDAdminMrk1,
                        CHDAgentMrk = (float)CHDAgentMrk1,
                        IATAComm = (float)Iatacommision,
                        //FareBasis = FareBasisCode,
                        fareBasis = FareBasisCode,
                        FBPaxType = "",
                        TotalFuelSur = (float)totFS,
                        sno = TransactionIdentifier + "~" + RPH + "~" + Obj.JSessionId,
                        depdatelcc = FullDepDateTime,
                        arrdatelcc = FullArrDateTime,
                        OriginalTF = (float)TotFare,
                        //OriginalTT = ((float)TotFare - (float)BaseFare),
                        OriginalTT = srvCharge,
                        Track_id = "",
                        FType = "",
                        IsCorp = IsCorpp,
                        Provider = "LCC"
                    });
                    seg++;
                }
            }

            return ObjFinalResult;


            // ---------------------------------- CREATE FINAL LIST END ----------------------------------------------- //

        }

        //public DataTable MyRecordListToDataTable(List<ResListItem> list)
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("OrgDestFrom", list[0].OrgDestFrom.GetType());
        //    dt.Columns.Add("OrgDestTo", list[0].OrgDestTo.GetType());

        //    dt.Columns.Add("Adult", list[0].Adult.GetType());
        //    dt.Columns.Add("Child", list[0].Child.GetType());
        //    dt.Columns.Add("Infant", list[0].Infant.GetType());
        //    dt.Columns.Add("TotPax", list[0].TotPax.GetType());

        //    dt.Columns.Add("DepartureLocation", list[0].DepAirportCode.GetType());
        //    dt.Columns.Add("DepartureCityName", list[0].DepartureCityName.GetType());

        //    dt.Columns.Add("ArrivalLocation", list[0].ArrAirportCode.GetType());
        //    dt.Columns.Add("ArrivalCityName", list[0].ArrivalCityName.GetType());
        //    dt.Columns.Add("DepartureDate", list[0].DepartureDate.GetType());
        //    dt.Columns.Add("Departure_Date", list[0].Departure_Date.GetType());
        //    dt.Columns.Add("DepartureTime", list[0].DepartureTime.GetType());

        //    dt.Columns.Add("ArrivalDate", list[0].ArrivalDate.GetType());
        //    dt.Columns.Add("Arrival_Date", list[0].Arrival_Date.GetType());
        //    dt.Columns.Add("ArrivalTime", list[0].ArrivalTime.GetType());

        //    dt.Columns.Add("MarketingCarrier", list[0].MarketingCarrier.GetType());
        //    dt.Columns.Add("FlightIdentification", list[0].FlightIdentification.GetType());
        //    dt.Columns.Add("RBD", list[0].RBD.GetType());
        //    dt.Columns.Add("AvailableSeats", list[0].AvailableSeats.GetType());
        //    dt.Columns.Add("ValiDatingCarrier", list[0].ValiDatingCarrier.GetType());

        //    dt.Columns.Add("AdtFare", list[0].AdtFare.GetType());
        //    dt.Columns.Add("AdtBfare", list[0].AdtBfare.GetType());
        //    dt.Columns.Add("AdtTax", list[0].AdtTax.GetType());

        //    dt.Columns.Add("ChdFare", list[0].ChdFare.GetType());
        //    dt.Columns.Add("ChdBfare", list[0].ChdBfare.GetType());
        //    dt.Columns.Add("ChdTax", list[0].ChdTax.GetType());

        //    dt.Columns.Add("InfFare", list[0].InfFare.GetType());
        //    dt.Columns.Add("InfBfare", list[0].InfBfare.GetType());
        //    dt.Columns.Add("InfTax", list[0].InfTax.GetType());

        //    dt.Columns.Add("TotalBfare", list[0].TotBfare.GetType());
        //    dt.Columns.Add("TotalFare", list[0].TOT_FARE.GetType());
        //    dt.Columns.Add("TotalTax", list[0].TotalTax.GetType());

        //    dt.Columns.Add("LineItemNumber", list[0].LineNumber.GetType());
        //    dt.Columns.Add("Stax", list[0].STax.GetType());
        //    dt.Columns.Add("TFee", list[0].TFee.GetType());
        //    dt.Columns.Add("Discount", list[0].DisCount.GetType());

        //    dt.Columns.Add("SearchValue", list[0].Searchvalue.GetType());
        //    dt.Columns.Add("Leg", list[0].Leg.GetType());
        //    dt.Columns.Add("Flight", list[0].Flight.GetType());
        //    dt.Columns.Add("Tot_Dur", list[0].TotDur.GetType());
        //    dt.Columns.Add("TripType", list[0].TripType.GetType());
        //    dt.Columns.Add("EQ", list[0].EQ.GetType());
        //    dt.Columns.Add("Stops", list[0].Stops.GetType());
        //    dt.Columns.Add("AirLineName", list[0].AirLineName.GetType());
        //    dt.Columns.Add("Trip", list[0].Trip.GetType());
        //    dt.Columns.Add("Sector", list[0].Sector.GetType());
        //    dt.Columns.Add("TripCnt", list[0].TripCnt.GetType());
        //    dt.Columns.Add("Currency", list[0].Currency.GetType());
        //    dt.Columns.Add("CS", list[0].CS.GetType());


        //    dt.Columns.Add("ADTAdminMrk", list[0].ADTAdminMrk.GetType());
        //    dt.Columns.Add("ADTAgentMrk", list[0].ADTAgentMrk.GetType());
        //    dt.Columns.Add("CHDAdminMrk", list[0].CHDAdminMrk.GetType());
        //    dt.Columns.Add("CHDAgentMrk", list[0].CHDAgentMrk.GetType());
        //    dt.Columns.Add("IATAComm", list[0].IATAComm.GetType());
        //    dt.Columns.Add("fareBasis", list[0].FareBasis.GetType());
        //    dt.Columns.Add("FBPaxType", list[0].FBPaxType.GetType());
        //    dt.Columns.Add("AdtFSur", list[0].AdtFSur.GetType());
        //    dt.Columns.Add("ChdFSur", list[0].ChdFSur.GetType());
        //    dt.Columns.Add("InfFSur", list[0].InfFSur.GetType());

        //    dt.Columns.Add("TotalFuelSur", list[0].TotalFuelSur.GetType());
        //    dt.Columns.Add("Sno", list[0].SNO.GetType());
        //    dt.Columns.Add("DepDateLcc", list[0].DepDateLcc.GetType());
        //    dt.Columns.Add("ArrDateLcc", list[0].ArrDateLcc.GetType());
        //    dt.Columns.Add("OriginalTF", list[0].OriginalTF.GetType());
        //    dt.Columns.Add("OriginalTT", list[0].OriginalTT.GetType());
        //    dt.Columns.Add("Track_id", list[0].Track_id.GetType());
        //    dt.Columns.Add("FType", list[0].FType.GetType());

        //    foreach (ResListItem item in list)
        //    {
        //        dt.Rows.Add(
        //            item.OrgDestFrom,
        //            item.OrgDestTo,
        //            item.Adult,
        //            item.Child,
        //            item.Infant,
        //            item.TotPax,

        //            item.DepAirportCode,
        //            item.DepartureCityName,
        //            item.ArrAirportCode,
        //            item.ArrivalCityName,
        //            item.DepartureDate,
        //            item.Departure_Date,
        //            item.DepartureTime,

        //            item.ArrivalDate,
        //            item.Arrival_Date,
        //            item.ArrivalTime,

        //            item.MarketingCarrier,
        //            item.FlightIdentification,
        //            item.RBD,
        //            item.AvailableSeats,
        //            item.ValiDatingCarrier,

        //            item.AdtFare,
        //            item.AdtBfare,
        //            item.AdtTax,

        //            item.ChdFare,
        //            item.ChdBfare,
        //            item.ChdTax,

        //            item.InfFare,
        //            item.InfBfare,
        //            item.InfTax,

        //            item.TotBfare,
        //            item.TOT_FARE,
        //            item.TotalTax,

        //            item.LineNumber,
        //            item.STax,
        //            item.TFee,
        //            item.DisCount,

        //            item.Searchvalue,
        //            item.Leg,
        //            item.Flight,
        //            item.TotDur,
        //            item.TripType,
        //            item.EQ,
        //            item.Stops,
        //            item.AirLineName,
        //            item.Trip,
        //            item.Sector,
        //            item.TripCnt,
        //            item.Currency,
        //            item.CS,
        //            item.ADTAdminMrk,
        //            item.ADTAgentMrk,
        //            item.CHDAdminMrk,
        //            item.CHDAgentMrk,
        //            item.IATAComm,
        //            item.FareBasis,
        //            item.FBPaxType,
        //            item.AdtFSur,
        //            item.ChdFSur,
        //            item.InfFSur,
        //            item.TotalFuelSur,
        //            item.SNO,
        //            item.DepDateLcc,
        //            item.ArrDateLcc,
        //            item.OriginalTF,
        //            item.OriginalTT,
        //            item.Track_id,
        //            item.FType

        //            );
        //    }

        //    return dt;
        //}

        private string datecon(string MM)
        {
            string mm_str = null;
            switch (MM)
            {
                case "01":
                    mm_str = "JAN";
                    break;
                case "02":
                    mm_str = "FEB";
                    break;
                case "03":
                    mm_str = "MAR";
                    break;
                case "04":
                    mm_str = "APR";
                    break;
                case "05":
                    mm_str = "MAY";
                    break;
                case "06":
                    mm_str = "JUN";
                    break;
                case "07":
                    mm_str = "JUL";
                    break;
                case "08":
                    mm_str = "AUG";
                    break;
                case "09":
                    mm_str = "SEP";
                    break;
                case "10":
                    mm_str = "OCT";
                    break;
                case "11":
                    mm_str = "NOV";
                    break;
                case "12":
                    mm_str = "DEC";
                    break;
                default:

                    break;
            }

            return mm_str;

        }

        private string GetCityName(string CityCode)
        {
            string Cityname = "";
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("select cityName from airport_Name where CityCode='" + CityCode + "'", Conn);
                DataTable dtCityName = new DataTable();
                adap.Fill(dtCityName);
                Cityname = dtCityName.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                Cityname = CityCode;
            }
            return Cityname;
        }


        private Hashtable ServiceTax(string VC, double TotBFWI, double TotBFWOI, double FS, string Trip)
        {


            DataTable dtTax = new DataTable();
            Hashtable AirlineCharges = new Hashtable();
            SqlCommand sqlcom = new SqlCommand();
            string SqlQuery = "ServiceCharge";
            sqlcom = new SqlCommand(SqlQuery, Conn);
            sqlcom.Parameters.Add("@vc", SqlDbType.VarChar).Value = VC;
            sqlcom.Parameters.Add("@trip", SqlDbType.VarChar).Value = Trip;
            sqlcom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sqlcom);
            da.Fill(dtTax);

            try
            {
                //if (dtTax.Rows.Count > 0)
                //{
                //    AirlineCharges.Add("STax", 0);
                //    Math.Round(((TotBFWI * Convert.ToDouble(dtTax.Rows[0]["SrvTax"])) / 100), 0);
                //    AirlineCharges.Add("TF", Math.Round((((TotBFWOI + FS) * Convert.ToDouble(dtTax.Rows[0]["TranFee"])) / 100), 0));
                //    AirlineCharges.Add("IATAComm", dtTax.Rows[0]["IATAComm"]);
                //}
                //else
                //{
                AirlineCharges.Add("STax", 0);
                AirlineCharges.Add("STaxP", 0);
                AirlineCharges.Add("TF", 0);
                AirlineCharges.Add("IATAComm", 0);
                //}
            }
            catch (Exception ex)
            {
                AirlineCharges.Add("STax", 0);
                AirlineCharges.Add("STaxP", 0);
                AirlineCharges.Add("TF", 0);
                AirlineCharges.Add("IATAComm", 0);
            }
            return AirlineCharges;
        }

        private DataTable GetMarkUp(string AgentID, string distrubid, string Trip, string typeId)
        {

            DataTable dt = new DataTable();
            try
            {
                SqlCommand sqlcom = new SqlCommand();
                string SqlQuery = "GetMarkup";
                sqlcom = new SqlCommand(SqlQuery, Conn);
                sqlcom.Parameters.Add("@trip", SqlDbType.VarChar).Value = Trip;
                sqlcom.Parameters.Add("@agid", SqlDbType.VarChar).Value = AgentID;
                sqlcom.Parameters.Add("@distrid", SqlDbType.VarChar).Value = distrubid;
                sqlcom.Parameters.Add("@idtype", SqlDbType.VarChar).Value = typeId;
                sqlcom.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(sqlcom);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        private double CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray = null;
            double mrkamt = 0;

            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString() == "P"))
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"])) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString() == "F"))
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"]);
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"]);
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return mrkamt;
        }
    }
}
