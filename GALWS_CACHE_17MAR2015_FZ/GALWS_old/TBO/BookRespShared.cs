﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL.TBO.BookRespShared
{

    public class Error
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ChargeBU
    {
        public string key { get; set; }
        public double value { get; set; }
    }

    public class Fare
    {
        public string Currency { get; set; }
        public double BaseFare { get; set; }
        public double Tax { get; set; }
        public double YQTax { get; set; }
        public double AdditionalTxnFee { get; set; }
        public double AdditionalTxnFeeOfrd { get; set; }
        public double AdditionalTxnFeePub { get; set; }
        public double OtherCharges { get; set; }
        public List<ChargeBU> ChargeBU { get; set; }
        public double Discount { get; set; }
        public double PublishedFare { get; set; }
        public double CommissionEarned { get; set; }
        public double PLBEarned { get; set; }
        public double IncentiveEarned { get; set; }
        public double OfferedFare { get; set; }
        public double TdsOnCommission { get; set; }
        public double TdsOnPLB { get; set; }
        public double TdsOnIncentive { get; set; }
        public double ServiceFee { get; set; }
    }

    public class ChargeBU2
    {
        public string key { get; set; }
        public double value { get; set; }
    }

    public class Fare2
    {
        public string Currency { get; set; }
        public double BaseFare { get; set; }
        public double Tax { get; set; }
        public double YQTax { get; set; }
        public double AdditionalTxnFee { get; set; }
        public double AdditionalTxnFeeOfrd { get; set; }
        public double AdditionalTxnFeePub { get; set; }
        public double OtherCharges { get; set; }
        public List<ChargeBU2> ChargeBU { get; set; }
        public double Discount { get; set; }
        public double PublishedFare { get; set; }
        public double CommissionEarned { get; set; }
        public double PLBEarned { get; set; }
        public double IncentiveEarned { get; set; }
        public double OfferedFare { get; set; }
        public double TdsOnCommission { get; set; }
        public double TdsOnPLB { get; set; }
        public double TdsOnIncentive { get; set; }
        public double ServiceFee { get; set; }
    }

    public class Ticket
    {
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public string IssueDate { get; set; }
        public string ValidatingAirline { get; set; }
        public string Remarks { get; set; }
        public string ServiceFeeDisplayType { get; set; }
        public string Status { get; set; }
    }

    public class SegmentAdditionalInfo
    {
        public string FareBasis { get; set; }
        public object NVA { get; set; }
        public object NVB { get; set; }
        public string Baggage { get; set; }
        public string Meal { get; set; }
    }

    public class Passenger
    {
        public int PaxId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PaxType { get; set; }
        public string DateOfBirth { get; set; }
        public int Gender { get; set; }
        public string PassportNo { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public Fare2 Fare { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string Nationality { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public bool IsLeadPax { get; set; }
        public object FFAirlineCode { get; set; }
        public object FFNumber { get; set; }
        public Ticket Ticket { get; set; }
        public List<SegmentAdditionalInfo> SegmentAdditionalInfo { get; set; }
    }

    public class Airline
    {
        public string AirlineCode { get; set; }
        public string AirlineName { get; set; }
        public string FlightNumber { get; set; }
        public string FareClass { get; set; }
        public string OperatingCarrier { get; set; }
    }

    public class Airport
    {
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string Terminal { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class Origin
    {
        public Airport Airport { get; set; }
        public string DepTime { get; set; }
    }

    public class Airport2
    {
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string Terminal { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class Destination
    {
        public Airport2 Airport { get; set; }
        public string ArrTime { get; set; }
    }

    public class Segment
    {
        public int TripIndicator { get; set; }
        public int SegmentIndicator { get; set; }
        public Airline Airline { get; set; }
        public string AirlinePNR { get; set; }
        public Origin Origin { get; set; }
        public Destination Destination { get; set; }
        public int Duration { get; set; }
        public int GroundTime { get; set; }
        public int Mile { get; set; }
        public bool StopOver { get; set; }
        public string StopPoint { get; set; }
        public string StopPointArrivalTime { get; set; }
        public string StopPointDepartureTime { get; set; }
        public string Craft { get; set; }
        public bool IsETicketEligible { get; set; }
        public string FlightStatus { get; set; }
        public string Status { get; set; }
    }

    public class FareRule
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Airline { get; set; }
        public string FareBasisCode { get; set; }
        public string FareRuleDetail { get; set; }
        public object FareRestriction { get; set; }
    }

    public class FlightItinerary
    {
        public int BookingId { get; set; }
        public string PNR { get; set; }
        public bool IsDomestic { get; set; }
        public int Source { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string AirlineCode { get; set; }
        public string ValidatingAirlineCode { get; set; }
        public string AirlineRemark { get; set; }
        public bool IsLCC { get; set; }
        public bool NonRefundable { get; set; }
        public string FareType { get; set; }
        public Fare Fare { get; set; }
        public List<Passenger> Passenger { get; set; }
        public List<Segment> Segments { get; set; }
        public List<FareRule> FareRules { get; set; }
        public int Status { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceCreatedOn { get; set; }
    }

    public class Response2
    {
        public string PNR { get; set; }
        public int BookingId { get; set; }
        public bool SSRDenied { get; set; }
        public object SSRMessage { get; set; }
        public int Status { get; set; }
        public bool IsPriceChanged { get; set; }
        public bool IsTimeChanged { get; set; }
        public FlightItinerary FlightItinerary { get; set; }
        public int TicketStatus { get; set; }
        public object Message { get; set; }
    }

    public class Responses
    {
        public bool B2B2BStatus { get; set; }
        public Error Error { get; set; }
        public Response2 Response { get; set; }
        public int ResponseStatus { get; set; }
        public string TraceId { get; set; }
    }

    public class RootObject
    {
        public Responses Response { get; set; }
    }
}
