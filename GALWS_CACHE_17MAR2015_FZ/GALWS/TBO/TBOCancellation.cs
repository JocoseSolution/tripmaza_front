﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using STD.BAL.TBO;
using STD.BAL.TBO.BOOKSHARED;

using System.Configuration;

namespace GALWS.TBO
{
    public class TBOCancellation
    {
        public string TBOFightCancellation(DataSet Crd, string BookingResp, string TicketNo, string Remark, string Reqtype, string Constr, DataRow[] paxdtail, string Sector)
        {
            FlightCancellationDAL objCommonDal = new FlightCancellationDAL(Constr);
            Dictionary<string, string> Log = new Dictionary<string, string>(); string TraceId = "", exep = "", CancelReq = "", strJsonResponse = "", ResponseStatus = "Faild";
            try
            {
                string EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
                TBOAuthProcess objTbProcess = new TBOAuthProcess();
                string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), EndUserIp);

                int TicketId;
                STD.BAL.TBO.BookRespShared.RootObject Bookresult = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(BookingResp);

                List<STD.BAL.TBO.BookRespShared.Passenger> Passengers = new List<STD.BAL.TBO.BookRespShared.Passenger>();
                //Passengers = Bookresult.Response.Response.FlightItinerary.Passenger.Where(x => x.Title == PaxTitle && x.FirstName == PaxFirstname && x.LastName == PaxLastname).ToList();
                Passengers = Bookresult.Response.Response.FlightItinerary.Passenger;
                if (Passengers.Count() > 0)
                {
                    string BookingId = Bookresult.Response.Response.BookingId.ToString();
                    // TicketId = Passengers[0].Ticket.TicketId;
                    if (Reqtype == "Cancel")
                    {
                        CancelReq = SendChangeRequest(Crd.Tables[0].Rows[0]["ServerIP"].ToString(), BookingId, Remark, strTokenID, Sector, paxdtail, Passengers);
                        Log.Add("CancelReq", CancelReq);
                        TBOUtitlity.SaveFile(CancelReq, "CancelReq");

                        strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.BookUrl.Substring(0, ServiceUrl.BookUrl.Length - 4) + "SendChangeRequest", CancelReq);
                        Log.Add("CancelResp", strJsonResponse);
                        TBOUtitlity.SaveFile(strJsonResponse, "CancelResp");

                        try
                        {
                            GALWS.TBO.TBOCancellationShared.RootObject objResponse = JsonConvert.DeserializeObject<GALWS.TBO.TBOCancellationShared.RootObject>(strJsonResponse);

                            List<GALWS.TBO.TBOCancellationShared.TicketCRInfo> tcketinfo = new List<GALWS.TBO.TBOCancellationShared.TicketCRInfo>();
                            tcketinfo = objResponse.Response.TicketCRInfo;
                            if (tcketinfo.Count > 0)
                            {
                                for (int dd = 0; dd < paxdtail.Length; dd++)
                                {
                                    objCommonDal.UpdateCancleStatus(paxdtail[0]["OrderId"].ToString(), paxdtail[dd]["PaxId"].ToString(), tcketinfo.ElementAt(dd).Status == 1 ? "Cancelled" : ResponseStatus);
                                }
                            }
                        }
                        catch (Exception exx)
                        {

                            if (strJsonResponse.Contains("Successful"))
                                ResponseStatus = "Cancelled";
                            objCommonDal.UpdateCancleStatus(paxdtail[0]["OrderId"].ToString(), paxdtail[0]["PaxId"].ToString(), ResponseStatus);
                        }

                    }
                    else if (Reqtype == "HoldRelease")
                    {
                        CancelReq = "{\"EndUserIp\": \"" + Crd.Tables[0].Rows[0]["ServerIP"].ToString() + "\",\"TokenId\": \"" + strTokenID + "\",\"BookingId\": \"" + BookingId + "\",\"Source\": \"" + Remark + "\"}";

                        Log.Add("CancelReq", CancelReq);
                        TBOUtitlity.SaveFile(CancelReq, "CancelReq");

                        strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.BookUrl.Substring(0, ServiceUrl.BookUrl.Length - 4) + "SendChangeRequest", CancelReq);
                        Log.Add("CancelResp", strJsonResponse);
                        TBOUtitlity.SaveFile(strJsonResponse, "CancelResp");
                        if (strJsonResponse.Contains("Successful"))
                            ResponseStatus = "Cancelled";


                        //STD.BAL.TBO.BookRespShared.RootObject result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);
                        //ResponseStatus = result.Response.ResponseStatus;
                        //TraceId = result.Response.TraceId;
                    }

                    objCommonDal.InsertTBOCancleLog(paxdtail[0]["OrderId"].ToString(), TicketNo, Log.ContainsKey("CancelReq") == true ? Log["CancelReq"] : "", Log.ContainsKey("CancelResp") == true ? Log["CancelResp"] : "", ResponseStatus);

                }
                else
                    objCommonDal.InsertTBOCancleLog(paxdtail[0]["OrderId"].ToString(), TicketNo, "Guest Not find in Booking Response", "", ResponseStatus);
                //if (TicketNo.Length == 7)
                //    TicketId = Bookresult.Response.Response.FlightItinerary.Passenger[Convert.ToInt32(TicketNo.Substring(6)) - 1].Ticket.TicketId;
                //else
                //{
                //    Passengers = Bookresult.Response.Response.FlightItinerary.Passenger.Where(x => x.Ticket.TicketNumber == TicketNo).ToList();
                //    TicketId = Passengers[0].Ticket.TicketId;
                //}
            }
            catch (Exception ex)
            {
                exep += CancelReq + strJsonResponse;
                exep += ex.Message + ex.StackTrace.ToString();
            }
            finally
            {
                objCommonDal.InsertTBOCancleLog(paxdtail[0]["OrderId"].ToString(), TicketNo, Log.ContainsKey("CancelReq") == true ? Log["CancelReq"] : "", exep, ResponseStatus);
            }

            return ResponseStatus;
        }
        private string SendChangeRequest(string IP, string BookingId, string Remark, string strTokenID, string Sector, DataRow[] paxdtail, List<STD.BAL.TBO.BookRespShared.Passenger> Passengers)
        {
            string strJsonData = "{";
            try
            {
                strJsonData += "\"BookingId\": " + BookingId + ",";
                strJsonData += "\"RequestType\":2,";
                strJsonData += "\"CancellationType\":3,";

                strJsonData += "\"Sectors\":[";
                //for (int i = 0; i < FltDT.Rows.Count; i++)
                //{
                //    if (i > 0)
                //        strJsonData += ",";
                //    strJsonData += "{\"Origin\": \"" + FltDT.Rows[i]["DepCityOrAirportCode"].ToString() + "\",";
                //    strJsonData += "\"Destination\": \"" + FltDT.Rows[i]["ArrCityOrAirportCode"].ToString() + "\"}";
                //}
                string[] sctorarray = Sector.Split(',');
                for (int i = 0; i < sctorarray.Length; i++)
                {
                    string[] Segment = sctorarray[i].Split(':');
                    if (i > 0)
                        strJsonData += ",";
                    strJsonData += "{\"Origin\": \"" + Segment[0].ToString() + "\",";
                    strJsonData += "\"Destination\": \"" + Segment[1].ToString().ToUpper() + "\"}";
                }
                strJsonData += "],";

                strJsonData += "\"TicketId\":[ ";
                int j = 0;
                foreach (DataRow paxnames in paxdtail)
                {
                    List<STD.BAL.TBO.BookRespShared.Passenger> objpasenger = new List<STD.BAL.TBO.BookRespShared.Passenger>();
                    objpasenger = Passengers.Where(x => x.Title.ToUpper() == paxnames["Title"].ToString().ToUpper() && x.FirstName.ToUpper() == paxnames["FName"].ToString().ToUpper() && x.LastName.ToUpper() == paxnames["LName"].ToString().ToUpper()).ToList();

                    if (objpasenger.Count() > 0)
                    {
                        if (j > 0)
                            strJsonData += ",";
                        strJsonData += objpasenger[0].Ticket.TicketId;
                        j++;
                    }
                }

                strJsonData += "],";
                if (Remark != "")
                    strJsonData += "\"Remarks\": \"" + Remark + "\",";
                strJsonData += "\"EndUserIp\": \"" + IP + "\",";
                strJsonData += "\"TokenId\": \"" + strTokenID + "\"}";
            }
            catch (Exception ex)
            { }

            return strJsonData;
        }
        public string TBOFightCancellation_OnePaxAllSector(DataSet Crd, DataTable FltDT, string BookingResp, string TicketNo, string Remark, string Reqtype, string Constr, string PaxTitle, string PaxFirstname, string PaxLastname)
        {
            FlightCancellationDAL objCommonDal = new FlightCancellationDAL(Constr);
            Dictionary<string, string> Log = new Dictionary<string, string>(); string TraceId = "", exep = "", CancelReq = "", strJsonResponse = "", ResponseStatus = "Faild";
            try
            {
                string EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
                TBOAuthProcess objTbProcess = new TBOAuthProcess();
                string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), EndUserIp);

                int TicketId;
                STD.BAL.TBO.BookRespShared.RootObject Bookresult = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(BookingResp);

                List<STD.BAL.TBO.BookRespShared.Passenger> Passengers = new List<STD.BAL.TBO.BookRespShared.Passenger>();
                Passengers = Bookresult.Response.Response.FlightItinerary.Passenger.Where(x => x.Title == PaxTitle && x.FirstName == PaxFirstname && x.LastName == PaxLastname).ToList();

                if (Passengers.Count() > 0)
                {
                    string BookingId = Bookresult.Response.Response.BookingId.ToString();
                    TicketId = Passengers[0].Ticket.TicketId;
                    if (Reqtype == "Cancel")
                    {
                        CancelReq = SendChangeRequest_OnePax(Crd.Tables[0].Rows[0]["ServerIP"].ToString(), BookingId, Remark, strTokenID, FltDT, TicketId.ToString());
                        Log.Add("CancelReq", CancelReq);
                        TBOUtitlity.SaveFile(CancelReq, "CancelReq");

                        strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.BookUrl.Substring(0, ServiceUrl.BookUrl.Length - 4) + "SendChangeRequest", CancelReq);
                        Log.Add("CancelResp", strJsonResponse);
                        TBOUtitlity.SaveFile(strJsonResponse, "CancelResp");

                        if (strJsonResponse.Contains("Successful"))
                            ResponseStatus = "Cancelled";
                        // STD.BAL.TBO.BookRespShared.RootObject result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);
                        // ResponseStatus = result.Response.ResponseStatus;

                    }
                    else if (Reqtype == "HoldRelease")
                    {
                        CancelReq = "{\"EndUserIp\": \"" + Crd.Tables[0].Rows[0]["ServerIP"].ToString() + "\",\"TokenId\": \"" + strTokenID + "\",\"BookingId\": \"" + BookingId + "\",\"Source\": \"" + Remark + "\"}";

                        Log.Add("CancelReq", CancelReq);
                        TBOUtitlity.SaveFile(CancelReq, "CancelReq");

                        strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.BookUrl.Substring(0, ServiceUrl.BookUrl.Length - 4) + "SendChangeRequest", CancelReq);
                        Log.Add("CancelResp", strJsonResponse);
                        TBOUtitlity.SaveFile(strJsonResponse, "CancelResp");

                        if (strJsonResponse.Contains("Successful"))
                            ResponseStatus = "Cancelled";
                        //STD.BAL.TBO.BookRespShared.RootObject result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);
                        //ResponseStatus = result.Response.ResponseStatus;
                        //TraceId = result.Response.TraceId;
                    }

                    objCommonDal.InsertTBOCancleLog(Convert.ToString(FltDT.Rows[0]["OrderId"]), TicketNo, Log.ContainsKey("CancelReq") == true ? Log["CancelReq"] : "", Log.ContainsKey("CancelResp") == true ? Log["CancelResp"] : "", ResponseStatus);

                }
                else
                    objCommonDal.InsertTBOCancleLog(Convert.ToString(FltDT.Rows[0]["OrderId"]), TicketNo, "Guest Not find in Booking Response", PaxTitle + " " + PaxFirstname + " " + PaxLastname, ResponseStatus);
                //if (TicketNo.Length == 7)
                //    TicketId = Bookresult.Response.Response.FlightItinerary.Passenger[Convert.ToInt32(TicketNo.Substring(6)) - 1].Ticket.TicketId;
                //else
                //{
                //    Passengers = Bookresult.Response.Response.FlightItinerary.Passenger.Where(x => x.Ticket.TicketNumber == TicketNo).ToList();
                //    TicketId = Passengers[0].Ticket.TicketId;
                //}
            }
            catch (Exception ex)
            {
                exep += CancelReq + strJsonResponse;
                exep += ex.Message + ex.StackTrace.ToString();
            }
            finally
            {
                objCommonDal.InsertTBOCancleLog(Convert.ToString(FltDT.Rows[0]["OrderId"]), TicketNo, Log.ContainsKey("CancelReq") == true ? Log["CancelReq"] : "", exep, ResponseStatus);
            }

            return ResponseStatus;
        }
        private string SendChangeRequest_OnePax(string IP, string BookingId, string Remark, string strTokenID, DataTable FltDT, string TicketId)
        {
            string strJsonData = "{";
            try
            {
                strJsonData += "\"BookingId\": " + BookingId + ",";
                strJsonData += "\"RequestType\":2,";
                strJsonData += "\"CancellationType\":3,";

                strJsonData += "\"Sectors\":[";
                for (int i = 0; i < FltDT.Rows.Count; i++)
                {
                    if (i > 0)
                        strJsonData += ",";
                    strJsonData += "{\"Origin\": \"" + FltDT.Rows[i]["DepCityOrAirportCode"].ToString() + "\",";
                    strJsonData += "\"Destination\": \"" + FltDT.Rows[i]["ArrCityOrAirportCode"].ToString() + "\"}";
                }
                strJsonData += "],";
                strJsonData += "\"TicketId\":[ " + TicketId + "],";
                if (Remark != "")
                    strJsonData += "\"Remarks\": \"" + Remark + "\",";
                strJsonData += "\"EndUserIp\": \"" + IP + "\",";
                strJsonData += "\"TokenId\": \"" + strTokenID + "\"}";
            }
            catch (Exception ex)
            { }

            return strJsonData;
        }
    }
}
