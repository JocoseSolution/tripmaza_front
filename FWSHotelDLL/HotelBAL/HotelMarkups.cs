﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HotelShared;
namespace HotelBAL
{
   public class HotelMarkups
    {
       public MarkupList markupCalculation(DataSet mrkds, string star, string AgentID, string City, string Country, decimal TotalAmt, decimal ServicetaxPer)
       {
           DataRow[] AdminFilterArray = null; DataRow[] AgtFilterArray = null;
           MarkupList ObjMarkup = new MarkupList();
           ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = 0; ObjMarkup.AgentMrkPercent = 0; ObjMarkup.AdminMrkPercent = 0;
           ObjMarkup.AdminMrkType = ""; ObjMarkup.AgentMrkType = "";
           try
           {
               if (mrkds.Tables.Count > 0)
               {
                   //Admin Markup Calculation
                   if (mrkds.Tables[0].Rows.Count > 0)
                   {
                       try
                       {
                           AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");

                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length > 0)
                           {
                               if (AdminFilterArray[0][2].ToString() == "Percentage")
                               {
                                   ObjMarkup.AdminMrkAmt = (Convert.ToDecimal(AdminFilterArray[0][1]) * TotalAmt) / 100;
                               }
                               else if (AdminFilterArray[0][2].ToString() == "Fixed")
                               {
                                   ObjMarkup.AdminMrkAmt = Convert.ToDecimal(AdminFilterArray[0][1]);
                               }
                               ObjMarkup.AdminMrkPercent = Convert.ToDecimal(AdminFilterArray[0][1]);
                               ObjMarkup.AdminMrkType = AdminFilterArray[0][2].ToString();
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
                   }
                   ObjMarkup.VenderServiceTaxAmt = CalculateServicetax(TotalAmt, ServicetaxPer);
                   ObjMarkup.AgentServiceTaxAmt = CalculateServicetax(TotalAmt + ObjMarkup.AdminMrkAmt, ServicetaxPer);
                   //Agent Markup Calculation
                   if (mrkds.Tables[1].Rows.Count > 0)
                   {
                       try
                       {
                           AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "'");

                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL'");
                           }

                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length > 0)
                           {
                               if (AgtFilterArray[0][2].ToString() == "Percentage")
                               {
                                   ObjMarkup.AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentServiceTaxAmt)) / 100);  
                               }
                               else if (AgtFilterArray[0][2].ToString() == "Fixed")
                               {
                                   ObjMarkup.AgentMrkAmt = Convert.ToDecimal(AgtFilterArray[0][1]);
                               }
                               ObjMarkup.AgentMrkPercent = Convert.ToDecimal(AgtFilterArray[0][1]);
                               ObjMarkup.AgentMrkType = AgtFilterArray[0][2].ToString();
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
                   }
                   ObjMarkup.AdminMrkAmt = roundtotal(ObjMarkup.AdminMrkAmt); ObjMarkup.AgentMrkAmt = roundtotal(ObjMarkup.AgentMrkAmt);
                   ObjMarkup.TotelAmt = roundtotal(TotalAmt + ObjMarkup.AgentServiceTaxAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentMrkAmt);
               }
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); 
           }
           return ObjMarkup;
       }
       public MarkupList DesiyaRoomMarkupCalculation(DataSet mrkds, string star, string AgentID, string City, string Country, decimal TotalAmt, decimal ServicetaxPer, int RoomCount)
       {
           DataRow[] AdminFilterArray = null; DataRow[] AgtFilterArray = null;
           MarkupList ObjMarkup = new MarkupList();
           ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = 0; ObjMarkup.AgentMrkPercent = 0; ObjMarkup.AdminMrkPercent = 0;
           ObjMarkup.AdminMrkType = ""; ObjMarkup.AgentMrkType = "";
           try
           {
               if (mrkds.Tables.Count > 0)
               {
                   #region  //Admin Markup Calculation
                   if (mrkds.Tables[0].Rows.Count > 0)
                   {
                       try
                       {
                           AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");

                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length > 0)
                           {
                               if (AdminFilterArray[0][2].ToString() == "Percentage")
                               {
                                   ObjMarkup.AdminMrkAmt = (Convert.ToDecimal(AdminFilterArray[0][1]) * TotalAmt) / 100;
                               }
                               else if (AdminFilterArray[0][2].ToString() == "Fixed")
                               {
                                   ObjMarkup.AdminMrkAmt = Convert.ToDecimal(AdminFilterArray[0][1]) * RoomCount;
                               }
                               ObjMarkup.AdminMrkPercent = Convert.ToDecimal(AdminFilterArray[0][1]);
                               ObjMarkup.AdminMrkType = AdminFilterArray[0][2].ToString();
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
                   }
                   #endregion
                   ObjMarkup.VenderServiceTaxAmt = CalculateServicetax(TotalAmt, ServicetaxPer);
                   ObjMarkup.AgentServiceTaxAmt = CalculateServicetax(TotalAmt + ObjMarkup.AdminMrkAmt, ServicetaxPer);
                   #region //Agent Markup Calculation
                   if (mrkds.Tables[1].Rows.Count > 0)
                   {
                       try
                       {
                           AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "'");

                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL'");
                           }

                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length > 0)
                           {
                               if (AgtFilterArray[0][2].ToString() == "Percentage")
                               {
                                   ObjMarkup.AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentServiceTaxAmt)) / 100);
                               }
                               else if (AgtFilterArray[0][2].ToString() == "Fixed")
                               {
                                   ObjMarkup.AgentMrkAmt = Convert.ToDecimal(AgtFilterArray[0][1]) * RoomCount;
                               }
                               ObjMarkup.AgentMrkPercent = Convert.ToDecimal(AgtFilterArray[0][1]);
                               ObjMarkup.AgentMrkType = AgtFilterArray[0][2].ToString();
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
                   }
                   #endregion
                   ObjMarkup.AdminMrkAmt = roundtotal(ObjMarkup.AdminMrkAmt); ObjMarkup.AgentMrkAmt = roundtotal(ObjMarkup.AgentMrkAmt);
                   ObjMarkup.TotelAmt = roundtotal(TotalAmt + ObjMarkup.AgentServiceTaxAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentMrkAmt);
               }
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation");
           }
           return ObjMarkup;
       }
       public decimal DiscountMarkupCalculation(decimal AdminMarkup, decimal AgentMarkup, string AdminMarkupType, string AgentMarkupType, decimal TotalAmt, decimal ServicetaxPer)
       {
           decimal AdminMrkAmt = 0, AgentMrkAmt = 0, TotalAmoutwithMrk = 0; 
           try
           {
               switch (AdminMarkupType)
               {
                   case "Percentage":
                       AdminMrkAmt = roundtotal((AdminMarkup * TotalAmt) / 100);
                       break;
                   case "Fixed":
                       AdminMrkAmt = AdminMarkup;
                       break;
                   default:
                       AdminMrkAmt = 0;
                       break;
               }
               decimal AgentServiceTaxAmt = CalculateServicetax(TotalAmt + AdminMrkAmt, ServicetaxPer);
               switch (AgentMarkupType)
               {
                   case "Percentage":
                       AgentMrkAmt = roundtotal((AgentMarkup * (TotalAmt + AdminMrkAmt + AgentServiceTaxAmt)) / 100);
                       break;
                   case "Fixed":
                       AgentMrkAmt = AgentMarkup;
                       break;
                   default:
                       AgentMrkAmt = 0;
                       break;
               }
               TotalAmoutwithMrk = roundtotal(TotalAmt + AdminMrkAmt + AgentMrkAmt + AgentServiceTaxAmt);
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "DiscountMarkupCalculation");
           }
           return TotalAmoutwithMrk;
       }
       public decimal DiscountDesiyaRoomMarkupCalculation(decimal AdminMarkup, decimal AgentMarkup, string AdminMarkupType, string AgentMarkupType, decimal TotalAmt, decimal ServicetaxPer, int RoomCount)
       {
           decimal AdminMrkAmt = 0, AgentMrkAmt = 0, TotalAmoutwithMrk = 0;
           try
           {
               switch (AdminMarkupType)
               {
                   case "Percentage":
                       AdminMrkAmt = roundtotal((AdminMarkup * TotalAmt) / 100);
                       break;
                   case "Fixed":
                       AdminMrkAmt = AdminMarkup * RoomCount;
                       break;
                   default:
                       AdminMrkAmt = 0;
                       break;
               }
               decimal AgentServiceTaxAmt = CalculateServicetax(TotalAmt + AdminMrkAmt, ServicetaxPer);
               switch (AgentMarkupType)
               {
                   case "Percentage":
                       AgentMrkAmt = roundtotal((AgentMarkup * (TotalAmt + AdminMrkAmt + AgentServiceTaxAmt)) / 100);
                       break;
                   case "Fixed":
                       AgentMrkAmt = AgentMarkup * RoomCount;
                       break;
                   default:
                       AgentMrkAmt = 0;
                       break;
               }
               TotalAmoutwithMrk = roundtotal(TotalAmt + AdminMrkAmt + AgentMrkAmt + AgentServiceTaxAmt);
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "DiscountMarkupCalculation");
           }
           return TotalAmoutwithMrk;
       }
      
       public decimal PolicyMrkCalculation(DataSet mrkds, string star, string AgentID, string City, string Country, decimal TotalAmt, decimal ServicetaxPer)
       {
           DataRow[] AdminFilterArray = null; DataRow[] AgtFilterArray = null; decimal AdminMrkAmt = 0, AgentMrkAmt = 0, AgentServiceTaxAmt=0;
           try
           {
               if (mrkds.Tables.Count > 0)
               {
                   //Admin Markup Calculation
                   if (mrkds.Tables[0].Rows.Count > 0)
                   {
                       try
                       {
                           AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length == 0)
                           {
                               AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='ALL'");
                           }
                           if (AdminFilterArray.Length > 0)
                           {
                               if (AdminFilterArray[0][2].ToString() == "Percentage")
                               {
                                   AdminMrkAmt = (Convert.ToDecimal(AdminFilterArray[0][1]) * TotalAmt) / 100;
                               }
                               else if (AdminFilterArray[0][2].ToString() == "Fixed")
                               {
                                   AdminMrkAmt = Convert.ToDecimal(AdminFilterArray[0][1]);
                               }
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "PolicyMrkCalculationAdm"); }
                   }
                   
                   AgentServiceTaxAmt = CalculateServicetax(TotalAmt + AdminMrkAmt, ServicetaxPer);
                   //Agent Markup Calculation
                   if (mrkds.Tables[1].Rows.Count > 0)
                   {
                       try
                       {
                           AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "'");
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length == 0)
                           {
                               AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='ALL'");
                           }
                           if (AgtFilterArray.Length > 0)
                           {
                               if (AgtFilterArray[0][2].ToString() == "Percentage")
                               {
                                  AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + AdminMrkAmt + AgentServiceTaxAmt)) / 100);
                               }
                               else if (AgtFilterArray[0][2].ToString() == "Fixed")
                               {
                                   AgentMrkAmt = Convert.ToDecimal(AgtFilterArray[0][1]);
                               }
                           }
                       }
                       catch (Exception ex)
                       { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "PolicyMrkCalculationAgt"); }
                   }
                   TotalAmt = roundtotal(TotalAmt + AgentServiceTaxAmt + roundtotal(AdminMrkAmt) + roundtotal(AgentMrkAmt));
               }
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "PolicyMrkCalculation");
           }
           return TotalAmt;
       }

       public MarkupList OnlyPercentMrkCalculation(decimal AdminMarkup, decimal AgentMarkup, string AdminMarkupType, string AgentMarkupType, decimal TotalAmt, decimal ServicetaxPer)
       {
           MarkupList ObjMarkup = new MarkupList();
           ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = 0;
           try
           {
               switch (AdminMarkupType)
               {
                   case "Percentage":
                       ObjMarkup.AdminMrkAmt = roundtotal((AdminMarkup * TotalAmt) / 100);
                       break;
                   default:
                       ObjMarkup.AdminMrkAmt = 0;
                       break;
               }
               ObjMarkup.VenderServiceTaxAmt = CalculateServicetax(TotalAmt, ServicetaxPer);
               ObjMarkup.AgentServiceTaxAmt = CalculateServicetax(TotalAmt + ObjMarkup.AdminMrkAmt, ServicetaxPer);
               switch (AgentMarkupType)
               {
                   case "Percentage":
                       ObjMarkup.AgentMrkAmt = roundtotal((AgentMarkup * (TotalAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentServiceTaxAmt)) / 100);
                       break;
                   default:
                       ObjMarkup.AgentMrkAmt = 0;
                       break;
               }
               ObjMarkup.AdminMrkAmt = roundtotal(ObjMarkup.AdminMrkAmt); ObjMarkup.AgentMrkAmt = roundtotal(ObjMarkup.AgentMrkAmt);
               ObjMarkup.TotelAmt = roundtotal(TotalAmt + ObjMarkup.AgentServiceTaxAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentMrkAmt);
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "OnlyPercentMrkCalculation");
           }
           return ObjMarkup;
       }
      
       public decimal CalculateServicetax(decimal amt, decimal tax)
       {
           decimal amunts = 0;
           try
           {
               if (tax > 0)
               {
                   amunts = Math.Round(amt * (tax / 100), 0);
               }
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CalculateServicetax"); amunts = amt;
           }
           return amunts;
       }
       public decimal roundtotal(decimal amt)
       {
           try
           {
               
               if (amt.ToString().IndexOf(".") > 0)
               {
                   if (!amt.ToString().Contains(".00"))
                   {
                       string[] newamt = amt.ToString().Split('.');
                       if (newamt[1].Length > 0)
                       {
                           if (newamt[1] != "0")
                               amt = int.Parse(newamt[0]) + 1;
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "roundtotal");
               return Math.Round(amt, 0);
           }
           return Math.Round(amt, 0);
       }

        public DataRow[] FilterHotelDT(DataTable HotelDT, string HotelCode)
        {
            DataRow[] HotelFilterArray = null;
            try
            {
                HotelFilterArray = HotelDT.Select("VendorID ='" + HotelCode + "'");
            }
            catch (Exception ex) {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "FilterHotelDT_" + HotelCode);
            }
            return HotelFilterArray;
        }

        public DataRow[] FilterRoomDT(DataTable RoomDT, string RoomCode)
        {
            DataRow[] HotelFilterArray = null;
            try
            {
                HotelFilterArray = RoomDT.Select("RoomTypeID ='" + RoomCode + "'");
            }
            catch (Exception ex) {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "FilterRoomDT"); 
            }
            return HotelFilterArray;
        }

        public string SetTripAdvisorRating(double Ratings)
        {
            string InclImg = "";
            try
            {
                if (Ratings == 1)
                    InclImg = "1 / 5  <img src='../Images/Hotel/TripAdvisor/TA_1.gif' style='margin:2px;' />";
                else if (Ratings > 1 && Ratings < 2)
                    InclImg = Ratings.ToString() + " / 5  <img src='../Images/Hotel/TripAdvisor/TA_1.5.gif' style='margin:2px;' />";
                else if (Ratings == 2)
                    InclImg = "2 / 5  <img src='../Images/Hotel/TripAdvisor/TA_2.gif'  style='margin:2px;' />";
                else if (Ratings > 2 && Ratings < 3)
                    InclImg = Ratings.ToString() + " / 5  <img src='../Images/Hotel/TripAdvisor/TA_2.5.gif' style='margin:2px;' />";
                else if (Ratings == 3)
                    InclImg = "3 / 5  <img src='../Images/Hotel/TripAdvisor/TA_3.gif' style='margin:2px;' />";
                else if (Ratings > 3 && Ratings < 4)
                    InclImg = Ratings.ToString() + " / 5  <img src='../Images/Hotel/TripAdvisor/TA_3.5.gif' style='margin:2px;' />";
                else if (Ratings == 4)
                    InclImg = "4 / 5  <img src='../Images/Hotel/TripAdvisor/TA_4.gif' style='margin:2px;' />";
                else if (Ratings > 4 && Ratings < 5)
                    InclImg = Ratings.ToString() + " / 5  <img src='../Images/Hotel/TripAdvisor/TA_4.5.gif' style='margin:2px;' />";
                else if (Ratings == 4)
                    InclImg = "5 / 5  <img src='../Images/Hotel/TripAdvisor/TA_5.gif' style='margin:2px;' />";
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetTripAdvisorRating");
            }
            return InclImg;
        }



        public MarkupList EXmarkupCalculation(DataSet mrkds, string star, string AgentID, string City, string Country, decimal TotalAmt, decimal ServicetaxPer)
        {
            DataRow[] AdminFilterArray = null; DataRow[] AgtFilterArray = null;
            MarkupList ObjMarkup = new MarkupList();
            ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = 0; ObjMarkup.AgentMrkPercent = 0; ObjMarkup.AdminMrkPercent = 0;
            ObjMarkup.AdminMrkType = ""; ObjMarkup.AgentMrkType = "";
            try
            {
                if (mrkds.Tables.Count > 0)
                {

                    ObjMarkup.VenderServiceTaxAmt = CalculateServicetax(TotalAmt, ServicetaxPer);
                    ObjMarkup.AgentServiceTaxAmt = CalculateServicetax(TotalAmt + 0, ServicetaxPer);
                    //Agent Markup Calculation
                    if (mrkds.Tables[1].Rows.Count > 0)
                    {
                        try
                        {
                            AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "'");

                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "'");
                            }
                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "'");
                            }
                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "'");
                            }
                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL'");
                            }

                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='ALL'");
                            }
                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='ALL'");
                            }
                            if (AgtFilterArray.Length == 0)
                            {
                                AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='ALL'");
                            }
                            if (AgtFilterArray.Length > 0)
                            {
                                if (AgtFilterArray[0][2].ToString() == "Percentage")
                                {
                                    ObjMarkup.AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + 0 + ObjMarkup.AgentServiceTaxAmt)) / 100);
                                }
                                else if (AgtFilterArray[0][2].ToString() == "Fixed")
                                {
                                    ObjMarkup.AgentMrkAmt = Convert.ToDecimal(AgtFilterArray[0][1]);
                                }
                                ObjMarkup.AgentMrkPercent = Convert.ToDecimal(AgtFilterArray[0][1]);
                                ObjMarkup.AgentMrkType = AgtFilterArray[0][2].ToString();
                            }
                        }
                        catch (Exception ex)
                        { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXmarkupCalculation"); }
                    }
                    ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = roundtotal(ObjMarkup.AgentMrkAmt);
                    ObjMarkup.TotelAmt = roundtotal(TotalAmt + ObjMarkup.AgentServiceTaxAmt + 0 + ObjMarkup.AgentMrkAmt);
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXmarkupCalculation");
            }
            return ObjMarkup;
        }

    }
}


  //public MarkupList markupCalculation(DataSet mrkds, string star, string AgentID, string City, string Country, decimal TotalAmt, decimal ServicetaxPer)
  //     {
  //         DataRow[] AdminFilterArray = null; DataRow[] AgtFilterArray = null;
  //         MarkupList ObjMarkup = new MarkupList();
  //         ObjMarkup.AdminMrkAmt = 0; ObjMarkup.AgentMrkAmt = 0; ObjMarkup.AgentMrkPercent = 0; ObjMarkup.AdminMrkPercent = 0;
  //         ObjMarkup.AdminMrkType = ""; ObjMarkup.AgentMrkType = "";
  //         try
  //         {
  //             if (mrkds.Tables.Count > 0)
  //             {
  //                 //Admin Markup Calculation
  //                 if (mrkds.Tables[0].Rows.Count > 0)
  //                 {
  //                     try{
  //                     AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='ALL' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "' and  AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL' and AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='" + AgentID + "'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "' and  AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length == 0)
  //                     {
  //                         AdminFilterArray = mrkds.Tables[0].Select("Star ='ALL' and  City='ALL' and Country='ALL' and AgentID='ALL'");
  //                     }
  //                     if (AdminFilterArray.Length > 0)
  //                     {
  //                         if (AdminFilterArray[0][2].ToString() == "Percentage")
  //                         {
  //                             ObjMarkup.AdminMrkAmt = (Convert.ToDecimal(AdminFilterArray[0][1]) * TotalAmt) / 100;
  //                         }
  //                         else if (AdminFilterArray[0][2].ToString() == "Fixed")
  //                         {
  //                             ObjMarkup.AdminMrkAmt = Convert.ToDecimal(AdminFilterArray[0][1]);
  //                         }
  //                         ObjMarkup.AdminMrkPercent = Convert.ToDecimal(AdminFilterArray[0][1]);
  //                         ObjMarkup.AdminMrkType = AdminFilterArray[0][2].ToString();
  //                     }
  //                     }
  //                     catch(Exception ex)
  //                     { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
  //                 }
  //                 ObjMarkup.VenderServiceTaxAmt = CalculateServicetax(TotalAmt, ServicetaxPer);
  //                 ObjMarkup.AgentServiceTaxAmt = CalculateServicetax(TotalAmt + ObjMarkup.AdminMrkAmt, ServicetaxPer);
  //                 //Agent Markup Calculation
  //                 if (mrkds.Tables[1].Rows.Count > 0)
  //                 {
  //                     try{
  //                     AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='" + Country + "'");
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='" + Country + "'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='" + Country + "'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='" + City + "' and Country='ALL'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='" + Country + "'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='" + City + "' and Country='ALL'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='" + star + "' and  City='ALL' and Country='ALL'");
  //                     }
  //                     if (AgtFilterArray.Length == 0)
  //                     {
  //                         AgtFilterArray = mrkds.Tables[1].Select("Star ='ALL' and  City='ALL' and Country='ALL'");
  //                     }
  //                     if (AgtFilterArray.Length > 0)
  //                     {
  //                         if (AgtFilterArray[0][2].ToString() == "Percentage")
  //                         {
  //                             ObjMarkup.AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentServiceTaxAmt)) / 100);
  //                             //ObjMarkup.AgentMrkAmt = ((Convert.ToDecimal(AgtFilterArray[0][1]) * (TotalAmt + ObjMarkup.AdminMrkAmt)) / 100);
  //                         }
  //                         else if (AgtFilterArray[0][2].ToString() == "Fixed")
  //                         {
  //                             ObjMarkup.AgentMrkAmt = Convert.ToDecimal(AgtFilterArray[0][1]);
  //                         }
  //                         ObjMarkup.AgentMrkPercent = Convert.ToDecimal(AgtFilterArray[0][1]);
  //                         ObjMarkup.AgentMrkType = AgtFilterArray[0][2].ToString();
  //                     }
  //                     }
  //                      catch(Exception ex)
  //                     { HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); }
  //                 }
  //                 ObjMarkup.AdminMrkAmt = roundtotal(ObjMarkup.AdminMrkAmt); ObjMarkup.AgentMrkAmt = roundtotal(ObjMarkup.AgentMrkAmt);
  //                 ObjMarkup.TotelAmt = roundtotal(TotalAmt + ObjMarkup.AgentServiceTaxAmt + ObjMarkup.AdminMrkAmt + ObjMarkup.AgentMrkAmt);
  //                 //ObjMarkup.TotelAmt = roundtotal(TotalAmt +  ObjMarkup.AdminMrkAmt + ObjMarkup.AgentMrkAmt);
  //             }
  //         }
  //         catch (Exception ex)
  //         {
  //             HotelDAL.HotelDA.InsertHotelErrorLog(ex, "markupCalculation"); 
  //         }
  //         return ObjMarkup;
  //     }