﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.XPath;
using STD.Shared;
using STD.BAL;
using ITZERRORLOG;
using System.Xml.Linq;
using System.Collections;
using System.Web;

namespace GALWS.ITQAirApi
{
    public class AvailabilityAndPricing
    {
        public AvailabilityAndPricing()
        { }
        public List<FlightSearchResults> ITQAirApiAvailibility(FlightSearch objFlt, bool RTFS, string fareType, bool isLCC, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, List<FlightCityList> CityList, CredentialList CrdList, HttpContext contx, List<MISCCharges> MiscList)
        {
            HttpContext.Current = contx;
            string OFareType = "";
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>(); List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            try
            {
                string sessionids = ITQAirApiUtility.Authenticate(CrdList.LoginID, CrdList.UserID, CrdList.Password);
                string AvailibilityReq = AvailibilityRequest(objFlt, fareType, sessionids);
                string AvailibilityResponse = ITQAirApiUtility.PostJsionXML(CrdList.LoginPWD, "application/xml; charset=utf-8", AvailibilityReq);
                if (objFlt.Trip != Trip.I)
                {
                    ITQAirApiUtility.SaveFile(AvailibilityReq, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + fareType + RTFS.ToString(), objFlt.UID, objFlt.HidTxtAirLine);
                    ITQAirApiUtility.SaveFile(AvailibilityResponse, "SearchRes_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_" + fareType + RTFS.ToString(), objFlt.UID, objFlt.HidTxtAirLine);
                }
                else
                {
                    ITQAirApiUtility.SaveFile(AvailibilityReq, "SearchReq_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_I", objFlt.UID, objFlt.HidTxtAirLine);
                    ITQAirApiUtility.SaveFile(AvailibilityResponse, "SearchRes_" + objFlt.HidTxtDepCity.Split(',')[0] + "_" + objFlt.HidTxtArrCity.Split(',')[0] + "_I", objFlt.UID, objFlt.HidTxtAirLine);
                }
                try
                {
                    if (!AvailibilityResponse.Contains("<Errors>"))
                    {
                        XDocument ITQDocument = XDocument.Parse(AvailibilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        XElement AvailabilityResult = ITQDocument.Element("AirShopResponse").Element("Availibilities");

                        if (AvailabilityResult != null)
                        {
                            int LineNumber = 1;
                            IEnumerable<XElement> Availibilitys = AvailabilityResult.Elements("Availibility");
                            foreach (var Availibility in Availibilitys)
                            {
                                //IEnumerable<XElement> PricingInfos = Availibility.Element("PricingInfos").Elements("PricingInfo");
                                if (Availibility.Element("Itineraries").Elements("Itinerary").Count() > 0 && Availibility.Element("PricingInfos").Elements("PricingInfo").Count() > 0)
                                {
                                    int Leg = 1;
                                    int samelinenum = 0;


                                    foreach (var PricingInfos in Availibility.Element("PricingInfos").Elements("PricingInfo"))
                                    {
                                        foreach (var fltdtl in Availibility.Element("Itineraries").Elements("Itinerary"))
                                        {
                                            DataTable CommDt = new DataTable();
                                            Hashtable STTFTDS = new Hashtable();
                                            FlightSearchResults objfsr = new FlightSearchResults();
                                            float srvChargeAdt = 0, srvChargeChd = 0;
                                            objfsr.Stops = fltdtl.Attribute("StopCount").Value;
                                            objfsr.LineNumber = LineNumber;// Convert.ToInt32(Availibility.Attribute("ItemNo").Value); 
                                            objfsr.Flight = fltdtl.Attribute("Flight").Value;
                                            objfsr.Leg = Convert.ToInt32(fltdtl.Attribute("Leg").Value);
                                            objfsr.Adult = objFlt.Adult;
                                            objfsr.Child = objFlt.Child;
                                            objfsr.Infant = objFlt.Infant;

                                            objfsr.depdatelcc = fltdtl.Element("Origin").Element("AirportCode").Value.Trim();
                                            objfsr.arrdatelcc = fltdtl.Element("Destination").Element("AirportCode").Value.Trim();
                                            objfsr.Departure_Date = Convert.ToDateTime(fltdtl.Element("Origin").Element("DateTime").Value.Trim()).ToString("dd MMM");
                                            objfsr.Arrival_Date = Convert.ToDateTime(fltdtl.Element("Destination").Element("DateTime").Value.Trim()).ToString("dd MMM");
                                            objfsr.DepartureDate = Convert.ToDateTime(fltdtl.Element("Origin").Element("DateTime").Value.Trim()).ToString("ddMMyy");
                                            objfsr.ArrivalDate = Convert.ToDateTime(fltdtl.Element("Destination").Element("DateTime").Value.Trim()).ToString("ddMMyy");

                                            objfsr.FlightIdentification = fltdtl.Element("AirLine").Element("Identification").Value.Trim();
                                            objfsr.AirLineName = fltdtl.Element("AirLine").Element("Name").Value.Trim();
                                            objfsr.ValiDatingCarrier = fltdtl.Element("AirLine").Element("Code").Value.Trim();
                                            objfsr.OperatingCarrier = fltdtl.Element("AirLine").Element("Code").Value.Trim();
                                            objfsr.MarketingCarrier = fltdtl.Element("AirLine").Element("OperatingCarrier") != null ? fltdtl.Element("AirLine").Element("OperatingCarrier").Value.Trim() : "";

                                            if (objfsr.MarketingCarrier.Trim() == "")
                                                objfsr.MarketingCarrier = objfsr.OperatingCarrier;

                                            objfsr.DepartureLocation = fltdtl.Element("Origin").Element("AirportCode").Value.Trim();
                                            objfsr.DepartureCityName = fltdtl.Element("Origin").Element("CityName").Value.Trim();
                                            objfsr.DepartureTime = Convert.ToDateTime(fltdtl.Element("Origin").Element("DateTime").Value.Trim()).ToString("HHmm");
                                            objfsr.DepartureAirportName = fltdtl.Element("Origin").Element("AirportName").Value.Trim();
                                            objfsr.DepartureTerminal = "Terminal " + fltdtl.Element("Origin").Element("Terminal").Value;
                                            objfsr.DepAirportCode = objfsr.DepartureLocation;

                                            objfsr.ArrivalLocation = fltdtl.Element("Destination").Element("AirportCode").Value.Trim();
                                            objfsr.ArrivalCityName = fltdtl.Element("Destination").Element("CityName").Value.Trim();
                                            objfsr.ArrivalTime = Convert.ToDateTime(fltdtl.Element("Destination").Element("DateTime").Value.Trim()).ToString("HHmm");
                                            objfsr.ArrivalAirportName = fltdtl.Element("Destination").Element("AirportName").Value.Trim();
                                            objfsr.ArrivalTerminal = "Terminal " + fltdtl.Element("Destination").Element("Terminal").Value;
                                            objfsr.ArrAirportCode = objfsr.ArrivalLocation;
                                            objfsr.Trip = objFlt.Trip.ToString();
                                            objfsr.TotDur = fltdtl.Attribute("Duration").Value;
                                            objfsr.TripCnt = fltdtl.Attribute("Duration").Value;
                                            objfsr.TotalTripDur = Availibility.Element("TotalDuration").Value;

                                            objfsr.EQ = fltdtl.Attribute("EquipmentType").Value;
                                            objfsr.AvailableSeats = fltdtl.Attribute("AvailableSeats").Value;
                                            objfsr.FareRule = fltdtl.Attribute("Key").Value; //fltdtl.Element("ItineraryMetadata").Value;
                                            objfsr.sno = sessionids + "Ref!" + ITQDocument.Element("AirShopResponse").Element("Key").Value + "Ref!" + Availibility.Element("Provider").Value + "Ref!" + Availibility.Attribute("ItemNo").Value + "Ref!" + Availibility.Element("IsLCC").Value;

                                            objfsr.Provider = "TQ";

                                            #region Baggage info
                                            if (objfsr.ValiDatingCarrier == "UK")
                                                objfsr.BagInfo = objfsr.BagInfo = "Check-In Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckIn").Value + " Piece and Hand Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("Cabin").Value + " KG included.";
                                            else
                                                if(fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckIn").Value.Length>1)
                                                objfsr.BagInfo = "Check-In Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckIn").Value.Substring(0, 2) + " KG and Hand Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("Cabin").Value + " KG included.";
                                            else
                                                objfsr.BagInfo = "Check-In Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckIn").Value + " KG and Hand Baggage: " + fltdtl.Element("Baggage").Element("Allowance").Attribute("Cabin").Value + " KG included.";

                                            //if (fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckInPiece").Value != "0")
                                            //    objfsr.BagInfo += " Check-In Baggage should be " + fltdtl.Element("Baggage").Element("Allowance").Attribute("CheckInPiece").Value;
                                            //if (fltdtl.Element("Baggage").Element("Allowance").Attribute("CabinPiece").Value != "0")
                                            //    objfsr.BagInfo += " and Hand Baggage should be " + fltdtl.Element("Baggage").Element("Allowance").Attribute("CabinPiece").Value;
                                            #endregion

                                            objfsr.Searchvalue = PricingInfos.Element("Pricingkey").Value;
                                            #region Adding Fare

                                            #region Adult
                                            IEnumerable<XElement> ADTFareBreakDown = PricingInfos.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "ADT");
                                            IEnumerable<XElement> ADTFareInfos = PricingInfos.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "ADT");

                                            #region taxes
                                            foreach (var Taxes in ADTFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                            {
                                                if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                {
                                                    objfsr.AdtFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                                else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                {
                                                    objfsr.AdtOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                                if (Taxes.Attribute("TaxCode").Value == "K3")
                                                {
                                                    objfsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                                else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                {
                                                    objfsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                                else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                {
                                                    objfsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                                else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                {
                                                    objfsr.AdtYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                }
                                            }
                                            #endregion

                                            objfsr.fareBasis = ADTFareInfos.ElementAt(0).Element("FareBasis").Value;
                                            objfsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));
                                            objfsr.AdtCabin = ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                            objfsr.AdtRbd = ADTFareInfos.ElementAt(0).Element("PaxBookingClass").Value;
                                            objfsr.AdtFarebasis = ADTFareInfos.ElementAt(0).Element("PaxFareBasis").Value;
                                            objfsr.AdtFar = PricingInfos.Attribute("FareType").Value;
                                            OFareType = PricingInfos.Attribute("FareType").Value;
                                            #region Get MISC Markup Charges
                                            try
                                            {
                                                srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, OFareType, Convert.ToString(objfsr.AdtBfare), Convert.ToString(objfsr.AdtFSur));
                                            }
                                            catch { srvChargeAdt = 0; }
                                            #endregion

                                            objfsr.AdtWO = 0;
                                            objfsr.AdtTax = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeAdt;
                                            objfsr.AdtOT = objfsr.AdtTax;
                                            objfsr.AdtFareType = ADTFareBreakDown.ElementAt(0).Attribute("Refundable").Value;
                                            objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare;

                                            objfsr.FBPaxType = "ADT";
                                            objfsr.ElectronicTicketing = "";

                                            objfsr.ADTAdminMrk = 0;
                                            objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, objFlt.Trip.ToString());
                                            if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                            {
                                                objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                objfsr.AdtBfare = objfsr.AdtBfare + objfsr.ADTAdminMrk;
                                                objfsr.AdtFare = objfsr.AdtFare + objfsr.ADTAdminMrk;
                                            }
                                            #region Commision and TDS
                                            CommDt.Clear();
                                            STTFTDS.Clear();
                                            try
                                            {
                                                if (objFlt.Trip.ToString().ToUpper() == "D" || objFlt.Trip.ToString().ToUpper() == "I")
                                                {
                                                    #region Show hide Net Fare and Commission Calculation-Adult Pax
                                                    if (objfsr.Leg == 1)
                                                    {
                                                        //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Availibility.Element("Itineraries").Elements("Itinerary").Count().ToString(), contx);
                                                        CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objfsr.DepartureDate, objFlt.HidTxtDepCity.Split(',')[0].ToString().Trim() + "-" + objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), "", objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[1].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, OFareType, "1", contx, "TQ");
                                                        if (CommDt != null && CommDt.Rows.Count > 0)
                                                        {
                                                            objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                            objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                            STTFTDS.Clear();
                                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, objFlt.TDS);
                                                            objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                            objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                            objfsr.AdtEduCess = 0;
                                                            objfsr.AdtHighEduCess = 0;
                                                            objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                            objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                        }
                                                        else
                                                        {
                                                            objfsr.AdtDiscount1 = 0;
                                                            objfsr.AdtCB = 0;

                                                            objfsr.AdtSrvTax1 = 0;
                                                            objfsr.AdtDiscount = 0;
                                                            objfsr.AdtEduCess = 0;
                                                            objfsr.AdtHighEduCess = 0;
                                                            objfsr.AdtTF = 0;
                                                            objfsr.AdtTds = 0;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        objfsr.AdtDiscount1 = 0;
                                                        objfsr.AdtCB = 0;

                                                        objfsr.AdtSrvTax1 = 0;
                                                        objfsr.AdtDiscount = 0;
                                                        objfsr.AdtEduCess = 0;
                                                        objfsr.AdtHighEduCess = 0;
                                                        objfsr.AdtTF = 0;
                                                        objfsr.AdtTds = 0;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    objfsr.AdtDiscount1 = 0;
                                                    objfsr.AdtCB = 0;

                                                    objfsr.AdtSrvTax1 = 0;
                                                    objfsr.AdtDiscount = 0;
                                                    objfsr.AdtEduCess = 0;
                                                    objfsr.AdtHighEduCess = 0;
                                                    objfsr.AdtTF = 0;
                                                    objfsr.AdtTds = 0;
                                                }
                                            }
                                            catch (Exception es)
                                            {
                                                objfsr.AdtDiscount1 = 0;
                                                objfsr.AdtCB = 0;

                                                objfsr.AdtSrvTax1 = 0;
                                                objfsr.AdtDiscount = 0;
                                                objfsr.AdtEduCess = 0;
                                                objfsr.AdtHighEduCess = 0;
                                                objfsr.AdtTF = 0;
                                                objfsr.AdtTds = 0;
                                            }
                                            if (objFlt.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                    objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }
                                            #endregion

                                            decimal AdtOrgfare = Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("TotalFare").Value.Trim()));
                                            #endregion
                                            #region Child
                                            decimal ChdOrgfare = 0;
                                            IEnumerable<XElement> CHDFareBreakDown = PricingInfos.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "CHD");
                                            IEnumerable<XElement> CHDFareInfos = PricingInfos.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "CHD");

                                            if (objfsr.Child > 0)
                                            {
                                                #region taxes
                                                foreach (var Taxes in CHDFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                                {
                                                    if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                    {
                                                        objfsr.ChdFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                    {
                                                        objfsr.ChdOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    if (Taxes.Attribute("TaxCode").Value == "K3")
                                                    {
                                                        objfsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                    {
                                                        objfsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                    {
                                                        objfsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                    {
                                                        objfsr.ChdYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                }
                                                #endregion
                                                objfsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));

                                                #region Get MISC Markup Charges
                                                try
                                                {
                                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, OFareType, Convert.ToString(objfsr.ChdBFare), Convert.ToString(objfsr.ChdFSur));
                                                }
                                                catch { srvChargeChd = 0; }
                                                #endregion

                                                objfsr.ChdTax = (float)Math.Ceiling(Convert.ToDecimal(CHDFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeChd;
                                                objfsr.ChdCabin = CHDFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                                objfsr.ChdRbd = CHDFareInfos.ElementAt(0).Element("PaxBookingClass").Value; ;
                                                objfsr.ChdFarebasis = CHDFareInfos.ElementAt(0).Element("PaxFareBasis").Value;
                                                objfsr.ChdfareType = CHDFareBreakDown.ElementAt(0).Attribute("Refundable").Value;
                                                ChdOrgfare = Math.Ceiling(Convert.ToDecimal(CHDFareBreakDown.ElementAt(0).Element("TotalFare").Value.Trim()));
                                            }
                                            else
                                            {
                                                objfsr.ChdBFare = 0;
                                                objfsr.ChdTax = 0;
                                                objfsr.ChdCabin = "";
                                                objfsr.ChdRbd = "";
                                                objfsr.ChdFarebasis = "";
                                            }
                                            objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare;
                                            objfsr.ChdFar = PricingInfos.Attribute("FareType").Value;
                                            objfsr.CHDAdminMrk = 0;
                                            objfsr.CHDAgentMrk = 0;
                                            if (objfsr.Child > 0)
                                            {
                                                objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                if ((objFlt.Trip.ToString() == JourneyType.I.ToString()) && (objFlt.IsCorp == true))
                                                {
                                                    objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, objFlt.Trip.ToString());
                                                    objfsr.ChdBFare = objfsr.ChdBFare + objfsr.CHDAdminMrk;
                                                    objfsr.ChdFare = objfsr.ChdFare + objfsr.CHDAdminMrk;
                                                }
                                            }
                                            #region child commision and DTS
                                            CommDt.Clear();
                                            STTFTDS.Clear();
                                            try
                                            {
                                                if (objFlt.Trip.ToString().ToUpper() == "D" || objFlt.Trip.ToString().ToUpper() == "I")
                                                {
                                                    #region Show hide Net Fare and Commission Calculation-Child Pax
                                                    if (objfsr.Leg == 1)
                                                    {
                                                        //CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, objFlt.DepDate, objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, objFlt.RetDate, objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Availibility.Element("Itineraries").Elements("Itinerary").Count().ToString(), contx);
                                                        CommDt = objFltComm.GetFltComm_WithouDB(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, objfsr.DepartureDate, objFlt.HidTxtDepCity.Split(',')[0].ToString().Trim() + "-" + objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), "", objfsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[1].ToString().Trim(), objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, OFareType, "1", contx, "TQ");
                                                        if (CommDt != null && CommDt.Rows.Count > 0)
                                                        {
                                                            objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                            objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                            STTFTDS.Clear();
                                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, objFlt.TDS);
                                                            objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                            objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                            objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                            objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                        }
                                                        else
                                                        {
                                                            objfsr.ChdEduCess = 0;
                                                            objfsr.ChdHighEduCess = 0;
                                                            objfsr.ChdDiscount1 = 0;
                                                            objfsr.ChdCB = 0;
                                                            objfsr.ChdSrvTax1 = 0;
                                                            objfsr.ChdDiscount = 0;
                                                            objfsr.ChdTF = 0;
                                                            objfsr.ChdTds = 0;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        objfsr.ChdEduCess = 0;
                                                        objfsr.ChdHighEduCess = 0;
                                                        objfsr.ChdDiscount1 = 0;
                                                        objfsr.ChdCB = 0;
                                                        objfsr.ChdSrvTax1 = 0;
                                                        objfsr.ChdDiscount = 0;
                                                        objfsr.ChdTF = 0;
                                                        objfsr.ChdTds = 0;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    objfsr.ChdEduCess = 0;
                                                    objfsr.ChdHighEduCess = 0;
                                                    objfsr.ChdDiscount1 = 0;
                                                    objfsr.ChdCB = 0;
                                                    objfsr.ChdSrvTax1 = 0;
                                                    objfsr.ChdDiscount = 0;
                                                    objfsr.ChdTF = 0;
                                                    objfsr.ChdTds = 0;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                objfsr.ChdEduCess = 0;
                                                objfsr.ChdHighEduCess = 0;
                                                objfsr.ChdDiscount1 = 0;
                                                objfsr.ChdCB = 0;
                                                objfsr.ChdSrvTax1 = 0;
                                                objfsr.ChdDiscount = 0;
                                                objfsr.ChdTF = 0;
                                                objfsr.ChdTds = 0;
                                            }
                                            #endregion

                                            if (objFlt.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                    objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    objfsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }
                                            #endregion
                                            #region Infant
                                            if (objfsr.Infant > 0)
                                            {

                                                IEnumerable<XElement> INFFareBreakDown = PricingInfos.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "INF");
                                                IEnumerable<XElement> INFFareInfos = PricingInfos.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "INF");
                                                #region taxes
                                                foreach (var Taxes in INFFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                                {
                                                    if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                    {
                                                        objfsr.InfFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                    {
                                                        objfsr.InfOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    if (Taxes.Attribute("TaxCode").Value == "K3")
                                                    {
                                                        objfsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                    {
                                                        objfsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                    {
                                                        objfsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                    {
                                                        objfsr.InfYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                }
                                                #endregion
                                                objfsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(INFFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));
                                                objfsr.AdtCabin = INFFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                                objfsr.AdtRbd = "";
                                                objfsr.AdtFarebasis = INFFareInfos.ElementAt(0).Element("PaxFareBasis").Value;

                                                objfsr.InfTax = (float)Math.Ceiling(Convert.ToDecimal(INFFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeChd;
                                                objfsr.InfRbd = "";
                                                objfsr.InfFarebasis = INFFareInfos.ElementAt(0).Element("PaxFareBasis").Value;
                                                objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare;
                                                objfsr.InfFar = "NRM";
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(objfsr.InfFare.ToString()));
                                                        objfsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }
                                            #endregion

                                            objfsr.OriginalTF = (float)(AdtOrgfare * objfsr.Adult) + (float)(ChdOrgfare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant);
                                            objfsr.OriginalTT = (srvChargeAdt * objfsr.Adult) + (srvChargeChd * objfsr.Child);

                                            //Rounding the fare
                                            objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                            objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                            objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                            objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;
                                            objfsr.STax = (objfsr.AdtSrvTax * objfsr.Adult) + (objfsr.ChdSrvTax * objfsr.Child) + (objfsr.InfSrvTax * objfsr.Infant);
                                            objfsr.TFee = (objfsr.AdtTF * objfsr.Adult) + (objfsr.ChdTF * objfsr.Child) + (objfsr.InfTF * objfsr.Infant);
                                            objfsr.TotDis = (objfsr.AdtDiscount * objfsr.Adult) + (objfsr.ChdDiscount * objfsr.Child);
                                            objfsr.TotCB = (objfsr.AdtCB * objfsr.Adult) + (objfsr.ChdCB * objfsr.Child);
                                            objfsr.TotTds = (objfsr.AdtTds * objfsr.Adult) + (objfsr.ChdTds * objfsr.Child) + objfsr.InfTds;
                                            objfsr.TotMgtFee = (objfsr.AdtMgtFee * objfsr.Adult) + (objfsr.ChdMgtFee * objfsr.Child) + (objfsr.InfMgtFee * objfsr.Infant);

                                            objfsr.TotalFare = (objfsr.Adult * objfsr.AdtFare) + (objfsr.Child * objfsr.ChdFare) + (objfsr.Infant * objfsr.InfFare);
                                            objfsr.TotalFuelSur = (objfsr.Adult * objfsr.AdtFSur) + (objfsr.Child * objfsr.ChdFSur) + (objfsr.Infant * objfsr.InfFSur);
                                            objfsr.TotalTax = (objfsr.Adult * objfsr.AdtTax) + (objfsr.Child * objfsr.ChdTax) + (objfsr.Infant * objfsr.InfTax);
                                            objfsr.TotBfare = (objfsr.Adult * objfsr.AdtBfare) + (objfsr.Child * objfsr.ChdBFare) + (objfsr.Infant * objfsr.InfBfare);
                                            objfsr.TotMrkUp = (objfsr.ADTAdminMrk * objfsr.Adult) + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAdminMrk * objfsr.Child) + (objfsr.CHDAgentMrk * objfsr.Child);
                                            objfsr.TotalFare = objfsr.TotalFare + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                            objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAgentMrk * objfsr.Child));
                                            #endregion

                                            if (objFlt.RTF == true || objFlt.GDSRTF == true || RTFS == true)
                                            {
                                                objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                objfsr.FType = "RTF";
                                            }
                                            else
                                            {
                                                if (fareType == "IB")
                                                    objfsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                else
                                                    objfsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                            }
                                            if (fareType == "IB")
                                            {
                                                objfsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                                objfsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                            }
                                            else
                                            {
                                                objfsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                objfsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                            }

                                            objfsr.TripType = objFlt.TripType.ToString();
                                            objfsr.Trip = objFlt.Trip.ToString();

                                                    fsrList.Add(objfsr);
                                            //if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.D && RTFS == true)
                                            //{
                                            //    if (Convert.ToInt32(objfsr.Flight) == 1)
                                            //else if (objFlt.TripType == TripType.RoundTrip && objFlt.Trip == STD.Shared.Trip.I)
                                            //{
                                            //    if (Convert.ToInt32(objfsr.Flight) == 1)
                                            //        fsrList.Add(objfsr);
                                            //    else
                                            //        RfsrList.Add(objfsr);
                                            //}
                                            //else
                                            //{
                                            //    fsrList.Add(objfsr);
                                            //}



                                        }
                                        LineNumber++;

                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                { ExecptionLogger.FileHandling("ITQAirApiAvailibility", "Error_001", ex, "ITQFlight"); }

                    FinalList = fsrList;
                //    if (RfsrList.Count != 0)
                //        FinalList = RfsrList;
                //}
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("ITQAirApiAvailibility", "Error_001", ex, "ITQFlight"); }
            return objFltComm.AddFlightKey(FinalList, RTFS);
        }
        public List<FlightSearchResults> ITQAirApiItineraryPrice(DataSet Fltdtlds, DataSet Credencilas, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string agentType, string TDS, string ConStr, List<FlightSearchResults> objlist, List<MISCCharges> MiscList, HttpContext contx)
        {
            try
            {
                string OFareType = "";
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
                if (Fltdtlds != null)
                {
                    DataTable fltdt = Fltdtlds.Tables[0]; DataTable Credencialdt = Credencilas.Tables[0];
                    DataView dv1 = Credencialdt.DefaultView;
                    dv1.RowFilter = "TripType = '" + fltdt.Rows[0]["Trip"].ToString().Trim() + "'";
                    Credencialdt = dv1.ToTable();
                    if (fltdt.Rows.Count > 0 && Credencialdt.Rows.Count > 0)
                    {
                        string[] SnoSplit = new string[] { "Ref!" };
                        string[] SnoSplitDetails = fltdt.Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);

                        string ItineraryPriceRequest = PricingRequest(Credencialdt.Rows[0]["UserID"].ToString(), SnoSplitDetails[0], SnoSplitDetails[1], fltdt.Rows[0]["Searchvalue"].ToString(), SnoSplitDetails[3], SnoSplitDetails[2]);
                        ITQAirApiUtility.SaveFile(ItineraryPriceRequest, "ItineraryPriceRequest_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());

                        string ItineraryPriceResponse = ITQAirApiUtility.PostJsionXML(Credencialdt.Rows[0]["ServerUrlOrIP"].ToString(), "application/xml; charset=utf-8", ItineraryPriceRequest);
                        ITQAirApiUtility.SaveFile(ItineraryPriceResponse, "ItineraryPriceResponse_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());

                        if (ItineraryPriceResponse.Contains("<Status>"))//
                        {
                            XDocument ItqDocument = XDocument.Parse(ItineraryPriceResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));

                            if (ItqDocument.Element("PricingResponse").Element("Status").Value.Trim() == "Success")
                            {
                                XElement PricingResponse = ItqDocument.Element("PricingResponse");

                                GALWS.AirAsia.AirAsiaUtility objupdatedb = new GALWS.AirAsia.AirAsiaUtility();
                                objupdatedb.UpdateAirAsiaSessionId(PricingResponse.Element("ReferenceNo").Value, fltdt.Rows[0]["Track_id"].ToString(), ConStr);
                                if (PricingResponse.Element("IsPriceChange").Value == "true")
                                {
                                    XElement Availibility = PricingResponse.Element("AirPricingResponse").Element("Availibility");
                                    foreach (var Priceetenry in Availibility.Element("PricingInfos").Elements("PricingInfo"))
                                    {
                                        DataTable CommDt = new DataTable(); Hashtable STTFTDS = new Hashtable();
                                        foreach (var fltdtl in Availibility.Element("Itineraries").Elements("Itinerary"))
                                        {
                                            string snoKey = SnoSplitDetails[0] + "Ref!" + PricingResponse.Element("Key").Value + "Ref!" + Availibility.Element("Provider").Value + "Ref!" + Availibility.Attribute("ItemNo").Value + "Ref!" + Availibility.Element("IsLCC").Value;
                                            foreach (FlightSearchResults objfsr in objlist.Where(x => x.sno == snoKey && x.FlightIdentification.Trim() == fltdtl.Element("AirLine").Element("Identification").Value.Trim() && x.OperatingCarrier.Trim() == fltdtl.Element("AirLine").Element("Code").Value.Trim() && x.DepartureLocation.Trim() == fltdtl.Element("Origin").Element("AirportCode").Value.Trim() && x.ArrivalLocation.Trim() == fltdtl.Element("Destination").Element("AirportCode").Value.Trim()))
                                            {
                                                // objfsr.sno += "Ref!" + ItqDocument.Element("PricingResponse").Element("ReferenceNo").Value;
                                                float srvChargeAdt = 0, srvChargeChd = 0;
                                                #region Adult
                                                IEnumerable<XElement> ADTFareBreakDown = Priceetenry.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "ADT");
                                                IEnumerable<XElement> ADTFareInfos = Priceetenry.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "ADT");

                                                #region taxes
                                                foreach (var Taxes in ADTFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                                {
                                                    if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                    {
                                                        objfsr.AdtFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                    {
                                                        objfsr.AdtOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    if (Taxes.Attribute("TaxCode").Value == "K3")
                                                    {
                                                        objfsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                    {
                                                        objfsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                    {
                                                        objfsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                    else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                    {
                                                        objfsr.AdtYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                    }
                                                }
                                                #endregion

                                                objfsr.fareBasis = ADTFareInfos.ElementAt(0).Element("FareBasis").Value;
                                                objfsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));
                                                objfsr.AdtCabin = ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                                objfsr.AdtRbd = ADTFareInfos.ElementAt(0).Element("PaxBookingClass").Value;
                                                objfsr.AdtFarebasis = ADTFareInfos.ElementAt(0).Element("PaxFareBasis").Value;

                                                #region Get MISC Markup Charges
                                                try
                                                {
                                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, OFareType, Convert.ToString(objfsr.AdtBfare), Convert.ToString(objfsr.AdtFSur));
                                                }
                                                catch { srvChargeAdt = 0; }
                                                #endregion

                                                objfsr.AdtWO = 0;
                                                objfsr.AdtTax = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeAdt;
                                                //objfsr.AdtOT = objfsr.AdtTax;
                                                objfsr.AdtFareType = ADTFareBreakDown.ElementAt(0).Attribute("Refundable").Value;
                                                objfsr.AdtFare = objfsr.AdtTax + objfsr.AdtBfare;

                                                objfsr.FBPaxType = "ADT";
                                                objfsr.ElectronicTicketing = "";
                                                objfsr.AdtFar = Priceetenry.Attribute("FareType").Value;
                                                OFareType= Priceetenry.Attribute("FareType").Value;
                                                objfsr.ADTAdminMrk = 0;
                                                objfsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.AdtFare, fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper());
                                                if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == JourneyType.I.ToString() && objfsr.IsCorp == true)
                                                {
                                                    objfsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper());
                                                    objfsr.AdtBfare = objfsr.AdtBfare + objfsr.ADTAdminMrk;
                                                    objfsr.AdtFare = objfsr.AdtFare + objfsr.ADTAdminMrk;
                                                }
                                                #region Commision and TDS
                                                CommDt.Clear();
                                                STTFTDS.Clear();
                                                try
                                                {
                                                    if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                                    {
                                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                                        if (objfsr.Leg == 1)
                                                        {
                                                            //CommDt = objFltComm.GetFltComm_WithouDB(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.fareBasis, objfsr.OrgDestFrom, objfsr.OrgDestTo, objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Availibility.Element("PricingInfos").Elements("PricingInfo").Count().ToString());
                                                            CommDt = objFltComm.GetFltComm_WithouDB(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), 1, objfsr.AdtRbd, objfsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.fareBasis, objfsr.OrgDestFrom, objfsr.OrgDestTo, objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, OFareType, Availibility.Element("PricingInfos").Elements("PricingInfo").Count().ToString(), contx, "TQ");
                                                           
                                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                                            {
                                                                objfsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                                objfsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                STTFTDS.Clear();
                                                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objfsr.ValiDatingCarrier, objfsr.AdtDiscount1, objfsr.AdtBfare, objfsr.AdtFSur, TDS);
                                                                objfsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                                objfsr.AdtDiscount = objfsr.AdtDiscount1 - objfsr.AdtSrvTax1;
                                                                objfsr.AdtEduCess = 0;
                                                                objfsr.AdtHighEduCess = 0;
                                                                objfsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                objfsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                            }
                                                            else
                                                            {
                                                                objfsr.AdtDiscount1 = 0;
                                                                objfsr.AdtCB = 0;

                                                                objfsr.AdtSrvTax1 = 0;
                                                                objfsr.AdtDiscount = 0;
                                                                objfsr.AdtEduCess = 0;
                                                                objfsr.AdtHighEduCess = 0;
                                                                objfsr.AdtTF = 0;
                                                                objfsr.AdtTds = 0;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            objfsr.AdtDiscount1 = 0;
                                                            objfsr.AdtCB = 0;

                                                            objfsr.AdtSrvTax1 = 0;
                                                            objfsr.AdtDiscount = 0;
                                                            objfsr.AdtEduCess = 0;
                                                            objfsr.AdtHighEduCess = 0;
                                                            objfsr.AdtTF = 0;
                                                            objfsr.AdtTds = 0;
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        objfsr.AdtDiscount1 = 0;
                                                        objfsr.AdtCB = 0;

                                                        objfsr.AdtSrvTax1 = 0;
                                                        objfsr.AdtDiscount = 0;
                                                        objfsr.AdtEduCess = 0;
                                                        objfsr.AdtHighEduCess = 0;
                                                        objfsr.AdtTF = 0;
                                                        objfsr.AdtTds = 0;
                                                    }
                                                }
                                                catch (Exception es)
                                                {
                                                    objfsr.AdtDiscount1 = 0;
                                                    objfsr.AdtCB = 0;

                                                    objfsr.AdtSrvTax1 = 0;
                                                    objfsr.AdtDiscount = 0;
                                                    objfsr.AdtEduCess = 0;
                                                    objfsr.AdtHighEduCess = 0;
                                                    objfsr.AdtTF = 0;
                                                    objfsr.AdtTds = 0;
                                                }
                                                if (objfsr.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.AdtBfare.ToString()), decimal.Parse(objfsr.AdtFSur.ToString()), fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper(), decimal.Parse(objfsr.AdtFare.ToString()));
                                                        objfsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }
                                                #endregion

                                                decimal AdtOrgfare = Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("TotalFare").Value.Trim()));
                                                #endregion
                                                #region Child
                                                decimal ChdOrgfare = 0;
                                                IEnumerable<XElement> CHDFareBreakDown = Priceetenry.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "CHD");
                                                IEnumerable<XElement> CHDFareInfos = Priceetenry.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "CHD");

                                                if (objfsr.Child > 0)
                                                {
                                                    #region taxes
                                                    foreach (var Taxes in CHDFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                                    {
                                                        if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                        {
                                                            objfsr.ChdFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                        {
                                                            objfsr.ChdOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        if (Taxes.Attribute("TaxCode").Value == "K3")
                                                        {
                                                            objfsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                        {
                                                            objfsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                        {
                                                            objfsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                        {
                                                            objfsr.ChdYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                    }
                                                    #endregion
                                                    objfsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(ADTFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));

                                                    #region Get MISC Markup Charges
                                                    try
                                                    {
                                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, objfsr.ValiDatingCarrier, OFareType, Convert.ToString(objfsr.ChdBFare), Convert.ToString(objfsr.ChdFSur));
                                                    }
                                                    catch { srvChargeChd = 0; }
                                                    #endregion

                                                    objfsr.ChdTax = (float)Math.Ceiling(Convert.ToDecimal(CHDFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeChd;
                                                    objfsr.ChdCabin = CHDFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                                    objfsr.ChdRbd = CHDFareInfos.ElementAt(0).Element("PaxBookingClass").Value; ;
                                                    objfsr.ChdFarebasis = CHDFareInfos.ElementAt(0).Element("PaxFareBasis").Value;
                                                    objfsr.ChdfareType = CHDFareBreakDown.ElementAt(0).Attribute("Refundable").Value;
                                                    ChdOrgfare = Math.Ceiling(Convert.ToDecimal(CHDFareBreakDown.ElementAt(0).Element("TotalFare").Value.Trim()));
                                                }
                                                else
                                                {
                                                    objfsr.ChdBFare = 0;
                                                    objfsr.ChdTax = 0;
                                                    objfsr.ChdCabin = "";
                                                    objfsr.ChdRbd = "";
                                                    objfsr.ChdFarebasis = "";
                                                }
                                                objfsr.ChdFare = objfsr.ChdTax + objfsr.ChdBFare;
                                                objfsr.ChdFar = Priceetenry.Attribute("FareType").Value;
                                                objfsr.CHDAdminMrk = 0;
                                                objfsr.CHDAgentMrk = 0;
                                                if (objfsr.Child > 0)
                                                {
                                                    objfsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper());
                                                    if ((fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == JourneyType.I.ToString()) && (objfsr.IsCorp == true))
                                                    {
                                                        objfsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objfsr.ValiDatingCarrier, objfsr.ChdFare, fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper());
                                                        objfsr.ChdBFare = objfsr.ChdBFare + objfsr.CHDAdminMrk;
                                                        objfsr.ChdFare = objfsr.ChdFare + objfsr.CHDAdminMrk;
                                                    }
                                                }
                                                #region child commision and DTS
                                                CommDt.Clear();
                                                STTFTDS.Clear();
                                                try
                                                {
                                                    if (fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                                                    {
                                                        #region Show hide Net Fare and Commission Calculation-Child Pax
                                                        if (objfsr.Leg == 1)
                                                        {
                                                            //CommDt = objFltComm.GetFltComm_WithouDB(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, fltdt.Rows[0]["DepartureDate"].ToString(), objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, fltdt.Rows[0]["DepartureDate"].ToString(), objfsr.fareBasis, objfsr.OrgDestFrom, objfsr.OrgDestTo, objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, "NRM", Availibility.Element("PricingInfos").Elements("PricingInfo").Count().ToString());
                                                            CommDt = objFltComm.GetFltComm_WithouDB(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), 1, objfsr.ChdRbd, objfsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.OrgDestFrom + "-" + objfsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), objfsr.fareBasis, objfsr.OrgDestFrom, objfsr.OrgDestTo, objfsr.FlightIdentification, objfsr.OperatingCarrier, objfsr.MarketingCarrier, OFareType, Availibility.Element("PricingInfos").Elements("PricingInfo").Count().ToString(), contx, "TQ");
                                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                                            {
                                                                objfsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                                objfsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                                STTFTDS.Clear();
                                                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objfsr.ValiDatingCarrier, objfsr.ChdDiscount1, objfsr.ChdBFare, objfsr.ChdFSur, TDS);
                                                                objfsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                                objfsr.ChdDiscount = objfsr.ChdDiscount1 - objfsr.ChdSrvTax1;
                                                                objfsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                                objfsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                            }
                                                            else
                                                            {
                                                                objfsr.ChdEduCess = 0;
                                                                objfsr.ChdHighEduCess = 0;
                                                                objfsr.ChdDiscount1 = 0;
                                                                objfsr.ChdCB = 0;
                                                                objfsr.ChdSrvTax1 = 0;
                                                                objfsr.ChdDiscount = 0;
                                                                objfsr.ChdTF = 0;
                                                                objfsr.ChdTds = 0;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            objfsr.ChdEduCess = 0;
                                                            objfsr.ChdHighEduCess = 0;
                                                            objfsr.ChdDiscount1 = 0;
                                                            objfsr.ChdCB = 0;
                                                            objfsr.ChdSrvTax1 = 0;
                                                            objfsr.ChdDiscount = 0;
                                                            objfsr.ChdTF = 0;
                                                            objfsr.ChdTds = 0;
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        objfsr.ChdEduCess = 0;
                                                        objfsr.ChdHighEduCess = 0;
                                                        objfsr.ChdDiscount1 = 0;
                                                        objfsr.ChdCB = 0;
                                                        objfsr.ChdSrvTax1 = 0;
                                                        objfsr.ChdDiscount = 0;
                                                        objfsr.ChdTF = 0;
                                                        objfsr.ChdTds = 0;
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    objfsr.ChdEduCess = 0;
                                                    objfsr.ChdHighEduCess = 0;
                                                    objfsr.ChdDiscount1 = 0;
                                                    objfsr.ChdCB = 0;
                                                    objfsr.ChdSrvTax1 = 0;
                                                    objfsr.ChdDiscount = 0;
                                                    objfsr.ChdTF = 0;
                                                    objfsr.ChdTds = 0;
                                                }
                                                #endregion

                                                if (objfsr.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.ChdBFare.ToString()), decimal.Parse(objfsr.ChdFSur.ToString()), fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper(), decimal.Parse(objfsr.ChdFare.ToString()));
                                                        objfsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        objfsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }
                                                #endregion
                                                #region Infant
                                                if (objfsr.Infant > 0)
                                                {
                                                    IEnumerable<XElement> INFFareBreakDown = Priceetenry.Element("FareBreakDowns").Elements("FareBreakDown").Where(x => x.Attribute("PaxType").Value == "INF");
                                                    IEnumerable<XElement> INFFareInfos = Priceetenry.Element("FareInfos").Elements("FareInfo").Where(x => x.Attribute("PaxType").Value == "INF");
                                                    #region taxes
                                                    foreach (var Taxes in INFFareBreakDown.ElementAt(0).Element("Taxes").Elements("Tax"))
                                                    {
                                                        if (Taxes.Attribute("TaxCode").Value == "YQ")
                                                        {
                                                            objfsr.InfFSur = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "OT")
                                                        {
                                                            objfsr.InfOT = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        if (Taxes.Attribute("TaxCode").Value == "K3")
                                                        {
                                                            objfsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "WO")
                                                        {
                                                            objfsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "IN")
                                                        {
                                                            objfsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                        else if (Taxes.Attribute("TaxCode").Value == "YM")
                                                        {
                                                            objfsr.InfYR = (float)Math.Ceiling(Convert.ToDecimal(Taxes.Attribute("Amount").Value.Trim()));
                                                        }
                                                    }
                                                    #endregion
                                                    objfsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(INFFareBreakDown.ElementAt(0).Element("BaseFare").Value.Trim()));
                                                    objfsr.AdtCabin = INFFareInfos.ElementAt(0).Element("Cabin").Value.Trim() == "" ? "Y" : ADTFareInfos.ElementAt(0).Element("Cabin").Value.Trim();
                                                    objfsr.AdtRbd = "";
                                                    objfsr.AdtFarebasis = INFFareInfos.ElementAt(0).Element("PaxFareBasis").Value;

                                                    objfsr.InfTax = (float)Math.Ceiling(Convert.ToDecimal(INFFareBreakDown.ElementAt(0).Element("TotalTax").Value.Trim())) + srvChargeChd;
                                                    objfsr.InfRbd = "";
                                                    objfsr.InfFarebasis = INFFareInfos.ElementAt(0).Element("PaxFareBasis").Value;
                                                    objfsr.InfFare = objfsr.InfTax + objfsr.InfBfare;
                                                    objfsr.InfFar = "NRM";
                                                    if (objfsr.IsCorp == true)
                                                    {
                                                        try
                                                        {
                                                            DataTable MGDT = new DataTable();
                                                            MGDT = objFltComm.clac_MgtFee(agentType, objfsr.ValiDatingCarrier, decimal.Parse(objfsr.InfBfare.ToString()), decimal.Parse(objfsr.InfFSur.ToString()), fltdt.Rows[0]["Trip"].ToString().Trim().ToUpper(), decimal.Parse(objfsr.InfFare.ToString()));
                                                            objfsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                            objfsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                        }
                                                        catch { }
                                                    }

                                                }
                                                #endregion

                                                objfsr.OriginalTF = (float)(AdtOrgfare * objfsr.Adult) + (float)(ChdOrgfare * objfsr.Child) + (objfsr.InfFare * objfsr.Infant);
                                                objfsr.OriginalTT = (srvChargeAdt * objfsr.Adult) + (srvChargeChd * objfsr.Child);

                                                //Rounding the fare
                                                objfsr.AdtFare = (float)Math.Ceiling(objfsr.AdtFare);
                                                objfsr.ChdFare = (float)Math.Ceiling(objfsr.ChdFare);
                                                objfsr.InfFare = (float)Math.Ceiling(objfsr.InfFare);

                                                objfsr.RBD = objfsr.AdtRbd + ":" + objfsr.ChdRbd + ":" + objfsr.InfRbd;
                                                objfsr.STax = (objfsr.AdtSrvTax * objfsr.Adult) + (objfsr.ChdSrvTax * objfsr.Child) + (objfsr.InfSrvTax * objfsr.Infant);
                                                objfsr.TFee = (objfsr.AdtTF * objfsr.Adult) + (objfsr.ChdTF * objfsr.Child) + (objfsr.InfTF * objfsr.Infant);
                                                objfsr.TotDis = (objfsr.AdtDiscount * objfsr.Adult) + (objfsr.ChdDiscount * objfsr.Child);
                                                objfsr.TotCB = (objfsr.AdtCB * objfsr.Adult) + (objfsr.ChdCB * objfsr.Child);
                                                objfsr.TotTds = (objfsr.AdtTds * objfsr.Adult) + (objfsr.ChdTds * objfsr.Child) + objfsr.InfTds;
                                                objfsr.TotMgtFee = (objfsr.AdtMgtFee * objfsr.Adult) + (objfsr.ChdMgtFee * objfsr.Child) + (objfsr.InfMgtFee * objfsr.Infant);

                                                objfsr.TotalFare = (objfsr.Adult * objfsr.AdtFare) + (objfsr.Child * objfsr.ChdFare) + (objfsr.Infant * objfsr.InfFare);
                                                objfsr.TotalFuelSur = (objfsr.Adult * objfsr.AdtFSur) + (objfsr.Child * objfsr.ChdFSur) + (objfsr.Infant * objfsr.InfFSur);
                                                objfsr.TotalTax = (objfsr.Adult * objfsr.AdtTax) + (objfsr.Child * objfsr.ChdTax) + (objfsr.Infant * objfsr.InfTax);
                                                objfsr.TotBfare = (objfsr.Adult * objfsr.AdtBfare) + (objfsr.Child * objfsr.ChdBFare) + (objfsr.Infant * objfsr.InfBfare);
                                                objfsr.TotMrkUp = (objfsr.ADTAdminMrk * objfsr.Adult) + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAdminMrk * objfsr.Child) + (objfsr.CHDAgentMrk * objfsr.Child);
                                                objfsr.TotalFare = objfsr.TotalFare + objfsr.TotMrkUp + objfsr.STax + objfsr.TFee + objfsr.TotMgtFee;
                                                objfsr.NetFare = (objfsr.TotalFare + objfsr.TotTds) - (objfsr.TotDis + objfsr.TotCB + (objfsr.ADTAgentMrk * objfsr.Adult) + (objfsr.CHDAgentMrk * objfsr.Child));

                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ITQAirApiItineraryPrice", "Error_001", ex, "ITQFlight");
            }
            return objlist;
        }

        public string AvailibilityRequest(FlightSearch objFlt, string fareType, string Sessionids)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                if (objFlt.Trip == Trip.D)
                    objreqXml.Append("{ \"Trip\": 1 , ");
                else
                    objreqXml.Append("{\"Trip\": 2 ,");
                if (objFlt.TripType.ToString() == "RoundTrip" && (objFlt.RTF == true || objFlt.Trip == Trip.I))
                    objreqXml.Append("\"TripType\": 2 ,");
                else
                    objreqXml.Append("\"TripType\": 1 , ");

                objreqXml.Append("\"Adult\": " + objFlt.Adult + ", ");
                // if (objFlt.Child > 0)
                    objreqXml.Append("\"Child\": " + objFlt.Child + ", ");
                //if (objFlt.Infant > 0)
                    objreqXml.Append("\"Infant\": " + objFlt.Infant + ", ");
                if (objFlt.TripType.ToString() == "RoundTrip" && (objFlt.RTF == true || objFlt.Trip == Trip.I))
                    objreqXml.Append("\"NonStop\": false,\"RTF\":true, ");
                else
                objreqXml.Append("\"NonStop\": false,\"RTF\": " + objFlt.RTF.ToString().ToLower() + ", ");

                if (objFlt.Cabin != "")
                    objreqXml.Append("\"PreferredClass\": \"" + objFlt.Cabin + "\", ");
                else
                    objreqXml.Append("\"PreferredClass\": \"\", ");
                if (objFlt.HidTxtAirLine == "OF" || objFlt.AirLine == "OF")
                {
                    objFlt.AirLine = "";
                    objFlt.HidTxtAirLine = "";
                }
                if (objFlt.AirLine != "")
                    objreqXml.Append("\"PreferredCarrier\": \"" + objFlt.AirLine.Split(',')[1] + "\", ");
                else
                    objreqXml.Append("\"PreferredCarrier\": \"" + objFlt.HidTxtAirLine + "\", ");

                objreqXml.Append("\"SessionID\": \"" + Sessionids + "\", ");
                objreqXml.Append("\"Segments\": [ {  ");
                if (fareType == "IB")
                {
                    string BeganDate = Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2);
                    objreqXml.Append("\"Origin\": \"" + objFlt.HidTxtArrCity.Split(',')[0].Trim() + "\", \"Destination\": \"" + objFlt.HidTxtDepCity.Split(',')[0].Trim() + "\", ");
                    objreqXml.Append("\"DepartDate\": \"" + objFlt.RetDate + "\"");
                }
                else if (fareType == "OB")
                {
                    string BeganDate = Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2);
                    objreqXml.Append("\"Origin\": \"" + objFlt.HidTxtDepCity.Split(',')[0].Trim() + "\" , \"Destination\": \"" + objFlt.HidTxtArrCity.Split(',')[0].Trim() + "\", ");
                    objreqXml.Append("\"DepartDate\": \"" + objFlt.DepDate + "\"");
                }
                objreqXml.Append("}");

                if (objFlt.TripType.ToString() == "RoundTrip" && (objFlt.RTF == true || objFlt.Trip == Trip.I))
                {
                    string BeganDate = Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2);
                    objreqXml.Append(", {\"Origin\": \"" + objFlt.HidTxtArrCity.Split(',')[0].Trim() + "\" , \"Destination\": \"" + objFlt.HidTxtDepCity.Split(',')[0].Trim() + "\", ");
                    objreqXml.Append("\"DepartDate\": \"" + objFlt.RetDate + "\"}");
                }

                objreqXml.Append("]}");

            }
            catch (Exception ex)
            { }
            return objreqXml.ToString();
        }
        public string PricingRequest(string ClientCode, string SessionID, string Key, string Pricingkey, string ResultIndex, string Provider)
        {
            StringBuilder objreqJsion = new StringBuilder();
            objreqJsion.Append("{ \"ClientCode\": \"" + ClientCode + "\", \"SessionID\": \"" + SessionID + "\",");
            objreqJsion.Append(" \"Key\": \"" + Key + "\", \"Pricingkey\": \"" + Pricingkey + "\",");
            objreqJsion.Append(" \"ResultIndex\": \"" + ResultIndex + "\", \"Provider\": \"" + Provider + "\" }");
            return objreqJsion.ToString();
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            //decimal IATAComm = 0;
            decimal IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                if (StNew.Count > 0)
                {
                    STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                    TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                    IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                }
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                //STHT.Add("Tds", Math.Round((double.Parse(Dis.ToString()) * double.Parse(TDS)) / 100, 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
        {
            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;

            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln));
                        ln++;
                    }

                }

            }
            return Final;
        }
        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;
            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;

            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = item.OriginalTT + itemib.OriginalTT;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }


        public string ItqAirApiFareRules(string constr, string sno, string AirCode, string DepartDate, string DepartTime, string PriceKey, string User_id)
        {
            string FareRulestr = "";
            try
            {
                STD.DAL.Credentials objCrd = new STD.DAL.Credentials(constr);
                List<CredentialList> CrdList = objCrd.GetServiceCredentials("TQ");
                if (CrdList.Count > 0)
                {
                    string[] SnoSplit = new string[] { "Ref!" };
                    string[] SnoSplitDetails = sno.Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);

                    if (SnoSplitDetails[4] == "true")
                    {
                        string FareRulesRequest = "{ \"ClientCode\": \"" + CrdList[0].UserID + "\", \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"SupplierCode\": \"" + SnoSplitDetails[2] + "\", \"AirCode\": \"" + AirCode + "\", \"PnrNO\" : \"\", \"DepartDate\": \"\", \"DepartTime\": \"\" }";

                        string FareRulesResponse = ITQAirApiUtility.PostJsionXML(CrdList[0].LoginPWD.Replace("Availability", "FareRule"), "application/xml; charset=utf-8", FareRulesRequest);
                        ITQAirApiUtility.SaveFile(FareRulesRequest, "FareRulesRequest_", User_id, AirCode);
                        ITQAirApiUtility.SaveFile(FareRulesResponse, "FareRulesResponse_", User_id, AirCode);

                        if (FareRulesResponse.Contains("<Status>"))
                        {
                            XDocument ItqDocument = XDocument.Parse(FareRulesResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            if (ItqDocument.Element("FareRuleResponse").Element("Status").Value.Trim() == "Success")
                            {
                                XElement FareRuleResponse = ItqDocument.Element("FareRuleResponse");
                                foreach (var CancelPolicy in FareRuleResponse.Element("CancelPolicy").Elements("CancelFareRule"))
                                {
                                    FareRulestr += "If you cancel between " + CancelPolicy.Element("TimeFrame").Value + " , " + CancelPolicy.Element("AirlineFee").Value + " charge applicable. </br>";
                                }
                                foreach (var CancelPolicy in FareRuleResponse.Element("DateChange").Elements("DateChangeFareRule"))
                                {
                                    FareRulestr += "Date Change between " + CancelPolicy.Element("TimeFrame").Value + " , " + CancelPolicy.Element("AirlineFee").Value + " AirlineFee applicable. </br>";
                                }
                            }
                        }
                    }
                    else
                    {
                        string FareRulesRequest = "{ \"UserID\": \"" + CrdList[0].UserID + "\", \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"Pricingkey\": \"" + PriceKey + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\", \"ResultIndex\": \"" + SnoSplitDetails[3] + "\" }";
                        string FareRulesResponse = ITQAirApiUtility.PostJsionXML(CrdList[0].LoginPWD.Replace("Availability", "Non-LccFareRule"), "application/xml; charset=utf-8", FareRulesRequest);
                        ITQAirApiUtility.SaveFile(FareRulesRequest, "FareRulesRequest_", User_id, AirCode);
                        ITQAirApiUtility.SaveFile(FareRulesResponse, "FareRulesResponse_", User_id, AirCode);

                        if (FareRulesResponse.Contains("<Status>"))
                        {
                            XDocument ItqDocument = XDocument.Parse(FareRulesResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            if (ItqDocument.Element("FareRulesFlightRequest").Element("Status").Value.Trim() == "Success")
                            {
                                foreach (var CancelPolicy in ItqDocument.Element("FareRulesFlightRequest").Element("FareRulesFlight").Elements("FareRulesFlight"))
                                {
                                    // FareRulestr += "Date Change between " + CancelPolicy.Element("TimeFrame").Value + " , " + CancelPolicy.Element("AirlineFee").Value + " AirlineFee applicable. </br>";
                                    FareRulestr += CancelPolicy.Element("Rule").Value + " " + CancelPolicy.Element("DepartureType").Value;
                                    if (CancelPolicy.Element("FeeAmount").Value == "NR" || CancelPolicy.Element("FeeAmount").Value == "NP")
                                        FareRulestr += " Not Allowed.";
                                    else
                                        FareRulestr += " " + CancelPolicy.Element("FeeCurrency").Value + " " + CancelPolicy.Element("FeeAmount").Value + " charge Applicable.";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ItqAirApiFareRules", "Error_001", ex, "ITQAirApi");
            }
            return FareRulestr;
        }

    }
}