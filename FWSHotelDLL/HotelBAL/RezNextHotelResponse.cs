﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using HotelShared;
using HotelDAL;
using System.Xml;

namespace HotelBAL
{
  public  class RezNextHotelResponse
  {
      RezNextRequest objreq = new RezNextRequest();
      public HotelComposite RZHotel(HotelSearch SearchDetails)
      {
          HotelComposite obgHotelsCombo = new HotelComposite();
          try
          {
              if (SearchDetails.CountryCode == "IN" && SearchDetails.RezNextUrl != null && (SearchDetails.RezNextTrip == SearchDetails.HtlType || SearchDetails.RezNextTrip == "ALL"))
              {
                  SearchDetails.RZ_HotelSearchReq = objreq.RZHotelSearchRequest(SearchDetails);
                  SearchDetails.RZ_HotelSearchRes = RezNextPostXml(SearchDetails.RezNextUrl + "GetHotelInformation", SearchDetails.RZ_HotelSearchReq);
                  obgHotelsCombo.Hotelresults = RZHotelsPrice(SearchDetails);
              }
              else
              {
                  SearchDetails.RZ_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                  SearchDetails.RZ_HotelSearchReq = "not allow";
                  List<HotelResult> objHotellist = new List<HotelResult>();
                  objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.RZ_HotelSearchRes });
                  obgHotelsCombo.Hotelresults = objHotellist;
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RezNextHotelSearchResponse");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          obgHotelsCombo.HotelSearchDetail = SearchDetails;
          return obgHotelsCombo;
      }
      protected List<HotelResult> RZHotelsPrice(HotelSearch SearchDetails)
      {
          HotelDA objhtlDa = new HotelDA(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
          List<HotelResult> objHotellist = new List<HotelResult>();
          try
          {
              if (SearchDetails.RZ_HotelSearchRes.Contains("<Success/>"))
              {
                  string responses = SearchDetails.RZ_HotelSearchRes.Replace(" xmlns=\"http://schemas.xmlsoap.org/soap/envelope\"", String.Empty);
                  XDocument xmlresp = XDocument.Parse(responses);
                  var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                    select new { RoomTypes = htlprice.Element("RoomTypes"), RatePlans = htlprice.Element("RatePlans"), RoomRates = htlprice.Element("RoomRates"), BasicPropertyInfo = htlprice.Element("BasicPropertyInfo") }).ToList();
                  if (hotelprice.Count > 0)
                  {
                      int k = 1505;
                      foreach (var hoteldetails in hotelprice)
                      {
                          try 
                          {
                              string htlAddtrss = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine2") != null)
                                  if(hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine2").Value.Trim() !="")
                                      htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine2").Value.Trim();

                              htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode") != null)
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value.Trim() != "")
                                        htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Element("State") != null)
                                  htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("State").Value.Trim();

                              htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName").Value.Trim();
                                           
                              string HtlLatLong = "##", HotelServises="";
                              if (hoteldetails.BasicPropertyInfo.Element("Position") != null)
                                  HtlLatLong = hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude").Value + "##" + hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Longitude").Value;
                                          
                             foreach( var htlaminity in hoteldetails.BasicPropertyInfo.Element("HotelFeatures").Descendants("FeatureDescription"))
                             {
                                 if (htlaminity.Attribute("Code") != null)
                                        HotelServises += htlaminity.Attribute("Code").Value.Trim() + "#";
                             }
                             foreach(var htldtlrates in hoteldetails.RoomRates.Elements("RoomRate"))
                             {
                                 decimal baserate = 0, discount = 0, TotaldiscAmt = 0, ExtraGuest = 0, RackExtraGuest = 0; int c = 0;
                                 string RoomTypeCode = "", Inclusion="", HtlDescription="";
                                 foreach(var roomrates in htldtlrates.Elements("Rates") )
                                 {
                                      IEnumerable<string> hoteltype = from Rt in hoteldetails.RoomTypes.Descendants("RoomType")
                                                                     where Rt.Attribute("RoomTypeCode").Value == roomrates.Attribute("RoomTypeCode").Value
                                                                     select Rt.Attribute("MaxPersons").Value.ToString();
                                      if ((Convert.ToInt32(SearchDetails.AdtPerRoom[0]) + Convert.ToInt32(SearchDetails.ChdPerRoom[0])) <= Convert.ToInt32(hoteltype.ElementAt(0)))
                                      {
                                          decimal pricenow = 0, disamt = 0;
                                          if (Convert.ToInt32(SearchDetails.AdtPerRoom[0]) == 1 && Convert.ToInt32(SearchDetails.ChdPerRoom[0]) == 0)
                                          {
                                              pricenow = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Base").Attribute("SingleRate").Value.Trim());
                                              disamt = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Rack").Attribute("SingleRate").Value.Trim());
                                          }
                                          else
                                          {
                                              pricenow = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Base").Attribute("DoubleRate").Value.Trim());
                                              disamt = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Rack").Attribute("DoubleRate").Value.Trim());
                                          }
                                          if (c == 0)
                                          {
                                              baserate = pricenow; discount = disamt;
                                              RoomTypeCode = roomrates.Attribute("RoomTypeCode").Value + "|" + roomrates.Element("Inclusion").Attribute("MealPlanCode").Value.Trim(); c = 1;
                                              Inclusion = roomrates.Element("Inclusion").Attribute("MealPlanDescription").Value.Trim();
                                          }
                                          if (pricenow < baserate)
                                          {
                                              baserate = pricenow; discount = disamt;
                                              RoomTypeCode = roomrates.Attribute("RoomTypeCode").Value + "|" + roomrates.Element("Inclusion").Attribute("MealPlanCode").Value.Trim();
                                              Inclusion = roomrates.Element("Inclusion").Attribute("MealPlanDescription").Value.Trim();
                                          }
                                          if (Convert.ToInt32(SearchDetails.AdtPerRoom[0]) > 2)
                                          {
                                              ExtraGuest = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Base").Attribute("ExtraAdult").Value.Trim());
                                              RackExtraGuest = Convert.ToDecimal(roomrates.Element("Inclusion").Element("Rate").Element("Rack").Attribute("ExtraAdult").Value.Trim());
                                              if (Convert.ToInt32(SearchDetails.AdtPerRoom[0]) > 3)
                                              {
                                                  ExtraGuest = ExtraGuest * (Convert.ToInt32(SearchDetails.AdtPerRoom[0]) - 2);
                                                  RackExtraGuest = RackExtraGuest * (Convert.ToInt32(SearchDetails.AdtPerRoom[0]) - 2);
                                              }
                                          }
                                      }
                                      else
                                          goto maxGuest;
                                 }
                                 MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, baserate + ExtraGuest, SearchDetails.RZ_servicetax);
                                 if (discount > baserate)
                                     TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, discount + RackExtraGuest, SearchDetails.RZ_servicetax);
                                 if (hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value !="")
                                     HtlDescription="<strong>Description</strong>: " + hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value.Replace("/N", "");
                                  objHotellist.Add(new HotelResult
                                  {
                                      RatePlanCode = htldtlrates.Attribute("RatePlanCode").Value,
                                      hotelDiscoutAmt = TotaldiscAmt,
                                      hotelPrice = MarkList.TotelAmt,
                                      AgtMrk = MarkList.AgentMrkAmt,
                                      DiscountMsg = "",//objRatePlans.discountMsg,
                                      inclusions = Inclusion,
                                      RoomTypeCode = RoomTypeCode,
                                      HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value,
                                      HotelName = hoteldetails.BasicPropertyInfo.Attribute("HotelName").Value,
                                      HotelCityCode = SearchDetails.SearchCityCode,
                                      Location = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("Area").Value.Trim(),
                                      HotelCity = hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value.Trim(),
                                      HotelAddress = htlAddtrss,
                                      StarRating = hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value.Trim(),
                                      HotelDescription = HtlDescription,
                                      HotelThumbnailImg = hoteldetails.BasicPropertyInfo.Element("HotelImages").Element("ImagePath").Value.Trim(),
                                      Lati_Longi = HtlLatLong,
                                      HotelServices = HotelServises,
                                      ReviewRating = "",
                                      Provider = "RZ",
                                      PopulerId = k++
                                  });
                              maxGuest:
                                  string s = "";
                            }
                         }
                         catch (Exception ex)
                         {
                             HotelDA.InsertHotelErrorLog(ex, "RezNext Result_List-" + hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value + "-" + SearchDetails.SearchCity);
                            objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                         }
                    }
                }
                else
                {
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found,  Please modify your search.. " });
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }
              }
              else
              {
                  if (SearchDetails.RZ_HotelSearchRes.Contains("<Errors>"))
                  {
                      int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");

                      XDocument document = XDocument.Parse(SearchDetails.RZ_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                      var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                      if (Hotels_temps.Count > 0)
                      {
                          if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                              objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                      }
                  }
                  else
                  {
                      int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                      objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Rooms Not Available" });
                  }
              }
          }
          catch (Exception ex)
          {
              objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
              HotelDA.InsertHotelErrorLog(ex, "GetGTAHotelsPrice " + SearchDetails.SearchCity);
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          return objHotellist;
      }
      //public RoomComposite TGSelectedHotelResponse(HotelSearch SearchDetails)
      //{
      //    RoomComposite objroomlist = new RoomComposite();
      //    try
      //    {
      //        SearchDetails = TGHotels(SearchDetails);
      //        objroomlist = GetTGRoomList(SearchDetails.TG_HotelSearchRes, SearchDetails);
      //    }
      //    catch (Exception ex)
      //    {
      //        HotelDA.InsertHotelErrorLog(ex, "TGSelectedHotelResponse");
      //        HotelDA objhtlDa = new HotelDA();
      //        int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
      //    }
      //    return objroomlist;
      //}

      protected string RezNextPostXml(string url, string xml)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
              HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
              if (!string.IsNullOrEmpty(xml))
              {
                  Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  Http.Method = "POST";
                  byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                  Http.ContentLength = lbPostBuffer.Length;
                  Http.ContentType = "text";
                //  Http.Timeout = 56000;
                  using (Stream PostStream = Http.GetRequestStream())
                  {
                      PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                  }
              }

              using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
              {
                  if (WebResponse.StatusCode != HttpStatusCode.OK)
                  {
                      string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                      throw new ApplicationException(message);
                  }
                  else
                  {
                      Stream responseStream = WebResponse.GetResponseStream();
                      if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                      {
                          responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                      }
                      else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                      {
                          responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                      }
                      StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                      sbResult.Append(reader.ReadToEnd());
                      responseStream.Close();
                  }
              }
          }
          catch (WebException WebEx)
          {
              HotelDA objhtlDa = new HotelDA();
              HotelDA.InsertHotelErrorLog(WebEx, "RezNextPostXml");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "RZ", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "RZ", "HotelInsert");
                  sbResult.Append(WebEx.Message + "<Errors>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<Errors>");
              HotelDA.InsertHotelErrorLog(ex, "RezNextPostXml");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "RZ", "HotelInsert");
          }
          return sbResult.ToString();
      }

      public RoomComposite RZRoomList(HotelSearch SearchDetails)
      {
          List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
          RoomComposite objRoomDetals = new RoomComposite(); HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA();
          try
          {
                  string responses = SearchDetails.RZ_HotelSearchRes.Replace(" xmlns=\"http://schemas.xmlsoap.org/soap/envelope\"", String.Empty);
                  XDocument xmlresp = XDocument.Parse(responses);
                  var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay") where htlprice.Element("BasicPropertyInfo").Attribute("HotelCode").Value.Trim() == SearchDetails.HtlCode
                      select new { RoomTypes = htlprice.Element("RoomTypes"), RatePlans = htlprice.Element("RatePlans"), RoomRates = htlprice.Element("RoomRates"), BasicPropertyInfo = htlprice.Element("BasicPropertyInfo") }).ToList();
                 
                  if (hotelprice.Count > 0)
                  {
                      foreach (var hoteldetails in hotelprice)
                      {
                          #region Hotel Details
                          try
                          {
                              HotelDetail.HotelName = hoteldetails.BasicPropertyInfo.Attribute("HotelName").Value;
                              HotelDetail.HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                              HotelDetail.StarRating = hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value.Trim();
                              SearchDetails.StarRating = HotelDetail.StarRating;
                              try
                              {
                                  string htlAddtrss = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1").Value.Trim();
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1") != null)
                                      if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine2").Value.Trim() != "")
                                          htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1").Value.Trim();

                                  htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value.Trim();
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode") != null)
                                      htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value.Trim();
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Element("State") != null)
                                      htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("State").Value.Trim();

                                  HotelDetail.HotelAddress = htlAddtrss + ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName").Value.Trim(); ;
                             
                                  HotelDetail.Lati_Longi = "";
                                  if (hoteldetails.BasicPropertyInfo.Element("Position") != null)
                                      HotelDetail.Lati_Longi = hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude").Value + "##" + hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Longitude").Value;    
                                  HotelDetail.ThumbnailUrl = "Images/NoImage.jpg";
                                  if (hoteldetails.BasicPropertyInfo.Element("HotelImages").Element("ImagePath") != null)
                                      HotelDetail.ThumbnailUrl = hoteldetails.BasicPropertyInfo.Element("HotelImages").Element("ImagePath").Value.Trim();
                                  HotelDetail.HotelDescription = "";
                                  if (hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value != "")
                                      HotelDetail.HotelDescription = "<strong>DESCRIPTION: </strong>" + hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value.Replace("/N", "");
                                  HotelDetail.Location = "";
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("Area") != null)
                                      HotelDetail.Location = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("Area").Value;
                              }
                              catch (Exception ex)
                              {
                                  HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                              }
                              string imgdiv = "", HotelFacilty = "";
                              try
                              {
                                  int im = 0;
                                  foreach (var HtlImg in hoteldetails.BasicPropertyInfo.Element("HotelImages").Descendants("ImagePath"))
                                  {
                                      im++;
                                      imgdiv += "<img id='img" + im + "' src='" + HtlImg.Value + "' onmouseover='return ShowHtlImg(this);' alt='' title='' class='imageHtlDetailsshow' />";
                                  }
                              }
                              catch (Exception ex)
                              {
                                  HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail Image " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                              }
                              HotelDetail.HotelImage = imgdiv;
                              HotelDetail.Attraction = "";
           
                              foreach (var Amenities in hoteldetails.BasicPropertyInfo.Element("HotelFeatures").Descendants("FeatureDescription"))
                              {
                                  if (Amenities.Attribute("Description") != null)
                                  HotelFacilty += "<div class='check1'>" + Amenities.Attribute("Description").Value + "</div>";
                              }
                              HotelDetail.HotelAmenities = HotelFacilty;
                              HotelDetail.RoomAmenities = "";
                          }
                          catch (Exception ex)
                          {
                              HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail Amenities " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                          }
                          #endregion
                          #region Room Details
                          try
                          {
                              string TaxType = hoteldetails.BasicPropertyInfo.Element("TaxType").Value.Trim();
                              foreach (var htldtlrates in hoteldetails.RoomRates.Elements("RoomRate"))
                              {
                                  IEnumerable<XElement> rateplane = null;
                                  rateplane = from ell in hoteldetails.RatePlans.Descendants("RatePlan")
                                              where ell.Attribute("RatePlanCode").Value == htldtlrates.Attribute("RatePlanCode").Value select ell;
                                  XElement RatePlanType = rateplane.ElementAt(0);
                                 
                                  MarkupList MarkList = new MarkupList(); MarkupList TaxMarkList = new MarkupList(); MarkupList ExtraMarkList = new MarkupList();
                                  foreach (var roomrates in htldtlrates.Elements("Rates"))
                                  {
                                      RoomType objRoomType = new RoomType();
                                      objRoomType = RoomTypelist(hoteldetails.RoomTypes, roomrates.Attribute("RoomTypeCode").Value);
                                      foreach (var htl in roomrates.Elements("Inclusion"))
                                      {
                                          try
                                          {
                                              #region room rate parsing roomwise
                                              string RoowiseRoomRateStr = "", RoowiseTax = "";
                                              for (int i = 0; i < SearchDetails.NoofRoom; i++)
                                              {
                                                  int adt = Convert.ToInt32(SearchDetails.AdtPerRoom[i]), chd = Convert.ToInt32(SearchDetails.ChdPerRoom[i]);
                                                  decimal AmountBeforeTax1 = 0, RackRateBeforeTax1 = 0;
                                                  if ((adt + chd) <= Convert.ToInt32(objRoomType.maxGuest))
                                                  {
                                                      foreach (var htlrate in htl.Elements("Rate"))
                                                      {
                                                          decimal baserate = 0, ExtraGuest = 0, ExtraChild = 0, RackRate = 0, RackExtraGuest = 0, RackExtraChild = 0;
                                                          if (adt == 1 && chd == 0)
                                                          {
                                                              baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("SingleRate").Value.Trim());
                                                              RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("SingleRate").Value.Trim());
                                                          }
                                                          else
                                                          {
                                                              baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("DoubleRate").Value.Trim());
                                                              RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("DoubleRate").Value.Trim());
                                                          }
                                                          if (adt > 2)
                                                          {
                                                              ExtraGuest = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraAdult").Value.Trim());
                                                              RackExtraGuest = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraAdult").Value.Trim());
                                                              if (adt > 3)
                                                              {
                                                                  ExtraGuest = ExtraGuest * (adt - 2);
                                                                  RackExtraGuest = RackExtraGuest * (adt - 2);
                                                              }
                                                          }
                                                          if (adt > 1 && chd > 0)
                                                          {
                                                              ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                              RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                              if (chd > 1)
                                                              {
                                                                  ExtraChild = ExtraChild * chd;
                                                                  RackExtraChild = RackExtraGuest * chd;
                                                              }
                                                          }
                                                          else if (adt == 1 && chd > 1)
                                                          {
                                                              ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                              RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                              if (chd > 2)
                                                              {
                                                                  ExtraChild = ExtraChild * chd;
                                                                  RackExtraChild = RackExtraGuest * chd;
                                                              }
                                                          }

                                                          AmountBeforeTax1 += baserate + ExtraGuest + ExtraChild;
                                                          RackRateBeforeTax1 += RackRate + RackExtraGuest + RackExtraChild;
                                                      }
                                                      RoowiseRoomRateStr += AmountBeforeTax1.ToString() + "/";
                                                      try
                                                      {
                                                          if (TaxType == "Exclusive")
                                                              SearchDetails.PGCountry = "1";
                                                          else
                                                              SearchDetails.PGCountry = "0";
                                                          SearchDetails.PGCity = RackRateBeforeTax1.ToString();
                                                          SearchDetails.PGState = AmountBeforeTax1.ToString();
                                                          string taxreq = objreq.RZTaxAmountRequest(SearchDetails);
                                                          string taxresp = RezNextPostXml(SearchDetails.RezNextUrl + "GetTaxAmount", taxreq);
                                                          if (taxresp.Contains("<Success/>"))
                                                          {
                                                              XDocument edtaxresp = XDocument.Parse(taxresp.Replace(" xmlns:htn=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                                                              IEnumerable<string> hoteltax1 = from tx in edtaxresp.Elements("TaxInfoRS").Descendants("TaxAmount")
                                                                                              select tx.Attribute("Amount").Value.ToString();
                                                              RoowiseTax += hoteltax1.ElementAt(0) + "/";
                                                          }
                                                          int m = objhtlDa.SP_Htl_InsUpdBookingLog(roomrates.Attribute("RoomTypeCode").Value + "|" + htl.Attribute("MealPlanCode").Value.Trim() + "|" + htldtlrates.Attribute("RatePlanCode").Value, taxreq, taxresp, SearchDetails.SearchCity + "|" + SearchDetails.AdtPerRoom[0].ToString() + "|" + SearchDetails.ChdPerRoom[0].ToString(), SearchDetails.CheckInDate + SearchDetails.CheckOutDate);
                                                      }
                                                      catch (Exception ex)
                                                      {
                                                          HotelDA.InsertHotelErrorLog(ex, "RezNext GetTaxAmount " + SearchDetails.HotelName);
                                                      }
                                                  }
                                                  else
                                                      goto MAXGUEST;
                                              }
                                              #endregion

                                              #region roomrate parsing nightly rate wise
                                              string Policy = "", Org_RoomRateStr = "", MrkRoomrateStr = "", DisRoomrateStr = "";
                                              decimal AmountBeforeTax = 0, RackRateBeforeTax = 0, MrkTotalPrice = 0, TotaldiscAmt = 0, adminMrkAmt = 0, AgtMrkAmt = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, TotalTaxes = 0, MrkTaxes = 0, MrkExtraGuest = 0, Org_Roomrate = 0;
                                              int k = 0;
                                              foreach (var htlrate in htl.Elements("Rate"))
                                              {
                                                  decimal nightlyrate = 0, Discountrate = 0;
                                                  for (int i = 0; i < SearchDetails.NoofRoom; i++)
                                                  {
                                                      int adt = Convert.ToInt32(SearchDetails.AdtPerRoom[i]), chd = Convert.ToInt32(SearchDetails.ChdPerRoom[i]);
                                                      decimal baserate = 0, ExtraGuest = 0, ExtraChild = 0, RackRate = 0, RackExtraGuest = 0, RackExtraChild = 0;
                                                      if (adt == 1 && chd == 0)
                                                      {
                                                          baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("SingleRate").Value.Trim());
                                                          RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("SingleRate").Value.Trim());
                                                      }
                                                      else
                                                      {
                                                          baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("DoubleRate").Value.Trim());
                                                          RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("DoubleRate").Value.Trim());
                                                      }

                                                      if (adt > 2)
                                                      {
                                                          ExtraGuest = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraAdult").Value.Trim());
                                                          RackExtraGuest = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraAdult").Value.Trim());
                                                          if (adt > 3)
                                                          {
                                                              ExtraGuest = ExtraGuest * (adt - 2);
                                                              RackExtraGuest = RackExtraGuest * (adt - 2);
                                                          }
                                                      }

                                                      if (adt > 1 && chd > 0)
                                                      {
                                                          ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                          RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                          if (chd > 1)
                                                          {
                                                              ExtraChild = ExtraChild * chd;
                                                              RackExtraChild = RackExtraGuest * chd;
                                                          }
                                                      }
                                                      else if (adt == 1 && chd > 1)
                                                      {
                                                          ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                          RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                          if (chd > 2)
                                                          {
                                                              ExtraChild = ExtraChild * chd;
                                                              RackExtraChild = RackExtraGuest * chd;
                                                          }
                                                      }

                                                      Org_RoomRateStr += "Bs:" + baserate.ToString() + "-EC:" + ExtraChild.ToString() + "-EG:" + ExtraGuest.ToString() + "/";

                                                      AmountBeforeTax += baserate + ExtraChild + ExtraGuest;
                                                      RackRateBeforeTax += RackRate + RackExtraGuest + RackExtraChild;

                                                      MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelDetail.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (baserate + ExtraGuest), SearchDetails.RZ_servicetax);
                                                      ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ExtraChild, SearchDetails.RZ_servicetax);

                                                      //MrkTotalPrice += MarkList.TotelAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                                                      nightlyrate += MarkList.TotelAmt + ExtraMarkList.TotelAmt;
                                                      MrkExtraGuest += ExtraMarkList.TotelAmt;
                                                      adminMrkAmt += MarkList.AdminMrkAmt + ExtraMarkList.AdminMrkAmt;
                                                      AgtMrkAmt += MarkList.AgentMrkAmt + ExtraMarkList.AgentMrkAmt;
                                                      SericeTaxAmt += MarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                                                      VSericeTaxAmt += MarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;

                                                      if (RackRate > 0)
                                                          Discountrate += objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (RackRate + RackExtraGuest + RackExtraChild), SearchDetails.RZ_servicetax);
                                                  }
                                                  MrkTotalPrice += nightlyrate;
                                                  TotaldiscAmt += Discountrate;
                                                  MrkRoomrateStr += nightlyrate.ToString() + "/";
                                                  DisRoomrateStr += Discountrate.ToString() + "/";

                                                  if (htlrate.Element("TPA_Extensions").Element("CancelPenalties").Element("CancelPenalty") != null && k == 0)
                                                  {
                                                      Policy = "<div><ui>";
                                                      foreach (var Cancelplane in htlrate.Elements("TPA_Extensions").Elements("CancelPenalties").Descendants("CancelPenalty"))
                                                      {
                                                          Policy += "<li style='margin:4px 0 4px 0;'>" + Cancelplane.Attribute("CancelDescription").Value.Trim();
                                                          if (Cancelplane.Element("AmountPercent").Attribute("TaxInclusive").Value.Trim() == "true")
                                                              Policy += " with Tax Inclusive.";
                                                          else
                                                              Policy += " Tax also Charge.";
                                                          Policy += "</li>";
                                                      }
                                                      k = 1; Policy += "</ui></div>";
                                                  }
                                              }

                                              SearchDetails.PGCity = RackRateBeforeTax.ToString();
                                              SearchDetails.PGState = AmountBeforeTax.ToString();
                                              try
                                              {
                                                  string taxreq = objreq.RZTaxAmountRequest(SearchDetails);
                                                  string taxresp = RezNextPostXml(SearchDetails.RezNextUrl + "GetTaxAmount", taxreq);
                                                  if (taxresp.Contains("<Success/>"))
                                                  {
                                                      XDocument edtaxresp = XDocument.Parse(taxresp.Replace(" xmlns:htn=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                                                      IEnumerable<string> hoteltax1 = from tx in edtaxresp.Elements("TaxInfoRS").Descendants("TaxAmount")
                                                                                      select tx.Attribute("Amount").Value.ToString();
                                                      TotalTaxes = Convert.ToDecimal(hoteltax1.ElementAt(0));
                                                      if (TotalTaxes > 0)
                                                      {
                                                          TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, TotalTaxes, SearchDetails.RZ_servicetax);
                                                          MrkTaxes += TaxMarkList.TotelAmt;
                                                          MrkTotalPrice += TaxMarkList.TotelAmt;
                                                          adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                                          AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                                          SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                                          VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                                          Org_RoomRateStr += "/Tx:" + hoteltax1.ElementAt(0);
                                                          Org_Roomrate += AmountBeforeTax + TotalTaxes;

                                                          if (TotaldiscAmt > 0)
                                                              TotaldiscAmt += TaxMarkList.TotelAmt;
                                                      }
                                                      else
                                                          Org_Roomrate += AmountBeforeTax ;
                                                  }
                                                  else
                                                      TaxType = "False";
                                                  int m = objhtlDa.SP_Htl_InsUpdBookingLog(roomrates.Attribute("RoomTypeCode").Value + "|" + htl.Attribute("MealPlanCode").Value.Trim() + "|" + htldtlrates.Attribute("RatePlanCode").Value, taxreq, taxresp, SearchDetails.SearchCity + "|" + SearchDetails.AdtPerRoom[0].ToString() + "|" + SearchDetails.ChdPerRoom[0].ToString(), SearchDetails.CheckInDate + SearchDetails.CheckOutDate);
                                              }
                                              catch (Exception ex)
                                              {
                                                  HotelDA.InsertHotelErrorLog(ex, "RezNext GetTaxAmount " + SearchDetails.HotelName);
                                                  TaxType = "False";
                                              }
                                              #endregion

                                              objRoomList.Add(new RoomList
                                              {
                                                  HotelCode = HotelDetail.HotelCode,
                                                  RatePlanCode = htldtlrates.Attribute("RatePlanCode").Value,
                                                  RoomTypeCode = roomrates.Attribute("RoomTypeCode").Value + "|" + htl.Attribute("MealPlanCode").Value.Trim() + "|" + RatePlanType.Attribute("RatePlanType").Value,
                                                  RoomName = objRoomType.RoomTypeName,
                                                  discountMsg = "",
                                                  DiscountAMT = TotaldiscAmt,
                                                  Total_Org_Roomrate = Org_Roomrate,
                                                  TotalRoomrate = MrkTotalPrice,
                                                  AdminMarkupPer = MarkList.AdminMrkPercent,
                                                  AdminMarkupAmt = adminMrkAmt,
                                                  AdminMarkupType = MarkList.AdminMrkType,
                                                  AgentMarkupPer = MarkList.AgentMrkPercent,
                                                  AgentMarkupAmt = AgtMrkAmt,
                                                  AgentMarkupType = MarkList.AgentMrkType,
                                                  AgentServiseTaxAmt = SericeTaxAmt,
                                                  V_ServiseTaxAmt = VSericeTaxAmt,
                                                  AmountBeforeTax = AmountBeforeTax,
                                                  Taxes = TotalTaxes,
                                                  MrkTaxes = MrkTaxes,
                                                  ExtraGuest_Charge = MrkExtraGuest,
                                                  Smoking = objRoomType.RoomDescription,
                                                  inclusions = htl.Attribute("MealPlanDescription").Value.Trim(),
                                                  CancelationPolicy = Policy,
                                                  OrgRateBreakups = Org_RoomRateStr,
                                                  MrkRateBreakups = MrkRoomrateStr,
                                                  DiscRoomrateBreakups = DisRoomrateStr,
                                                  EssentialInformation = TaxType,
                                                  RoomDescription = RoowiseRoomRateStr + "#" + RoowiseTax,
                                                  Provider = "RZ",
                                                  RoomImage = objRoomType.RoomImage
                                              });
                                          }
                                          catch (Exception ex)
                                          {
                                              HotelDA.InsertHotelErrorLog(ex, "Reznext ADDRoomList " + SearchDetails.SearchCity);
                                              objRoomList.Add(new RoomList
                                              {
                                                  TotalRoomrate = 0,
                                                  HtlError = ex.Message
                                              });
                                          }
                                      }
                                  MAXGUEST:
                                      string s = "";
                                  }
                              }
                          }
                          catch (Exception ex)
                          {
                              HotelDA.InsertHotelErrorLog(ex, " Reznext ADDRoomList " + SearchDetails.SearchCity);
                              objRoomList.Add(new RoomList
                              {
                                  TotalRoomrate = 0,
                                  HtlError = ex.Message
                              });
                          }
                          #endregion
                      }
                  }
                  else
                  {
                      objRoomList.Add(new RoomList
                      {
                          TotalRoomrate = 0, HtlError = "Reznext Room Details Not found"
                      });
                      int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                  }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "Reznext RoomList " + SearchDetails.SearchCity);
              objRoomList.Add(new RoomList
              {
                  TotalRoomrate = 0, HtlError = ex.Message
              });
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          objRoomDetals.SelectedHotelDetail = HotelDetail;
          objRoomDetals.RoomDetails = objRoomList;
          return objRoomDetals;
      }

      public HotelBooking RZHotelsPreBooking(HotelBooking HotelDetail)
      {
          try
          {
              if (HotelDetail.RezNextUrl != null)
              {
                  HotelDetail.ProBookingReq = objreq.RZPreBookingRequest(HotelDetail);
                  HotelDetail.ProBookingRes = RezNextPostXml(HotelDetail.RezNextUrl + "GetHotelInformation", HotelDetail.ProBookingReq);

                  if (HotelDetail.ProBookingRes.Contains("<Success/>"))
                  {
                      XDocument document = XDocument.Parse(HotelDetail.ProBookingRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", "").Replace(" xmlns=\"http://schemas.xmlsoap.org/soap/envelope\"", String.Empty));
                      var hotelprice = (from htlprice in document.Descendants("RoomStays").Descendants("RoomStay").Descendants("RoomRates").Descendants("RoomRate")
                                        where htlprice.Attribute("RatePlanCode").Value.Trim() == HotelDetail.RoomPlanCode
                                        select new { RoomRates = htlprice }).ToList();
                      if( hotelprice.Count > 0)
                      {
                          string[] roomtype = HotelDetail.RoomTypeCode.Split('|');
                          var abc = (from Rates in hotelprice[0].RoomRates.Descendants("Rates") where Rates.Attribute("RoomTypeCode").Value.Trim() == roomtype[0] select new { HotelRates = Rates }).ToList();
                          if (abc.Count > 0)
                          { 
                              foreach (var Hrice in abc[0].HotelRates.Descendants("Inclusion").Where(s => s.Attribute("MealPlanCode").Value == roomtype[1]))
                              {
                                 decimal AmountBeforeTax = 0, Taxses=0;
                                 for (int i = 0; i < HotelDetail.NoofRoom; i++)
                                 {
                                     decimal AmountBeforeTax1 = 0, Trackrate1 = 0, Taxses1=0;
                                     foreach (var htlrate in Hrice.Descendants("Rate"))
                                     {
                                         int adt = Convert.ToInt32(HotelDetail.AdtPerRoom[i]), chd = Convert.ToInt32(HotelDetail.ChdPerRoom[i]);
                                         decimal baserate = 0, RackRate = 0, ExtraGuest = 0, RackExtraGuest = 0, ExtraChild = 0, RackExtraChild=0;
                                         if (adt == 1 && chd == 0)
                                          {
                                              baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("SingleRate").Value.Trim());
                                              RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("SingleRate").Value.Trim());
                                          }
                                          else
                                          {
                                              baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("DoubleRate").Value.Trim());
                                              RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("DoubleRate").Value.Trim());
                                          }

                                         if (adt > 2)
                                          {
                                              ExtraGuest = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraAdult").Value.Trim());
                                              RackExtraGuest = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraAdult").Value.Trim());
                                              if (adt > 3)
                                              {
                                                  ExtraGuest = ExtraGuest * (adt - 2);
                                                  RackExtraGuest = RackExtraGuest * (adt - 2);
                                              }
                                          }

                                         if (adt > 1 && chd > 0)
                                          {
                                              ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                              RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                              if (chd > 1)
                                              {
                                                  ExtraChild = ExtraChild * chd ;
                                                  RackExtraChild = RackExtraGuest * chd ;
                                              }
                                          }
                                         else if (adt == 1 && chd > 1)
                                          {
                                              ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                              RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                              if (chd > 2)
                                              {
                                                  ExtraChild = ExtraChild * chd;
                                                  RackExtraChild = RackExtraGuest * chd;
                                              }
                                          }

                                          AmountBeforeTax1 += baserate + ExtraGuest + ExtraChild;
                                          Trackrate1 += RackRate + RackExtraGuest + RackExtraChild;
                                      }

                                     try
                                     {
                                         if (HotelDetail.TaxType == "Exclusive")
                                             HotelDetail.TaxType = "1";
                                         else
                                             HotelDetail.TaxType = "0";
                                         string taxreq = objreq.RZPreBookingTaxAmountRequest(HotelDetail, Trackrate1, AmountBeforeTax1);
                                         string taxresp = RezNextPostXml(HotelDetail.RezNextUrl + "GetTaxAmount", taxreq);
                                         if (taxresp.Contains("<Success/>"))
                                         {
                                             XDocument xmlresp = XDocument.Parse(taxresp.Replace(" xmlns:htn=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                                             IEnumerable<string> hoteltax1 = from tx in xmlresp.Elements("TaxInfoRS").Descendants("TaxAmount")
                                                                             select tx.Attribute("Amount").Value.ToString();
                                            Taxses1=Convert.ToDecimal(hoteltax1.ElementAt(0));
                                         }
                                         else
                                         {
                                             HotelDetail.ProBookingID = "false";
                                             HotelDetail.BookingID = "Exception in tax";
                                         }
                                         HotelDA objhtlDa = new HotelDA();
                                         int m = objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetail.Orderid, taxreq, taxresp, "RZ", "HtlUpdateAgreement");
                                     }
                                     catch (Exception ex)
                                     {
                                         HotelDA.InsertHotelErrorLog(ex, "RezNext GetTaxAmount in prebooking " + HotelDetail.HotelName);
                                         HotelDetail.BookingID = ex.Message; HotelDetail.ProBookingID = "false";
                                     }
                                     AmountBeforeTax += AmountBeforeTax1;
                                     Taxses += Taxses1;
                                  }
                                 if (HotelDetail.Total_Org_Roomrate >= AmountBeforeTax + Taxses)
                                 {
                                     HotelDetail.ProBookingID = "true";
                                 }
                                 else
                                 {
                                     HotelDetail.BookingID = "Rate Not matching"; HotelDetail.ProBookingID = "false";
                                 }
                              }
                          }
                          else
                          {
                            HotelDetail.BookingID = "Room Type not found"; HotelDetail.ProBookingID = "false";
                         }
                      }
                      else
                      {
                          HotelDetail.BookingID = "Hotel Not Found"; HotelDetail.ProBookingID = "false";
                      }
                  }
                  else
                  {
                      HotelDetail.BookingID = "Exception"; HotelDetail.ProBookingID = "false";
                  }
              }
              else
              {
                  HotelDetail.BookingID = "BookingNotAlowed"; HotelDetail.ProBookingID = "false";
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RZHotelsPreBooking");
              HotelDetail.BookingID = ex.Message; HotelDetail.ProBookingID = "false";
          }
          return HotelDetail;
      }

      public HotelBooking RZHotelsBooking(HotelBooking HotelDetail)
      {
          try
          {
              HotelDetail = objreq.RZBookingRequest(HotelDetail);
              HotelDetail.BookingConfRes = RezNextPostXml(HotelDetail.RezNextUrl + "GetBookingRequest", HotelDetail.BookingConfReq);
              if (HotelDetail.BookingConfRes.Contains("<Success/>"))
              {
                  XDocument bookingxml = XDocument.Parse(HotelDetail.BookingConfRes.Replace(" schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRS.xsd\"", String.Empty));
                  IEnumerable<string> bookinfo1 = from el in bookingxml.Descendants("HotelReservationIDs").Descendants("HotelReservationID")
                                                  where el.Attribute("ResID_Type").Value == "10" select el.Attribute("ResID_Value").Value.ToString();
                  HotelDetail.BookingID = bookinfo1.ElementAt(0);
                  HotelDetail.Status = HotelStatus.Confirm.ToString();
              }
              else
              {
                  HotelDetail.BookingID = "";
                  HotelDetail.ProBookingID = "Exception";
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RZHotelsBooking");
              HotelDetail.BookingID = ""; HotelDetail.ProBookingID = ex.Message;
          }

          return HotelDetail;
      }
      public HotelCancellation RZHotelsCancelation(HotelCancellation HotelDetail)
      {
          HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
          try
          {
              HotelDetail = objreq.RZCancellationRequest(HotelDetail);
              HotelDetail.BookingCancelRes = RezNextPostXml(HotelDetail.HotelUrl + "GetBookingRequest", HotelDetail.BookingCancelReq);

              if (HotelDetail.BookingCancelRes.Contains("<Success/>"))
              {
                  string proresp = HotelDetail.BookingCancelRes.Replace("schemaLocation=\"http://www.opentravel.org/OTA/2003/05 OTA_HotelResNotifRS.xsd\"", String.Empty);
                  XDocument xmlresp = XDocument.Parse(proresp);

                  IEnumerable<string> bookingcanstatus = from el in xmlresp.Descendants("OTA_HotelResNotifRS") select el.Attribute("ResResponseType").Value.ToString();
                  IEnumerable<string> bookingcan = from can in xmlresp.Descendants("ResGlobalInfo") select can.Element("RefundAmount").Value.ToString();

                  HotelDetail.CancelStatus = bookingcanstatus.ElementAt(0);
                  HotelDetail.CancellationCharge = Convert.ToDecimal(bookingcan.ElementAt(0));
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "TGHotelsCancelation");
          }
          return HotelDetail;
      }
      protected string GetTaxAmount(HotelSearch SearchDetails)
      {
            string htltax="False"; 
             try
             {
                string taxresp = RezNextPostXml(SearchDetails.RezNextUrl + "GetTaxAmount", objreq.RZTaxAmountRequest(SearchDetails));
                if(taxresp.Contains("<Success/>"))  
                {
                    XDocument xmlresp = XDocument.Parse(taxresp.Replace(" xmlns:htn=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                    var hoteltax = (from htlprice in xmlresp.Descendants("TaxInfoRS")select new { taxamt = htlprice.Element("TaxAmount")}).ToList();
                    if (hoteltax.Count > 0)
                      htltax = hoteltax[0].taxamt.Attribute("Amount").Value.Trim();
                }
              //  int m = objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.ProBookingReq, HotelDetails.ProBookingRes, HotelDetails.Provider, "HtlUpdateProBooking");
             }
             catch(Exception ex)
             {
                 HotelDA.InsertHotelErrorLog(ex, "RezNext GetTaxAmount " + SearchDetails.HotelName); 
             }
          return htltax;
      }
      protected RoomType RoomTypelist(XElement hoteldetails, string RoomTypeCode)
      {
          RoomType objRoomType = new RoomType();
          try
          {
              foreach (var htldtl in hoteldetails.Descendants("RoomType").Where(x => x.Attribute("RoomTypeCode").Value == RoomTypeCode))
              {
                  objRoomType.maxGuest = htldtl.Attribute("MaxPersons").Value;
                  objRoomType.RoomTypeName = htldtl.Element("RoomDescription").Attribute("Text").Value;
                  objRoomType.RoomDescription = "";
                  objRoomType.RoomImage = htldtl.Element("RoomDescription").Element("ImagePath").Value;
                  foreach (var htlaminity in htldtl.Element("RoomAmenities").Elements("AmenityDescription"))
                  {
                      if (htlaminity.Attribute("Description") != null)
                         objRoomType.RoomDescription += "<div class='check1'>" + htlaminity.Attribute("Description").Value + "</div>";
                  }
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RezNext RoomTypelist " + RoomTypeCode);
          }
          return objRoomType;
      }

      protected HotelRatePlan RatePlanlist(XElement hoteldetails, string rateplan)
      {
          HotelRatePlan objRatePlans = new HotelRatePlan();
          try
          {
              foreach (var htldtl in hoteldetails.Parent.Descendants("RatePlans").Elements("RatePlan").Where(x => x.Attribute("RatePlanCode").Value == rateplan))
              {
                  string discount = "";
                  foreach (var htl in htldtl.Descendants("RatePlanDescription").Elements("Text"))
                  {
                      discount += htl.Value;
                  }
                  string inclusions = "";
                  foreach (var htl in htldtl.Descendants("RatePlanInclusions").Descendants("RatePlanInclusionDesciption").Elements("Text"))
                  {
                      inclusions += htl.Value;
                  }
                  objRatePlans.RatePlanCode = htldtl.Attribute("RatePlanCode").Value;
                  objRatePlans.discountMsg = discount;
                  objRatePlans.inclusions = inclusions;
              }
          }
          catch (Exception ex)
          { HotelDA.InsertHotelErrorLog(ex, "RatePlanlist"); }
          return objRatePlans;
      }

      protected RoomRate RoomRatelist(XElement hoteldetails, string RoomPlanCode, HotelSearch SearchDetails)
      {
          RoomRate objRoomRate = new RoomRate(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
          MarkupList TaxMarkList = new MarkupList(); MarkupList ExtraMarkList = new MarkupList();
          try
          {
              string Org_RoomRateStr = "", MrkRoomrateStr = "", Disc_RoomrateStr = "", RoomTypeCode = "";
              decimal AmountBeforeTax = 0, MrkTotalPrice = 0, TotaldiscAmt = 0, adminMrkAmt = 0, AgtMrkAmt = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, TotalTaxes = 0, TotExtraGuestTaxes = 0, MrkTaxes = 0, MrkExtraGuest = 0;

              foreach (var roomrate in hoteldetails.Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == RoomPlanCode))
              {
                  decimal Basefare = 0, Taxes = 0, ExtraGuesttax = 0, disamt = 0;
                  RoomTypeCode = roomrate.Attribute("RoomID").Value;
                  Basefare = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value);
                  Taxes = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value);
                  if (roomrate.Element("Rates").Element("Rate").Element("AdditionalGuestAmounts") != null)
                  {
                      decimal extrarate = 0;
                      foreach (var addigust in roomrate.Elements("Rates").Elements("Rate").Elements("AdditionalGuestAmounts").Elements("AdditionalGuestAmount"))
                      {
                          extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                      }
                      ExtraGuesttax = extrarate;
                  }

                  if (roomrate.Element("Rates").Element("Rate").Element("Discount") != null)
                      disamt = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value);

                  Org_RoomRateStr += "Bs:" + Basefare + "-Tx:" + Taxes + "-EG:" + ExtraGuesttax + "-Ds" + disamt + "/";

                  AmountBeforeTax += Basefare - disamt;
                  TotalTaxes += Taxes + ExtraGuesttax;

                  MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (Basefare - disamt), SearchDetails.TG_servicetax);
                  TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Taxes, SearchDetails.TG_servicetax);
                  ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ExtraGuesttax, SearchDetails.TG_servicetax);

                  MrkTotalPrice += MarkList.TotelAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                  MrkTaxes += TaxMarkList.TotelAmt;
                  MrkExtraGuest += ExtraMarkList.TotelAmt;
                  adminMrkAmt += MarkList.AdminMrkAmt + TaxMarkList.AdminMrkAmt + ExtraMarkList.AdminMrkAmt;
                  AgtMrkAmt += MarkList.AgentMrkAmt + TaxMarkList.AgentMrkAmt + ExtraMarkList.AgentMrkAmt;
                  SericeTaxAmt += MarkList.AgentServiceTaxAmt + TaxMarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                  VSericeTaxAmt += MarkList.VenderServiceTaxAmt + TaxMarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;
                  MrkRoomrateStr += MarkList.TotelAmt.ToString() + "/";

                  decimal MrkDiscAmt = 0;
                  if (disamt > 0)
                  {
                      MrkDiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (Basefare), 0);
                      TotaldiscAmt += MrkDiscAmt + ExtraMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                  }
                  Disc_RoomrateStr += MrkDiscAmt.ToString() + "/";
              }

              objRoomRate.RoomTypeCode = RoomTypeCode;
              objRoomRate.RatePlanCode = RoomPlanCode;

              objRoomRate.Total_Org_Roomrate = (AmountBeforeTax + TotalTaxes);
              objRoomRate.TotalRoomrate = MrkTotalPrice;
              objRoomRate.DiscountAMT = TotaldiscAmt;
              objRoomRate.AmountBeforeTax = AmountBeforeTax;
              objRoomRate.Taxes = TotalTaxes;
              objRoomRate.ExtraGuest_Charge = MrkExtraGuest;
              objRoomRate.MrkTaxes = MrkTaxes;
              objRoomRate.Org_RoomrateBreakups = Org_RoomRateStr;
              objRoomRate.Mrk_RoomrateBreakups = MrkRoomrateStr;
              objRoomRate.Disc_RoomrateBreakups = Disc_RoomrateStr;
              objRoomRate.AdminMarkupPer = MarkList.AdminMrkPercent;
              objRoomRate.AdminMarkupAmt = adminMrkAmt;
              objRoomRate.AdminMarkupType = MarkList.AdminMrkType;
              objRoomRate.AgentMarkupPer = MarkList.AgentMrkPercent;
              objRoomRate.AgentMarkupAmt = AgtMrkAmt;
              objRoomRate.AgentMarkupType = MarkList.AgentMrkType;
              objRoomRate.ServiseTaxAmt = SericeTaxAmt;
              objRoomRate.V_ServiseTaxAmt = VSericeTaxAmt;
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RoomRatelist_" + RoomPlanCode + "_" + SearchDetails.HtlCode);
          }
          return objRoomRate;
      }

      public string RezNextCancellationPolicy(HotelSearch SearchDetails)
      {
          string Policy = "";
          try
          {
              string[] roomtype = SearchDetails.HtlRoomCode.Split('|');
              string responses = SearchDetails.RZ_HotelSearchRes.Replace(" xmlns=\"http://schemas.xmlsoap.org/soap/envelope\"", String.Empty);
              XDocument xmlresp = XDocument.Parse(responses);
              var HotelsPlicy = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                 where htlprice.Element("BasicPropertyInfo").Attribute("HotelCode").Value.Trim() == SearchDetails.HtlCode && htlprice.Element("RoomRates").Element("RoomRate").Attribute("RatePlanCode").Value.Trim() == roomtype[0]
                                 select new { RoomR = htlprice.Element("RoomRates").Element("RoomRate") }).ToList();
              if (HotelsPlicy.Count > 0)
              {
                      foreach (var hoteldetails in HotelsPlicy)
                      {
                          foreach (var roomrates in hoteldetails.RoomR.Elements("Rates").Where(x => x.Attribute("RoomTypeCode").Value == roomtype[1]))
                          {
                              foreach (var htlrate in roomrates.Elements("Inclusion").Where(y => y.Attribute("MealPlanCode").Value == roomtype[2]))
                              {
                                      Policy = "<Div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                                      if (htlrate.Element("Rate").Element("TPA_Extensions").Element("CancelPenalties").Element("CancelPenalty") != null)
                                      {
                                          Policy = "<div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span><ui>";
                                          foreach (var Cancelplane in htlrate.Element("Rate").Elements("TPA_Extensions").Elements("CancelPenalties").Descendants("CancelPenalty"))
                                          {
                                              Policy += "<li style='margin:4px 0 4px 0;'>" + Cancelplane.Attribute("CancelDescription").Value.Trim();
                                              if (Cancelplane.Element("AmountPercent").Attribute("TaxInclusive").Value.Trim() == "true")
                                                  Policy += " with Tax Inclusive.";
                                              else
                                                  Policy += " Tax also Charge.";
                                              Policy += "</li>";
                                          }
                                      }
                                      Policy += "</Div>";
                              }
                          }
                      }
                      
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "RezNextCancellationPolicy");
              return ex.Message;
          }
          return Policy;
      }

      public RoomComposite RZRoomList_Bakup(HotelSearch SearchDetails)
      {
          List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
          RoomComposite objRoomDetals = new RoomComposite(); HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA();
          try
          {
              string responses = SearchDetails.RZ_HotelSearchRes.Replace(" xmlns=\"http://schemas.xmlsoap.org/soap/envelope\"", String.Empty);
              XDocument xmlresp = XDocument.Parse(responses);
              var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                where htlprice.Element("BasicPropertyInfo").Attribute("HotelCode").Value.Trim() == SearchDetails.HtlCode
                                select new { RoomTypes = htlprice.Element("RoomTypes"), RatePlans = htlprice.Element("RatePlans"), RoomRates = htlprice.Element("RoomRates"), BasicPropertyInfo = htlprice.Element("BasicPropertyInfo") }).ToList();

              if (hotelprice.Count > 0)
              {
                  foreach (var hoteldetails in hotelprice)
                  {
                      #region Hotel Details
                      try
                      {
                          HotelDetail.HotelName = hoteldetails.BasicPropertyInfo.Attribute("HotelName").Value;
                          HotelDetail.HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                          HotelDetail.StarRating = hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value.Trim();
                          SearchDetails.StarRating = HotelDetail.StarRating;
                          try
                          {
                              string htlAddtrss = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1") != null)
                                  if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine2").Value.Trim() != "")
                                      htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Attribute("AddressLine1").Value.Trim();

                              htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode") != null)
                                  htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value.Trim();
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Element("State") != null)
                                  htlAddtrss += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("State").Value.Trim();

                              HotelDetail.HotelAddress = htlAddtrss + ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName").Value.Trim(); ;

                              HotelDetail.Lati_Longi = "";
                              if (hoteldetails.BasicPropertyInfo.Element("Position") != null)
                                  HotelDetail.Lati_Longi = hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude").Value + "##" + hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Longitude").Value;
                              HotelDetail.ThumbnailUrl = "Images/NoImage.jpg";
                              if (hoteldetails.BasicPropertyInfo.Element("HotelImages").Element("ImagePath") != null)
                                  HotelDetail.ThumbnailUrl = hoteldetails.BasicPropertyInfo.Element("HotelImages").Element("ImagePath").Value.Trim();
                              HotelDetail.HotelDescription = "";
                              if (hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value != "")
                                  HotelDetail.HotelDescription = "<strong>DESCRIPTION: </strong>" + hoteldetails.BasicPropertyInfo.Element("HotelDescription").Value.Replace("/N", "");
                              HotelDetail.Location = "";
                              if (hoteldetails.BasicPropertyInfo.Element("Address").Attribute("Area") != null)
                                  HotelDetail.Location = hoteldetails.BasicPropertyInfo.Element("Address").Attribute("Area").Value;
                          }
                          catch (Exception ex)
                          {
                              HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                          }
                          string imgdiv = "", HotelFacilty = "";
                          try
                          {
                              int im = 0;
                              foreach (var HtlImg in hoteldetails.BasicPropertyInfo.Element("HotelImages").Descendants("ImagePath"))
                              {
                                  im++;
                                  imgdiv += "<img id='img" + im + "' src='" + HtlImg.Value + "' onmouseover='return ShowHtlImg(this);' alt='' title='' class='imageHtlDetailsshow' />";
                              }
                          }
                          catch (Exception ex)
                          {
                              HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail Image " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                          }
                          HotelDetail.HotelImage = imgdiv;
                          HotelDetail.Attraction = "";

                          foreach (var Amenities in hoteldetails.BasicPropertyInfo.Element("HotelFeatures").Descendants("FeatureDescription"))
                          {
                              if (Amenities.Attribute("Description") != null)
                                  HotelFacilty += "<div class='check1'>" + Amenities.Attribute("Description").Value + "</div>";
                          }
                          HotelDetail.HotelAmenities = HotelFacilty;
                          HotelDetail.RoomAmenities = "";
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, "RezNext Hotel Detail Amenities " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                      }
                      #endregion
                      #region Room Details
                      try
                      {
                          string Policy = "";
                          foreach (var htldtlrates in hoteldetails.RoomRates.Elements("RoomRate"))
                          {
                              IEnumerable<XElement> rateplane = null;
                              rateplane = from ell in hoteldetails.RatePlans.Descendants("RatePlan")
                                          where ell.Attribute("RatePlanCode").Value == htldtlrates.Attribute("RatePlanCode").Value
                                          select ell;
                              XElement RatePlanType = rateplane.ElementAt(0);
                              MarkupList MarkList = new MarkupList(); MarkupList TaxMarkList = new MarkupList(); MarkupList ExtraMarkList = new MarkupList();
                              foreach (var roomrates in htldtlrates.Elements("Rates"))
                              {
                                  RoomType objRoomType = new RoomType();
                                  objRoomType = RoomTypelist(hoteldetails.RoomTypes, roomrates.Attribute("RoomTypeCode").Value);
                                  foreach (var htl in roomrates.Elements("Inclusion"))
                                  {
                                      try
                                      {
                                          string Org_RoomRateStr = "", MrkRoomrateStr = "", DisRoomrateStr = "", TaxType = hoteldetails.BasicPropertyInfo.Element("TaxType").Value.Trim();
                                          decimal AmountBeforeTax = 0, RackRateBeforeTax = 0, MrkTotalPrice = 0, TotaldiscAmt = 0, adminMrkAmt = 0, AgtMrkAmt = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, TotalTaxes = 0, MrkTaxes = 0, MrkExtraGuest = 0, Org_Roomrate = 0;
                                          int k = 0;
                                          foreach (var htlrate in htl.Elements("Rate"))
                                          {
                                              decimal nightlyrate = 0, Discountrate = 0;
                                              for (int i = 0; i < SearchDetails.NoofRoom; i++)
                                              {
                                                  decimal baserate = 0, ExtraGuest = 0, ExtraChild = 0, RackRate = 0, RackExtraGuest = 0, RackExtraChild = 0;
                                                  if (Convert.ToInt32(SearchDetails.AdtPerRoom[i]) == 1 && Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                                  {
                                                      baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("SingleRate").Value.Trim());
                                                      RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("SingleRate").Value.Trim());
                                                  }
                                                  else
                                                  {
                                                      baserate = Convert.ToDecimal(htlrate.Element("Base").Attribute("DoubleRate").Value.Trim());
                                                      RackRate = Convert.ToDecimal(htlrate.Element("Rack").Attribute("DoubleRate").Value.Trim());
                                                  }

                                                  if (Convert.ToInt32(SearchDetails.AdtPerRoom[i]) > 2)
                                                  {
                                                      ExtraGuest = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraAdult").Value.Trim());
                                                      RackExtraGuest = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraAdult").Value.Trim());
                                                  }

                                                  if (Convert.ToInt32(SearchDetails.AdtPerRoom[i]) != 1 && Convert.ToInt32(SearchDetails.ChdPerRoom[0]) > 0)
                                                  {
                                                      ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                      RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                  }
                                                  else if (Convert.ToInt32(SearchDetails.AdtPerRoom[i]) == 1 && Convert.ToInt32(SearchDetails.ChdPerRoom[0]) > 1)
                                                  {
                                                      ExtraChild = Convert.ToDecimal(htlrate.Element("Base").Attribute("ExtraChild").Value.Trim());
                                                      RackExtraChild = Convert.ToDecimal(htlrate.Element("Rack").Attribute("ExtraChild").Value.Trim());
                                                  }

                                                  Org_RoomRateStr += "Bs:" + baserate.ToString() + "-EC:" + ExtraChild.ToString() + "-EG:" + ExtraGuest.ToString() + "/";

                                                  AmountBeforeTax += baserate + ExtraChild + ExtraGuest;
                                                  RackRateBeforeTax += RackRate + RackExtraGuest + RackExtraChild;

                                                  MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelDetail.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (baserate + ExtraGuest), SearchDetails.RZ_servicetax);
                                                  ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ExtraChild, SearchDetails.RZ_servicetax);

                                                  //MrkTotalPrice += MarkList.TotelAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                                                  nightlyrate += MarkList.TotelAmt + ExtraMarkList.TotelAmt;
                                                  MrkExtraGuest += ExtraMarkList.TotelAmt;
                                                  adminMrkAmt += MarkList.AdminMrkAmt + ExtraMarkList.AdminMrkAmt;
                                                  AgtMrkAmt += MarkList.AgentMrkAmt + ExtraMarkList.AgentMrkAmt;
                                                  SericeTaxAmt += MarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                                                  VSericeTaxAmt += MarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;

                                                  if (RackRate > 0)
                                                      Discountrate += objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (RackRate + RackExtraGuest + RackExtraChild), SearchDetails.RZ_servicetax);
                                              }
                                              MrkTotalPrice += nightlyrate;
                                              TotaldiscAmt += Discountrate;
                                              MrkRoomrateStr += nightlyrate.ToString() + "/";
                                              DisRoomrateStr += Discountrate.ToString() + "/";

                                              if (htlrate.Element("TPA_Extensions").Element("CancelPenalties").Element("CancelPenalty") != null && k == 0)
                                              {
                                                  Policy = "<div><ui>";
                                                  foreach (var Cancelplane in htlrate.Elements("TPA_Extensions").Elements("CancelPenalties").Descendants("CancelPenalty"))
                                                  {
                                                      Policy += "<li style='margin:4px 0 4px 0;'>" + Cancelplane.Attribute("CancelDescription").Value.Trim();
                                                      if (Cancelplane.Element("AmountPercent").Attribute("TaxInclusive").Value.Trim() == "true")
                                                          Policy += " with Tax Inclusive.";
                                                      else
                                                          Policy += " Tax also Charge.";
                                                      Policy += "</li>";
                                                  }
                                                  k = 1; Policy += "</ui></div>";
                                              }
                                          }

                                          if (TaxType == "Exclusive")
                                              SearchDetails.PGCountry = "1";
                                          else
                                              SearchDetails.PGCountry = "0";
                                          SearchDetails.PGCity = RackRateBeforeTax.ToString();
                                          SearchDetails.PGState = AmountBeforeTax.ToString();
                                          try
                                          {
                                              string taxreq = objreq.RZTaxAmountRequest(SearchDetails);
                                              string taxresp = RezNextPostXml(SearchDetails.RezNextUrl + "GetTaxAmount", taxreq);
                                              if (taxresp.Contains("<Success/>"))
                                              {
                                                  XDocument edtaxresp = XDocument.Parse(taxresp.Replace(" xmlns:htn=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                                                  IEnumerable<string> hoteltax1 = from tx in edtaxresp.Elements("TaxInfoRS").Descendants("TaxAmount")
                                                                                  select tx.Attribute("Amount").Value.ToString();
                                                  TotalTaxes = Convert.ToDecimal(hoteltax1.ElementAt(0));
                                                  if (TotalTaxes > 0)
                                                  {
                                                      TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, TotalTaxes, SearchDetails.RZ_servicetax);
                                                      MrkTaxes += TaxMarkList.TotelAmt;
                                                      MrkTotalPrice += TaxMarkList.TotelAmt;
                                                      adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                                      AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                                      SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                                      VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                                      Org_RoomRateStr += "/Tx:" + hoteltax1.ElementAt(0);
                                                      Org_Roomrate += AmountBeforeTax + TotalTaxes;

                                                      if (TotaldiscAmt > 0)
                                                          TotaldiscAmt += TaxMarkList.TotelAmt;
                                                  }
                                              }
                                              else
                                                  TaxType = "False";
                                              int m = objhtlDa.SP_Htl_InsUpdBookingLog(roomrates.Attribute("RoomTypeCode").Value + "|" + htl.Attribute("MealPlanCode").Value.Trim() + "|" + htldtlrates.Attribute("RatePlanCode").Value, taxreq, taxresp, SearchDetails.SearchCity + "|" + SearchDetails.AdtPerRoom.ToString() + "|" + SearchDetails.ChdPerRoom.ToString(), SearchDetails.CheckInDate + SearchDetails.CheckOutDate);
                                          }
                                          catch (Exception ex)
                                          {
                                              HotelDA.InsertHotelErrorLog(ex, "RezNext GetTaxAmount " + SearchDetails.HotelName);
                                              TaxType = "False";
                                          }
                                          objRoomList.Add(new RoomList
                                          {
                                              HotelCode = HotelDetail.HotelCode,
                                              RatePlanCode = htldtlrates.Attribute("RatePlanCode").Value,
                                              RoomTypeCode = roomrates.Attribute("RoomTypeCode").Value + "|" + htl.Attribute("MealPlanCode").Value.Trim() + "|" + RatePlanType.Attribute("RatePlanType").Value,
                                              RoomName = objRoomType.RoomTypeName,
                                              discountMsg = "",
                                              DiscountAMT = TotaldiscAmt,
                                              Total_Org_Roomrate = Org_Roomrate,
                                              TotalRoomrate = MrkTotalPrice,
                                              AdminMarkupPer = MarkList.AdminMrkPercent,
                                              AdminMarkupAmt = adminMrkAmt,
                                              AdminMarkupType = MarkList.AdminMrkType,
                                              AgentMarkupPer = MarkList.AgentMrkPercent,
                                              AgentMarkupAmt = AgtMrkAmt,
                                              AgentMarkupType = MarkList.AgentMrkType,
                                              AgentServiseTaxAmt = SericeTaxAmt,
                                              V_ServiseTaxAmt = VSericeTaxAmt,
                                              AmountBeforeTax = AmountBeforeTax,
                                              Taxes = TotalTaxes,
                                              MrkTaxes = MrkTaxes,
                                              ExtraGuest_Charge = MrkExtraGuest,
                                              Smoking = objRoomType.RoomDescription,
                                              inclusions = htl.Attribute("MealPlanDescription").Value.Trim(),
                                              CancelationPolicy = Policy,
                                              OrgRateBreakups = Org_RoomRateStr,
                                              MrkRateBreakups = MrkRoomrateStr,
                                              DiscRoomrateBreakups = DisRoomrateStr,
                                              EssentialInformation = TaxType,
                                              RoomDescription = "",
                                              Provider = "RZ",
                                              RoomImage = objRoomType.RoomImage
                                          });
                                      }
                                      catch (Exception ex)
                                      {
                                          HotelDA.InsertHotelErrorLog(ex, "Reznext ADDRoomList");
                                          objRoomList.Add(new RoomList
                                          {
                                              TotalRoomrate = 0,
                                              HtlError = ex.Message
                                          });
                                      }
                                  }
                                  // HotelDetail.RoomAmenities = objRoomType.RoomDescription;
                              }
                          }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, " Reznext ADDRoomList");
                          objRoomList.Add(new RoomList
                          {
                              TotalRoomrate = 0,
                              HtlError = ex.Message
                          });
                      }
                      #endregion
                  }
              }
              else
              {
                  objRoomList.Add(new RoomList
                  {
                      TotalRoomrate = 0,
                      HtlError = "Reznext Room Details Not found"
                  });
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "Reznext RoomList");
              objRoomList.Add(new RoomList
              {
                  TotalRoomrate = 0,
                  HtlError = ex.Message
              });
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.RZ_HotelSearchReq, SearchDetails.RZ_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          objRoomDetals.SelectedHotelDetail = HotelDetail;
          objRoomDetals.RoomDetails = objRoomList;
          return objRoomDetals;
      }
    }
}


