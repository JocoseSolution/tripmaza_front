﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace STD.BAL.TBO.SSR
{

    public class TOBSSR
    {

        public SSRResponse GetSSR(string EndUserIp, string TraceId, string ResultIndex, DataSet Crd)
        {
            SSRResponse result = new SSRResponse();
            TBOAuthProcess objTbProcess = new TBOAuthProcess();
            string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]));
            SSRRequest objSSRReq = new SSRRequest();

            objSSRReq.EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
            objSSRReq.TraceId = TraceId;
            objSSRReq.ResultIndex = ResultIndex;
            objSSRReq.TokenId = strTokenID;
            string strJsonData = JsonConvert.SerializeObject(objSSRReq);
            string strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.SSRUrl, strJsonData);
            TBOUtitlity.SaveFile(strJsonData, "SSRReq_"+ResultIndex);
            TBOUtitlity.SaveFile(strJsonResponse, "SSRRes_" + ResultIndex);
            result = JsonConvert.DeserializeObject<SSRResponse>(strJsonResponse);

            return result;

        }


        public List<MealDynamic> GetTBOMeals(SSRResponse SSRResp, string type)
        {
            List<MealDynamic> result = new List<MealDynamic>();

            try
            {
                if (type.ToUpper() == "O")
                {
                    result = SSRResp.Response.MealDynamic[0].Where(x => x.Price > 0 && x.Quantity > 0).ToList();
                }
                else if (type.ToUpper() == "R")
                {
                    result = SSRResp.Response.MealDynamic[1].Where(x => x.Price > 0 && x.Quantity > 0).ToList();
                }
            }
            catch (Exception ex)
            { }

            return result;
        }


        public List<Baggage> GetTBOBaggage(SSRResponse SSRResp, string type)
        {
            List<Baggage> result = new List<Baggage>();
            try
            {
                if (type.ToUpper() == "O")
                {
                    result = SSRResp.Response.Baggage[0].Where(x => x.Price > 0 && x.Weight > 0).ToList();
                }
                else if (type.ToUpper() == "R")
                {
                    result = SSRResp.Response.Baggage[1].Where(x => x.Price > 0 && x.Weight > 0).ToList();
                }
            }
            catch (Exception ex)
            { }
            return result;
        }



    }


    public class SSRRequest
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string ResultIndex { get; set; }
    }



    public class Error
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Response
    {
        public List<List<Baggage>> Baggage { get; set; }
        public Error Error { get; set; }
        public List<List<MealDynamic>> MealDynamic { get; set; }
        public int ResponseStatus { get; set; }
        public string TraceId { get; set; }
    }

    public class SSRResponse
    {
        public Response Response { get; set; }
    }


    public class Baggage
    {
        public int WayType { get; set; }
        public string Code { get; set; }
        public int Description { get; set; }
        public int Weight { get; set; }
        public string Currency { get; set; }
        public double Price { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }



    public class MealDynamic
    {
        public int WayType { get; set; }
        public string Code { get; set; }
        public int Description { get; set; }
        public string AirlineDescription { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }

}
