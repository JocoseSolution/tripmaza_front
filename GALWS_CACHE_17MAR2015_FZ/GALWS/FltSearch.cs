﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.IO;

/// <summary>
/// Summary description for Class3
/// </summary>
namespace AirArabia
{

    public class FltSearch
    {


        //public string OriginDestinationInformation { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string OriginLocation { get; set; }
        public string DestinationLocation { get; set; }
        public string dTotalNo_Adt { get; set; }
        public string dTotalNo_CHD { get; set; }
        public string dTotalNo_INF { get; set; }
        public string TripType { get; set; }
        public string ServerUrlorIp { get; set; }
        public string Port { get; set; }
        public string CorporateId { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
     

    }

    public class PriceQuoteRequest
    {

        public string ArrivalDate { get; set; }
        public string DepartureDate { get; set; }
        public string RPH { get; set; }
        public string FlightNumber { get; set; }
        public string ArrAirportCode { get; set; }
        public string DepAirportCode { get; set; }
        public int LineNo { get; set; }



    }

}
