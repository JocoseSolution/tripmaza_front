﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Globalization;

namespace GRP_Booking
{
    public class GroupBooking
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        public void INSERTFLIGHTDETAILS(string RequestId, string From, string DepDate, string DepTime, string DepAirportCode, string To, string ArvlDate, string ArvlTime, string ArvlAirportCode, string Airline, string FlightNo, string Stops, string TripType, string CreatedBY, string Sector, string RefRequestID, string FlightStatus, string Flight)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("USP_GRP_INSERTFLIGHTDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RequestId", RequestId);
                cmd.Parameters.AddWithValue("@From", From);
                cmd.Parameters.AddWithValue("@DepDate", DepDate);
                cmd.Parameters.AddWithValue("@DepTime", DepTime);
                cmd.Parameters.AddWithValue("@DepAirportCode", DepAirportCode);
                cmd.Parameters.AddWithValue("@To", To);
                cmd.Parameters.AddWithValue("@ArvlDate", ArvlDate);
                cmd.Parameters.AddWithValue("@ArvlTime", ArvlTime);
                cmd.Parameters.AddWithValue("@ArvlAirportCode", ArvlAirportCode);
                cmd.Parameters.AddWithValue("@Airline", Airline);
                cmd.Parameters.AddWithValue("@FlightNo", FlightNo);
                cmd.Parameters.AddWithValue("@Stops", Stops);
                cmd.Parameters.AddWithValue("@TripType", TripType);
                cmd.Parameters.AddWithValue("@CreatedBY", CreatedBY);
                cmd.Parameters.AddWithValue("@Sector", Sector);
                cmd.Parameters.AddWithValue("@RefRequestID", RefRequestID);
                cmd.Parameters.AddWithValue("@Flight", Flight);
                cmd.Parameters.AddWithValue("@FlightStatus", FlightStatus);
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "INSERTFLIGHTDETAILS");
            }
        }
        public void INSERTBOOKINGDETAILS(string RequestId, string TripType, string Trip, int NoOfPax, int AdultCount, int ChildCount, int InfantCount, string ServiceType, decimal ExpactedPrice, string PaymentStatus, string Remarks, string CreatedBy, string ProbableDays, string UserType,string BookingName)
        {
            try
            {
                SqlCommand cmd1 = new SqlCommand("USP_GRP_INSERTBOOKINGDETAILS", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@RequestId", RequestId);
                cmd1.Parameters.AddWithValue("@TripType", TripType);
                cmd1.Parameters.AddWithValue("@Trip", Trip);
                cmd1.Parameters.AddWithValue("@NoOfPax", NoOfPax);
                cmd1.Parameters.AddWithValue("@AdultCount", AdultCount);
                cmd1.Parameters.AddWithValue("@ChildCount", ChildCount);
                cmd1.Parameters.AddWithValue("@InfantCount", InfantCount);
                cmd1.Parameters.AddWithValue("@ServiceType", ServiceType);
                cmd1.Parameters.AddWithValue("@ExpactedPrice", ExpactedPrice);
                cmd1.Parameters.AddWithValue("@PaymentStatus", PaymentStatus);
                cmd1.Parameters.AddWithValue("@Remarks", Remarks);
                cmd1.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                cmd1.Parameters.AddWithValue("@ProbableDays", ProbableDays);
                cmd1.Parameters.AddWithValue("@UserType", UserType);
                cmd1.Parameters.AddWithValue("@BookingName", BookingName);
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                cmd1.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "INSERTBOOKINGDETAILS");
            }
        }
        public DataSet ShowBookingData(string ReqID, string PStatus, string FDate, string TDate, string USERTYPE, string UserID)
        {
            DataSet DS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_BOOKINGSTATUS";
                    sqlcmd.Parameters.Add("@RequestID", SqlDbType.VarChar).Value = ReqID;
                    sqlcmd.Parameters.Add("@PaymentStatus", SqlDbType.VarChar).Value = PStatus;
                    sqlcmd.Parameters.Add("@FormDate", SqlDbType.VarChar).Value = FDate;
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = TDate;
                    sqlcmd.Parameters.Add("@UserType", SqlDbType.VarChar).Value = USERTYPE;
                    sqlcmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(DS);
                    con.Close();
                    DS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "ShowBookingData");
            }
            return DS;
        }
        public DataSet GroupRequestDetails(string RequestID, string CMD_Type, string CreatedBY, string USERTYPE)
        {
            DataSet GDS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_REQUESTIDDETAILS";
                    sqlcmd.Parameters.Add("@RequestID", SqlDbType.VarChar).Value = RequestID;
                    sqlcmd.Parameters.Add("@Cmd_Type", SqlDbType.VarChar).Value = CMD_Type;
                    sqlcmd.Parameters.Add("@CreatedBy", SqlDbType.VarChar).Value = CreatedBY;
                    sqlcmd.Parameters.Add("@UserBy", SqlDbType.VarChar).Value = USERTYPE;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(GDS);
                    con.Close();
                    GDS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "GroupRequestDetails");
            }
            return GDS;
        }
        public void PARTNERPRICES(string RequestID, decimal PPrice, string PRemarks, string Provider, string CMD_TYPE, string RefRequestID, string ValidOffer)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_PARTNERPRICES";
                    sqlcmd.Parameters.AddWithValue("@RequestID", RequestID);
                    sqlcmd.Parameters.AddWithValue("@PPrice", PPrice);
                    sqlcmd.Parameters.AddWithValue("@PRemarks", PRemarks);
                    sqlcmd.Parameters.AddWithValue("@Provider", Provider);
                    sqlcmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    sqlcmd.Parameters.AddWithValue("@RefRequestID", RefRequestID);
                    sqlcmd.Parameters.AddWithValue("@ValidOffer", ValidOffer);
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "PARTNERPRICES");
            }
        }
        public void FREEZEPPRICE(string RequestID, decimal bookedPrice, string BKG_PartnerName, string updatedby, string CMD_TYPE, string RefRequestID, string BookingType)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_FREEZEPPRICE";
                    sqlcmd.Parameters.AddWithValue("@RequestID", RequestID);
                    sqlcmd.Parameters.AddWithValue("@bookedPrice", bookedPrice);
                    sqlcmd.Parameters.AddWithValue("@BKG_PartnerName", BKG_PartnerName);
                    sqlcmd.Parameters.AddWithValue("@updatedby", updatedby);
                    sqlcmd.Parameters.AddWithValue("@RefRequestID", RefRequestID);
                    sqlcmd.Parameters.AddWithValue("@BookingType", BookingType);
                    sqlcmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "FREEZEPPRICE");
            }
        }
        public void INSERTFINALFLIGHTDETAILS(string Departure_Location, string Arrival_Location, string Departure_Date, string Departure_Time, string Arrival_Date, string Arrival_Time, string Aircode, string FlightNumber, int counter, string UPDATEBDBY)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_INSERTFINALFLIGHTDETAILS";
                    sqlcmd.Parameters.AddWithValue("@Departure_Location", Departure_Location);
                    sqlcmd.Parameters.AddWithValue("@Arrival_Location", Arrival_Location);
                    sqlcmd.Parameters.AddWithValue("@Departure_Date", Departure_Date);
                    sqlcmd.Parameters.AddWithValue("@Departure_Time", Departure_Time);
                    sqlcmd.Parameters.AddWithValue("@Arrival_Date", Arrival_Date);
                    sqlcmd.Parameters.AddWithValue("@Arrival_Time", Arrival_Time);
                    sqlcmd.Parameters.AddWithValue("@Aircode", Aircode);
                    sqlcmd.Parameters.AddWithValue("@FlightNumber", FlightNumber);
                    sqlcmd.Parameters.AddWithValue("@counter", counter);
                    sqlcmd.Parameters.AddWithValue("@UPDATEBDBY", UPDATEBDBY);
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "INSERTFINALFLIGHTDETAILS");
            }
        }
        public void INSERTFINALBOOKINGDETAILS(int AdultCount, int ChildCount, int InfantCount, string Remarks, string ProbableDays, decimal ExpactedPrice, int counter, string UPDATEBDBY)
        {
            try
            {
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    cmd1.CommandTimeout = 900;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = "USP_GRP_INSERTFINALBOOKINGDETAILS";
                    cmd1.Parameters.AddWithValue("@AdultCount", AdultCount);
                    cmd1.Parameters.AddWithValue("@ChildCount", ChildCount);
                    cmd1.Parameters.AddWithValue("@InfantCount", InfantCount);
                    cmd1.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd1.Parameters.AddWithValue("@ProbableDays", ProbableDays);
                    cmd1.Parameters.AddWithValue("@ExpactedPrice", ExpactedPrice);
                    cmd1.Parameters.AddWithValue("@counter", counter);
                    cmd1.Parameters.AddWithValue("@UPDATEBDBY", UPDATEBDBY);
                    cmd1.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "INSERTFINALBOOKINGDETAILS");
            }
        }
        public void GRP_InsertPaxDetails(string ReqID, string TITLE, string FName, string MName, string LName, string PAXTYPE, string DOB, string GENDER, string PassportExpireDate, string PassportNo, string IssueCountryCode, string NationalityCode, string CMD_TYPE, string ISPaxDetails)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_INSERTPAXDETAILS";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = ReqID;
                    sqlcmd.Parameters.Add("@TITLE", SqlDbType.VarChar).Value = TITLE;
                    sqlcmd.Parameters.Add("@FName", SqlDbType.VarChar).Value = FName;
                    sqlcmd.Parameters.Add("@MName", SqlDbType.VarChar).Value = MName;
                    sqlcmd.Parameters.Add("@LName", SqlDbType.VarChar).Value = LName;
                    sqlcmd.Parameters.Add("@PAXTYPE", SqlDbType.VarChar).Value = PAXTYPE;
                    sqlcmd.Parameters.Add("@DOB", SqlDbType.VarChar).Value = DOB;
                    sqlcmd.Parameters.Add("@GENDER", SqlDbType.VarChar).Value = GENDER;
                    sqlcmd.Parameters.Add("@PassportExpireDate", SqlDbType.VarChar).Value = PassportExpireDate;
                    sqlcmd.Parameters.Add("@PassportNo", SqlDbType.VarChar).Value = PassportNo;
                    sqlcmd.Parameters.Add("@IssueCountryCode", SqlDbType.VarChar).Value = IssueCountryCode;
                    sqlcmd.Parameters.Add("@NationalityCode", SqlDbType.VarChar).Value = NationalityCode;
                    sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_TYPE;
                    sqlcmd.Parameters.Add("@ISPaxDetails", SqlDbType.VarChar).Value = ISPaxDetails;
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "GRP_InsertPaxDetails");
            }
        }
        public void RequestStatus(string RequestID, string USER, string CMD_TYPE)
        {
            try
            {
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    cmd1.CommandTimeout = 900;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = "USP_GRP_RequestStatus";
                    cmd1.Parameters.AddWithValue("@RequestID", RequestID);
                    cmd1.Parameters.AddWithValue("@USER", USER);
                    cmd1.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    cmd1.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "RequestStatus");
            }
        }
        public int SendMail(string toEMail, string @from, string bcc, string cc, string smtpClient, string userID, string pass, string body, string subject, string AttachmentFile)
        {
            System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
            msgMail.To.Clear();
            msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
            msgMail.From = new System.Net.Mail.MailAddress(@from);
            if (!string.IsNullOrEmpty(bcc))
            {
                msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                msgMail.CC.Add(cc);
            }
            if (!string.IsNullOrEmpty(AttachmentFile))
            {
                msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
            }

            msgMail.Subject = subject;
            msgMail.IsBodyHtml = true;
            msgMail.Body = body;
            try
            {
                objMail.Credentials = new System.Net.NetworkCredential(userID, pass);
                objMail.Host = smtpClient;
                objMail.Send(msgMail);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "SendMail");
                return 0;
            }
        }
        public void CancelByAdmin(string RequestID, decimal bookedPrice, string BKG_PartnerName, string updatedby, string CMD_TYPE)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_CANCELBYADMIN";
                    sqlcmd.Parameters.AddWithValue("@RequestID", RequestID);
                    sqlcmd.Parameters.AddWithValue("@bookedPrice", bookedPrice);
                    sqlcmd.Parameters.AddWithValue("@BKG_PartnerName", BKG_PartnerName);
                    sqlcmd.Parameters.AddWithValue("@updatedby", updatedby);
                    sqlcmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "CancelByAdmin");
            }
        }
        //public DataSet RequestDetails(string RequestID, string CMD_Type)
        //{
        //    DataSet GDS = new DataSet();
        //    try
        //    {
        //        using (SqlCommand sqlcmd = new SqlCommand())
        //        {

        //            sqlcmd.Connection = con;
        //            con.Open();
        //            sqlcmd.CommandTimeout = 900;
        //            sqlcmd.CommandType = CommandType.StoredProcedure;
        //            sqlcmd.CommandText = "USP_GRP_MAILINFODETAILS";
        //            sqlcmd.Parameters.Add("@RequestID", SqlDbType.VarChar).Value = RequestID;
        //            sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_Type;
        //            SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
        //            da.Fill(GDS);
        //            con.Close();
        //            GDS.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogTrace.WriteErrorLog(ex, "ShowBookingData");
        //    }
        //    return GDS;
        //}
        public string GETEMAILID(string RequestID, string CMD_TYPE)
        {
            string Result = "";
            try
            {
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    cmd1.CommandTimeout = 900;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = "USP_GRP_GETEMAILADDRESS";
                    cmd1.Parameters.AddWithValue("@RequestID", RequestID);
                    cmd1.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    Result = Convert.ToString(cmd1.ExecuteScalar());
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "GETEMAILID");
            }
            return Result;
        }
        public string PAYMENTSTATUS(string RequestID, string CMD_TYPE)
        {
            string Result = "";
            try
            {
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    cmd1.CommandTimeout = 900;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandText = "USP_GRP_PAYMENTSTATUS";
                    cmd1.Parameters.AddWithValue("@REQUESTID", RequestID);
                    cmd1.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
                    Result = Convert.ToString(cmd1.ExecuteScalar());
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "PAYMENTSTATUS");
            }
            return Result;
        }
        public DataSet GROUPTICKETINFO(string REQUESTID, string GDSPNRNO, string AIRLINEPNRNO, string TICKETNO, string COUNTERID, string CMD_TYPE, string TABLE)
        {
            DataSet GDS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_UPDATETICKETPNR";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = REQUESTID;
                    sqlcmd.Parameters.Add("@GDSPNRNO", SqlDbType.VarChar).Value = GDSPNRNO;
                    sqlcmd.Parameters.Add("@AIRLINEPNRNO", SqlDbType.VarChar).Value = AIRLINEPNRNO;
                    sqlcmd.Parameters.Add("@TICKETNO", SqlDbType.VarChar).Value = TICKETNO;
                    sqlcmd.Parameters.Add("@COUNTERID", SqlDbType.VarChar).Value = Convert.ToInt32(COUNTERID);
                    sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_TYPE;
                    sqlcmd.Parameters.Add("@TABLE", SqlDbType.VarChar).Value = TABLE;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(GDS);
                    con.Close();
                    GDS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "GROUPTICKETINFO");
            }
            return GDS;
        }
        public void UPDATEGROUPTICKETINFO(string REQUESTID, string GDSPNRNO, string AIRLINEPNRNO, string TICKETNO, string COUNTERID, string CMD_TYPE, string TABLE)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_UPDATETICKETPNR";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = REQUESTID;
                    sqlcmd.Parameters.Add("@GDSPNRNO", SqlDbType.VarChar).Value = GDSPNRNO;
                    sqlcmd.Parameters.Add("@AIRLINEPNRNO", SqlDbType.VarChar).Value = AIRLINEPNRNO;
                    sqlcmd.Parameters.Add("@TICKETNO", SqlDbType.VarChar).Value = TICKETNO;
                    sqlcmd.Parameters.Add("@COUNTERID", SqlDbType.VarChar).Value = Convert.ToInt32(COUNTERID);
                    sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_TYPE;
                    sqlcmd.Parameters.Add("@TABLE", SqlDbType.VarChar).Value = TABLE;
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "UPDATEGROUPTICKETINFO");
            }
        }
        public void UPDATEPAYMENTSTATUS(string REQUESTID, string USERID, string CMD_TYPE)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_UPDATEPAYMENTSTATUS";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = REQUESTID;
                    sqlcmd.Parameters.Add("@USERID", SqlDbType.VarChar).Value = USERID;
                    sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_TYPE;
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "UPDATEPAYMENTSTATUS");
            }
        }
        public int DateDiff(string To, string From)
        {
            DateTime DTo;
            DateTime DFrom;
            TimeSpan TS;
            int NoOfDays;

            DTo = DateTime.ParseExact(To, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DFrom = DateTime.ParseExact(From, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            TS = DFrom.Subtract(DTo);
            return NoOfDays = Convert.ToInt32(TS.TotalDays);
        }
        public DataSet SENDPAYMENTLINK(string REQUESTID, string USERID)
        {
            DataSet GDS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_GetStatus";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = REQUESTID;
                    sqlcmd.Parameters.Add("@USERID", SqlDbType.VarChar).Value = USERID;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(GDS);
                    con.Close();
                    GDS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "SENDPAYMENTLINK");
            }
            return GDS;
        }
        public DataSet PAXINFO(string REQUESTID)
        {
            DataSet GDS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_GETPAXINFO";
                    sqlcmd.Parameters.Add("@REQUESTID", SqlDbType.VarChar).Value = REQUESTID;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(GDS);
                    con.Close();
                    GDS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "PAXINFO");
            }
            return GDS;
        }
        public void UPDATEPAXINFOR(string Title, string FName, string MName, string LName, string PaxType, string DOB, string Gender, string PassportExpireDate, string PassportNo, string IssueCountryCode, string NationalityCode, int COUNTER, string CMD_TYPE)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_UPDATEPAXINFO";
                    sqlcmd.Parameters.Add("@TITLE", SqlDbType.VarChar).Value = Title;
                    sqlcmd.Parameters.Add("@FNAME", SqlDbType.VarChar).Value = FName;
                    sqlcmd.Parameters.Add("@MNAME", SqlDbType.VarChar).Value = MName;
                    sqlcmd.Parameters.Add("@LNAME", SqlDbType.VarChar).Value = LName;
                    sqlcmd.Parameters.Add("@PaxType", SqlDbType.VarChar).Value = PaxType;
                    sqlcmd.Parameters.Add("@DOB", SqlDbType.VarChar).Value = DOB;
                    sqlcmd.Parameters.Add("@Gender", SqlDbType.VarChar).Value = Gender;
                    sqlcmd.Parameters.Add("@PassportNo", SqlDbType.VarChar).Value = PassportNo;
                    sqlcmd.Parameters.Add("@PassportExpireDate", SqlDbType.VarChar).Value = PassportExpireDate;
                    sqlcmd.Parameters.Add("@NationalityCode", SqlDbType.VarChar).Value = NationalityCode;
                    sqlcmd.Parameters.Add("@IssueCountryCode", SqlDbType.VarChar).Value = IssueCountryCode;
                    sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = CMD_TYPE;
                    sqlcmd.Parameters.Add("@COUNTER", SqlDbType.Int).Value = COUNTER;
                    sqlcmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "UPDATEPAXINFOR");
            }
        }
        public DataSet AirportInfo(string DestinationCode)
        {
            DataSet GDS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "USP_GRP_FEATCHAIRPORTINFO";
                    sqlcmd.Parameters.Add("@DepAirportCode", SqlDbType.VarChar).Value = DestinationCode;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(GDS);
                    con.Close();
                    GDS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "AirportInfo");
            }
            return GDS;
        }
        public DataSet GetFltDtlsGroupBooking(string trackid, string UserId)
        {
            DataSet DS = new DataSet();
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.Connection = con;
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                    else
                    {
                        con.Open();
                    }
                    sqlcmd.CommandTimeout = 900;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "GetSelectedFltDtls_Gal_GROUPBOOKING";
                    sqlcmd.Parameters.Add("@TrackId", SqlDbType.VarChar).Value = trackid;
                    sqlcmd.Parameters.Add("@UserId", SqlDbType.VarChar).Value = UserId;
                    SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                    da.Fill(DS);
                    con.Close();
                    DS.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorLogTrace.WriteErrorLog(ex, "GetFltDtlsGroupBooking");
            }
            return DS;
        }
    }
}
