﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using GoAirServiceDll;
using System.Data;
using STD.Shared;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace STD.BAL
{
    public class GoAirSearchFlightsBAL
    {
        RadixxFlights flights = new RadixxFlights();
        RadixxTravelAgents agent = new RadixxTravelAgents();
        public string LoginID { get; set; }
        public string LoginPass { get; set; }
        public string CorporateID { get; set; }
        public string UserID { get; set; }
        public string UserPass { get; set; }
        public string securityGUID { get; set; }
        public string IP { get; set; }

        public GoAirSearchFlightsBAL(string ip, string loginId, string loginPass, string CorporateId, string UserId, string userPass)
        {
            LoginID = loginId;// "GoAir_Test";
            LoginPass = loginPass;// "confirm";
            CorporateID = CorporateId;// "DEL83021";
            UserID = UserId;// "springtest";
            UserPass = userPass;// "P@ssword1";
            securityGUID = GetSecurityGUID();
            IP = ip;
        }

        private string GetSecurityGUID()
        {
            RadixxSecurity sec = new RadixxSecurity();
            return sec.GetSecurityGUID(LoginID, LoginPass);

        }

        public ArrayList GetFlightAvailability(FlightSearch f, List<FlightCityList> CityList)
        {
            ArrayList resultList = new ArrayList();
            if (agent.LoginTravelAgencyUser(securityGUID, CorporateID, UserID, UserPass))
            {
                resultList = GetFlightAvailabilityRespose(flights.GetFareQuote(securityGUID, CreateSearchCriteria(f), IP), f, CityList);

            }
            return resultList;
        }

        private string CreateSearchCriteria(FlightSearch f)
        {
            string[] depD = f.DepDate.Split('/');
            string[] arrD = f.RetDate.Split('/');
            int filtermethod = 102;//f.RTF == true ? 22 : 102;


            string departureDate = depD.Length > 1 ? (depD[2] + "-" + (depD[1].Trim().Length > 1 ? depD[1].Trim() : "0" + depD[1].Trim()) + "-" + (depD[0].Trim().Length > 1 ? depD[0].Trim() : "0" + depD[0].Trim()) + "T00:00:00") : null;
            string arrivalDate = arrD.Length > 1 ? (arrD[2] + "-" + (arrD[1].Trim().Length > 1 ? arrD[1].Trim() : "0" + arrD[1].Trim()) + "-" + (arrD[0].Trim().Length > 1 ? arrD[0].Trim() : "0" + arrD[0].Trim()) + "T00:00:00") : null;

            XDocument xmldoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"), new XElement("RadixxFareQuoteRequest",
                                     new XElement("CurrencyOfFareQuote", "INR"),
           new XElement("PromotionalCode", null),
           new XElement("IataNumberOfRequestor", CorporateID),
            new XElement("FareQuoteDetails",

                                                      new XElement("FareQuoteDetail",
                                                          new XElement("Origin", f.HidTxtDepCity.Split(',')[0]), new XElement("Destination", f.HidTxtArrCity.Split(',')[0]),
                                                          new XElement("DateOfDeparture", departureDate), new XElement("FareTypeCategory", 1),
                                                          new XElement("Cabin", f.Cabin), new XElement("NumberOfDaysBefore", 0),
                                                          new XElement("NumberOfDaysAfter", 0), new XElement("LanguageCode", null),
                                                          new XElement("TicketPackageID", null), new XElement("FareFilterMethod", filtermethod),
                                                          new XElement("FareQuoteRequestInfos",
                                                            f.Adult > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 1), new XElement("TotalSeatsRequired", f.Adult))) : null,

                                                             f.Child > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 6), new XElement("TotalSeatsRequired", f.Child))) : null,
                                                             f.Infant > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 5), new XElement("TotalSeatsRequired", f.Infant))) : null)),

                                                           f.TripType == TripType.RoundTrip ?

                                                     (new XElement("FareQuoteDetail",
                                                          new XElement("Origin", f.HidTxtArrCity.Split(',')[0]), new XElement("Destination", f.HidTxtDepCity.Split(',')[0]),
                                                          new XElement("DateOfDeparture", arrivalDate), new XElement("FareTypeCategory", 1),
                                                          new XElement("Cabin", f.Cabin), new XElement("NumberOfDaysBefore", 0),
                                                          new XElement("NumberOfDaysAfter", 0), new XElement("LanguageCode", null),
                                                          new XElement("TicketPackageID", null), new XElement("FareFilterMethod", filtermethod),
                                                          new XElement("FareQuoteRequestInfos",
                                                            f.Adult > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 1), new XElement("TotalSeatsRequired", f.Adult))) : null,

                                                             f.Child > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 6), new XElement("TotalSeatsRequired", f.Child))) : null,
                                                             f.Infant > 0 ? (
                                                              new XElement("FareQuoteRequestInfo",
                                                            new XElement("PassengerTypeID", 5), new XElement("TotalSeatsRequired", f.Infant))) : null))) : null

                                                            )));






            return xmldoc.ToString();
        }

        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode == depLocCode select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode == depLocCode select ct).ToList())[0].City;
            }

            return name;
        }

        private ArrayList GetFlightAvailabilityRespose(string responseXml, FlightSearch f, List<FlightCityList> CityList)
        {

            XDocument xmlDoc = XDocument.Parse(responseXml);
            DataSet ds = new DataSet();

            #region Goair Parse result
            var colval = from FSD in xmlDoc.Root.Elements("FlightSegmentDetails").Elements("FlightSegment")
                         select new
                         {
                             LFID = (int)FSD.Attribute("LFID"),
                             DepartureDate = (string)FSD.Attribute("DepartureDate"),
                             International = (string)FSD.Attribute("International"),
                             LegCount = (int)FSD.Attribute("LegCount"),
                             FlightLegDetails = from FlightLegItem in FSD.Element("FlightLegDetails").Elements("FlightLeg")
                                                select new
                                                {
                                                    LFID = (int)FSD.Attribute("LFID"),
                                                    PFID = (string)FlightLegItem.Attribute("PFID"),
                                                    DepartureDate = (string)FlightLegItem.Attribute("DepartureDate")
                                                },


                             FareTypeItems = f.RTF == true ? (from FareTypes in FSD.Element("FareTypes").Elements("FareType")
                                                              where ((FareTypes.Attribute("FareTypeName")).Value.Contains("(ROUNDTRIP)"))
                                                              select new
                                                              {

                                                                  FareTypeID = (string)FareTypes.Attribute("FareTypeID"),
                                                                  FareTypeName = (string)FareTypes.Attribute("FareTypeName"),

                                                                  FareInfoItems = from FareInfo in FareTypes.Element("FareInfos").Elements("FareInfo")
                                                                                  select new
                                                                                  {
                                                                                      LFID = (int)FSD.Attribute("LFID"),
                                                                                      DepartureDate = (string)FSD.Attribute("DepartureDate"),
                                                                                      International = (string)FSD.Attribute("International"),
                                                                                      LegCount = (string)FSD.Attribute("LegCount"),
                                                                                      FareID = (string)FareInfo.Attribute("FareID"),
                                                                                      FCCode = (string)FareInfo.Attribute("FCCode"),
                                                                                      FBCode = (string)FareInfo.Attribute("FBCode"),
                                                                                      FareTypeName = (string)FareTypes.Attribute("FareTypeName"),
                                                                                      FareTypeID = (string)FareTypes.Attribute("FareTypeID"),
                                                                                      BaseFareAmtNoTaxes = (string)FareInfo.Attribute("BaseFareAmtNoTaxes"),
                                                                                      BaseFareAmt = (string)FareInfo.Attribute("BaseFareAmt"),
                                                                                      FareAmtNoTaxes = (string)FareInfo.Attribute("FareAmtNoTaxes"),
                                                                                      FareAmt = (string)FareInfo.Attribute("FareAmt"),
                                                                                      BaseFareAmtInclTax = (string)FareInfo.Attribute("BaseFareAmtInclTax"),
                                                                                      FareAmtInclTax = (string)FareInfo.Attribute("FareAmtInclTax"),
                                                                                      PvtFare = (string)FareInfo.Attribute("PvtFare"),
                                                                                      PTCID = (string)FareInfo.Attribute("PTCID"),
                                                                                      Cabin = (string)FareInfo.Attribute("Cabin"),
                                                                                      SeatsAvailable = (string)FareInfo.Attribute("SeatsAvailable"),
                                                                                      RoundTrip = (string)FareInfo.Attribute("RoundTrip"),


                                                                                      TaxItems = from taxItem in FareInfo.Element("ApplicableTaxDetails").Elements("ApplicableTax")
                                                                                                 select new
                                                                                                 {

                                                                                                     TaxID = (string)taxItem.Attribute("TaxID"),
                                                                                                     Amt = (string)taxItem.Attribute("Amt"),
                                                                                                     InitiatingTaxID = (string)taxItem.Attribute("InitiatingTaxID")

                                                                                                 },
                                                                                      FlightLegDetails = from FlightLegItem in FSD.Element("FlightLegDetails").Elements("FlightLeg")
                                                                                                         select new
                                                                                                         {
                                                                                                             PFID = (string)FlightLegItem.Attribute("PFID"),
                                                                                                             DepartureDate = (string)FlightLegItem.Attribute("DepartureDate")
                                                                                                         },




                                                                                  }

                                                              }) : (from FareTypes in FSD.Element("FareTypes").Elements("FareType")

                                                                    select new
                                                                    {

                                                                        FareTypeID = (string)FareTypes.Attribute("FareTypeID"),
                                                                        FareTypeName = (string)FareTypes.Attribute("FareTypeName"),

                                                                        FareInfoItems = from FareInfo in FareTypes.Element("FareInfos").Elements("FareInfo")
                                                                                        select new
                                                                                        {
                                                                                            LFID = (int)FSD.Attribute("LFID"),
                                                                                            DepartureDate = (string)FSD.Attribute("DepartureDate"),
                                                                                            International = (string)FSD.Attribute("International"),
                                                                                            LegCount = (string)FSD.Attribute("LegCount"),
                                                                                            FareID = (string)FareInfo.Attribute("FareID"),
                                                                                            FCCode = (string)FareInfo.Attribute("FCCode"),
                                                                                            FBCode = (string)FareInfo.Attribute("FBCode"),
                                                                                            FareTypeName = (string)FareTypes.Attribute("FareTypeName"),
                                                                                            FareTypeID = (string)FareTypes.Attribute("FareTypeID"),
                                                                                            BaseFareAmtNoTaxes = (string)FareInfo.Attribute("BaseFareAmtNoTaxes"),
                                                                                            BaseFareAmt = (string)FareInfo.Attribute("BaseFareAmt"),
                                                                                            FareAmtNoTaxes = (string)FareInfo.Attribute("FareAmtNoTaxes"),
                                                                                            FareAmt = (string)FareInfo.Attribute("FareAmt"),
                                                                                            BaseFareAmtInclTax = (string)FareInfo.Attribute("BaseFareAmtInclTax"),
                                                                                            FareAmtInclTax = (string)FareInfo.Attribute("FareAmtInclTax"),
                                                                                            PvtFare = (string)FareInfo.Attribute("PvtFare"),
                                                                                            PTCID = (string)FareInfo.Attribute("PTCID"),
                                                                                            Cabin = (string)FareInfo.Attribute("Cabin"),
                                                                                            SeatsAvailable = (string)FareInfo.Attribute("SeatsAvailable"),
                                                                                            RoundTrip = (string)FareInfo.Attribute("RoundTrip"),


                                                                                            TaxItems = from taxItem in FareInfo.Element("ApplicableTaxDetails").Elements("ApplicableTax")
                                                                                                       select new
                                                                                                       {

                                                                                                           TaxID = (string)taxItem.Attribute("TaxID"),
                                                                                                           Amt = (string)taxItem.Attribute("Amt"),
                                                                                                           InitiatingTaxID = (string)taxItem.Attribute("InitiatingTaxID")

                                                                                                       },
                                                                                            FlightLegDetails = from FlightLegItem in FSD.Element("FlightLegDetails").Elements("FlightLeg")
                                                                                                               select new
                                                                                                               {
                                                                                                                   PFID = (string)FlightLegItem.Attribute("PFID"),
                                                                                                                   DepartureDate = (string)FlightLegItem.Attribute("DepartureDate")
                                                                                                               },

                                                                                        }

                                                                    })
                         };



            var taxSegment = from TSD in xmlDoc.Root.Elements("TaxDetails").Elements("Tax")
                             select new
                             {
                                 TaxID = (string)TSD.Attribute("TaxID"),
                                 CodeType = (string)TSD.Attribute("CodeType"),
                                 TaxCurr = (string)TSD.Attribute("TaxCurr")
                             };


            var LegDetails = from LGD in xmlDoc.Root.Elements("LegDetails").Elements("Leg")
                             select new
                             {
                                 PFID = (string)LGD.Attribute("PFID"),
                                 Origin = (string)LGD.Attribute("Origin"),
                                 Destination = (string)LGD.Attribute("Destination"),
                                 DepartureDate = (string)LGD.Attribute("DepartureDate"),
                                 FlightNum = (string)LGD.Attribute("FlightNum"),
                                 International = (string)LGD.Attribute("International"),
                                 ArrivalDate = (string)LGD.Attribute("ArrivalDate"),
                                 FlightTime = (string)LGD.Attribute("FlightTime")

                             };


            var Segment = from SD in xmlDoc.Root.Elements("SegmentDetails").Elements("Segment")
                          select new
                          {
                              LFID = (int)SD.Attribute("LFID"),
                              Origin = (string)SD.Attribute("Origin"),
                              Destination = (string)SD.Attribute("Destination"),
                              DepartureDate = (string)SD.Attribute("DepartureDate"),
                              CarrierCode = (string)SD.Attribute("CarrierCode"),
                              FlightNum = (string)SD.Attribute("FlightNum"),
                              International = (string)SD.Attribute("International"),
                              ArrivalDate = (string)SD.Attribute("ArrivalDate"),
                              FlightTime = (string)SD.Attribute("FlightTime"),
                              Stops = (string)SD.Attribute("Stops"),
                              AircraftType = (string)SD.Attribute("AircraftType"),
                              OperatingCarrier = (string)SD.Attribute("OperatingCarrier"),


                          };

            #endregion

            List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            string[] ardateArr = f.RetDate.Split('/');
            string inpArrdate = ardateArr[2] + "-" + ardateArr[1] + "-" + ardateArr[0];
            int linenum = 1;
            #region Parse result convert to list
            foreach (var fsd in colval)
            {
                foreach (var faretypeItem in fsd.FareTypeItems)
                {
                    int legcount = 1;
                    foreach (var legitem in fsd.FlightLegDetails)
                    {
                        long legC = fsd.FlightLegDetails.LongCount();
                        FlightSearchResults fsr = new FlightSearchResults();
                        foreach (var legdetailsM in LegDetails)
                        {
                            if (legitem.PFID == legdetailsM.PFID)
                            {
                                fsr.depdatelcc = legdetailsM.DepartureDate.Trim();
                                fsr.arrdatelcc = legdetailsM.ArrivalDate.Trim();
                                fsr.Departure_Date = legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                fsr.Arrival_Date = legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                fsr.FlightIdentification = legdetailsM.FlightNum.Trim();
                                fsr.DepartureDate = Convert.ToDateTime(legdetailsM.DepartureDate.Trim()).ToString("ddMMyy");
                                fsr.ArrivalDate = Convert.ToDateTime(legdetailsM.ArrivalDate.Trim()).ToString("ddMMyy");
                                //if (fsr.depdatelcc.Contains(inpArrdate))
                                //{
                                //    fsr.OrgDestFrom = Utility.Left(f.HidTxtArrCity, 3);
                                //    fsr.OrgDestTo = Utility.Left(f.HidTxtDepCity, 3);
                                //}
                                //else
                                //{
                                //    fsr.OrgDestFrom = Utility.Left(f.HidTxtDepCity, 3);
                                //    fsr.OrgDestTo = Utility.Left(f.HidTxtArrCity, 3);
                                //}
                                if (f.DepDate.Trim() == f.RetDate.Trim())
                                {
                                    var segOrigib = Segment.Where(x => x.LFID == fsd.LFID).ToList()[0];


                                    if (segOrigib.Origin.Trim().ToUpper() == Utility.Left(f.HidTxtArrCity, 3).ToUpper())
                                    {
                                        fsr.OrgDestFrom = Utility.Left(f.HidTxtArrCity, 3);
                                        fsr.OrgDestTo = Utility.Left(f.HidTxtDepCity, 3);


                                    }
                                    else
                                    {
                                        fsr.OrgDestFrom = Utility.Left(f.HidTxtDepCity, 3);
                                        fsr.OrgDestTo = Utility.Left(f.HidTxtArrCity, 3);
                                    }

                                }
                                else
                                {
                                    if (fsr.depdatelcc.Contains(inpArrdate))
                                    {
                                        fsr.OrgDestFrom = Utility.Left(f.HidTxtArrCity, 3);
                                        fsr.OrgDestTo = Utility.Left(f.HidTxtDepCity, 3);
                                    }
                                    else
                                    {
                                        fsr.OrgDestFrom = Utility.Left(f.HidTxtDepCity, 3);
                                        fsr.OrgDestTo = Utility.Left(f.HidTxtArrCity, 3);
                                    }
                                }

                                if (f.RTF == true)
                                    fsr.Sector = Utility.Left(f.HidTxtDepCity, 3) + ":" + Utility.Left(f.HidTxtArrCity, 3) + ":" + Utility.Left(f.HidTxtDepCity, 3);
                                else
                                    fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo;

                                fsr.DepartureLocation = legdetailsM.Origin.Trim();
                                fsr.ArrivalLocation = legdetailsM.Destination.Trim();
                                fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, legdetailsM.Destination.Trim());
                                fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, legdetailsM.Origin.Trim());
                                fsr.DepartureTime = legdetailsM.DepartureDate.Remove(0, legdetailsM.DepartureDate.IndexOf("T") + 1).Replace(":", "").Remove(4);
                                fsr.ArrivalTime = legdetailsM.ArrivalDate.Remove(0, legdetailsM.ArrivalDate.IndexOf("T") + 1).Replace(":", "").Remove(4);
                                fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, legdetailsM.Origin.Trim());
                                fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, legdetailsM.Destination.Trim());

                                fsr.Trip = f.Trip.ToString();
                                fsr.TripType = f.TripType.ToString();
                                fsr.LineNumber = linenum;
                                fsr.Leg = legcount;
                            }
                        }

                        foreach (var fareInfo in faretypeItem.FareInfoItems)
                        {
                            decimal totalTax = 0;
                            decimal Fuel_Tax = 0;
                            foreach (var taxItem in fareInfo.TaxItems)
                            {
                                foreach (var texsItem in taxSegment)
                                {
                                    if ((taxItem.TaxID == texsItem.TaxID) && (texsItem.CodeType == "FUEL"))
                                    {
                                        Fuel_Tax = Fuel_Tax + Convert.ToDecimal(taxItem.Amt);
                                    }
                                }
                            }

                            totalTax = Convert.ToDecimal(fareInfo.BaseFareAmtInclTax) - Convert.ToDecimal(fareInfo.BaseFareAmt);
                            if (Convert.ToInt16(fareInfo.PTCID) == 1)
                            {

                                fsr.AdtAvlStatus = fareInfo.SeatsAvailable.Trim();
                                fsr.AdtBfare = float.Parse(fareInfo.BaseFareAmt);
                                fsr.AdtCabin = fareInfo.Cabin.Trim();
                                fsr.AdtFSur = float.Parse(Fuel_Tax.ToString());
                                fsr.AdtTax = float.Parse((totalTax).ToString());
                                fsr.AdtOT = float.Parse((totalTax - Fuel_Tax).ToString());
                                fsr.AdtRbd = fareInfo.FCCode.Trim();
                                fsr.AdtFarebasis = fareInfo.FBCode.Trim();
                                fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                fsr.AdtFare = float.Parse(fareInfo.BaseFareAmtInclTax);
                                fsr.sno = fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                fsr.fareBasis = fsr.AdtFarebasis;
                                fsr.FBPaxType = "ADT";

                            }

                            if (Convert.ToInt16(fareInfo.PTCID) == 6)
                            {

                                fsr.ChdAvlStatus = fareInfo.SeatsAvailable.Trim();
                                fsr.ChdBFare = float.Parse(fareInfo.BaseFareAmt);
                                fsr.ChdCabin = fareInfo.Cabin.Trim();
                                fsr.ChdFSur = float.Parse(Fuel_Tax.ToString());
                                fsr.ChdTax = float.Parse((totalTax).ToString());
                                fsr.ChdOT = float.Parse((totalTax - Fuel_Tax).ToString());
                                fsr.ChdRbd = fareInfo.FCCode.Trim();
                                fsr.ChdFarebasis = fareInfo.FBCode.Trim();
                                fsr.ChdfareType = fareInfo.FareTypeID.Trim();
                                // fsr.ChdfareTypeName = fareInfo.FareTypeName.Trim();
                                fsr.ChdFare = float.Parse(fareInfo.BaseFareAmtInclTax);
                                //if (f.Adult == 0)
                                //{
                                //    fsr.sno = fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                //}

                            }
                            if (Convert.ToInt16(fareInfo.PTCID) == 5)
                            {

                                fsr.InfAvlStatus = fareInfo.SeatsAvailable.Trim();
                                fsr.InfBfare = float.Parse(fareInfo.BaseFareAmt);
                                fsr.InfCabin = fareInfo.Cabin.Trim();
                                fsr.InfFSur = float.Parse(Fuel_Tax.ToString());
                                fsr.InfTax = float.Parse((totalTax).ToString());
                                fsr.InfOT = float.Parse((totalTax - Fuel_Tax).ToString());
                                fsr.InfRbd = fareInfo.FCCode.Trim();
                                fsr.InfFarebasis = fareInfo.FBCode.Trim();
                                fsr.InfFareType = fareInfo.FareTypeID.Trim();
                                //fsr.InfFareTypeName = fareInfo.FareTypeName.Trim();
                                fsr.InfFare = float.Parse(fareInfo.BaseFareAmtInclTax);
                                //if (f.Adult == 0 && f.Child == 0)
                                //{
                                //    fsr.sno = fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                //}

                            }
                            fsr.AirLineName = "GoAir";
                            fsr.Stops = (legC - 1).ToString().Trim() + "-Stop";
                            fsr.Adult = f.Adult;
                            fsr.Child = f.Child;
                            fsr.Infant = f.Infant;
                            fsr.TotPax = f.Adult + f.Child;
                            foreach (var seg in Segment)
                            {
                                if (fareInfo.LFID == seg.LFID)
                                {
                                    fsr.EQ = seg.AircraftType.Trim();
                                    fsr.MarketingCarrier = seg.CarrierCode.Trim();
                                    fsr.OperatingCarrier = seg.OperatingCarrier.Trim();
                                    fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(seg.FlightTime.Trim()));
                                    fsr.ValiDatingCarrier = seg.CarrierCode.Trim();
                                }
                            }
                        }

                        fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                        fsr.OriginalTF = fsr.TotalFare;
                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                        fsr.OriginalTT = fsr.TotalTax;
                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                        fsr.Trip = f.Trip.ToString();

                        int status = CheckObjectAddTOList(f, fsr);

                        if (status == 1)
                        {
                            if (f.TripType == TripType.RoundTrip)
                            {
                                if (f.RTF == true)
                                {
                                    //if (Convert.ToDateTime(fsr.depdatelcc).Date >= Convert.ToDateTime(inpArrdate).Date) //fsr.depdatelcc.Contains(inpArrdate))
                                    //{
                                    //    fsr.Flight = "2";
                                    //    fsr.TripType = "R";// TripType.RoundTrip.ToString();
                                    //    if (f.RTF == true)
                                    //        fsr.FType = "RTF";
                                    //    else
                                    //        fsr.FType = "InBound";
                                    //    RfsrList.Add(fsr);
                                    //}
                                    //else
                                    //{
                                    //    fsr.Flight = "1";
                                    //    fsr.TripType = "O";// TripType.OneWay.ToString();
                                    //    if (f.RTF == true)
                                    //        fsr.FType = "RTF";
                                    //    else
                                    //        fsr.FType = "OutBound";
                                    //    fsrList.Add(fsr);
                                    //}
                                    //----------------------------------------------
                                    //if (Convert.ToDateTime(fsr.depdatelcc).Date >= Convert.ToDateTime(inpArrdate).Date)
                                    //{
                                        var segOrigib = Segment.Where(x => x.LFID == fsd.LFID).ToList()[0];
                                        if (segOrigib.Origin.Trim().ToUpper() == Utility.Left(f.HidTxtArrCity, 3).ToUpper())
                                        {                                            
                                            fsr.Flight = "2";
                                            fsr.TripType = "R";// TripType.RoundTrip.ToString();
                                            if (f.RTF == true)
                                                fsr.FType = "RTF";
                                            else
                                                fsr.FType = "InBound";
                                            RfsrList.Add(fsr);
                                        }
                                        else
                                        {                                           
                                            fsr.Flight = "1";
                                            fsr.TripType = "O";// TripType.OneWay.ToString();
                                            if (f.RTF == true)
                                                fsr.FType = "RTF";
                                            else
                                                fsr.FType = "OutBound";
                                            fsrList.Add(fsr);
                                        }
                                   // }
                                    //-------------------------
                                    
                                }
                                else
                                {
                                    if (fsr.depdatelcc.Contains(inpArrdate))
                                    {
                                        fsr.Flight = "2";
                                        fsr.TripType = "R";// TripType.RoundTrip.ToString();
                                        if (f.RTF == true)
                                            fsr.FType = "RTF";
                                        else
                                            fsr.FType = "InBound";
                                        RfsrList.Add(fsr);
                                    }
                                    else
                                    {
                                        fsr.Flight = "1";
                                        fsr.TripType = "O";// TripType.OneWay.ToString();
                                        if (f.RTF == true)
                                            fsr.FType = "RTF";
                                        else
                                            fsr.FType = "OutBound";
                                        fsrList.Add(fsr);
                                    }

                                }

                                //if (fsr.depdatelcc.Contains(inpArrdate))
                                //{
                                //    fsr.Flight = "2";
                                //    fsr.TripType = "R";// TripType.RoundTrip.ToString();
                                //    if (f.RTF == true)
                                //        fsr.FType = "RTF";
                                //    else
                                //        fsr.FType = "InBound";
                                //    RfsrList.Add(fsr);
                                //}
                                //else
                                //{
                                //    fsr.Flight = "1";
                                //    fsr.TripType = "O";// TripType.OneWay.ToString();
                                //    if (f.RTF == true)
                                //        fsr.FType = "RTF";
                                //    else
                                //        fsr.FType = "OutBound";
                                //    fsrList.Add(fsr);
                                //}
                            }
                            else
                            {
                                fsr.Flight = "1";
                                fsrList.Add(fsr);
                            }
                            legcount = legcount + 1;
                        }
                    }
                    linenum++;
                }
            }
            #endregion

            ArrayList finalResult1 = new ArrayList();
            if (f.RTF)
            {
                var linefsrlist = fsrList.Select(x => x.LineNumber).Distinct().ToList();
                for (int i = 0; i < linefsrlist.Count; i++)
                {
                    fsrList.Where(x => x.LineNumber == linefsrlist[i]).ToList().ForEach(y => y.LineNumber = i + 1);
                }
                var linefsrlistR = RfsrList.Select(x => x.LineNumber).Distinct().ToList();
                List<FlightSearchResults> fsrListR = new List<FlightSearchResults>();
                for (int j = 0; j < linefsrlistR.Count; j++)
                {
                    int p = j + linefsrlist.Count + 1;
                    List<FlightSearchResults> fsrListR1 = RfsrList.Where(x => x.LineNumber == linefsrlistR[j]).Select(x => (FlightSearchResults)x.Clone()).ToList<FlightSearchResults>();
                    fsrListR1.ForEach(y => y.LineNumber = p);
                    fsrListR.AddRange(fsrListR1);
                    fsrListR1.Clear();
                }
                finalResult1 = RoundTripFare(fsrList, fsrListR);
            }
            else if (f.TripType == TripType.RoundTrip && f.RTF == false)
            {
                finalResult1.Add(fsrList);
                finalResult1.Add(RfsrList);
            }
            else if (f.TripType == TripType.OneWay)
            {
                finalResult1.Add(fsrList);
            }
            return finalResult1;
        }

        private int CheckObjectAddTOList(FlightSearch f, FlightSearchResults fsr)
        {
            int status = 0;


            if (f.Adult > 0 && fsr.AdtBfare != 0)
            {
                if (f.Child > 0 && fsr.ChdBFare != 0)
                {
                    if (f.Infant > 0 && fsr.InfBfare != 0)
                    {
                        status = 1;
                    }
                    else if (f.Infant == 0 && fsr.InfBfare == 0)
                    {
                        status = 1;
                    }
                }
                else if (f.Child == 0 && fsr.ChdBFare == 0)
                {
                    if (f.Infant > 0 && fsr.InfBfare != 0)
                    {
                        status = 1;
                    }
                    else if (f.Infant == 0 && fsr.InfBfare == 0)
                    {
                        status = 1;
                    }
                }

            }
            else if (f.Adult == 0 && fsr.AdtBfare == 0)
            {
                if (f.Child > 0 && fsr.ChdBFare != 0)
                {
                    if (f.Infant > 0 && fsr.InfBfare != 0)
                    {
                        status = 1;
                    }
                    else if (f.Infant == 0 && fsr.InfBfare == 0)
                    {
                        status = 1;
                    }
                }
                else if (f.Child == 0 && fsr.ChdBFare == 0)
                {
                    if (f.Infant > 0 && fsr.InfBfare != 0)
                    {
                        status = 1;
                    }
                    else if (f.Infant == 0 && fsr.InfBfare == 0)
                    {
                        status = 1;
                    }
                }
            }
            else
            {
                status = 2;
            }




            return status;
        }

        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }

        public ArrayList RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
        {
            // List<FlightSearchResults> objO = (List<FlightSearchResults>)List[0];
            //  List<FlightSearchResults> objR = (List<FlightSearchResults>)List[1];
            int ln = 1;//For Total Line No.
            int k = 1;
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            while (k <= LnOb)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();
                int l = 1 + LnOb;
                while (l <= LnIb)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    List<FlightSearchResults> st = new List<FlightSearchResults>();
                    //int sta = 0;
                    //for (int i = 0; i < OB.Count; i++)
                    //{
                    //    for(int j=0;j<IB.Count;j++)
                    //    {
                    if (OB[OB.Count - 1].depdatelcc.Split('T')[0].ToLower().Trim() == IB[0].depdatelcc.Split('T')[0].ToLower().Trim())
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            st = Merge(OB, IB, ln);
                            ln++;
                        }
                    }
                    else
                    {
                        st = Merge(OB, IB, ln);
                        ln++;
                    }
                    //    }
                    //}

                    foreach (FlightSearchResults item in st)
                    {
                        Final.Add(item);
                    }
                    ///Increment Total Ln
                    l++;//Increment IB Ln
                }
                k++; //Increment OW Ln
            }
            Comb.Add(Final);
            return Comb;
        }

        //public ArrayList RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
        //{
        //    // List<FlightSearchResults> objO = (List<FlightSearchResults>)List[0];
        //    //  List<FlightSearchResults> objR = (List<FlightSearchResults>)List[1];
        //    int ln = 1;//For Total Line No.
        //    int k = 1;
        //    int LnOb = objO[objO.Count - 1].LineNumber;
        //    int LnIb = objR[objR.Count - 1].LineNumber;
        //    ArrayList Comb = new ArrayList();
        //    List<FlightSearchResults> Final = new List<FlightSearchResults>();
        //    while (k <= LnOb)
        //    {
        //        var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();
        //        int l = 1 + LnOb;
        //        while (l <= LnIb)
        //        {
        //            var IB = (from c in objR where c.LineNumber == l select c).ToList();
        //            List<FlightSearchResults> st;
        //            st = Merge(OB, IB, ln);
        //            foreach (FlightSearchResults item in st)
        //            {
        //                Final.Add(item);
        //            }
        //            ln++;///Increment Total Ln
        //            l++;//Increment IB Ln
        //        }
        //        k++; //Increment OW Ln
        //    }
        //    Comb.Add(Final);
        //    return Comb;
        //}

        public List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0,
                ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0,
                CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0,
             InfSrvTax = 0, InfTF = 0;

            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax;
            
            //ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            //ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;
            //AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount;
            //AdtCB = AdtCB + item.AdtCB + itemib.AdtCB;
            //AdtSrvTax = AdtSrvTax + item.AdtSrvTax + itemib.AdtSrvTax;
            //AdtTF = AdtTF + item.AdtTF + itemib.AdtTF;
            //AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            //IATAComm = IATAComm + item.IATAComm + itemib.IATAComm;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax;
            //CHDAdminMrk = CHDAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;
            //CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            //ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            //ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            //ChdSrvTax = ChdSrvTax + item.ChdSrvTax + itemib.ChdSrvTax;
            //ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            //ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant);
            //float STax = (AdtSrvTax * Adult) + (ChdSrvTax * Child) + (InfSrvTax * Infant);
            //float TFee = (AdtTF * Adult) + (ChdTF * Child) + (InfTF * Infant);
            //float TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            //float TotCB = (AdtCB * Adult) + (ChdCB * Child);
            //float TotTds = (AdtTds * Adult) + (ChdTds * Child);// +InfTds;
            //float TotMrkUp = (ADTAdminMrk * Adult) + (ADTAgentMrk * Adult) + (CHDAdminMrk * Child) + (CHDAgentMrk * Child);
            //TotalFare = TotalFare + TotMrkUp + STax + TFee;
            //float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));

            #endregion
            foreach (FlightSearchResults a in OB)
            {
                var PrcF = (FlightSearchResults)a.Clone();
                PrcF.LineNumber = Ln;
                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                //PrcF.ADTAdminMrk = ADTAdminMrk;
                //PrcF.ADTAgentMrk = ADTAgentMrk;
                //PrcF.AdtDiscount = AdtDiscount;
                //PrcF.AdtCB = AdtCB;
                //PrcF.AdtSrvTax = AdtSrvTax;
                //PrcF.AdtTF = AdtTF;
                //PrcF.AdtTds = AdtTds;
                //PrcF.IATAComm = IATAComm;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;
                //PrcF.CHDAdminMrk = CHDAdminMrk;
                //PrcF.CHDAgentMrk = CHDAgentMrk;
                //PrcF.ChdDiscount = ChdDiscount;
                //PrcF.ChdCB = ChdCB;
                //PrcF.ChdSrvTax = ChdSrvTax;
                //PrcF.ChdTF = ChdTF;
                //PrcF.ChdTds = ChdTds;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                //PrcF.STax = STax;
                //PrcF.TFee = TFee;
                //PrcF.TotDis = TotDis;
                //PrcF.TotTds = TotTds;
                //PrcF.TotMrkUp = TotMrkUp;
                PrcF.OriginalTF = OriginalTF;
                PrcF.OriginalTT = OriginalTT;
                #endregion
                Final.Add(PrcF);

            }

            foreach (FlightSearchResults b in IB)
            {
                var PrcF = (FlightSearchResults)b.Clone();
                PrcF.LineNumber = Ln;

                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                //PrcF.ADTAdminMrk = ADTAdminMrk;
                //PrcF.ADTAgentMrk = ADTAgentMrk;
                //PrcF.AdtDiscount = AdtDiscount;
                //PrcF.AdtCB = AdtCB;
                //PrcF.AdtSrvTax = AdtSrvTax;
                //PrcF.AdtTF = AdtTF;
                //PrcF.AdtTds = AdtTds;
                //PrcF.IATAComm = IATAComm;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;
                //PrcF.CHDAdminMrk = CHDAdminMrk;
                //PrcF.CHDAgentMrk = CHDAgentMrk;
                //PrcF.ChdDiscount = ChdDiscount;
                //PrcF.ChdCB = ChdCB;
                //PrcF.ChdSrvTax = ChdSrvTax;
                //PrcF.ChdTF = ChdTF;
                //PrcF.ChdTds = ChdTds;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                //PrcF.STax = STax;
                //PrcF.TFee = TFee;
                //PrcF.TotDis = TotDis;
                //PrcF.TotTds = TotTds;
                //PrcF.TotMrkUp = TotMrkUp;
                PrcF.OriginalTF = OriginalTF;
                PrcF.OriginalTT = OriginalTT;
                #endregion

                Final.Add(PrcF);
            }

            return Final;
        }

        private FlightSearchResults GetSumFareNew(FlightSearchResults fo, FlightSearchResults fr, FlightSearchResults Tofsr, int lineNum)
        {
            var fo1 = (FlightSearchResults)fo.Clone();
            var fr1 = (FlightSearchResults)fr.Clone();
            var Tofsr1 = (FlightSearchResults)Tofsr.Clone();
            float AdtFare = fo1.AdtFare + fr1.AdtFare;
            float AdtBfare = fo1.AdtBfare + fr1.AdtBfare;
            float AdtTax = fo1.AdtTax + fr1.AdtTax;
            float AdtFSur = fo1.AdtFSur + fr1.AdtFSur;//add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            float AdtOT = fo1.AdtOT + fr1.AdtOT; ; //// added TO TABLE
            float ChdFare = fo1.ChdFare + fr1.ChdFare;
            float ChdBFare = fo1.ChdBFare + fr1.ChdBFare;
            float ChdTax = fo1.ChdTax + fr1.ChdTax;
            float ChdFSur = fo1.ChdFSur + fr1.ChdFSur;
            float ChdOT = fo1.ChdOT + fr1.ChdOT; // added TO TABLE; // added TO TABLE
            float InfFare = fo1.InfFare + fr1.InfFare;
            float InfBfare = fo1.InfBfare + fr1.InfBfare;
            float InfTax = fo1.InfTax + fr1.InfTax;
            float InfFSur = fo1.InfFSur + fr1.InfFSur; //YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#           
            float InfOT = fo1.InfOT + fr1.InfOT; // added TO TABLE        
            float TotBfare = fo1.TotBfare + fr1.TotBfare;
            float TotalFare = fo1.TotalFare + fr1.TotalFare;
            float TotalTax = fo1.TotalTax + fr1.TotalTax;
            float TotalFuelSur = fo1.TotalFuelSur + fr1.TotalFuelSur;
            float NetFare = fo1.NetFare + fr1.NetFare;
            // int LineNumber = lineNum;


            Tofsr1.AdtFare = AdtFare;
            Tofsr1.AdtBfare = AdtBfare;
            Tofsr1.AdtTax = AdtTax;
            Tofsr1.AdtFSur = AdtFSur;
            Tofsr1.AdtOT = AdtOT;
            Tofsr1.ChdFare = ChdFare;
            Tofsr1.ChdBFare = ChdBFare;
            Tofsr1.ChdTax = ChdTax;
            Tofsr1.ChdFSur = ChdFSur;
            Tofsr1.ChdOT = ChdOT;
            Tofsr1.InfFare = InfFare;
            Tofsr1.InfBfare = InfBfare;
            Tofsr1.InfTax = InfTax;
            Tofsr1.InfFSur = InfFSur;
            Tofsr1.InfOT = InfOT;
            Tofsr1.TotBfare = TotBfare;
            Tofsr1.TotalFare = TotalFare;
            Tofsr1.TotalTax = TotalTax;
            Tofsr1.TotalFuelSur = TotalFuelSur;
            Tofsr1.NetFare = NetFare;
            Tofsr1.LineNumber = lineNum;






            return Tofsr1;
        }

        private FlightSearchResults GetSumFare(FlightSearchResults fo, FlightSearchResults fr, FlightSearchResults fsr, int lineNum)
        {

            string OrgDestFrom = fsr.OrgDestFrom;
            string OrgDestTo = fsr.OrgDestTo;
            string DepartureLocation = fsr.DepartureLocation;
            string DepartureCityName = fsr.DepartureCityName;
            string DepAirportCode = fsr.DepAirportCode;
            string DepartureAirportName = fsr.DepartureAirportName;
            string DepartureTerminal = fsr.DepartureTerminal;
            string ArrivalLocation = fsr.ArrivalLocation; ;
            string ArrivalCityName = fsr.ArrivalCityName;
            string ArrAirportCode = fsr.ArrAirportCode;
            string ArrivalAirportName = fsr.ArrAirportCode;
            string ArrivalTerminal = fsr.ArrivalTerminal;
            string DepartureDate = fsr.DepartureDate;
            string Departure_Date = fsr.Departure_Date;
            string DepartureTime = fsr.DepartureTime;
            string ArrivalDate = fsr.ArrivalDate;
            string Arrival_Date = fsr.Arrival_Date;
            string ArrivalTime = fsr.ArrivalTime;
            int Adult = fsr.Adult;
            int Child = fsr.Child;
            int Infant = fsr.Infant;
            int TotPax = fsr.TotPax;
            string MarketingCarrier = fsr.MarketingCarrier;
            string OperatingCarrier = fsr.OperatingCarrier;
            string FlightIdentification = fsr.FlightIdentification;
            string ValiDatingCarrier = fsr.ValiDatingCarrier;
            string AirLineName = fsr.AirLineName;
            string AvailableSeats = fsr.AvailableSeats;
            string ElectronicTicketing = fsr.ElectronicTicketing; // added TO TABLE
            string ProductDetailQualifier = fsr.ProductDetailQualifier; ////


            string AdtCabin = fsr.AdtCabin;//added TO TABLE
            string ChdCabin = fsr.ChdCabin; //added TO TABLE
            string InfCabin = fsr.InfCabin;//added TO TABLE
            string AdtRbd = fsr.AdtRbd; ;//added TO TABLE
            string ChdRbd = fsr.ChdRbd;//added TO TABLE
            string InfRbd = fsr.InfRbd;//added TO TABLE
            string AdtFareType = fsr.AdtFareType;//added TO TABLE
            //  string AdtFareTypeName = fsr.AdtFareTypeName; //added TO TABLE
            string AdtFarebasis = fsr.AdtFarebasis; //added TO TABLE
            string ChdfareType = fsr.ChdfareType;//added TO TABLE
            // string ChdfareTypeName = fsr.ChdfareTypeName; //added TO TABLE
            string ChdFarebasis = fsr.ChdFarebasis; //added TO TABLE
            string InfFareType = fsr.InfFareType; //added TO TABLE
            // string InfFareTypeName = fsr.InfFareTypeName; //added TO TABLE
            string InfFarebasis = fsr.InfFarebasis; //added TO TABLE -farebasis changed-to- Farebasis
            string InfFar = fsr.InfFar; ////
            string ChdFar = fsr.ChdFar; ////
            string AdtFar = fsr.AdtFar; /////
            string AdtBreakPoint = fsr.AdtBreakPoint;
            string AdtAvlStatus = fsr.AdtAvlStatus;
            string ChdBreakPoint = fsr.ChdBreakPoint;
            string ChdAvlStatus = fsr.ChdAvlStatus;
            string InfBreakPoint = fsr.InfBreakPoint;
            string InfAvlStatus = fsr.InfAvlStatus;
            string fareBasis = fsr.fareBasis;
            string FBPaxType = fsr.FBPaxType;
            string RBD = fsr.RBD;
            string FareRule = fsr.FareRule;
            string FareDet = fsr.FareDet;

            float AdtFare = fo.AdtFare + fr.AdtFare;
            float AdtBfare = fo.AdtBfare + fr.AdtBfare;
            float AdtTax = fo.AdtTax + fr.AdtTax;
            float AdtFSur = fo.AdtFSur + fr.AdtFSur; ; //add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            float AdtYR = fsr.AdtYR;
            float AdtWO = fsr.AdtWO;
            float AdtIN = fsr.AdtIN;
            float AdtQ = fsr.AdtQ;
            float AdtJN = fsr.AdtJN;
            float AdtOT = fo.AdtOT + fr.AdtOT; ; //// added TO TABLE
            float AdtSrvTax = fsr.AdtSrvTax;   // added TO TABLE
            float AdtEduCess = fsr.AdtEduCess;
            float AdtHighEduCess = fsr.AdtHighEduCess;
            float AdtTF = fsr.AdtTF;
            float ADTAdminMrk = fsr.ADTAdminMrk;
            float ADTAgentMrk = fsr.ADTAgentMrk;
            float ADTDistMrk = fsr.ADTDistMrk;
            float AdtDiscount = fsr.AdtDiscount;  //-AdtComm  
            float AdtCB = fsr.AdtCB;
            float AdtTds = fsr.AdtTds;



            float ChdFare = fo.ChdFare + fr.ChdFare;

            float ChdBFare = fo.ChdBFare + fr.ChdBFare;

            float ChdTax = fo.ChdTax + fr.ChdTax;

            float ChdFSur = fo.ChdFSur + fr.ChdFSur;

            float ChdYR = fsr.ChdYR;

            float ChdWO = fsr.ChdWO;

            float ChdIN = fsr.ChdIN;

            float ChdQ = fsr.ChdQ;

            float ChdJN = fsr.ChdJN;

            float ChdOT = fo.ChdOT + fr.ChdOT; // added TO TABLE; // added TO TABLE

            float ChdSrvTax = fsr.ChdSrvTax; // added TO TABLE 

            float ChdEduCess = fsr.ChdEduCess;

            float ChdHighEduCess = fsr.ChdHighEduCess;

            float ChdTF = fsr.ChdTF;

            float CHDAdminMrk = fsr.CHDAdminMrk;

            float CHDAgentMrk = fsr.CHDAgentMrk;

            float CHDDistMrk = fsr.CHDDistMrk;

            float ChdTds = fsr.ChdTds;

            float ChdDiscount = fsr.ChdDiscount; //-ChdComm

            float ChdCB = fsr.ChdCB;




            float InfFare = fo.InfFare + fr.InfFare;

            float InfBfare = fo.InfBfare + fr.InfBfare;

            float InfTax = fo.InfTax + fr.InfTax;

            float InfFSur = fo.InfFSur + fr.InfFSur; //YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              

            float InfYR = fsr.InfYR;

            float InfWO = fsr.InfWO;

            float InfIN = fsr.InfIN;

            float InfQ = fsr.InfQ;

            float InfJN = fsr.InfJN;

            float InfOT = fo.InfOT + fr.InfOT; // added TO TABLE         

            float InfSrvTax = fsr.InfSrvTax; // added TO TABLE

            float InfEduCess = fsr.InfEduCess; ////

            float InfHighEduCess = fsr.InfHighEduCess; ////

            float InfTF = fsr.InfTF;

            float InfAdminMrk = fsr.InfAdminMrk; // added TO TABLE

            float InfAgentMrk = fsr.InfAgentMrk; // added TO TABLE

            float InfDistMrk = fsr.InfDistMrk;

            float InfTds = fsr.InfTds;

            float InfDiscount = fsr.InfDiscount; //InfComm       

            float InfCB = fsr.InfCB; // added TO TABLE

            float TotBfare = fo.TotBfare + fr.TotBfare;

            float TotalFare = fo.TotalFare + fr.TotalFare;

            float TotalTax = fo.TotalTax + fr.TotalTax;

            float TotalFuelSur = fo.TotalFuelSur + fr.TotalFuelSur;

            float NetFare = fo.NetFare + fr.NetFare;

            float TotMrkUp = fsr.TotMrkUp;

            float STax = fsr.STax;

            float TFee = fsr.TFee;

            float TotDis = fsr.TotDis;

            float TotCB = fsr.TotCB;

            float TotTds = fsr.TotTds;

            float IATAComm = fsr.IATAComm;
            string Searchvalue = fsr.Searchvalue;
            int LineNumber = fsr.LineNumber;
            int Leg = fsr.Leg;
            string Flight = fsr.Flight;
            string TotDur = fsr.TotDur;
            string TripType = fsr.TripType;
            string EQ = fsr.EQ;
            string Stops = fsr.Stops;
            string Trip = fsr.Trip;
            string Sector = fsr.Sector;
            string TripCnt = fsr.TripCnt;

            string sno = fsr.sno;
            string depdatelcc = fsr.depdatelcc;
            string arrdatelcc = fsr.arrdatelcc;
            float OriginalTF = TotalFare;
            float OriginalTT = TotalTax;
            string Track_id = fsr.Track_id;
            string FType = fsr.FType;
            bool IsLTC = fsr.IsLTC;
            string Provider = fsr.Provider;

            FlightSearchResults ff = new FlightSearchResults();



            ff.OrgDestFrom = OrgDestFrom;
            ff.OrgDestTo = OrgDestTo;
            ff.DepartureLocation = DepartureLocation;
            ff.DepartureCityName = DepartureCityName;
            ff.DepAirportCode = DepAirportCode;
            ff.DepartureAirportName = DepartureAirportName;
            ff.DepartureTerminal = DepartureTerminal;
            ff.ArrivalLocation = ArrivalLocation; ;
            ff.ArrivalCityName = ArrivalCityName;
            ff.ArrAirportCode = ArrAirportCode;
            ff.ArrivalAirportName = ArrAirportCode;
            ff.ArrivalTerminal = ArrivalTerminal;
            ff.DepartureDate = DepartureDate;
            ff.Departure_Date = Departure_Date;
            ff.DepartureTime = DepartureTime;
            ff.ArrivalDate = ArrivalDate;
            ff.Arrival_Date = Arrival_Date;
            ff.ArrivalTime = ArrivalTime;
            ff.Adult = Adult;
            ff.Child = Child;
            ff.Infant = Infant;
            ff.TotPax = TotPax;
            ff.MarketingCarrier = MarketingCarrier;
            ff.OperatingCarrier = OperatingCarrier;
            ff.FlightIdentification = FlightIdentification;
            ff.ValiDatingCarrier = ValiDatingCarrier;
            ff.AirLineName = AirLineName;
            ff.AvailableSeats = AvailableSeats;
            ff.ElectronicTicketing = ElectronicTicketing; // added TO TABLE
            ff.ProductDetailQualifier = ProductDetailQualifier; ////


            ff.AdtCabin = AdtCabin;//added TO TABLE
            ff.ChdCabin = ChdCabin; //added TO TABLE
            ff.InfCabin = InfCabin;//added TO TABLE
            ff.AdtRbd = AdtRbd; ;//added TO TABLE
            ff.ChdRbd = ChdRbd;//added TO TABLE
            ff.InfRbd = InfRbd;//added TO TABLE
            ff.AdtFareType = AdtFareType;//added TO TABLE
            //ff.AdtFareTypeName = AdtFareTypeName; //added TO TABLE
            ff.AdtFarebasis = AdtFarebasis; //added TO TABLE
            ff.ChdfareType = ChdfareType;//added TO TABLE
            //ff.ChdfareTypeName = ChdfareTypeName; //added TO TABLE
            ff.ChdFarebasis = ChdFarebasis; //added TO TABLE
            ff.InfFareType = InfFareType; //added TO TABLE
            // ff.InfFareTypeName = InfFareTypeName; //added TO TABLE
            ff.InfFarebasis = InfFarebasis; //added TO TABLE -farebasis changed-to- Farebasis
            ff.InfFar = InfFar; ////
            ff.ChdFar = ChdFar; ////
            ff.AdtFar = AdtFar; /////
            ff.AdtBreakPoint = AdtBreakPoint;
            ff.AdtAvlStatus = AdtAvlStatus;
            ff.ChdBreakPoint = ChdBreakPoint;
            ff.ChdAvlStatus = ChdAvlStatus;
            ff.InfBreakPoint = InfBreakPoint;
            ff.InfAvlStatus = InfAvlStatus;
            ff.fareBasis = fareBasis;
            ff.FBPaxType = FBPaxType;
            ff.RBD = RBD;
            ff.FareRule = FareRule;
            ff.FareDet = FareDet;

            ff.AdtFare = AdtFare;
            ff.AdtBfare = AdtBfare;
            ff.AdtTax = AdtTax;
            ff.AdtFSur = AdtFSur; //add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            ff.AdtYR = AdtYR;
            ff.AdtWO = AdtWO;
            ff.AdtIN = AdtIN;
            ff.AdtQ = AdtQ;
            ff.AdtJN = AdtJN;
            ff.AdtOT = AdtOT; //// added TO TABLE
            ff.AdtSrvTax = AdtSrvTax;   // added TO TABLE
            ff.AdtEduCess = AdtEduCess;
            ff.AdtHighEduCess = AdtHighEduCess;
            ff.AdtTF = AdtTF;
            ff.ADTAdminMrk = ADTAdminMrk;
            ff.ADTAgentMrk = ADTAgentMrk;
            ff.ADTDistMrk = ADTDistMrk;
            ff.AdtDiscount = AdtDiscount;  //-AdtComm  
            ff.AdtCB = AdtCB;
            ff.AdtTds = AdtTds;



            ff.ChdFare = ChdFare;

            ff.ChdBFare = ChdBFare;

            ff.ChdTax = ChdTax;

            ff.ChdFSur = ChdFSur;

            ff.ChdYR = ChdYR;

            ff.ChdWO = ChdWO;

            ff.ChdIN = ChdIN;

            ff.ChdQ = ChdQ;

            ff.ChdJN = ChdJN;

            ff.ChdOT = ChdOT; // added TO TABLE

            ff.ChdSrvTax = ChdSrvTax; // added TO TABLE 

            ff.ChdEduCess = ChdEduCess;

            ff.ChdHighEduCess = ChdHighEduCess;

            ff.ChdTF = ChdTF;

            ff.CHDAdminMrk = CHDAdminMrk;

            ff.CHDAgentMrk = CHDAgentMrk;

            ff.CHDDistMrk = CHDDistMrk;

            ff.ChdTds = ChdTds;

            ff.ChdDiscount = ChdDiscount; //-ChdComm

            ff.ChdCB = ChdCB;




            ff.InfFare = InfFare;

            ff.InfBfare = InfBfare;

            ff.InfTax = InfTax;

            ff.InfFSur = InfFSur; //YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              

            ff.InfYR = InfYR;

            ff.InfWO = InfWO;

            ff.InfIN = InfIN;

            ff.InfQ = InfQ;

            ff.InfJN = InfJN;

            ff.InfOT = InfOT; // added TO TABLE         

            ff.InfSrvTax = InfSrvTax; // added TO TABLE

            ff.InfEduCess = InfEduCess; ////

            ff.InfHighEduCess = InfHighEduCess; ////

            ff.InfTF = InfTF;

            ff.InfAdminMrk = InfAdminMrk; // added TO TABLE

            ff.InfAgentMrk = InfAgentMrk; // added TO TABLE

            ff.InfDistMrk = InfDistMrk;

            ff.InfTds = InfTds;

            ff.InfDiscount = InfDiscount; //InfComm       

            ff.InfCB = InfCB; // added TO TABLE

            ff.TotBfare = TotBfare;

            ff.TotalFare = TotalFare;

            ff.TotalTax = TotalTax;

            ff.TotalFuelSur = TotalFuelSur;

            ff.NetFare = NetFare;

            ff.TotMrkUp = TotMrkUp;

            ff.STax = STax;

            ff.TFee = TFee;

            ff.TotDis = TotDis;

            ff.TotCB = TotCB;

            ff.TotTds = TotTds;

            ff.IATAComm = IATAComm;
            ff.Searchvalue = Searchvalue;
            ff.LineNumber = fsr.LineNumber;
            ff.Leg = Leg;
            ff.Flight = Flight;
            ff.TotDur = TotDur;
            ff.TripType = TripType;
            ff.EQ = EQ;
            ff.Stops = Stops;
            ff.Trip = Trip;
            ff.Sector = Sector;
            ff.TripCnt = TripCnt;

            ff.sno = sno;
            ff.depdatelcc = depdatelcc;
            ff.arrdatelcc = arrdatelcc;
            ff.OriginalTF = OriginalTF;
            ff.OriginalTT = OriginalTT;
            ff.Track_id = Track_id;
            ff.FType = FType;
            ff.IsLTC = IsLTC;
            ff.Provider = Provider;
            ff.LineNumber = lineNum;








            return ff;

        }

        private FlightSearchResults GetNewObject(FlightSearchResults fsr, int lineNum)
        {

            string OrgDestFrom = fsr.OrgDestFrom;
            string OrgDestTo = fsr.OrgDestTo;
            string DepartureLocation = fsr.DepartureLocation;
            string DepartureCityName = fsr.DepartureCityName;
            string DepAirportCode = fsr.DepAirportCode;
            string DepartureAirportName = fsr.DepartureAirportName;
            string DepartureTerminal = fsr.DepartureTerminal;
            string ArrivalLocation = fsr.ArrivalLocation; ;
            string ArrivalCityName = fsr.ArrivalCityName;
            string ArrAirportCode = fsr.ArrAirportCode;
            string ArrivalAirportName = fsr.ArrAirportCode;
            string ArrivalTerminal = fsr.ArrivalTerminal;
            string DepartureDate = fsr.DepartureDate;
            string Departure_Date = fsr.Departure_Date;
            string DepartureTime = fsr.DepartureTime;
            string ArrivalDate = fsr.ArrivalDate;
            string Arrival_Date = fsr.Arrival_Date;
            string ArrivalTime = fsr.ArrivalTime;
            int Adult = fsr.Adult;
            int Child = fsr.Child;
            int Infant = fsr.Infant;
            int TotPax = fsr.TotPax;
            string MarketingCarrier = fsr.MarketingCarrier;
            string OperatingCarrier = fsr.OperatingCarrier;
            string FlightIdentification = fsr.FlightIdentification;
            string ValiDatingCarrier = fsr.ValiDatingCarrier;
            string AirLineName = fsr.AirLineName;
            string AvailableSeats = fsr.AvailableSeats;
            string ElectronicTicketing = fsr.ElectronicTicketing; // added TO TABLE
            string ProductDetailQualifier = fsr.ProductDetailQualifier; ////


            string AdtCabin = fsr.AdtCabin;//added TO TABLE
            string ChdCabin = fsr.ChdCabin; //added TO TABLE
            string InfCabin = fsr.InfCabin;//added TO TABLE
            string AdtRbd = fsr.AdtRbd; ;//added TO TABLE
            string ChdRbd = fsr.ChdRbd;//added TO TABLE
            string InfRbd = fsr.InfRbd;//added TO TABLE
            string AdtFareType = fsr.AdtFareType;//added TO TABLE
            // string AdtFareTypeName =fsr.AdtFareTypeName; //added TO TABLE
            string AdtFarebasis = fsr.AdtFarebasis; //added TO TABLE
            string ChdfareType = fsr.ChdfareType;//added TO TABLE
            // string ChdfareTypeName =fsr.ChdfareTypeName; //added TO TABLE
            string ChdFarebasis = fsr.ChdFarebasis; //added TO TABLE
            string InfFareType = fsr.InfFareType; //added TO TABLE
            // string InfFareTypeName =fsr.InfFareTypeName; //added TO TABLE
            string InfFarebasis = fsr.InfFarebasis; //added TO TABLE -farebasis changed-to- Farebasis
            string InfFar = fsr.InfFar; ////
            string ChdFar = fsr.ChdFar; ////
            string AdtFar = fsr.AdtFar; /////
            string AdtBreakPoint = fsr.AdtBreakPoint;
            string AdtAvlStatus = fsr.AdtAvlStatus;
            string ChdBreakPoint = fsr.ChdBreakPoint;
            string ChdAvlStatus = fsr.ChdAvlStatus;
            string InfBreakPoint = fsr.InfBreakPoint;
            string InfAvlStatus = fsr.InfAvlStatus;
            string fareBasis = fsr.fareBasis;
            string FBPaxType = fsr.FBPaxType;
            string RBD = fsr.RBD;
            string FareRule = fsr.FareRule;
            string FareDet = fsr.FareDet;

            float AdtFare = fsr.AdtFare;
            float AdtBfare = fsr.AdtBfare;
            float AdtTax = fsr.AdtTax;
            float AdtFSur = fsr.AdtFSur; //add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            float AdtYR = fsr.AdtYR;
            float AdtWO = fsr.AdtWO;
            float AdtIN = fsr.AdtIN;
            float AdtQ = fsr.AdtQ;
            float AdtJN = fsr.AdtJN;
            float AdtOT = fsr.AdtOT; //// added TO TABLE
            float AdtSrvTax = fsr.AdtSrvTax;   // added TO TABLE
            float AdtEduCess = fsr.AdtEduCess;
            float AdtHighEduCess = fsr.AdtHighEduCess;
            float AdtTF = fsr.AdtTF;
            float ADTAdminMrk = fsr.ADTAdminMrk;
            float ADTAgentMrk = fsr.ADTAgentMrk;
            float ADTDistMrk = fsr.ADTDistMrk;
            float AdtDiscount = fsr.AdtDiscount;  //-AdtComm  
            float AdtCB = fsr.AdtCB;
            float AdtTds = fsr.AdtTds;



            float ChdFare = fsr.ChdFare;

            float ChdBFare = fsr.ChdBFare;

            float ChdTax = fsr.ChdTax;

            float ChdFSur = fsr.ChdFSur;

            float ChdYR = fsr.ChdYR;

            float ChdWO = fsr.ChdWO;

            float ChdIN = fsr.ChdIN;

            float ChdQ = fsr.ChdQ;

            float ChdJN = fsr.ChdJN;

            float ChdOT = fsr.ChdOT; // added TO TABLE

            float ChdSrvTax = fsr.ChdSrvTax; // added TO TABLE 

            float ChdEduCess = fsr.ChdEduCess;

            float ChdHighEduCess = fsr.ChdHighEduCess;

            float ChdTF = fsr.ChdTF;

            float CHDAdminMrk = fsr.CHDAdminMrk;

            float CHDAgentMrk = fsr.CHDAgentMrk;

            float CHDDistMrk = fsr.CHDDistMrk;

            float ChdTds = fsr.ChdTds;

            float ChdDiscount = fsr.ChdDiscount; //-ChdComm

            float ChdCB = fsr.ChdCB;




            float InfFare = fsr.InfFare;

            float InfBfare = fsr.InfBfare;

            float InfTax = fsr.InfTax;

            float InfFSur = fsr.InfFSur; //YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              

            float InfYR = fsr.InfYR;

            float InfWO = fsr.InfWO;

            float InfIN = fsr.InfIN;

            float InfQ = fsr.InfQ;

            float InfJN = fsr.InfJN;

            float InfOT = fsr.InfOT; // added TO TABLE         

            float InfSrvTax = fsr.InfSrvTax; // added TO TABLE

            float InfEduCess = fsr.InfEduCess; ////

            float InfHighEduCess = fsr.InfHighEduCess; ////

            float InfTF = fsr.InfTF;

            float InfAdminMrk = fsr.InfAdminMrk; // added TO TABLE

            float InfAgentMrk = fsr.InfAgentMrk; // added TO TABLE

            float InfDistMrk = fsr.InfDistMrk;

            float InfTds = fsr.InfTds;

            float InfDiscount = fsr.InfDiscount; //InfComm       

            float InfCB = fsr.InfCB; // added TO TABLE

            float TotBfare = fsr.TotBfare;

            float TotalFare = fsr.TotalFare;

            float TotalTax = fsr.TotalTax;

            float TotalFuelSur = fsr.TotalFuelSur;

            float NetFare = fsr.NetFare;

            float TotMrkUp = fsr.TotMrkUp;

            float STax = fsr.STax;

            float TFee = fsr.TFee;

            float TotDis = fsr.TotDis;

            float TotCB = fsr.TotCB;

            float TotTds = fsr.TotTds;

            float IATAComm = fsr.IATAComm;
            string Searchvalue = fsr.Searchvalue;
            int LineNumber = lineNum;
            int Leg = fsr.Leg;
            string Flight = fsr.Flight;
            string TotDur = fsr.TotDur;
            string TripType = fsr.TripType;
            string EQ = fsr.EQ;
            string Stops = fsr.Stops;
            string Trip = fsr.Trip;
            string Sector = fsr.Sector;
            string TripCnt = fsr.TripCnt;

            string sno = fsr.sno;
            string depdatelcc = fsr.depdatelcc;
            string arrdatelcc = fsr.arrdatelcc;
            float OriginalTF = fsr.OriginalTF;
            float OriginalTT = fsr.OriginalTT;
            string Track_id = fsr.Track_id;
            string FType = fsr.FType;
            bool IsLTC = fsr.IsLTC;
            string Provider = fsr.Provider;

            FlightSearchResults ff = new FlightSearchResults();



            ff.OrgDestFrom = OrgDestFrom;
            ff.OrgDestTo = OrgDestTo;
            ff.DepartureLocation = DepartureLocation;
            ff.DepartureCityName = DepartureCityName;
            ff.DepAirportCode = DepAirportCode;
            ff.DepartureAirportName = DepartureAirportName;
            ff.DepartureTerminal = DepartureTerminal;
            ff.ArrivalLocation = ArrivalLocation; ;
            ff.ArrivalCityName = ArrivalCityName;
            ff.ArrAirportCode = ArrAirportCode;
            ff.ArrivalAirportName = ArrAirportCode;
            ff.ArrivalTerminal = ArrivalTerminal;
            ff.DepartureDate = DepartureDate;
            ff.Departure_Date = Departure_Date;
            ff.DepartureTime = DepartureTime;
            ff.ArrivalDate = ArrivalDate;
            ff.Arrival_Date = Arrival_Date;
            ff.ArrivalTime = ArrivalTime;
            ff.Adult = Adult;
            ff.Child = Child;
            ff.Infant = Infant;
            ff.TotPax = TotPax;
            ff.MarketingCarrier = MarketingCarrier;
            ff.OperatingCarrier = OperatingCarrier;
            ff.FlightIdentification = FlightIdentification;
            ff.ValiDatingCarrier = ValiDatingCarrier;
            ff.AirLineName = AirLineName;
            ff.AvailableSeats = AvailableSeats;
            ff.ElectronicTicketing = ElectronicTicketing; // added TO TABLE
            ff.ProductDetailQualifier = ProductDetailQualifier; ////


            ff.AdtCabin = AdtCabin;//added TO TABLE
            ff.ChdCabin = ChdCabin; //added TO TABLE
            ff.InfCabin = InfCabin;//added TO TABLE
            ff.AdtRbd = AdtRbd; ;//added TO TABLE
            ff.ChdRbd = ChdRbd;//added TO TABLE
            ff.InfRbd = InfRbd;//added TO TABLE
            ff.AdtFareType = AdtFareType;//added TO TABLE
            //ff.AdtFareTypeName = AdtFareTypeName; //added TO TABLE
            ff.AdtFarebasis = AdtFarebasis; //added TO TABLE
            ff.ChdfareType = ChdfareType;//added TO TABLE
            // ff.ChdfareTypeName = ChdfareTypeName; //added TO TABLE
            ff.ChdFarebasis = ChdFarebasis; //added TO TABLE
            ff.InfFareType = InfFareType; //added TO TABLE
            // ff.InfFareTypeName = InfFareTypeName; //added TO TABLE
            ff.InfFarebasis = InfFarebasis; //added TO TABLE -farebasis changed-to- Farebasis
            ff.InfFar = InfFar; ////
            ff.ChdFar = ChdFar; ////
            ff.AdtFar = AdtFar; /////
            ff.AdtBreakPoint = AdtBreakPoint;
            ff.AdtAvlStatus = AdtAvlStatus;
            ff.ChdBreakPoint = ChdBreakPoint;
            ff.ChdAvlStatus = ChdAvlStatus;
            ff.InfBreakPoint = InfBreakPoint;
            ff.InfAvlStatus = InfAvlStatus;
            ff.fareBasis = fareBasis;
            ff.FBPaxType = FBPaxType;
            ff.RBD = RBD;
            ff.FareRule = FareRule;
            ff.FareDet = FareDet;

            ff.AdtFare = AdtFare;
            ff.AdtBfare = AdtBfare;
            ff.AdtTax = AdtTax;
            ff.AdtFSur = AdtFSur; //add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            ff.AdtYR = AdtYR;
            ff.AdtWO = AdtWO;
            ff.AdtIN = AdtIN;
            ff.AdtQ = AdtQ;
            ff.AdtJN = AdtJN;
            ff.AdtOT = AdtOT; //// added TO TABLE
            ff.AdtSrvTax = AdtSrvTax;   // added TO TABLE
            ff.AdtEduCess = AdtEduCess;
            ff.AdtHighEduCess = AdtHighEduCess;
            ff.AdtTF = AdtTF;
            ff.ADTAdminMrk = ADTAdminMrk;
            ff.ADTAgentMrk = ADTAgentMrk;
            ff.ADTDistMrk = ADTDistMrk;
            ff.AdtDiscount = AdtDiscount;  //-AdtComm  
            ff.AdtCB = AdtCB;
            ff.AdtTds = AdtTds;



            ff.ChdFare = ChdFare;

            ff.ChdBFare = ChdBFare;

            ff.ChdTax = ChdTax;

            ff.ChdFSur = ChdFSur;

            ff.ChdYR = ChdYR;

            ff.ChdWO = ChdWO;

            ff.ChdIN = ChdIN;

            ff.ChdQ = ChdQ;

            ff.ChdJN = ChdJN;

            ff.ChdOT = ChdOT; // added TO TABLE

            ff.ChdSrvTax = ChdSrvTax; // added TO TABLE 

            ff.ChdEduCess = ChdEduCess;

            ff.ChdHighEduCess = ChdHighEduCess;

            ff.ChdTF = ChdTF;

            ff.CHDAdminMrk = CHDAdminMrk;

            ff.CHDAgentMrk = CHDAgentMrk;

            ff.CHDDistMrk = CHDDistMrk;

            ff.ChdTds = ChdTds;

            ff.ChdDiscount = ChdDiscount; //-ChdComm

            ff.ChdCB = ChdCB;




            ff.InfFare = InfFare;

            ff.InfBfare = InfBfare;

            ff.InfTax = InfTax;

            ff.InfFSur = InfFSur; //YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              

            ff.InfYR = InfYR;

            ff.InfWO = InfWO;

            ff.InfIN = InfIN;

            ff.InfQ = InfQ;

            ff.InfJN = InfJN;

            ff.InfOT = InfOT; // added TO TABLE         

            ff.InfSrvTax = InfSrvTax; // added TO TABLE

            ff.InfEduCess = InfEduCess; ////

            ff.InfHighEduCess = InfHighEduCess; ////

            ff.InfTF = InfTF;

            ff.InfAdminMrk = InfAdminMrk; // added TO TABLE

            ff.InfAgentMrk = InfAgentMrk; // added TO TABLE

            ff.InfDistMrk = InfDistMrk;

            ff.InfTds = InfTds;

            ff.InfDiscount = InfDiscount; //InfComm       

            ff.InfCB = InfCB; // added TO TABLE

            ff.TotBfare = TotBfare;

            ff.TotalFare = TotalFare;

            ff.TotalTax = TotalTax;

            ff.TotalFuelSur = TotalFuelSur;

            ff.NetFare = NetFare;

            ff.TotMrkUp = TotMrkUp;

            ff.STax = STax;

            ff.TFee = TFee;

            ff.TotDis = TotDis;

            ff.TotCB = TotCB;

            ff.TotTds = TotTds;

            ff.IATAComm = IATAComm;
            ff.Searchvalue = Searchvalue;
            ff.LineNumber = lineNum;
            ff.Leg = Leg;
            ff.Flight = Flight;
            ff.TotDur = TotDur;
            ff.TripType = TripType;
            ff.EQ = EQ;
            ff.Stops = Stops;
            ff.Trip = Trip;
            ff.Sector = Sector;
            ff.TripCnt = TripCnt;

            ff.sno = sno;
            ff.depdatelcc = depdatelcc;
            ff.arrdatelcc = arrdatelcc;
            ff.OriginalTF = OriginalTF;
            ff.OriginalTT = OriginalTT;
            ff.Track_id = Track_id;
            ff.FType = FType;
            ff.IsLTC = IsLTC;
            ff.Provider = Provider;








            return ff;

        }

        private string GetMonthName(int monthNo)
        {

            Dictionary<int, string> month = new Dictionary<int, string>();

            month.Add(01, "JAN");
            month.Add(02, "FEB");
            month.Add(03, "MAR");
            month.Add(04, "APR");
            month.Add(05, "MAY");
            month.Add(06, "JUN");
            month.Add(07, "JUL");
            month.Add(08, "AUG");
            month.Add(09, "SEP");
            month.Add(10, "OCT");
            month.Add(11, "NOV");
            month.Add(12, "DEC");

            return month[monthNo];

        }
    }
}
