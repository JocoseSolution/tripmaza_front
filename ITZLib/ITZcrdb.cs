﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
////using System.Threading.Tasks;

namespace ITZLib
{
    // For Credit Debit and Response info
    public class ITZcrdb
    {
        ITZLib.ErrorLogs.ErrorLog errLog = new ITZLib.ErrorLogs.ErrorLog();

        //For Debit responce//
        public DebitResponse ITZDebit(_CrOrDb GetDrParam)
        {
            DebitRequest objDreq = new DebitRequest();
            DebitResponse objDres = new DebitResponse();
            CrOrDb objCrOrDb = new CrOrDb();
            CrOrDbService objcrdbSer = new CrOrDbService();
            try
            {
                // ITZComman Class _CrOrDb//
                objCrOrDb.AMOUNT = GetDrParam._AMOUNT;
                objCrOrDb.MERCHANT_KEY = GetDrParam._MERCHANT_KEY;
                objCrOrDb.MODE = GetDrParam._MODE;
                objCrOrDb.ORDERID = GetDrParam._ORDERID;
                //////objCrOrDb.PASSWORD = GetDrParam._PASSWORD;
                objCrOrDb.PASSWORD = "";
                objCrOrDb.DECODE = GetDrParam._DECODE;
                objCrOrDb.CHECKSUM = GetDrParam._CHECKSUM;
                objCrOrDb.DESCRIPTION = GetDrParam._DESCRIPTION;
                objCrOrDb.SERVICE_TYPE = GetDrParam._SERVICE_TYPE;
                objDreq.CrOrDb = objCrOrDb;
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objcrdbSer.Debit(objDreq);
        }
        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(this.GetType());
            serializer.Serialize(stringwriter, this);
            return stringwriter.ToString();
        }
        //For Credit responce//
        public CreditResponse ITZCredit(_CrOrDb GetCrParam)
        {
            CreditRequest objCrReq = new CreditRequest();
            CreditResponse objCrRes = new CreditResponse();
            CrOrDb objCRorDr = new CrOrDb();
            CrOrDbService objcrdbSer = new CrOrDbService();
            try
            {
                // ITZComman Class _CrOrDb//
                objCRorDr.DECODE = GetCrParam._DECODE;
                objCRorDr.MERCHANT_KEY = GetCrParam._MERCHANT_KEY;
                objCRorDr.AMOUNT = GetCrParam._AMOUNT;
                objCRorDr.ORDERID = GetCrParam._ORDERID;
                objCRorDr.MODE = GetCrParam._MODE;
                objCRorDr.CHECKSUM = GetCrParam._CHECKSUM;
                objCRorDr.SERVICE_TYPE = GetCrParam._SERVICE_TYPE;
                objCRorDr.DESCRIPTION = GetCrParam._DESCRIPTION;
                objCrReq.CrOrDb = objCRorDr;
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objcrdbSer.Credit(objCrReq);
        }

        //For Refund responce//
        public RefundResponse ITZRefund(_CrOrDb GetRfParam)
        {
            RefundRequest objRfReq = new RefundRequest();
            RefundResponse objRfRes = new RefundResponse();
            CrOrDb objRef = new CrOrDb();
            CrOrDbService objcrdbSer = new CrOrDbService();
            try
            {
                // ITZComman Class _CrOrDb//
                objRef.AMOUNT = GetRfParam._AMOUNT;
                objRef.MERCHANT_KEY = GetRfParam._MERCHANT_KEY;
                objRef.ORDERID = GetRfParam._ORDERID;
                objRef.MODE = GetRfParam._MODE;
                objRef.REFUNDTYPE = GetRfParam._REFUNDTYPE;
                objRef.REFUNDORDERID = GetRfParam._REFUNDORDERID;
                objRef.CHECKSUM = GetRfParam._CHECKSUM;
                objRef.DESCRIPTION = GetRfParam._DESCRIPTION;
                objRfReq.CrOrDb = objRef;
                
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objcrdbSer.Refund(objRfReq);
        }
    }
}

////using System;
////using System.Collections.Generic;
////using System.Linq;
////using System.Text;
//////using System.Threading.Tasks;

////namespace ITZLib
////{
////   // For Credit Debit and Response info
////    public class ITZcrdb
////    {
////        //For Debit responce//
////        public DebitResponse ITZDebit(_CrOrDb GetDrParam)
////        {
////            try
////            {
////                // ITZComman Class _CrOrDb//
////                DebitRequest objDreq = new DebitRequest();
////                DebitResponse objDres = new DebitResponse();
////                CrOrDb objCrOrDb = new CrOrDb();

////                objCrOrDb.AMOUNT = GetDrParam._AMOUNT;
////                objCrOrDb.MERCHANT_KEY = GetDrParam._MERCHANT_KEY;
////                objCrOrDb.MODE = GetDrParam._MODE;
////                objCrOrDb.ORDERID = GetDrParam._ORDERID;
////                objCrOrDb.PASSWORD = GetDrParam._PASSWORD;
////                objCrOrDb.DECODE = GetDrParam._DECODE;
////                objCrOrDb.CHECKSUM = GetDrParam._CHECKSUM;
////                objCrOrDb.DESCRIPTION = GetDrParam._DESCRIPTION;
////                objCrOrDb.SERVICE_TYPE = GetDrParam._SERVICE_TYPE;

////                objDreq.CrOrDb = objCrOrDb;

////                CrOrDbService objcrdbSer = new CrOrDbService();
////                return objcrdbSer.Debit(objDreq);
////            }
////            catch (Exception ex)
////            {
////                throw ex;
////            }
////        }

////        //For Credit responce//
////        public CreditResponse ITZCredit(_CrOrDb GetCrParam)
////        {
////            try
////            {
////                // ITZComman Class _CrOrDb//
////                CreditRequest objCrReq = new CreditRequest();
////                CreditResponse objCrRes = new CreditResponse();
////                CrOrDb objCRorDr = new CrOrDb();

////                objCRorDr.DECODE = GetCrParam._DECODE;
////                objCRorDr.MERCHANT_KEY = GetCrParam._MERCHANT_KEY;
////                objCRorDr.AMOUNT = GetCrParam._AMOUNT;
////                objCRorDr.ORDERID = GetCrParam._ORDERID;
////                objCRorDr.MODE = GetCrParam._MODE;
////                objCRorDr.CHECKSUM = GetCrParam._CHECKSUM;
////                objCRorDr.SERVICE_TYPE = GetCrParam._SERVICE_TYPE;
////                objCRorDr.DESCRIPTION = GetCrParam._DESCRIPTION;

////                objCrReq.CrOrDb = objCRorDr;

////                CrOrDbService objcrdbSer = new CrOrDbService();
////                return objcrdbSer.Credit(objCrReq);
////            }
////            catch (Exception ex)
////            {
////                throw ex;
////            }
////        }

////        //For Refund responce//
////        public RefundResponse ITZRefund(_CrOrDb GetRfParam)
////        {
////            try
////            {
////                // ITZComman Class _CrOrDb//
////                RefundRequest objRfReq = new RefundRequest();
////                RefundResponse objRfRes = new RefundResponse();
////                CrOrDb objRef = new CrOrDb();

////                objRef.MERCHANT_KEY = GetRfParam._MERCHANT_KEY;
////                objRef.ORDERID = GetRfParam._ORDERID;
////                objRef.MODE = GetRfParam._MODE;
////                objRef.REFUNDTYPE = GetRfParam._REFUNDTYPE;
////                objRef.REFUNDORDERID = GetRfParam._REFUNDORDERID;
////                objRef.CHECKSUM = GetRfParam._CHECKSUM;
////                objRef.DESCRIPTION = GetRfParam._DESCRIPTION;

////                objRfReq.CrOrDb = objRef;

////                CrOrDbService objcrdbSer = new CrOrDbService();
////                return objcrdbSer.Refund(objRfReq);
////            }
////            catch (Exception ex)
////            {
////                throw ex;
////            }
////        }
////    }
////}