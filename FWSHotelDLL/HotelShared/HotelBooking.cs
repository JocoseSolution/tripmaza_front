﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace HotelShared
{
   public class HotelBooking
    {
        public string HotelCityList { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string SearchType { get; set; }
        public string HotelLocation { get; set; }
        public string RoomRPH { get; set; }
        public string BookingType { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string HotelName { get; set; }
        public string HtlCode { get; set; }
        public string StarRating { get; set; }
        public string RoomName { get; set; }
        public string RoomPlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public string HotelAddress { get; set; }
        public string SharingBedding { get; set; }
        public string Refundable { get; set; }
        public int NoofRoom { get; set; }
        public int NoofNight { get; set; }
        public string GuestDetails { get; set; }
        public int TotAdt { get; set; }
        public int TotChd { get; set; }
        public string ExtraCot { get; set; }
        public int ExtraRoom { get; set; }

        public ArrayList AdtPerRoom { get; set; }
        public ArrayList ChdPerRoom { get; set; }
        public int[,] ChdAge { get; set; }
        public ArrayList AdditinalGuest { get; set; }

        public string HtlType { get; set; }
        public string Provider { get; set; }
        public decimal CurrancyRate { get; set; }
        public decimal servicetax { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal ExtraGuestTax { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public string ThumbImg { get; set; }

        public string AgencyName { get; set; }
        public string AgentID { get; set; }
        public string AdminMrkType { get; set; }
        public decimal AdminMrkPer { get; set; }
        public decimal AdminMrkAmt { get; set; }
        public string AgentMrkType { get; set; }
        public decimal AgentMrkPer { get; set; }
        public decimal AgentMrkAmt { get; set; }
        public string DistrMrkType { get; set; }
        public decimal DistrMrkPer { get; set; }
        public decimal DistrMrkAmt { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal ServiseTaxAmt { get; set; }
        public decimal MrkAmt { get; set; }
        public string CommisionType { get; set; }
        public decimal CommisionPer { get; set; }
        public decimal CommisionAmt { get; set; }
        public decimal AgentDebitAmt { get; set; }
        public string TaxType { get; set; }

        public string HotelPolicy { get; set; }
        public string HotelContactNo { get; set; }

        public string Status { get; set; }
        public string LoginID { get; set; }
        public string Orderid { get; set; }
        public string ProBookingID { get; set; }
        public string BookingID { get; set; }
        public string ReferenceNo { get; set; }
        public string IPAddress { get; set; }

        public string TGUrl { get; set; }
        public string TGUsername { get; set; }
        public string TGPassword { get; set; }
        public string TGPropertyId { get; set; }

        public string GTAURL { get; set; }
        public string GTAClintID { get; set; }
        public string GTAPassword { get; set; }
        public string GTAEmailAddress { get; set; }

        public string RegionId { get; set; }
        public string RoomXMLURL { get; set; }
        public string RoomXMLUserID { get; set; }
        public string RoomXMLPassword { get; set; }
        public string RoomXMLOrgID { get; set; }

        public string EXURL { get; set; }
        public string EXAPIKEY { get; set; }
        public string EXCID { get; set; }

        public string RezNextUrl { get; set; }
        public string RezNextUserName { get; set; }
        public string RezNextPassword { get; set; }
        public string RezNextSystemId { get; set; }

        public string HotelSearchReq { get; set; }
        public string HotelSearchRes { get; set; }
        public string ProBookingReq { get; set; }
        public string ProBookingRes { get; set; }
        public string BookingConfReq { get; set; }
        public string BookingConfRes { get; set; }
        public string BookingCancelReq { get; set; }
        public string BookingCancelRes { get; set; }

        public string PGTitle { get; set; }
        public string PGFirstName { get; set; }
        public string PGLastName { get; set; }
        public string PGAddress { get; set; }
        public string PGCity { get; set; }
        public string PGState { get; set; }
        public string PGCountry { get; set; }
        public string PGPin { get; set; }
        public string PGEmail { get; set; }
        public string PGContact { get; set; }

        public string PgCharges { get; set; }
        public string PaymentMode { get; set; }

        public string TGXmlHeader
        {
            get { return "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>"; }
        }
        public string TGXmlFooter
        {
            get { return "<ResGlobalInfo><Guarantee GuaranteeType='PrePay'/></ResGlobalInfo></HotelReservation></HotelReservations></OTA_HotelResRQ></soap:Body></soap:Envelope>"; }
        }
        public string TGHotelBookingHeader
        {
            get { return "<OTA_HotelResRQ xmlns='http://www.opentravel.org/OTA/2003/05' CorrelationID='Orderids' TransactionIdentifier='' Version='1.003'><POS><Source ISOCurrency='INR'>"; }
        }
        
        public string GTAXmlHeader
        {
            get { return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Source>"; }
        }
        public string GTARequestMode
        {
            get { return "<RequestMode>SYNCHRONOUS</RequestMode></RequestorPreferences></Source><RequestDetails>"; }
        }
        public string GTAXmlFooter
        {
            get { return "</RequestDetails></Request>"; }
        }

        public string EXSecretKey { get; set; }
        public string EXRateKey { get; set; }
        //public string EXRateCode { get; set; }
       // public string EXClintID { get; set; }
       // public string EXEmailAddress { get; set; }
       // public string EXPassword { get; set; }
       // public string EXAPIKEY { get; set; }
       // public string EXCID { get; set; }
        public string EXBedTypeId { get; set; }
        public string EXSmoking { get; set; }
    }
}
