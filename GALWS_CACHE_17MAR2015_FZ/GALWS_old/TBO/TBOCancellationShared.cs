﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GALWS.TBO.TBOCancellationShared
{
   public class TicketCRInfo
   {
       public int ChangeRequestId { get; set; }
       public int TicketId { get; set; }
       public int Status { get; set; }
       public string Remarks { get; set; }
       public int ChangeRequestStatus { get; set; }
   }

   public class Response
   {
       public bool B2B2BStatus { get; set; }
       public List<TicketCRInfo> TicketCRInfo { get; set; }
       public int ResponseStatus { get; set; }
       public string TraceId { get; set; }
   }
   public class RootObject
   {
       public Response Response { get; set; }
   }
}
