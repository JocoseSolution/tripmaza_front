﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using ITZERRORLOG;
using STD.BAL;
namespace GALWS.ITQAirApi
{
    public class BookingAndAddPassanger
    {
        public List<ItqAirApiSSR> ItqAirApiSpecialService(List<ItqAirApiSSR> SSR, string constr, DataTable fltdt)
        {
            try
            {
                STD.DAL.Credentials objCrd = new STD.DAL.Credentials(constr);
                List<STD.Shared.CredentialList> CrdList = objCrd.GetServiceCredentials("TQ");

                if (fltdt.Rows.Count > 0 && CrdList.Count > 0)
                {
                    string[] SnoSplit = new string[] { "Ref!" };
                    string[] SnoSplitDetails = fltdt.Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);
                    string SpecialServiceRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + fltdt.Rows[0]["Searchvalue"].ToString() + "\", \"UserID\": \"" + CrdList[0].UserID + "\" }";
                    string SpecialServiceResponse = ITQAirApiUtility.PostJsionXML(CrdList[0].LoginPWD.Replace("Availability", "SpecialService"), "application/xml; charset=utf-8", SpecialServiceRequest);
                    ITQAirApiUtility.SaveFile(SpecialServiceRequest, "SpecialServiceRequest_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());
                    ITQAirApiUtility.SaveFile(SpecialServiceResponse, "SpecialServiceResponse_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());
                    if (SpecialServiceResponse.Contains("<Status>"))
                    {
                        XDocument ItqDocument = XDocument.Parse(SpecialServiceResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        if (ItqDocument.Element("SpecialServiceAvailibilityResponse").Element("Status").Value.Trim() == "Success")
                        {
                            XElement baggageDynamicList = ItqDocument.Element("SpecialServiceAvailibilityResponse").Element("Result").Element("SpecialServiceResponse").Element("baggageDynamicList");
                            XElement mealDynamicList = ItqDocument.Element("SpecialServiceAvailibilityResponse").Element("Result").Element("SpecialServiceResponse").Element("mealDynamicList");

                            foreach (var baggageinfo in baggageDynamicList.Elements("BaggageDynamic"))
                            {
                                SSR.Add(new ItqAirApiSSR
                                {
                                    Code = baggageinfo.Element("Code").Value,
                                    WayType = baggageinfo.Element("WayType").Value,
                                    //Description = "Checked baggage " + baggageinfo.Element("Weight").Value + "KG",
                                    Weight = baggageinfo.Element("Weight").Value,
                                    Amount = Convert.ToDecimal(baggageinfo.Element("Price").Value),
                                    Origin = baggageinfo.Element("Origin").Value,
                                    Destination = baggageinfo.Element("Destination").Value,
                                    Description = baggageinfo.Element("Description").Value,
                                    SSRType = "Baggage"
                                });
                            }
                            foreach (var Mealinfo in mealDynamicList.Elements("MealDynamic"))
                            {
                                SSR.Add(new ItqAirApiSSR
                                {
                                    Code = Mealinfo.Element("Code").Value,
                                    WayType = Mealinfo.Element("WayType").Value,
                                    AirlineDescription = Mealinfo.Element("AirlineDescription").Value,
                                    Amount = Convert.ToDecimal(Mealinfo.Element("Price").Value),
                                    Origin = Mealinfo.Element("Origin").Value,
                                    Destination = Mealinfo.Element("Destination").Value,
                                    Description = Mealinfo.Element("Description").Value,
                                    Quantity = Mealinfo.Element("Quantity").Value,
                                    MealCategory = Mealinfo.Element("MealCategory").Value,
                                    SSRType = "Meal"
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ItqAirApiSpecialService", "Error_001", ex, "ITQAirApi");

            }
            return SSR;
        }

        public string AddPassanger(DataTable gstTQ, string URL, string Mobile, string Email, DataTable fltdt, DataTable MealBages, DataSet PaxDs, DataSet AgencyDs, decimal OriginalFare, ref Dictionary<string, string> xml)
        {
            string TotalCost = "FAILURE", Reqxml = "", Resxml = "";
            try
            {
                if (fltdt.Rows.Count > 0)
                {
                    string[] SnoSplit = new string[] { "Ref!" };
                    string[] SnoSplitDetails = fltdt.Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);
                    string addPaxRequest = AddPassangerRequest(gstTQ, SnoSplitDetails[0], SnoSplitDetails[1], fltdt.Rows[0]["Searchvalue"].ToString(), Mobile, Email, PaxDs, AgencyDs, MealBages, OriginalFare);
                    //   string addPaxRequest = AddPassangerRequest(SnoSplitDetails[0], SnoSplitDetails[1], fltdt.Rows[0]["Searchvalue"].ToString(), Mobile, Email, PaxDs, AgencyDs, MealBages, OriginalFare);
                    string AddPaxResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", addPaxRequest);
                    ITQAirApiUtility.SaveFile(addPaxRequest, "addPaxRequest_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());
                    ITQAirApiUtility.SaveFile(AddPaxResponse, "AddPaxResponse_" + fltdt.Rows[0]["Track_id"].ToString(), fltdt.Rows[0]["User_id"].ToString());
                    if (!AddPaxResponse.Contains("<Errors>"))
                    {
                        XDocument ItqDocument = XDocument.Parse(AddPaxResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        // if (ItqDocument.Element("AddPassengerResponse").Element("Status").Value.Trim() == "Success")
                        // {
                        XElement GetItineraryPriceResult = ItqDocument.Element("AddPassengerResponse");
                        TotalCost = OriginalFare.ToString();
                        // }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("AddPassanger", "Error_001", ex, "ITQAirApi");
                xml.Add("UPPAXREQ", "");
                xml.Add("UPPAXRES", "");
                xml.Add("UCCONREQ", "");
                xml.Add("UCCONRES", "");
                xml.Add("STATEREQ", "");
                xml.Add("STATERES", "");
                xml.Add("APBREQ", "");
                xml.Add("APBRES", "");
                xml.Add("BC-REQ", "");
                xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
            }
            finally
            {
                xml.Add("SSR", Reqxml + "<br>" + Resxml);
            }
            return TotalCost;
        }

        public string ITQairApiBooking(string URL, string Searchvalue, string Track_id, string User_id, string[] SnoSplitDetails, ref string AirPnr)
        {
            string GDSPNR = "FQ-API";
            // string AirPnr = "FQ-API";
            try
            {
                string BookingRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + Searchvalue + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
                ITQAirApiUtility.SaveFile(BookingRequest, "Booking_" + Track_id, User_id);
                string BookingResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", BookingRequest);
                ITQAirApiUtility.SaveFile(BookingResponse, "Booking_" + Track_id, User_id);
                if (BookingResponse.Contains("<Status>"))
                {
                    XDocument ItqDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    GDSPNR = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("PNR").Value;
                    AirPnr = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("FlightDetails").Element("Segment").Element("AirLinePNR").Value;
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ITQairApiBooking", "Error_001", ex, "ITQAirAPI");
            }
            return GDSPNR;
        }

        public Hashtable ITQairApiTiketting(string URL, string GDSPNR, string Searchvalue, string Track_id, string User_id, string[] SnoSplitDetails)
        {
            Hashtable TKTHT = new Hashtable();
            ArrayList TktNoArray = new ArrayList();
            try
            {
                string BookingRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + Searchvalue + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
                string BookingResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", BookingRequest);
                ITQAirApiUtility.SaveFile(BookingRequest, "Ticketing_" + Track_id, User_id);
                ITQAirApiUtility.SaveFile(BookingResponse, "Ticketing_" + Track_id, User_id);


                if (BookingResponse.Contains("<Status>"))
                {
                    XDocument ItqDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    if (ItqDocument.Element("BookingResponse").Element("Status").Value.Trim() == "Success")
                    {

                        if (ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("BookingStatus").Value.ToUpper() == "Ticketed".ToUpper())
                        {

                            foreach (var paxtkt in ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("CustomerInfo").Element("PassengerTicketDetails").Elements("PassengerTicketDetails"))
                            {
                                if (paxtkt.Element("PaxTypeCode").Value.ToUpper() == "INF")
                                {
                                    TktNoArray.Add(paxtkt.Element("FirstName").Value + paxtkt.Element("LastName").Value + "/" + paxtkt.Element("TicketNumber").Value);
                                }
                                else
                                {
                                    TktNoArray.Add(paxtkt.Element("FirstName").Value + paxtkt.Element("LastName").Value + paxtkt.Element("Title").Value + "/" + paxtkt.Element("TicketNumber").Value);
                                }


                            }
                            TKTHT.Add("TktNoArray", TktNoArray);
                            //  AirPnr = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("FlightDetails").Element("AirLinePNR").Value;
                            GDSPNR = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("PNR").Value;
                            TKTHT.Add("PNRRTReq", BookingRequest);
                            TKTHT.Add("PNRRTRes", BookingResponse);
                            TKTHT.Add("AirPnr", GDSPNR);
                            //TKTHT.Add("SSReq", SSReq);
                            //TKTHT.Add("SSRes", SSRes);
                            //TKTHT.Add("DOCPRDReq", TktReq);
                            //TKTHT.Add("DOCPRDRes", TktRes);
                            //TKTHT.Add("PNRRT2Req", STTReq + "<br/>" + PNBF2Req);
                            //TKTHT.Add("PNRRT2Res", STTRes + "<br/>" + PNBF2Res);
                            //TKTHT.Add("SEReq", SEReq);
                            //TKTHT.Add("SERes", SERes);
                        }
                        else
                        {
                            TktNoArray.Add("AirLine is not able to make ETicket");
                            TKTHT.Add("TktNoArray", TktNoArray);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                TktNoArray.Add("AirLine is not able to make ETicket");
                TKTHT.Add("TktNoArray", TktNoArray);
                ExecptionLogger.FileHandling("ITQairApiTiketting", "Error_001", ex, "ITQAirAPI");
            }
            //finally
            //{
            //    FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

            //    objCommonBAL.InsertMULTIBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
            //        , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
            //        , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
            //        , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            
            //}
            return TKTHT;
        }
        public string AddPassangerRequest(DataTable GstDetails, string Sessionids, string Key, string ReferenceNo, string PgMobile, string PgEmail, DataSet PaxDs, DataSet AgencyDs, DataTable MealBages, decimal OriginalFare)
        {
            StringBuilder objreqJsion = new StringBuilder();
            try
            {
                // DataRow[] newFltDsIN = fltdt.Select("TripType='R'");

                objreqJsion.Append("{ \"SessionID\": \"" + Sessionids + "\", \"Key\": \"" + Key + "\", \"ReferenceNo\": \"" + ReferenceNo + "\",");
                objreqJsion.Append(" \"CustomerInfo\": {  \"Email\": \"" + PgEmail + "\", \"Mobile\": \"" + PgMobile + "\", ");
                objreqJsion.Append("\"Address\" : \"" + AgencyDs.Tables[0].Rows[0]["Address"].ToString() + "\", ");
                objreqJsion.Append("\"City\" : \"" + AgencyDs.Tables[0].Rows[0]["City"].ToString() + "\", ");
                objreqJsion.Append("\"State\" : \"" + AgencyDs.Tables[0].Rows[0]["State"].ToString() + "\", ");
                objreqJsion.Append("\"CountryCode\" : \"IN\", \"CountryName\" : \"India\", ");
                objreqJsion.Append("\"ZipCode\" : \"" + AgencyDs.Tables[0].Rows[0]["zipcode"].ToString() + "\", ");

                objreqJsion.Append("\"PassengerDetails\": [ {");
                for (int i = 0; i < PaxDs.Tables[0].Rows.Count; i++)
                {
                    #region Pax Information
                    objreqJsion.Append("\"Title\": \"" + PaxDs.Tables[0].Rows[i]["Title"].ToString() + "\", ");
                    objreqJsion.Append("\"Gender\" : \"" + PaxDs.Tables[0].Rows[i]["Gender"].ToString() + "\",");
                    objreqJsion.Append("\"FirstName\" : \"" + PaxDs.Tables[0].Rows[i]["FName"].ToString() + "\",");
                    objreqJsion.Append("\"MiddleName\" : \"" + PaxDs.Tables[0].Rows[i]["MName"].ToString() + "\",");
                    objreqJsion.Append("\"LastName\" : \"" + PaxDs.Tables[0].Rows[i]["LName"].ToString() + "\",");
                    objreqJsion.Append("\"DateOfBirth\" : \"" + PaxDs.Tables[0].Rows[i]["DOB"].ToString() + "\",");
                    objreqJsion.Append("\"PaxType\" : \"" + PaxDs.Tables[0].Rows[i]["PaxType"].ToString() + "\",");
                    objreqJsion.Append("\"PassportNumber\": \"" + PaxDs.Tables[0].Rows[i]["PassportNo"].ToString() + "\", ");
                    if (MealBages.Rows.Count > 0)
                    {
                        DataRow[] MealDRs = MealBages.Select("MealCode <>''");
                        if (MealDRs.Length > 0)
                        {
                            string[] strmeals = MealDRs[i]["MealDesc"].ToString().Split('_');
                            if (strmeals.Length > 0)
                            {
                                objreqJsion.Append("\"MealType\" : \"" + strmeals[2] + "\", ");
                                objreqJsion.Append("\"MealCode\" : \"" + MealBages.Rows[0]["MealCode"].ToString() + "\", ");
                            }
                        }
                    }
                    else
                    {
                        objreqJsion.Append("\"MealType\" : \"\", ");
                        objreqJsion.Append("\"MealCode\" : \"\", ");
                    }
                    objreqJsion.Append("\"FFNo\" : \"\", ");
                    objreqJsion.Append("\"InfAssPaxName\" : \"\", ");
                    objreqJsion.Append("\"TicketNo\" : \"\", ");
                    objreqJsion.Append("\"Status\" : \"\", ");
                    objreqJsion.Append("\"Nationality\" : \"IN\", ");
                    objreqJsion.Append("\"IssuingCountry\" : \"IN\", ");
                    objreqJsion.Append("\"ExpiryDate\" : \"\", ");
                    objreqJsion.Append("\"FrequentFlyerAirline\" : \"\", ");
                    objreqJsion.Append("\"SeatListDetails\" : [] ");
                    if (i < PaxDs.Tables[0].Rows.Count - 1)
                        objreqJsion.Append("}, ");
                    if (i == PaxDs.Tables[0].Rows.Count - 1)
                        objreqJsion.Append("}");
                    else
                        objreqJsion.Append("{ ");
                    #endregion
                }
                objreqJsion.Append("], \"PassengerTicketDetails\": [], ");
                objreqJsion.Append("\"Payment\": { } },");
                objreqJsion.Append("\"SSRInfo\": [");
                if (MealBages.Rows.Count > 0)
                {
                    DataRow[] MealDR = MealBages.Select("MealCode <>''");
                    DataRow[] BaggageDR = MealBages.Select("BaggageCode <> ''");

                    for (int i = 0; i < MealDR.Length; i++)
                    {
                        string[] strmeal = MealDR[i]["MealDesc"].ToString().Split('_');
                        if (i > 0)
                            objreqJsion.Append(",");
                        objreqJsion.Append("{\"TrackID\": \"" + ReferenceNo + "\", ");
                        objreqJsion.Append("\"WayType\": \"" + strmeal[1] + "\", ");
                        objreqJsion.Append("\"Description\": \"" + strmeal[0] + "\", ");
                        objreqJsion.Append("\"AirlineDescription\": \"" + strmeal[2] + "\", ");
                        objreqJsion.Append("\"PaxNumber\": \"" + (i + 1).ToString() + "\", ");
                        objreqJsion.Append("\"TripType\": \"" + MealDR[i]["TripType"].ToString() + "\", ");
                        objreqJsion.Append("\"SSRCode\": \"" + MealDR[i]["MealCode"].ToString() + "\", ");
                        objreqJsion.Append("\"SSRPrice\": \"" + MealDR[i]["MealPrice"].ToString() + "\", ");
                        objreqJsion.Append("\"DepartureStation\": \"" + strmeal[3] + "\", ");
                        objreqJsion.Append("\"ArrivalStation\": \"" + strmeal[4] + "\", ");
                        objreqJsion.Append("\"SSRType\": \"MEAL\",");
                        objreqJsion.Append("\"PaxType\": \"" + strmeal[5] + "\"} ");
                    }
                    for (int i = 0; i < BaggageDR.Length; i++)
                    {
                        string[] strbag = BaggageDR[i]["BaggageDesc"].ToString().Split('_');
                        if (i > 0 || BaggageDR.Length > 0)
                            objreqJsion.Append(",");
                        objreqJsion.Append("{\"TrackID\": \"" + ReferenceNo + "\",");
                        objreqJsion.Append("\"WayType\": \"" + strbag[1] + "\",");
                        objreqJsion.Append("\"Description\": \"" + strbag[0] + "\",");
                        objreqJsion.Append("\"AirlineDescription\": \"\",");
                        objreqJsion.Append("\"PaxNumber\": \"" + (i + 1).ToString() + "\",");
                        objreqJsion.Append("\"TripType\": \"" + BaggageDR[i]["TripType"].ToString() + "\",");
                        objreqJsion.Append("\"SSRCode\": \"" + BaggageDR[i]["BaggageCode"].ToString() + "\", ");
                        objreqJsion.Append("\"SSRPrice\": \"" + BaggageDR[i]["BaggagePrice"].ToString() + "\", ");
                        objreqJsion.Append("\"DepartureStation\": \"" + strbag[3] + "\",");
                        objreqJsion.Append("\"ArrivalStation\": \"" + strbag[4] + "\",");
                        objreqJsion.Append("\"SSRType\": \"BAGGAGE\",");
                        objreqJsion.Append("\"Weight\": \"" + strbag[2] + "\",");
                        objreqJsion.Append("\"PaxType\": \"" + strbag[5] + "\"}");
                    }
                }
                objreqJsion.Append("], \"TotalAmount\": \"" + OriginalFare.ToString() + "\", ");
                objreqJsion.Append("\"SSRAmount\": 0, ");
                objreqJsion.Append("\"Discount\": 0, ");
                objreqJsion.Append("\"GrandTotalFare\": \"" + OriginalFare.ToString() + "\", ");
                //objreqJsion.Append("\"IsGSTProvided\": false}");
                if (GstDetails.Rows.Count > 0)
                {
                    //GSTNO,GST_Company_Address,GST_Company_Name,GST_PhoneNo,GST_Email
                    if (!string.IsNullOrEmpty(GstDetails.Rows[0]["GSTNO"].ToString()))
                    {
                        objreqJsion.Append("\"IsGSTProvided\": true,");
                        objreqJsion.Append("\"GstDetails\": {");

                        objreqJsion.Append("\"GSTNumber\": \"" + GstDetails.Rows[0]["GSTNO"].ToString() + "\",");
                        objreqJsion.Append("\"Name\": \"" + GstDetails.Rows[0]["GST_Company_Name"].ToString() + "\",");
                        objreqJsion.Append("\"Email\": \"" + GstDetails.Rows[0]["GST_Email"].ToString() + "\",");
                        objreqJsion.Append("\"Address\": \"" + GstDetails.Rows[0]["GST_Company_Address"].ToString() + "\",");
                        objreqJsion.Append("\"City\": \"" + GstDetails.Rows[0]["GST_Company_Address"].ToString() + "\",");
                        objreqJsion.Append("\"Pincode\": \"" + GstDetails.Rows[0]["GST_Company_Address"].ToString() + "\", ");
                        objreqJsion.Append("\"State\": \"" + GstDetails.Rows[0]["GST_Company_Address"].ToString() + "\", ");
                        objreqJsion.Append("\"ContactNo\": \"" + GstDetails.Rows[0]["GST_PhoneNo"].ToString() + "\"}} ");


                    }
                    else
                    {
                        objreqJsion.Append("\"IsGSTProvided\": false}");
                    }
                }
              

            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("AddPassangerRequest", "Error_001", ex, "ITQAirApi"); }
            return objreqJsion.ToString();
        }

        public string ItqAirApiLccFareRule(string URL, string ClientCode, string Track_id, string SessionID, string SupplierCode, string AirCode, string PnrNO, string fareType, string ReqType, string User_id)
        {
            string FareRule = "";
            try
            {
                string SpecialServiceRequest = "{\"ClientCode\": \"" + ClientCode + "\", \"SessionID\": \"" + SessionID + "\", \"SupplierCode\": \"" + SupplierCode + "\", \"AirCode\": \"" + AirCode + "\", \"DepartDate\": \"\", \"DepartTime\": \"\"";
                if (ReqType == "Booking")
                    SpecialServiceRequest += ", \"PnrNO\": \"" + PnrNO + "\" }";
                else
                    SpecialServiceRequest += "}";
                string SpecialServiceResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", SpecialServiceRequest);
                ITQAirApiUtility.SaveFile(SpecialServiceRequest, "FareRuleRequest_" + ReqType + Track_id, User_id);
                ITQAirApiUtility.SaveFile(SpecialServiceResponse, "FareRuleResponse_" + ReqType + Track_id, User_id);
                if (SpecialServiceResponse.Contains("<Status>"))
                {
                    XDocument ItqDocument = XDocument.Parse(SpecialServiceResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    if (ItqDocument.Element("SpecialServiceAvailibilityResponse").Element("Status").Value.Trim() == "Success")
                    {
                        XElement GetItineraryPriceResult = ItqDocument.Element("SpecialServiceAvailibilityResponse");
                        FareRule = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ItqAirApiLccFareRule", "Error_001", ex, "ITQAirApi");

            }
            return FareRule;
        }
        public string ItqAirApiGDSFareRule(string URL, DataTable fltdt)
        {
            string Farerule = "";
            try
            {
                //string[] SnoSplit = new string[] { "Ref!" };
                //string[] SnoSplitDetails = fltdt.Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);
                //string SpecialServiceRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + fltdt.Rows[0]["Searchvalue"].ToString() + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
                //string SpecialServiceResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", SpecialServiceRequest);
                //ITQAirApiUtility.SaveFile(SpecialServiceRequest, "SpecialServiceRequest_" + fltdt.Rows[0]["Track_id"].ToString());
                //ITQAirApiUtility.SaveFile(SpecialServiceResponse, "SpecialServiceResponse_" + fltdt.Rows[0]["Track_id"].ToString());
                //if (SpecialServiceResponse.Contains("<Status>"))
                //{
                //    XDocument ItqDocument = XDocument.Parse(SpecialServiceResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                //    if (ItqDocument.Element("SpecialServiceAvailibilityResponse").Element("Status").Value.Trim() == "Success")
                //    {
                //        XElement GetItineraryPriceResult = ItqDocument.Element("SpecialServiceAvailibilityResponse");
                //        Farerule = "Success";
                //    }
                //}
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ItqAirApiGDSFareRule", "Error_001", ex, "ITQAirApi");

            }
            return Farerule;
        }

    }
    public class ItqAirApiSSR
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string SSRType { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Weight { get; set; }
        public string AirlineDescription { get; set; }
        public string MealCategory { get; set; }
        public string Quantity { get; set; }
        public string Currency { get; set; }
        public string WayType { get; set; }
    }
}
