﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace STD.BAL
{
    public class YAAirBook
    {
        public string YAFlightBook(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, DataTable MBDT, ref ArrayList TktNoArray, string Constr)
        {
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";
            StringBuilder YABookReq = new StringBuilder();
            string res = "";
            string exep = "";
            DataSet datasetPrice1 = new DataSet();
            string airpriceReq = "";
            string airPriceResp = "";

            bool refundStatus = false;
            try
            {
                try
                {
                    DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                    DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                    DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");


                    DataRow[] FltO = FltDT.Select("TripType='O'", "counter ASC");
                    DataRow[] FltR = FltDT.Select("TripType='R'", "counter ASC");
                    string ODR = "";
                    if (Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["TripType"]).Trim().ToUpper() == "R" && Convert.ToString(FltDT.Rows[0]["Trip"]).ToUpper().Trim() == "D")
                    {
                        ODR = FltR[0]["Searchvalue"].ToString();
                    }


                    YAAirPrice objAirPrice = new YAAirPrice();
                    //objAirPrice.AirPrice(FltO[0]["Searchvalue"].ToString(), ODR, ADTPax.Count(), CHDPax.Count(), INFPax.Count(), STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, ref Log, ref exep, "", Constr, Convert.ToString(FltDT.Rows[0]["Trip"]).ToUpper().Trim());


                    FlightCommonBAL objbal = new FlightCommonBAL(Constr);

                    datasetPrice1 = objbal.InserAndGetYAPricingLog(Convert.ToString(FltDT.Rows[0]["Track_Id"]), "", "", "get");
                    try
                    {
                        airPriceResp = Convert.ToString(datasetPrice1.Tables[0].Rows[0]["Price_Res"]);
                        airpriceReq = Convert.ToString(datasetPrice1.Tables[0].Rows[0]["Price_Req"]);
                        Log.Add("OTA_AirPriceRS_Req",airpriceReq);
                        Log.Add("OTA_AirPriceRS_Res",airPriceResp);                      
                    
                    }
                    catch (Exception ex)
                    {
                    }

                    decimal TotPrice = Math.Floor(objAirPrice.ParseAirPrice(airPriceResp));

                    string itenary = objAirPrice.GetItineraryDeatilsFromPriceRS(airPriceResp);

                    decimal BgPr = 0;
                    if (MBDT.Rows.Count > 0)
                    {

                        for (int jj = 0; jj < MBDT.Rows.Count; jj++)
                        {
                            BgPr = BgPr + Convert.ToDecimal(MBDT.Rows[jj]["BaggagePrice"]) + Convert.ToDecimal(MBDT.Rows[jj]["MealPrice"]);
                        }
                    }

                    decimal totfare = Math.Floor(Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]));

                    decimal diff =  TotPrice - totfare ;

                    //if (totfare < Convert.ToDecimal(TotPrice) || Convert.ToDecimal(TotPrice) <= 0 || totfare <= 0)
                    if (diff>1 || Convert.ToDecimal(TotPrice) <= 0 || totfare <= 0)
                    {
                        exep = exep + "Fare Amount Diffrence new fare:" + TotPrice.ToString() + " old Fare:" + totfare.ToString();
                        refundStatus = true;
                        goto FareMisMatch;
                    }




                    #region Booking Request
                    string dest = "";
                    if (Convert.ToString(FltDT.Rows[0]["Trip"]).ToUpper().Trim() == "I")
                    {
                        dest = "INT";
                    }


                    YABookReq.Append("<?xml version='1.0' encoding='UTF-8'?>");
                    YABookReq.Append("<soap:Envelope xmlns='http://www.opentravel.org/OTA/2003/05' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>");
                    YABookReq.Append(" <soap:Body>");
                    YABookReq.Append("<ota:OTA_ItineraryBookRQ xmlns:ota='http://www.opentravel.org/OTA/2003/05'>");
                    YABookReq.Append("<POS xmlns='http://www.opentravel.org/OTA/2003/05'>");
                    YABookReq.Append("<Source AgentSine='' PseudoCityCode='NPCK' TerminalID='1'>");
                    YABookReq.Append("<RequestorID ID='AFFILIATE' />");
                    YABookReq.Append("</Source>");
                    YABookReq.Append("<YatraRequests>");
                    string midOfficeID = ConfigurationManager.AppSettings["MidOfficeAgentID"].ToString();
                    YABookReq.Append("<YatraRequest DoNotHitCache='true' DoNotCache='false' MidOfficeAgentID='" + midOfficeID + "' AffiliateID='YTFABTRAVEL' YatraRequestTypeCode='SMPA'  Destination='" + dest + "'  />");//RequestType='SR' SplitResponses='true'
                    YABookReq.Append("</YatraRequests>");
                    YABookReq.Append("</POS>");
                    string uniqIdn = DateTime.Now.ToString("dMyyHms");

                    YABookReq.Append("<OJ_PaymentRQ xmlns='http://www.openjawtech.com/2005' RetransmissionIndicator='false' SequenceNmbr='1' TransactionIdentifier='" + uniqIdn + "' TransactionStatusCode='A' Version='0.0' productID='XXXXXXXX' type='payment'>");
                    YABookReq.Append("<PaymentDetails>");
                    YABookReq.Append("<PaymentDetail GuaranteeIndicator='false'>");
                    YABookReq.Append("<PaymentCard CardCode='NA' CardNumber='NA' CardType='NA' EffectiveDate='NA' ExpireDate='NA' ExtendPaymentIndicator='false' ExtendedPaymentQuantity='0' SeriesCode='NA' SignatureOnFileIndicator='false'>");
                    YABookReq.Append("<CardHolderName FirstName='" + Convert.ToString(ADTPax[0]["FName"]) + "' MiddleName='ID'>" + Convert.ToString(ADTPax[0]["FName"]) + "</CardHolderName>");
                    YABookReq.Append("<CardNumber>NA</CardNumber>");
                    YABookReq.Append("<CardIssuerName BankID='19389'/>");
                    YABookReq.Append("<CardHolderTelephone PhoneTechType='1'/>");
                    YABookReq.Append("<Phone MobileNumber='" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim() + "' Number=''/>");
                    YABookReq.Append("<Email>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</Email>");

                    YABookReq.Append("<Location><IPAddress>" + Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]) + "</IPAddress></Location>");
                    YABookReq.Append("</PaymentCard>");
                    YABookReq.Append("<PaymentAmount Amount='" + (TotPrice + BgPr).ToString() + "' CurrencyCode='INR' DecimalPlaces='2'/>");
                    YABookReq.Append("</PaymentDetail>");
                    YABookReq.Append("</PaymentDetails>");
                    YABookReq.Append("<AuthDetails Cancelled='false' Fulfilled='true' PaymentMedium='CREDITPOOL'/>");
                    YABookReq.Append("</OJ_PaymentRQ>");



                    YABookReq.Append("<OTA_AirBookRQ TransactionIdentifier='" + uniqIdn + "' Version='0.0'>");
                    YABookReq.Append(itenary);
                    YABookReq.Append("<TravelerInfo>");

                    #region Adult
                    if (ADTPax.Count() > 0)
                    {
                        for (int i = 0; i < ADTPax.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["DOB"])) && Convert.ToString(ADTPax[i]["DOB"]).Trim().Length > 7)
                            {
                                string dob = ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0];
                                YABookReq.Append("<AirTraveler AccompaniedByInfant='false' BirthDate='" + dob + "' PassengerTypeCode='ADT'>");
                            }
                            else
                            {
                                YABookReq.Append("<AirTraveler AccompaniedByInfant='false' BirthDate='--' PassengerTypeCode='ADT'>");
                            }




                            YABookReq.Append("<PersonName>");
                            YABookReq.Append("<NamePrefix>" + Convert.ToString(ADTPax[i]["Title"]) + "</NamePrefix>");
                            YABookReq.Append("<GivenName>" + Convert.ToString(ADTPax[i]["FName"]) + " " + Convert.ToString(ADTPax[i]["MName"]) + "</GivenName>");
                            YABookReq.Append("<Surname>" + Convert.ToString(ADTPax[i]["LName"]) + "</Surname>");
                            YABookReq.Append("<NameTitle/>");
                            YABookReq.Append("</PersonName>");
                            YABookReq.Append("<Telephone AreaCityCode='' CountryAccessCode='' DefaultInd='false' FormattedInd='false' MobileNumber='" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim() + "'/>");
                            YABookReq.Append("<Email DefaultInd='false' EmailType='1'>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</Email>");
                            YABookReq.Append("<Address FormattedInd='false'>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress1"] + "</AddressLine>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress2"] + "</AddressLine>");
                            YABookReq.Append("<PostalCode>" + ConfigurationManager.AppSettings["companyzip"] + "</PostalCode>");
                            YABookReq.Append("<StateProv StateCode=''/>");
                            YABookReq.Append("</Address>");
                            YABookReq.Append("<ProofRequests>");
                            YABookReq.Append("<ProofRequest CompanyID='' ProofType='PP' ProofNumber=''></ProofRequest>");
                            YABookReq.Append("</ProofRequests>");
                            YABookReq.Append("<TravelerRefNumber RPH='1'/>");


                            if (MBDT.Rows.Count > 0)
                            {

                                DataRow[] MBDR = MBDT.Select("PaxId=" + ADTPax[i]["PaxId"], "PaxId ASC");

                                for (int mb = 0; mb < MBDR.Count(); mb++)
                                {
                                    string uid = "1";

                                    if (Convert.ToString(MBDR[mb]["TripType"]).Trim().ToUpper() == "R")
                                    {
                                        uid = "2";
                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["MealPrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.MealDynamic objMeal = new STD.BAL.TBO.BOOKSHARED.MealDynamic();
                                        string[] mbdesc = Convert.ToString(MBDR[mb]["MealDesc"]).Split('_');


                                        YABookReq.Append("<Meals>");
                                        YABookReq.Append(" <Meal amount='" + Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString()) + "' code='" + Convert.ToString(MBDR[mb]["MealCode"]) + "' conversion='" + Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString()) + "' currency='INR' desc='" + mbdesc[0] + "' rph='1' uid='" + uid + "' uniqueIdentifier='" + uid + "'/>");
                                        YABookReq.Append("</Meals>");

                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["BaggagePrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.Baggage objBag = new STD.BAL.TBO.BOOKSHARED.Baggage();

                                        string[] mbdesc = Convert.ToString(MBDR[mb]["BaggageDesc"]).Split('_');
                                        YABookReq.Append("<Baggages>");
                                        YABookReq.Append("<Baggage amount='" + Convert.ToDecimal(MBDR[mb]["BaggagePrice"].ToString()) + "' code='" + Convert.ToString(MBDR[mb]["BaggageCode"]) + "' conversion='" + Convert.ToString(MBDR[mb]["BaggagePrice"]) + "' currency='INR' rph='" + uid + "' uniqueIdentifier='" + uid + "'/>");
                                        YABookReq.Append("</Baggages>");

                                    }

                                }

                            }

                            YABookReq.Append(" </AirTraveler>");
                        }

                    }
                    #endregion
                    #region Child

                    if (CHDPax.Count() > 0)
                    {
                        for (int i = 0; i < CHDPax.Count(); i++)
                        {
                            string dob = CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0];

                            YABookReq.Append("<AirTraveler AccompaniedByInfant='false' BirthDate='" + dob + "' PassengerTypeCode='CHD'>");
                            YABookReq.Append("<PersonName>");

                            string title = Convert.ToString(CHDPax[i]["Title"]).Trim().ToUpper() == "MSTR" ? "Master" : "Miss";
                            YABookReq.Append("<NamePrefix>" + title + "</NamePrefix>");
                            YABookReq.Append("<GivenName>" + Convert.ToString(CHDPax[i]["FName"]) + " " + Convert.ToString(CHDPax[i]["MName"]) + "</GivenName>");
                            YABookReq.Append("<Surname>" + Convert.ToString(CHDPax[i]["LName"]) + "</Surname>");
                            YABookReq.Append(" <NameTitle/>");
                            YABookReq.Append("</PersonName>");
                            YABookReq.Append("<Telephone AreaCityCode='' CountryAccessCode='' DefaultInd='false' FormattedInd='false' MobileNumber='" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim() + "'/>");
                            YABookReq.Append("<Email DefaultInd='false' EmailType='1'>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</Email>");
                            YABookReq.Append("<Address FormattedInd='false'>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress1"] + "</AddressLine>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress2"] + "</AddressLine>");
                            YABookReq.Append("<PostalCode>" + ConfigurationManager.AppSettings["companyzip"] + "</PostalCode>");
                            YABookReq.Append("<StateProv StateCode=''/>");
                            YABookReq.Append("</Address>");
                            YABookReq.Append("<ProofRequests>");
                            YABookReq.Append("<ProofRequest CompanyID='' ProofType='PP' ProofNumber=''></ProofRequest>");
                            YABookReq.Append("</ProofRequests>");
                            YABookReq.Append("<TravelerRefNumber RPH='1'/>");

                            if (MBDT.Rows.Count > 0)
                            {

                                DataRow[] MBDR = MBDT.Select("PaxId=" + CHDPax[i]["PaxId"], "PaxId ASC");

                                for (int mb = 0; mb < MBDR.Count(); mb++)
                                {
                                    string uid = "1";

                                    if (Convert.ToString(MBDR[mb]["TripType"]).Trim().ToUpper() == "R")
                                    {
                                        uid = "2";
                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["MealPrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.MealDynamic objMeal = new STD.BAL.TBO.BOOKSHARED.MealDynamic();
                                        string[] mbdesc = Convert.ToString(MBDR[mb]["MealDesc"]).Split('_');


                                        YABookReq.Append("<Meals>");
                                        YABookReq.Append(" <Meal amount='" + Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString()) + "' code='" + Convert.ToString(MBDR[mb]["MealCode"]) + "' conversion='" + Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString()) + "' currency='INR' desc='" + mbdesc[0] + "' rph='1' uid='" + uid + "' uniqueIdentifier='" + uid + "'/>");
                                        YABookReq.Append("</Meals>");

                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["BaggagePrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.Baggage objBag = new STD.BAL.TBO.BOOKSHARED.Baggage();

                                        string[] mbdesc = Convert.ToString(MBDR[mb]["BaggageDesc"]).Split('_');
                                        YABookReq.Append("<Baggages>");
                                        YABookReq.Append("<Baggage amount='" + Convert.ToDecimal(MBDR[mb]["BaggagePrice"].ToString()) + "' code='" + Convert.ToString(MBDR[mb]["BaggageCode"]) + "' conversion='" + Convert.ToString(MBDR[mb]["BaggagePrice"]) + "' currency='INR' rph='" + uid + "' uniqueIdentifier='" + uid + "'/>");
                                        YABookReq.Append("</Baggages>");

                                    }

                                }

                            }

                            YABookReq.Append("</AirTraveler>");


                        }


                    }
                    #endregion

                    #region Infant

                    if (INFPax.Count() > 0)
                    {
                        for (int i = 0; i < INFPax.Count(); i++)
                        {
                            string dob = INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0];
                            YABookReq.Append("<AirTraveler AccompaniedByInfant='false' BirthDate='" + dob + "' PassengerTypeCode='INF'>");
                            YABookReq.Append("<PersonName>");

                            string title = Convert.ToString(INFPax[i]["Title"]).Trim().ToUpper() == "MSTR" ? "Master" : "Miss";
                            YABookReq.Append("<NamePrefix>" + title + "</NamePrefix>");
                            YABookReq.Append("<GivenName>" + Convert.ToString(INFPax[i]["FName"]) + "</GivenName>");
                            YABookReq.Append("<Surname>" + Convert.ToString(INFPax[i]["LName"]) + "</Surname>");
                            YABookReq.Append("<NameTitle/>");
                            YABookReq.Append("</PersonName>");
                            YABookReq.Append("<Telephone AreaCityCode='' CountryAccessCode='' DefaultInd='false' FormattedInd='false' MobileNumber='" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim() + "'/>");
                            YABookReq.Append("<Email DefaultInd='false' EmailType='1'>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</Email>");
                            YABookReq.Append("<Address FormattedInd='false'>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress1"] + "</AddressLine>");
                            YABookReq.Append("<AddressLine>" + ConfigurationManager.AppSettings["companyaddress2"] + "</AddressLine>");
                            YABookReq.Append("<PostalCode>" + ConfigurationManager.AppSettings["companyzip"] + "</PostalCode>");
                            YABookReq.Append("<StateProv StateCode=''/>");
                            YABookReq.Append("</Address>");
                            YABookReq.Append("<ProofRequests>");
                            YABookReq.Append("<ProofRequest CompanyID='' ProofType='PP' ProofNumber=''></ProofRequest>");
                            YABookReq.Append("</ProofRequests>");
                            YABookReq.Append("<TravelerRefNumber RPH='1'/>");
                            YABookReq.Append(" </AirTraveler>");

                        }

                    }
                    #endregion


                    YABookReq.Append("</TravelerInfo>");
                    YABookReq.Append("<Fulfillment/>");
                    YABookReq.Append("<Ticketing CancelOnExpiryInd='false' ReverseTktgSegmentsInd='false' TicketType='eTicket' TimeLimitMinutes='0'/>");
                    YABookReq.Append("</OTA_AirBookRQ>");
                    YABookReq.Append("</ota:OTA_ItineraryBookRQ>");
                    YABookReq.Append("</soap:Body>");
                    YABookReq.Append("</soap:Envelope>");


                    #endregion
                }
                catch (Exception ex1)
                {

                    exep = exep + ex1.Message + Convert.ToString(ex1.StackTrace);
                    refundStatus = true;
                    goto FareMisMatch;
                }

                res = YAUtility.PostXml(MedthodSVCUrl.SvcURL, YABookReq.ToString(), "", "", MedthodSVCUrl.BookMTHD, ref exep);


                string resStr = res.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                    .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "").Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                    .Replace("xmlns:oj=\"http://www.openjawtech.com/2005\"", "").Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                    .Replace("ns1:", "").Replace("oj:", "").Replace("soapenv:", "")
                    .Replace("xmlns=\"http://www.openjawtech.com/2005\"", "")
                    .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "");

                XDocument xmlDoc = XDocument.Parse(resStr);

                IEnumerable<XElement> xlAirReservation = from el in xmlDoc.Descendants("OTA_ItineraryBookRS").Descendants("OTA_AirBookRS").Descendants("AirReservation").Descendants("BookingReferenceID")
                                                         select el;
                for (int ptn = 0; ptn < xlAirReservation.Count(); ptn++)
                {
                    PNR = xlAirReservation.ElementAt(ptn).Attribute("ID").Value.Trim();
                }

            FareMisMatch:
                {
                    if (refundStatus)
                    {
                        PNR = PNR + "-FQ-FRM";
                    }

                }

            }
            catch (Exception ex)
            {
                try
                {
                    exep = exep + ex.Message + " stackTrace:" + ex.StackTrace.ToString();
                }
                catch { }
                PNR = "-FQ-EXP";

            }
            finally
            {
                YAUtility.SaveXml(YABookReq.ToString(), "", "OTA_ItineraryBookRQ_Req");
                YAUtility.SaveXml(res, "", "OTA_ItineraryBookRQ_Res");

                Log.Add("OTA_ItineraryBookRQ_Req", YABookReq.ToString());
                Log.Add("OTA_ItineraryBookRQ_Res", res);

                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                objCommonBAL.InsertYABookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                    , Log.ContainsKey("OTA_ItineraryBookRQ_Req") == true ? Log["OTA_ItineraryBookRQ_Req"] : "", Log.ContainsKey("OTA_ItineraryBookRQ_Res") == true ? Log["OTA_ItineraryBookRQ_Res"] : ""
                    , Log.ContainsKey("OTA_AirPriceRS_Req") == true ? Log["OTA_AirPriceRS_Req"] : ""
                    , Log.ContainsKey("OTA_AirPriceRS_Res") == true ? Log["OTA_AirPriceRS_Res"] : "", exep);

            }

            if (PNR.ToUpper().Contains("PENDING"))
            {
                PNR = PNR + "-FQ-EXP";
            }

            return PNR;
        }








    }
}
