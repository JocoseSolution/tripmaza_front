﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Diagnostics;

namespace GRP_Booking
{
    public class ErrorLogTrace
    {
        public static bool WriteErrorLog(Exception Exptn, string ModuleName)
        {
            string LogDirectory = String.Empty;
            bool successStatus = false;
            string fileType = "", fileNames = "";

            StackTrace trace = new System.Diagnostics.StackTrace(Exptn, true);
            string linenumber = Convert.ToString(trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber());
            string ErrorMsg = Exptn.Message;
            string PageName = trace.GetFrame((trace.FrameCount - 1)).GetFileName();

            fileNames = PageName.Substring(PageName.LastIndexOf("\\"), PageName.Length - PageName.LastIndexOf("\\"));
            fileType = fileNames.Substring(fileNames.Length - 7);

            if (fileType == "aspx.vb" || fileType == "aspx.cs")
            {
                if (fileType == "aspx.vb")
                {
                    fileNames = fileNames.Substring(1).Replace(".aspx.vb", "");
                }
                if (fileType == "aspx.cs")
                {
                    fileNames = fileNames.Substring(1).Replace(".aspx.cs", "");
                }
            }
            else if (fileNames != "aspx.vb" || fileNames != "aspx.vb")
            {
                fileType = fileNames.Substring(fileNames.Length - 2);

                if (fileType == "vb" || fileType == "cs")
                {
                    if (fileType == "vb")
                    {
                        fileNames = fileNames.Substring(1).Replace(".vb", "");
                    }
                    if (fileType == "cs")
                    {
                        fileNames = fileNames.Substring(1).Replace(".cs", "");
                    }
                }
            }
            string FolderPath = ConfigurationManager.AppSettings["FilePath"].ToString() + DateTime.Now.ToString("dd-MM-yyyy");
            string ModuleFolderPath = FolderPath + "\\" + ModuleName;

            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
                if (!Directory.Exists(ModuleFolderPath))
                {
                    Directory.CreateDirectory(ModuleFolderPath);
                }
            }
            else if (!Directory.Exists(ModuleFolderPath))
            {
                Directory.CreateDirectory(ModuleFolderPath);
            }
            LogDirectory = ModuleFolderPath + "\\" + fileNames + ".txt";
            lock (typeof(ErrorLogTrace))
            {
                StreamWriter m_logSWriter = null;
                try
                {
                    m_logSWriter = new StreamWriter(LogDirectory, true);
                    string ErrorTime = "ErrorTime :" + DateTime.Now.ToString();
                    m_logSWriter.WriteLine(ErrorTime);
                    m_logSWriter.WriteLine(trace);
                    linenumber = "LineNumber :" + linenumber;
                    m_logSWriter.WriteLine(linenumber);
                    ErrorMsg = "ErrorMassage :" + ErrorMsg;
                    m_logSWriter.WriteLine(ErrorMsg);
                    fileNames = "PageName :" + PageName;
                    m_logSWriter.WriteLine(PageName);
                    ModuleName = "ModuleName :" + ModuleName;
                    m_logSWriter.WriteLine(ModuleName);
                    m_logSWriter.WriteLine("=======================================================");
                    successStatus = true;
                }
                catch
                {

                }
                finally
                {
                    if (m_logSWriter != null)
                    {
                        m_logSWriter.Close();
                    }
                }
            }
            return successStatus;
        }
    }
}
