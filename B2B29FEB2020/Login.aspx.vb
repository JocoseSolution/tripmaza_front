﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports ITZLib

Partial Class Login
    Inherits System.Web.UI.Page

    Dim userid As String
    Dim pwd As String
    Dim AgencyName As String
    Dim User As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    Dim id As String
    Dim usertype As String
    Dim typeid As String
    Dim dset As DataSet
    Private det As New Details()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

        Catch ex As Exception

        End Try

    End Sub
    Public Sub LogInValidUser(ByVal uid As String, ByVal upwd As String, ByVal decod As String, ByVal lastlog As String)
        Try

            userid = uid
            pwd = upwd
            dset = user_auth(userid, pwd, decod, lastlog)
            If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
                'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                'lblerr.Text = "Your UserID Seems to be Incorrect"
            ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
                'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                'lblerr.Text = "Your Password Seems to be Incorrect"
            Else
                id = dset.Tables(0).Rows(0)("UID").ToString()
                usertype = dset.Tables(0).Rows(0)("UserType").ToString()
                typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
                User = dset.Tables(0).Rows(0)("Name").ToString()
                If usertype = "TA" Then
                    AgencyName = dset.Tables(0).Rows(0)("Name").ToString()
                    Session("AgencyName") = AgencyName
                End If

                Session("UID") = id
                Session("UserType") = usertype
                Session("TypeID") = typeid
                Session("User_Type") = User
                Session("IsCorp") = False
                FormsAuthentication.RedirectFromLoginPage(userid, False)

                If User = "ACC" Then
                    Response.Redirect("SprReports/Accounts/Ledger.aspx")
                ElseIf User = "ADMIN" Then
                    Session("ADMINLogin") = userid
                    Response.Redirect("IBEHome.aspx")

                ElseIf User = "EXEC" Then
                    Session("ExecTrip") = dset.Tables(0).Rows(0)("Trip").ToString()
                    Response.Redirect("SprReports/admin/profile.aspx")
                ElseIf User = "AGENT" And typeid = "TA1" Then
                    If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                        Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
                    End If
                    Response.Redirect("IBEHome.aspx")

                ElseIf User = "AGENT" And typeid = "TA2" Then
                    If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                        Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp").ToString())
                    End If
                    Response.Redirect("SprReports/Accounts/Ledger.aspx")
                ElseIf User = "SALES" Then
                    Response.Redirect("IBEHome.aspx")
                ElseIf usertype = "DI" Then

                    Response.Redirect("SprReports/Accounts/Ledger.aspx")
                    'END CHANGES FOR DISTR
                End If


            End If
        Catch ex As Exception
            'lblerr.Text = ex.Message

        End Try
    End Sub

    Public Function user_auth(ByVal uid As String, ByVal passwd As String, ByVal decod As String, ByVal lastlog As String) As DataSet

        adap = New SqlDataAdapter("UserLogin", con)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@uid", uid)
        adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd)
        adap.SelectCommand.Parameters.AddWithValue("@parmdecodeitz", decod)
        adap.SelectCommand.Parameters.AddWithValue("@parmlastloginitz", lastlog)
        Dim ds As New DataSet()
        adap.Fill(ds)
        Return ds
    End Function

End Class
