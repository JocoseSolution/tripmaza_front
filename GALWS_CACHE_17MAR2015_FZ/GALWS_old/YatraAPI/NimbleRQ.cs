﻿using STD.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace STD.BAL
{
    public class YANimbleRQ
    {

        public string GetNimbleRequest(FlightSearch objFlt, bool isSplFare, string fareType, bool isLCC, string serviceUrl, string methodName, ref string exep)
        {

            StringBuilder YANimbleReq = new StringBuilder();
            string res = "";
            string val = "";
            string midOfficeID = ConfigurationManager.AppSettings["MidOfficeAgentID"].ToString();

            string dest = "";

            if (objFlt.Trip == Trip.I)
            {
                dest = "INT";
            }

            YANimbleReq.Append("<?xml version='1.0' encoding='UTF-8'?>");

            YANimbleReq.Append("<soap:Envelope xmlns=\"http://www.opentravel.org/OTA/2003/05\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">");
            YANimbleReq.Append("<soap:Body>");
            YANimbleReq.Append("<YT_NimbleRQ>");
            YANimbleReq.Append("<POS>");
            YANimbleReq.Append("<Source>");
            YANimbleReq.Append("<RequestorID ID=\"AFFILIATE\"></RequestorID>");
            YANimbleReq.Append("</Source>");
            YANimbleReq.Append("<YatraRequests>");
            YANimbleReq.Append("<YatraRequest MidOfficeAgentID=\"" + midOfficeID + "\" Destination=\"" + dest + "\" AffiliateID=\"YTFABTRAVEL\" />");
            YANimbleReq.Append("</YatraRequests>");
            YANimbleReq.Append("</POS>");

            if (fareType == "INB")
            {
                YANimbleReq.Append("<OriginDestinationInformation>");
                YANimbleReq.Append("<OriginLocation LocationCode=\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"/>");
                YANimbleReq.Append("<DestinationLocation LocationCode=\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\" />");
                YANimbleReq.Append("</OriginDestinationInformation>");

            }
            else if (fareType == "OutB")
            {
                YANimbleReq.Append("<OriginDestinationInformation>");
                YANimbleReq.Append("<OriginLocation LocationCode=\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"/>");
                YANimbleReq.Append("<DestinationLocation LocationCode=\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\" />");
                YANimbleReq.Append("</OriginDestinationInformation>");

            }

            if (isSplFare)
            {
                YANimbleReq.Append("<OriginDestinationInformation>");
                YANimbleReq.Append("<OriginLocation LocationCode=\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"/>");
                YANimbleReq.Append("<DestinationLocation LocationCode=\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\" />");
                YANimbleReq.Append("</OriginDestinationInformation>");

            }


            YANimbleReq.Append("</YT_NimbleRQ>");
            YANimbleReq.Append("</soap:Body>");
            YANimbleReq.Append("</soap:Envelope>");


            try
            {
                res = YAUtility.PostXml(MedthodSVCUrl.SvcURL, YANimbleReq.ToString(), "", "", methodName, ref exep);
            }
            catch (Exception ex) { res = ex.Message + ex.StackTrace.ToString(); }
            YAUtility.SaveXml(YANimbleReq.ToString(), "", "YT_NimbleRQ_Req");
           
            try
            {

              


                string str = res.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                      .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                      .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                      .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                      .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                      .Replace("ns1:", "");

                XDocument xmlDoc = XDocument.Parse(str);




                val = xmlDoc.Descendants("YT_NimbleRS").Descendants("Supplier").Where(x => x.Attributes("AirlineCode").Any() == true).Where(y => y.Attribute("AirlineCode").Value.ToUpper().Trim() == objFlt.HidTxtAirLine.ToUpper().Trim()).Select(z => z.Attribute("Code")).FirstOrDefault().Value.Trim();
                       


            }
            catch (Exception ex1)
            {
                exep = exep + ex1.Message + ex1.StackTrace.ToString();
                val = "";
            }
            YAUtility.SaveXml(res + exep +val, "", "YT_NimbleRQ_Res");
          
            return val;

        }

    }
}
