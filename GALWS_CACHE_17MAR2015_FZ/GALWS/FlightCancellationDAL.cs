﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GALWS
{
   public class FlightCancellationDAL
    {

        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter adap;
        private SqlDatabase DBHelper;

        public FlightCancellationDAL(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }

        public int InsertTBOCancleLog(string orderId, string TicketNo, string CancelReq, string CancelResp, string CancelStatus)
        {
            int i = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_Insert_TBO_Cancel_Logs";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@TicketNo", DbType.String, TicketNo);
                DBHelper.AddInParameter(cmd, "@CancelReq", DbType.String, CancelReq);
                DBHelper.AddInParameter(cmd, "@CancelResp", DbType.String, CancelResp);
                DBHelper.AddInParameter(cmd, "@CancelStatus", DbType.String, CancelStatus);
                i = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));
            }
            catch
            { }

            return i;
        }
        public int InsertGDSCancleLog(string orderId, string TicketNo, string CancelReq, string CancelResp, string CancelStatus)
        {
            int i = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_Insert_GDS_Cancel_Logs";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@TicketNo", DbType.String, TicketNo);
                DBHelper.AddInParameter(cmd, "@Req", DbType.String, CancelReq);
                DBHelper.AddInParameter(cmd, "@Resp", DbType.String, CancelResp);
                DBHelper.AddInParameter(cmd, "@TransactionName", DbType.String, CancelStatus);
                i = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));
            }
            catch
            { }

            return i;
        }

        public DataSet Fltlogds(string orderId, DataSet logds)
        {
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "USP_LOGTRACKER";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@PNRNO", DbType.String, "");
                DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@STATUS", DbType.String, "RETRIVE");
                logds = DBHelper.ExecuteDataSet(cmd);
            }
            catch
            { }

            return logds;
        }
        public DataTable PaxDetailsDt(string Paxids, DataTable Paxdt)
        {
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "GetPaxDetailsCancellation";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@PaxID", DbType.String, Paxids);
                Paxdt= DBHelper.ExecuteDataSet(cmd).Tables[0];
               
            }
            catch
            { }

            return Paxdt;
        }


        public int UpdateCancleStatus(string orderId, string paxid, string CancelStatus)
        {
            int i = 0;
            try
            {
                DbCommand cmd = new SqlCommand();
                cmd.CommandText = "Usp_update_OnlineCancellation";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, orderId);
                DBHelper.AddInParameter(cmd, "@paxid", DbType.String, paxid);
                DBHelper.AddInParameter(cmd, "@CancelStatus", DbType.String, CancelStatus);
                i = Convert.ToInt32(DBHelper.ExecuteNonQuery(cmd));
            }
            catch
            { }

            return i;
        }
        public List<GALWS.AirAsia.CurrencyRate> CurrancyExchangeRate(string CountryCode, List<GALWS.AirAsia.CurrencyRate> currancyinfo)
        {
            if (CountryCode != "IN")
            {
                SqlDatabase DBhelperfws = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                SqlCommand sqlcmd = new SqlCommand("SP_InsertCurrencyDetails");
                try
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("@CountryName", "");
                    sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
                    sqlcmd.Parameters.AddWithValue("@CurrancyCode", "");
                    sqlcmd.Parameters.AddWithValue("@ExchangeRate", 0);
                    sqlcmd.Parameters.AddWithValue("@UpdateBy", "");
                    sqlcmd.Parameters.AddWithValue("@Reqtype", "SelectCurrency");
                    sqlcmd.Parameters.AddWithValue("@IPAddress", "");
                    sqlcmd.Parameters.AddWithValue("@ID", 0);
                    var dbr = DBhelperfws.ExecuteReader(sqlcmd);
                    while (dbr.Read())
                    {
                        currancyinfo.Add(new GALWS.AirAsia.CurrencyRate
                        {
                            CountryName = dbr["CountryName"] != null ? dbr["CountryName"].ToString() : "",
                            CountryCode = dbr["CountryCode"] != null ? dbr["CountryCode"].ToString() : "",
                            CurrancyCode = dbr["CurrancyCode"] != null ? dbr["CurrancyCode"].ToString() : "",
                            ExchangeRate = dbr["ExchangeRate"] != null ? Convert.ToDecimal(dbr["ExchangeRate"]) : 0
                        });
                    }
                }
                catch (Exception ex)
                {

                }
                finally { sqlcmd.Dispose(); }
            }
            else
            {
                currancyinfo.Add(new GALWS.AirAsia.CurrencyRate
                {
                    CountryName = "India",
                    CountryCode = "IN",
                    CurrancyCode = "INR",
                    ExchangeRate = 1
                });
            }
            return currancyinfo;
        }
        public string HaveAnyPromoCodeAplicable(string Departure, string Arrival, string TravelPeriodFrom, string Supplier)
       {
            string Promocode="";
            // SqlDatabase DBhelperfws = new SqlDatabase(constr);Supplier
             SqlCommand sqlcmd = new SqlCommand("USP_InserUpdateAirAsiaPromoCode");
                try
                {			
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("@PromoCode", "");
                    sqlcmd.Parameters.AddWithValue("@Departure", Departure);
                    sqlcmd.Parameters.AddWithValue("@Arrival", Arrival);
                    sqlcmd.Parameters.AddWithValue("@TravelPeriodFrom", TravelPeriodFrom);
                   // sqlcmd.Parameters.AddWithValue("@TravelPeriodTo", TravelPeriodTo);
                    //sqlcmd.Parameters.AddWithValue("@BookingPeriodFrom", BookingPeriodFrom);
                   // sqlcmd.Parameters.AddWithValue("@BookingPeriodTo", BookingPeriodTo);
                    sqlcmd.Parameters.AddWithValue("@Supplier", Supplier);
                    sqlcmd.Parameters.AddWithValue("@RequestType", "SELECT");
                    Promocode = Convert.ToString(DBHelper.ExecuteScalar(sqlcmd));                 
                }
                catch (Exception ex)
                {

                }
                finally { sqlcmd.Dispose(); }
                return Promocode;
       }
    }
}
