﻿using STD.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using GALWS;
using OnlineCancellationSHARED;
namespace OnlineCancellationBAL
{
    
   public  class PNRCancellationBAL
    {
        string connStr = "";
        public PNRCancellationBAL(string connectionString)
        {
            connStr = connectionString;
        }

        public List<OnlineCancellationSHARED.SegmentDetail> GetCancellationSegmentDetails(string OrderID, string PaxID, string Action)
        {
            List<OnlineCancellationSHARED.SegmentDetail> obj = new List<OnlineCancellationSHARED.SegmentDetail>();
            int legCount = 0;
            int TotPax = 0;
            int TotPaxLive = 0;
            string Msg = "";
            try
            {
                OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
                obj = objDal.GetCancellationSegmentDetails(OrderID, PaxID, Action);
                string VC = obj.Select(x => x.VC).Distinct().First();
                string Provider = obj.Select(x => x.Provider).Distinct().First();
                TotPax = obj.Select(x => x.PaxId).Distinct().ToList().Count();
                //if (Provider == "1G")
                //{
                //    TotPax = obj.Select(x => x.PaxId).Distinct().ToList().Count();
                //    string GDSPNR = obj.Select(x => x.GDSPNR).Distinct().First();
                //    string Trip = obj.Select(x => x.Trip).Distinct().First();
                //    string CorpId = obj.Select(x => x.CorpId).Distinct().First();
                //    UAPITrans uAPITrans = new UAPITrans(connStr);
                //    Dictionary<string, string> xmlo = new Dictionary<string, string>();
                //    string BkgRes = uAPITrans.RetriveBooking(Provider, GDSPNR, false, VC, Trip, CorpId, ref xmlo);
                //    Msg = xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : ((string.IsNullOrEmpty(BkgRes) || BkgRes.Contains("faultcode")) ? "Unable to retrieve pnr from host." : "");
                //    //BkgRes = System.IO.File.ReadAllText(@"C:\Users\manish.gupta\Desktop\test.xml");
                //    XDocument xd = XDocument.Parse(BkgRes);
                //    System.Xml.Linq.XNamespace Pns = "http://www.travelport.com/schema/air_" + VersionUAPI.APIVersion + "";
                //    System.Xml.Linq.XNamespace universal = "http://www.travelport.com/schema/universal_" + VersionUAPI.APIVersion + "";
                //    System.Xml.Linq.XNamespace common = "http://www.travelport.com/schema/common_" + VersionUAPI.APIVersion + "";
                //    var AirSegment = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "AirSegment");
                //    var AirPricingInfo = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "AirPricingInfo");
                //    var TicketInfo = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "DocumentInfo").Descendants(Pns + "TicketInfo");
                //    var BookingTraveler = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(common + "BookingTraveler");
                //    TotPaxLive = BookingTraveler.Count();
                //    foreach (var airSegment in AirSegment)
                //    {
                //        string ArrCode = "";
                //        string DepCode = "";
                //        string ArrivalStation = airSegment.Attributes("Destination").Any() == true ? airSegment.Attributes("Destination").First().Value : "";
                //        string DepartureStation = airSegment.Attributes("Origin").Any() == true ? airSegment.Attributes("Origin").First().Value : "";
                //        string DeptDate = airSegment.Attributes("DepartureTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("DepartureTime").First().Value).ToString("dd MMM yyyy") : "";
                //        string DeptTime = airSegment.Attributes("DepartureTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("DepartureTime").First().Value).ToString("HH:mm") : "";
                //        string ArrDate = airSegment.Attributes("ArrivalTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("ArrivalTime").First().Value).ToString("dd MMM yyyy") : "";
                //        string ArrTime = airSegment.Attributes("ArrivalTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("ArrivalTime").First().Value).ToString("HH:mm") : "";
                //        string FltNo = airSegment.Attributes("FlightNumber").Any() == true ? airSegment.Attributes("FlightNumber").First().Value.Trim() : "";
                //        string Key = airSegment.Attributes("Key").Any() == true ? airSegment.Attributes("Key").First().Value.Trim() : "";

                //        for (int i = 0; i < obj.Count; i++)
                //        {
                //            if (ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && DepartureStation.Trim() == obj[i].DepAirCode.Trim() && obj[i].DeptDate == Convert.ToDateTime(DeptDate).ToString("ddMMyy") && obj[i].FltNo.Trim() == FltNo)
                //            {
                //                try
                //                {
                //                    obj[i].DeptDate = DeptDate;
                //                    obj[i].DeptTime = DeptTime;
                //                    obj[i].ArrDate = ArrDate;
                //                    obj[i].ArrTime = ArrTime;
                //                }
                //                catch (Exception ex) { }
                //                ArrCode = ArrivalStation;
                //                DepCode = DepartureStation;
                //                obj[i].Segment = DepCode + ":" + ArrCode;
                //                obj[i].SegmentStatus = "Live";
                //                obj[i].root = DepCode + ArrCode;
                //                obj[i].Key = Key;
                //                try
                //                {
                //                    if (obj[i].TicketNumber == "" && TicketInfo.Count() > 0)
                //                    {
                //                        var AirPricingInfo_Key = AirPricingInfo.Where(x => x.Descendants(Pns + "BookingInfo").Select(y => y.Attribute("SegmentRef").Value).Where(z => z.ToString() == Key).Any()).Attributes("Key").ToList(); // ? AirPricingInfo.Where(x => x.Descendants(Pns + "BookingInfo").Attributes("SegmentRef").First().Value == Key).Attributes("Key").ToList() : null;
                //                        if (AirPricingInfo_Key != null)
                //                        {
                //                            foreach (var APK in AirPricingInfo_Key)
                //                            {
                //                                try
                //                                {
                //                                    var TicketInfoalt = TicketInfo.Where(x => x.Attributes("AirPricingInfoRef").Any());
                //                                    obj[i].TicketNumber = TicketInfoalt.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName && x.Attributes("AirPricingInfoRef").First().Value == APK.Value).Any() ? TicketInfoalt.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName && x.Attributes("AirPricingInfoRef").First().Value == APK.Value).Attributes("Number").First().Value : "";
                //                                }
                //                                catch { }
                //                                if (obj[i].TicketNumber != "") break;
                //                            }
                //                        }

                //                        // obj[i].TicketNumber = TicketInfo.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName).Attributes("Number").First().Value;
                //                    }
                //                }
                //                catch (Exception ex) { }
                //            }
                //        }
                //    }
                //}
                //else
                //{
                #region 6E and SG V4
                //List<CredentialList> CrdList;

               
               //DAL.Credentials objCrd = new DAL.Credentials(connStr);
               //FlightCommonBAL objFleSBal = new FlightCommonBAL(connStr);
                    string Trip = obj.Select(x => x.Trip).Distinct().First();
                    string CorpId = obj.Select(x => x.CorpId).Distinct().First();
                //DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(OrderID);
                    OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(connStr);
                    DataSet FltHdrDs = objDA.GetHdrDetails(OrderID);

                //DataTable FltDT = ds.Tables[0];
                //string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);

                STD.DAL.Credentials objCrd = new STD.DAL.Credentials(connStr);
                List<STD.Shared.CredentialList> CrdList = objCrd.GetServiceCredentials("");
                CrdList = CrdList.Where(X => X.AirlineCode == VC && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == Trip).ToList();

                //CrdList = objCrd.GetGALBookingCredentials(Trip, CorpId, VC.ToUpper(), VC, idType, "LCC");
                    #endregion
                    if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                    {

                        if (VC == "6E")
                        {
                            TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                            navitaire.indigo.bm.ver4.GetBookingResponse BkgRes = STD.BAL._6ENAV420._6ENAV.RetriveBooking(OrderID, out Msg);
                            TotPaxLive = BkgRes.Booking.Passengers.Count();
                            foreach (var j in BkgRes.Booking.Journeys)
                            {
                                #region Get root
                                int TSeg = j.Segments.Count();
                                int n = 0;
                                string root = "";
                                foreach (var seg in j.Segments)
                                {
                                    if (n == 0)
                                    {
                                        root = seg.DepartureStation;
                                    }
                                    if (TSeg == (n + 1))
                                    {
                                        root += seg.ArrivalStation;
                                    }
                                    n = n + 1;
                                }
                                #endregion
                                #region SegmentUpdate
                                foreach (var seg in j.Segments)
                                {
                                    legCount += seg.Legs.Count();
                                    for (int i = 0; i < obj.Count; i++)
                                    {
                                        string ArrCode = "";
                                        string DepCode = "";
                                        foreach (var leg in seg.Legs)
                                        {
                                            if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                                            {
                                                try
                                                {
                                                    obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                                                    obj[i].DeptTime = leg.STD.ToString("HH:mm");
                                                    obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                                                    obj[i].ArrTime = leg.STA.ToString("HH:mm");
                                                }
                                                catch (Exception ex) { }
                                                ArrCode = seg.ArrivalStation;
                                                DepCode = seg.DepartureStation;
                                                obj[i].Segment = DepCode + ":" + ArrCode;
                                                obj[i].SegmentStatus = "Live";
                                                obj[i].root = root;
                                                break;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                        }
                        if (VC == "SG")
                        {
                            TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                            navitaire.SG.ver4.GetBookingResponse BkgRes = STD.BAL.SGNAV420.SGNAV4.RetriveBooking(OrderID, out Msg);
                            TotPaxLive = BkgRes.Booking.Passengers.Count();
                            foreach (var j in BkgRes.Booking.Journeys)
                            {
                                #region Get root
                                int TSeg = j.Segments.Count();
                                int n = 0;
                                string root = "";
                                foreach (var seg in j.Segments)
                                {
                                    if (n == 0)
                                    {
                                        root = seg.DepartureStation;
                                    }
                                    if (TSeg == (n + 1))
                                    {
                                        root += seg.ArrivalStation;
                                    }
                                    n = n + 1;
                                }
                                #endregion
                                #region SegmentUpdate
                                foreach (var seg in j.Segments)
                                {
                                    legCount += seg.Legs.Count();
                                    for (int i = 0; i < obj.Count; i++)
                                    {
                                        string ArrCode = "";
                                        string DepCode = "";
                                        foreach (var leg in seg.Legs)
                                        {
                                            if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                                            {
                                                try
                                                {
                                                    obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                                                    obj[i].DeptTime = leg.STD.ToString("HH:mm");
                                                    obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                                                    obj[i].ArrTime = leg.STA.ToString("HH:mm");
                                                }
                                                catch (Exception ex) { }
                                                ArrCode = seg.ArrivalStation;
                                                DepCode = seg.DepartureStation;
                                                obj[i].Segment = DepCode + ":" + ArrCode;
                                                obj[i].SegmentStatus = "Live";
                                                obj[i].root = root;
                                                break;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                        }
                    }

                    if (VC == "G8")
                    {
                        TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                        navitaire.bm.ver4.GetBookingResponse BkgRes = STD.BAL.G8NAV.G8NAV4.RetriveBooking(OrderID, out Msg);
                        TotPaxLive = BkgRes.Booking.Passengers.Count();
                        foreach (var j in BkgRes.Booking.Journeys)
                        {
                            #region Get root
                            int TSeg = j.Segments.Count();
                            int n = 0;
                            string root = "";
                            foreach (var seg in j.Segments)
                            {
                                if (n == 0)
                                {
                                    root = seg.DepartureStation;
                                }
                                if (TSeg == (n + 1))
                                {
                                    root += seg.ArrivalStation;
                                }
                                n = n + 1;
                            }
                            #endregion
                            #region SegmentUpdate
                            foreach (var seg in j.Segments)
                            {

                                legCount += seg.Legs.Count();
                                for (int i = 0; i < obj.Count; i++)
                                {
                                    string ArrCode = "";
                                    string DepCode = "";
                                    foreach (var leg in seg.Legs)
                                    {
                                        if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                                        {
                                            try
                                            {
                                                obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                                                obj[i].DeptTime = leg.STD.ToString("HH:mm");
                                                obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                                                obj[i].ArrTime = leg.STA.ToString("HH:mm");
                                            }
                                            catch (Exception ex) { }
                                            ArrCode = seg.ArrivalStation;
                                            DepCode = seg.DepartureStation;
                                            obj[i].root = root;
                                            obj[i].Segment = DepCode + ":" + ArrCode;
                                            obj[i].SegmentStatus = "Live";
                                            break;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                //}
                if ((obj.Count < legCount) || (TotPax != TotPaxLive && TotPax != 0))
                {
                    obj = new List<OnlineCancellationSHARED.SegmentDetail>();
                    obj.Add(new OnlineCancellationSHARED.SegmentDetail
                    {
                        Error = Msg != "" ? Msg : "Mismatch in Booking details.",
                    });
                }
            }
            catch (Exception ex)
            {
                obj = new List<OnlineCancellationSHARED.SegmentDetail>();
                obj.Add(new OnlineCancellationSHARED.SegmentDetail
                {
                    Error = Msg != "" ? Msg : "Mismatch in Booking details.",
                });
            }
            return obj;
        }
       
       public string CheckTicketStatusBySegment(string orderid, string paxid, string segment)
        {
            string lstPax = "";
            try
            {
                OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
                lstPax = objDal.CheckTicketStatusBySegment(orderid, paxid, segment);
            }
            catch (Exception ex)
            {
            }
            return lstPax;
        }

       public string CancelBooking(List<SegmentDetail> TotalPaxSegList, List<SegmentDetail> SelectedPaxSegList, string Orderid, ObjCancelPNR CanObj, string RefNo)
       {
           string res = "";
           string CanSegResult = "";
           string CanResult = "";
           try
           {
               string GDSPNR = "";
               string AIRLINEPNR = "";
               string Trip = "";
               string CorpId = "";
               bool IsUr = false;
               List<string> KeyList = null;
               string VC = SelectedPaxSegList.Select(x => x.VC).Distinct().FirstOrDefault();
               string Provider = SelectedPaxSegList.Select(x => x.Provider).Distinct().FirstOrDefault();

               #region 6E and SG V4


               //List<SHARED.Flight.CredentialList> CrdList;
               //DAL.Credentials objCrd = new DAL.Credentials(connStr);
               //FlightCommonBAL objFleSBal = new FlightCommonBAL(connStr);

               //DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(Orderid);
               //DataTable FltDT = ds.Tables[0];

               //string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
               //if (FltDT.Rows[0]["Status"].ToString() == "CP")
               //    CorpId = Convert.ToString(FltDT.Rows[0]["CorpId"]);
               //else
               //    CorpId = Convert.ToString(FltDT.Rows[0]["EntityId"]);
               //Trip = Convert.ToString(FltDT.Rows[0]["Trip"]);
               //CrdList = objCrd.GetGALBookingCredentials(Trip, CorpId, VC.ToUpper(), VC, idType, "LCC");
               OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(connStr);
               DataSet FltHdrDs = objDA.GetHdrDetails(Orderid);
               STD.DAL.Credentials objCrd = new STD.DAL.Credentials(connStr);
               List<STD.Shared.CredentialList> CrdList = objCrd.GetServiceCredentials("");
               CrdList = CrdList.Where(X => X.AirlineCode == VC && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["Trip"]).ToUpper().Trim()).ToList();

               #endregion


               CanObj.ExecID = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["AgentId"]).ToUpper().Trim();
               CanResult = AddCancelInfo(CanObj);
               AddCancelInfoCan(CanObj); //mk
                
               string[] resultC = CanResult.Split('_');
               if (resultC != null && resultC.Length == 2 && resultC[0] == "Success" && Convert.ToInt32(resultC[1]) == SelectedPaxSegList.Count)
               {
                   CanSegResult = AddCancelSegmentInfo(CanObj);
                   string[] resultS = CanSegResult.Split('_');
                   if (resultS != null && resultS.Length == 2 && resultS[0] == "Success" && Convert.ToInt32(resultS[1]) == SelectedPaxSegList.Count)
                   {
                       //if (Provider == "1G")
                       //{
                       //    GDSPNR = TotalPaxSegList.Select(x => x.GDSPNR).Distinct().First();
                       //    Trip = TotalPaxSegList.Select(x => x.Trip).Distinct().First();
                       //    CorpId = TotalPaxSegList.Select(x => x.CorpId).Distinct().First();
                       //    KeyList = SelectedPaxSegList.Select(x => x.Key).Distinct().ToList();

                       //    UAPITrans objUAPITrns = new UAPITrans(connStr);
                       //    res = objUAPITrns.UAPICancelBooking(Orderid, RefNo, GDSPNR, Trip, CorpId, VC, Provider, IsUr, KeyList);

                       //}

                       if (VC == "6E")
                       {
                           if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                           {
                               res =STD.BAL._6ENAV420._6ENAV.FinalCancellation(Orderid, RefNo);
                           }
                           //else
                           //{
                           //    res = STD.BAL.SpiceAPI.FinalCancellation(Orderid, RefNo);
                           //}
                           //try
                           //{
                           //    FlightCommonBAL IFLT = new FlightCommonBAL(connStr);
                           //    List<SHARED.Flight.FltHeader> FltHeaderList = new List<SHARED.Flight.FltHeader>();
                           //    FltHeaderList = IFLT.GetFltHeaderDetails(Orderid);
                           //    if (res.Contains("Success_"))
                           //    {
                           //        GDSPNR = TotalPaxSegList.Select(x => x.GDSPNR).Distinct().First();
                           //        AIRLINEPNR = TotalPaxSegList.Select(x => x.AIRLINEPNR).Distinct().First();
                           //        Trip = TotalPaxSegList.Select(x => x.Trip).Distinct().First();
                           //        CorpId = TotalPaxSegList.Select(x => x.CorpId).Distinct().First();
                           //        VC = "";
                           //        Provider = FltHeaderList[0].Provider_Original;
                           //        if (AIRLINEPNR != GDSPNR)
                           //        {
                           //            #region Passivedelete
                           //            SHARED.Flight.PassiveBookingDetails objPsvBkg = new SHARED.Flight.PassiveBookingDetails();
                           //            objPsvBkg.Trip = Trip;
                           //            objPsvBkg.TCCode = CorpId;
                           //            UAPITrans objUAPITrns = new UAPITrans(connStr);
                           //            try
                           //            {
                           //                objUAPITrns.UAPIPassiveDelete(objPsvBkg, GDSPNR, "Cancel", Orderid, RefNo);
                           //            }
                           //            catch (Exception ex) { ERROR.ExecptionLogger.FileHandling("UAPIPassiveDelete(UAPIPassiveDelete_6ESG)", "Error_009", ex, "UAPIPassiveDelete"); }
                           //            #endregion

                           //        }
                           //    }
                           //}
                           //catch (Exception ex) { ERROR.ExecptionLogger.FileHandling("FinalCancellation(FinalCancellation_6ESG)", "Error_009", ex, "FinalCancellation"); }
                       }
                       else if (VC == "SG")
                       {
                           if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                           {
                               res = STD.BAL.SGNAV420.SGNAV4.FinalCancellation(Orderid, RefNo);
                           }
                           //else
                           //{
                           //    res = BAL.SpiceAPI.FinalCancellation(Orderid, RefNo);
                           //}

                           //try
                           //{
                           //    FlightCommonBAL IFLT = new FlightCommonBAL(connStr);
                           //    List<SHARED.Flight.FltHeader> FltHeaderList = new List<SHARED.Flight.FltHeader>();
                           //    FltHeaderList = IFLT.GetFltHeaderDetails(Orderid);
                           //    if (res.Contains("Success_"))
                           //    {
                           //        GDSPNR = TotalPaxSegList.Select(x => x.GDSPNR).Distinct().First();
                           //        AIRLINEPNR = TotalPaxSegList.Select(x => x.AIRLINEPNR).Distinct().First();
                           //        Trip = TotalPaxSegList.Select(x => x.Trip).Distinct().First();
                           //        CorpId = TotalPaxSegList.Select(x => x.CorpId).Distinct().First();
                           //        VC = "";
                           //        Provider = FltHeaderList[0].Provider_Original;
                           //        if (AIRLINEPNR != GDSPNR)
                           //        {
                           //            #region Passivedelete
                           //            SHARED.Flight.PassiveBookingDetails objPsvBkg = new SHARED.Flight.PassiveBookingDetails();
                           //            objPsvBkg.Trip = Trip;
                           //            objPsvBkg.TCCode = CorpId;
                           //            UAPITrans objUAPITrns = new UAPITrans(connStr);
                           //            try
                           //            {
                           //                objUAPITrns.UAPIPassiveDelete(objPsvBkg, GDSPNR, "Cancel", Orderid, RefNo);
                           //            }
                           //            catch (Exception ex) { ERROR.ExecptionLogger.FileHandling("UAPIPassiveDelete(UAPIPassiveDelete_6ESG)", "Error_009", ex, "UAPIPassiveDelete"); }
                           //            #endregion

                           //        }

                           //    }
                           //}
                           //catch (Exception ex) { ERROR.ExecptionLogger.FileHandling("FinalCancellation(FinalCancellation_6ESG)", "Error_009", ex, "FinalCancellation"); }
                       }
                       else if (VC == "G8")
                       {
                           res = STD.BAL.G8NAV.G8NAV4.FinalCancellation(Orderid, RefNo);
                           //FlightCommonBAL IFLT = new FlightCommonBAL(connStr);
                           //List<SHARED.Flight.FltHeader> FltHeaderList = new List<SHARED.Flight.FltHeader>();
                           //FltHeaderList = IFLT.GetFltHeaderDetails(Orderid);
                           //if (res.Contains("Success_"))
                           //{
                           //    GDSPNR = TotalPaxSegList.Select(x => x.GDSPNR).Distinct().First();
                           //    AIRLINEPNR = TotalPaxSegList.Select(x => x.AIRLINEPNR).Distinct().First();
                           //    Trip = TotalPaxSegList.Select(x => x.Trip).Distinct().First();
                           //    CorpId = TotalPaxSegList.Select(x => x.CorpId).Distinct().First();
                           //    VC = "";
                           //    Provider = FltHeaderList[0].Provider_Original;
                           //    if (AIRLINEPNR != GDSPNR)
                           //    {
                           //        #region Passivedelete
                           //        SHARED.Flight.PassiveBookingDetails objPsvBkg = new SHARED.Flight.PassiveBookingDetails();
                           //        objPsvBkg.Trip = Trip;
                           //        objPsvBkg.TCCode = CorpId;
                           //        UAPITrans objUAPITrns = new UAPITrans(connStr);
                           //        try
                           //        {
                           //            objUAPITrns.UAPIPassiveDelete(objPsvBkg, GDSPNR, "Cancel", Orderid, RefNo);
                           //        }
                           //        catch (Exception ex) { ERROR.ExecptionLogger.FileHandling("UAPIPassiveDelete(UAPIPassiveDelete_G8)", "Error_009", ex, "UAPIPassiveDelete"); }
                           //        #endregion
                           //    }

                           //}
                       }
                   }
               }


           }
           catch (Exception ex)
           {
               res = ex.Message.ToString();
           }
           return res;
       }
       public string CheckCancelBookingAmount(List<SegmentDetail> TotalPaxSegList, List<SegmentDetail> SelectedPaxSegList, string Orderid, ObjCancelPNR CanObj, string RefNo)
       {
           string res = "";
           string CanSegResult = "";
           string CanResult = "";
           try
           {
               string GDSPNR = "";
               string AIRLINEPNR = "";
               string Trip = "";
               string CorpId = "";
               bool IsUr = false;
               List<string> KeyList = null;
               string VC = SelectedPaxSegList.Select(x => x.VC).Distinct().FirstOrDefault();
               string Provider = SelectedPaxSegList.Select(x => x.Provider).Distinct().FirstOrDefault();

               #region 6E and SG V4
              
               OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(connStr);
               DataSet FltHdrDs = objDA.GetHdrDetails(Orderid);
               STD.DAL.Credentials objCrd = new STD.DAL.Credentials(connStr);
               List<STD.Shared.CredentialList> CrdList = objCrd.GetServiceCredentials("");
               CrdList = CrdList.Where(X => X.AirlineCode == VC && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["Trip"]).ToUpper().Trim()).ToList();

               #endregion


               CanObj.ExecID = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["AgentId"]).ToUpper().Trim();
               CanResult = AddCancelInfo(CanObj);
               string[] resultC = CanResult.Split('_');
               if (resultC != null && resultC.Length == 2 && resultC[0] == "Success" && Convert.ToInt32(resultC[1]) == SelectedPaxSegList.Count)
               {
                   CanSegResult = AddCancelSegmentInfo(CanObj);
                   string[] resultS = CanSegResult.Split('_');
                   if (resultS != null && resultS.Length == 2 && resultS[0] == "Success" && Convert.ToInt32(resultS[1]) == SelectedPaxSegList.Count)
                   {

                       if (VC == "6E")
                       {
                           if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                           {
                               res = STD.BAL._6ENAV420._6ENAV.CheckFinalCancellationAmount(Orderid, RefNo);
                           }
                           
                       }
                       else if (VC == "SG")
                       {
                           if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                           {
                               res = STD.BAL.SGNAV420.SGNAV4.CheckFinalCancellationAmount(Orderid, RefNo);
                           }
                       }
                       else if (VC == "G8")
                       {
                           res = STD.BAL.G8NAV.G8NAV4.CheckFinalCancellationAmount(Orderid, RefNo);
                          
                       }
                   }
               }


           }
           catch (Exception ex)
           {
               res = ex.Message.ToString();
           }
           return res;
       }

       public string AddCancelInfo(ObjCancelPNR obj)
       {
           string objList = "Failure_0";
           try
           {
               OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
               objList = objDal.AddCancelInfo(obj);
           }
           catch (Exception ex)
           {
           }
           return objList;
       }
        public string AddCancelInfoCan(ObjCancelPNR obj)
        {
            string objList = "Failure_0";
            try
            {
                OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
                objList = objDal.AddCancelInfoCan(obj);
            }
            catch (Exception ex)
            {
            }
            return objList;
        }

        
       public string AddCancelSegmentInfo(ObjCancelPNR obj)
       {
           string objList = "Failure_0";
           try
           {
               OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
               objList = objDal.AddCancelSegmentInfo(obj);
           }
           catch (Exception ex)
           {
           }
           return objList;
       }
       public string AddToLimit(string UserId, float Amount)
       {
           string result = "";
           try
           {
               OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
               result = objDal.AddToLimit(UserId, Amount);
           }
           catch (Exception ex)
           {
           }
           return result;
       }
        public string UpdateCrdLimit(string UserId, float Amount)
        {
            string result = "";
            try
            {
                OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
                result = objDal.UpdateCrdLimit(UserId, Amount);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public string UpdateRefundToLedger(CanRefund canRefnd)
       {
           string result = "";
           try
           {
               OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
               result = objDal.UpdateRefundToLedger(canRefnd);
           }
           catch (Exception ex)
           {
           }
           return result;
       }
       public string GetCancellationCharge(CancellationCharge canCharge)
       {
           string result = "";
           try
           {
               OnlineCancellationDAL.PNRCancellationDAL objDal = new OnlineCancellationDAL.PNRCancellationDAL(connStr.Trim());
               result = objDal.GetCancellationCharge(canCharge);
           }
           catch (Exception ex)
           {
           }
           return result;
       }
    }

}

   
