﻿using ITZERRORLOG;
using STD.BAL;
using STD.DAL;
using STD.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GALWS.ITQAirApi
{
   public class TQSeatBooking
    {
        public static List<SeatMapFinal> SeatBooking(string orderId)
        {
            List<SeatMapFinal> SeatMapFinal_ALL = new List<SeatMapFinal>();

            string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            //List<CredentialList> CrdList;
            FlightCommonBAL ObjcBAL = new FlightCommonBAL(ConStr);
            DataSet dsCrd = ObjcBAL.GetSearchCredentilas("TQ");
            DataTable FltDT;
            DataTable PaxDT;
            DataTable MealBagDT;
            FlightCommonBAL objFleSBal = new FlightCommonBAL(ConStr);            
            Credentials objCrd = new Credentials(ConStr);           
            Dictionary<string, string> Xml = new Dictionary<string, string>();            
            try
            {

                DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(orderId);
                FltDT = ds.Tables[0];
                PaxDT = ds.Tables[1];
                MealBagDT = ds.Tables.Count > 2 ? ds.Tables[2] : new DataTable();
                string vc = Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]);
                string Provider = Convert.ToString(FltDT.Rows[0]["Provider"]);
                decimal OriginalTF = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"].ToString());
                decimal InfFare = Convert.ToDecimal(FltDT.Rows[0]["InfFare"].ToString());
                string strTrip = Convert.ToString(FltDT.Rows[0]["Trip"]);
                string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
                string crdType = Convert.ToString(FltDT.Rows[0]["crdType"]);
                //CrdList = objCrd.GetGALBookingCredentialsSeat(Provider.ToUpper(), strTrip, vc, crdType);
                string[] SnoSplit = new string[] { "Ref!" };
                string[] SnoSplitDetails = FltDT.Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);
                string url =   dsCrd.Tables[0].Rows[0]["ServerUrlOrIP"].ToString().Replace("Pricing", "SeatMap");
                SeatMapFinal_ALL = ITQairApiSeatBooking(url, FltDT.Rows[0]["Searchvalue"].ToString(), FltDT.Rows[0]["Track_id"].ToString(), FltDT.Rows[0]["User_id"].ToString(), SnoSplitDetails, FltDT.Rows[0]["OrgDestFrom"].ToString(), FltDT.Rows[0]["OrgDestTo"].ToString());
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ITQSeatBooking", "Error_008", ex, "Seat");
                
            }
            return SeatMapFinal_ALL;

        }
        public static List<SeatMapFinal> ITQairApiSeatBooking(string URL, string Searchvalue, string Track_id, string User_id, string[] SnoSplitDetails ,string Destination,string Origin)
        {
            List<SeatMapFinal> LIST = new List<SeatMapFinal>();


            try
           {
                string SeatRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + Searchvalue + "\",\"UserID\": \"" + User_id + "\",\"Destination\": \"" + Destination + "\",\"Origin\": \"" + Origin + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
                ITQAirApiUtility.SaveFile(SeatRequest, "SeatmapBooking_" + Track_id, User_id);
                string BookingResponse = ITQAirApiUtility.PostJsionXML(URL, "application/xml; charset=utf-8", SeatRequest);
                ITQAirApiUtility.SaveFile(BookingResponse, "seatmapResponseBooking_" + Track_id, User_id);
                if (BookingResponse.Contains("<SeatMapFinal>"))
                {
                    XDocument ItqDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                   var ddsg=  ItqDocument.Element("SeatMapLayoutResponse").Descendants("deckData").Descendants("SeatMapAll").Descendants("SeatMapFinal").ToList();
                 
                    XmlSerializer serializer = new XmlSerializer(typeof(SeatMapFinal));
                    foreach(var M in ddsg)
                    {
                        using (TextReader reader = new StringReader(M.ToString()))
                        {
                            SeatMapFinal result = (SeatMapFinal)serializer.Deserialize(reader);
                            LIST.Add(result);
                        }
                    }
                    
                    // LIST = DeserializeFromXmlString(ddsg[0].ToString());

                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("ITQairApiBooking", "Error_001", ex, "ITQAirAPI");
            }
            return LIST;
        }
      

    }
}
