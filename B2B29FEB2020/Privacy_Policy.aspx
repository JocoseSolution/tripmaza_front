﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="true" CodeFile="Privacy_Policy.aspx.cs" Inherits="Privacy_Policy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">

            <div class="theme-hero-area-bg" style="background-image: url(https://www.bluebanyan.co.in/img/common/privacy-policy-cover.jpg);"></div>
            <%--<div class="theme-hero-area-mask theme-hero-area-mask-half"></div>--%>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Pricvacy Policy</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">

            <p style="text-align: justify">
                The Privacy Policy is applicable to the websites of Flyagainonline. This privacy statement also does not apply to the websites of our business partners, corporate affiliates or to any other third parties, even if their websites are linked to Flyagainonline.net Website. We recommend you review the privacy statements of the other parties with whom you interact. For the purposes of this privacy policy, "corporate affiliates" means any entity or joint venture that is wholly or partially owned or controlled by Flyagainonline. "Business partners" means any subcontractor, vendor or other entity with whom we have an ongoing business relationship to provide products, services, or information.
The following terms governs the collection, use and protection of your Personal Information by the Website. This Privacy Policy shall be applicable to users or customers who purchases any product or services from the Website or visits the Website. By visiting and/ or using the Website you have agreed to the following Privacy Policy.
            </p>


            <h3>Introduction:
            </h3>
            <p style="text-align: justify">
                As a registered member of the Website and/or as a visitor (if applicable and as the case may be), you will be entitled to savings and benefits on hotel reservations. In addition, look forward to receiving monthly newsletters and exclusive promotions offering special deals. That's why we've provided this Privacy Policy, which sets forth our policies regarding the collection, use and protection of the Personal Information of those using or visiting the Website.
            </p>

            <h3>Personal Information:
            </h3>
            <p style="text-align: justify">
                Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, e - mail address, credit card number, cardholder name, expiration date, information about the travel, bookings, co-passengers, and any and all details that may be requested while any customers visits or uses the website. When you browse the Website, Flyagainonline.net may collect information regarding the domain and host from which you access the Internet, the Internet Protocol address of the computer or Internet Service Provider you are using, and anonymous site statistical data. The Website uses cookie and tracking technology depending on the features offered. Personal Information will not be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties. We encourage you to review our Privacy Policy, and become familiar with it, but you should know that we do not sell or rent our customers' Personal Information to third parties. Please note that we review our Privacy Policy from time to time, and we may make periodic changes to the policy in connection with that review. Therefore, you may wish to bookmark this page and/or periodically review this page to make sure you have the latest version. Regardless of later updates, we will abide by the privacy practices described to you in this Privacy Policy at the time you provided us with your Personal Information. When browsing our Website, you are not required to provide any Personal Information unless and until you choose to make a purchase or sign up for one of our e-mail newsletters or other services as described below.
            </p>

            <h3>Making a Purchase:
            </h3>
            <p style="text-align: justify">
                In order to purchase travel and/or related services through our Website, you must provide us with certain Personal Information such as your name, credit card number and expiration date, credit card billing address, telephone number, e - mail address and the name or names of the person(s) traveling (if not you). We require this information to process, fulfil and confirm your reservations and transactions and keep you informed of each transaction's status. If you are making a reservation for one or more travellers other than yourself, you must confirm and represent that each of these other travellers have agreed, in advance, that you may disclose their Personal Information to us.
            </p>

            <h3>Member Registration: </h3>
            <p style="text-align: justify">
                If you choose to become a registered member of Website, you must provide your name, address, telephone number, e-mail address, a unique login name, password, and password validation, and a password hint to help you remember your password. This information is collected on the registration form for several reasons including but not limited to:
            </p>

            <ul>
                <li>Personal identification;</li>
                <li>To complete hotel and other reservations;</li>
                <li>To allow us to contact you for customer service purposes, if necessary ;
                </li>
                <li>To customize the content of our site to strive to meet your specific needs;
                </li>
                <li>To make product or other improvements to our site. In addition, we need your e - mail address to confirm your new member registration and each reservation you transact on our Website. As a Website member you will also occasionally receive updates from us about fare sales in your area, special offers, new Flyagainonline.net services, and other noteworthy items. However, you may choose at any time to no longer receive these types of e - mail messages. Please see our Opt - Out Policy described below for details;
                </li>

            </ul>

            <h3>Member Profile:
            </h3>
            <p style="text-align: justify">
                As a Website member, you can choose to complete your online profile by providing us with travel preferences, credit card billing information and other Personal Information. This information is primarily used to assist you in making reservations quickly without having to type in the same information repeatedly. For example, you can store your Hotel Rewards Programme numbers so that when your make a reservation at that Hotel, we may automatically pre - fill the Rewards Programme number input box for you.
            </p>

            <h3>Online Surveys:</h3>
            <p style="text-align: justify">
                Flyagainonline.net values opinions and comments from members, so we frequently conduct online surveys. Participation in these surveys is entirely optional. Typically, the information is aggregated, and used to make improvements to the Website and its services and to develop appealing content, features and promotions for Website members. Survey participants are anonymous unless otherwise stated in the survey.
            </p>

            <h3>Automatic Logging of Session Data:</h3>
            <p style="text-align: justify">
                We automatically log generic information about your computer's connection to the Internet, which we call "session data", that is anonymous and not linked to any Personal Information. Session data consists of things such as IP address, operating system and type of browser software being used and the activities conducted by the user while on the Website. An IP address is a number that lets computers attached to the internet, such as our web servers, know where to send data back to the user, such as the pages of the Website the user wishes to view. We collect session data because it helps us analyse such things as what items visitors are likely to click on most, the way visitors are clicking through the Website, how many visitors are surfing to various pages on the Website, how long visitors to the Website are staying and how often they are visiting. It also helps us diagnose problems with our servers and lets us better administer our systems. Although such information does not identify any visitor personally, it is possible to determine from an IP address a visitor's Internet Service Provider (ISP), and the approximate geographic location of his or her point of connectivity.
            </p>


            <h3>Others:</h3>
            <p style="text-align: justify">
                From time to time we may add or enhance services available on the Website. To the extent these services are provided, and used by you, we will use the information you provide to facilitate the service requested. For example, if you email us with a question, we will use your email address, name, nature of the question, etc. to respond to your question. We may also store such information to assist us in making the Website the better and easier to use. With whom (if anyone) your Personal Information is shared? When you reserve or purchase travel services through the Website, we must provide certain of your Personal Information to the hotel, car - rental agency, travel agency or other involved third party to enable the successful fulfilment of your travel arrangements. However, we do not sell or rent individual customer names or other Personal Information to third parties. We use non - personally identifiable information in aggregate form to build higher quality, more useful online services by performing statistical analysis of the collective characteristics and behaviour of our customers and visitors, and by measuring demographics and interests regarding specific areas of the Website. We may provide anonymous statistical information based on this data to suppliers, advertisers, affiliates and other current and potential Business partners. We may also use such aggregate data to inform these third parties as to the number of people who have seen and clicked on links to their websites. Occasionally, Flyagainonline.net will hire a third party to act on our behalf for projects such as market - research surveys and contest - entry processing and will provide information to these third parties specifically for use in connection with these projects. The information we provide to such third parties is protected by a confidentiality agreement and is to be used solely for completing the specific project. How you can opt-out of receiving our promotional e-mails? At any time, you may unsubscribe from our newsletter or similar services through the "My Account" link on the Website home page. As a member or promotion/sweepstakes entrant, you will occasionally receive e-mail updates from us about fare sales in your area, special offers, new Flyagainonline.net services, and other noteworthy items. We hope you will find these updates interesting and informative. Of course, if you would rather not receive them, please click on the "unsubscribe" link or follow the instructions in each e - mail message or send us an email admin@flyagainonline.net. Flyagainonline reserves the right to limit membership to those who will accept e - mails. Members will be notified via e - mail prior to any actions taken. What safeguards we have in place to protect your Personal Information? All payments on the Website is VeriSign SSL secured This means all Personal Information you provide on the Website is transmitted using SSL (Secure Socket Layer) encryption. SSL is a proven coding system that lets your browser automatically encrypt, or scramble, data before you send it to us. The same process happens when you make travel purchases on the Website. What other information should I know about my privacy? Website contains links to other websites. Please note that when you click on one of these links, you are entering another website for which Flyagainonline has no responsibility. We encourage you to read the privacy statements of all such sites as their policies may be materially different from this Privacy Policy. Of course, you are solely responsible for maintaining the secrecy of your passwords, and your Website membership account information. Please be very careful, responsible, and alert with this information, especially whenever you're online. In addition to the circumstances described above, Flyagainonline may disclose the Website member information if required to do so by law, court order, as requested by other government or law enforcement authority, or in the good faith belief that disclosure is otherwise necessary or advisable including, without limitation, to protect the rights or properties of Flyagainonline or any or all of its affiliates, associates, employees, directors or officers or when we have reason to believe that disclosing the information is necessary to identify, contact or bring legal action against someone who may be causing interference with our rights or properties, whether intentionally or otherwise, or when anyone else could be harmed by such activities. In addition, if Flyagainonline.net or substantially all of its assets are acquired, our customer information will most likely also be transferred in connection with such acquisition this policy is effective as of. Any material changes in the way Faretripbox.com uses Personal Information will be described in future versions of this Privacy Statement. Of course, you may always submit concerns regarding our Privacy Statement or our privacy practices via email to admin@flyagainonline.net Please reference the privacy policy in your subject line. Faretripbox.com will attempt to respond to all reasonable concerns or inquiries within five business days of receipt. Thank you for using the Website!
            </p>

         


        </div>
    </div>
</asp:Content>

