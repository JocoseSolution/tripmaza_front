﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using SWIFTMoneyBillPayments;

public partial class BillPayment : System.Web.UI.Page
{
    private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();
            Session["Validate"] = UserId;
        }
        else
        {
            Response.Redirect("/");
        }
    }

    #region [Web Method Calling Section]
    [WebMethod]
    public static string BindOpratorByType(string servicetype, string selectname)
    {
        string result = string.Empty;

        try
        {
            result = SMBPApiService.GetMobileOprator(servicetype, selectname);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> BindInputByLabelDel(string nametype, string servicetype, string spkey, string fetchid, string isbillfetch)
    {
        List<string> resultStr = new List<string>();

        try
        {
            StringBuilder result = new StringBuilder();
            StringBuilder labels = new StringBuilder();

            labels.Append("<div class='col-md-12'><p class='text-danger'>Notes:</p></div>");

            DataTable dtLabel = SMBPApiService.GetInputByLabelDel(fetchid);

            if (isbillfetch.ToLower().Trim() == "false" && nametype != "mobile")
            {
                DataRow row = dtLabel.NewRow();
                row["BillUpdation"] = "";
                row["Index"] = "1";
                row["Labels"] = "Amount";
                row["FieldMinLen"] = "1";
                row["FieldMaxLen"] = "10";
                dtLabel.Rows.Add(row);
            }

            List<string> indexcount = new List<string>();

            string BillUpdation = string.Empty;
            string Index = string.Empty;
            string Labels = string.Empty;
            string FieldMinLen = string.Empty;
            string FieldMaxLen = string.Empty;

            labels.Append("<div class='col-md-12'><p style='color: #a5a4a4;'>");
            for (int i = 0; i < dtLabel.Rows.Count; i++)
            {
                if (!indexcount.Contains(dtLabel.Rows[i]["Index"].ToString()))
                {
                    BillUpdation = dtLabel.Rows[i]["BillUpdation"].ToString();
                    Index = dtLabel.Rows[i]["Index"].ToString();
                    Labels = dtLabel.Rows[i]["Labels"].ToString().Replace(":", "").Trim();
                    FieldMinLen = dtLabel.Rows[i]["FieldMinLen"].ToString();
                    FieldMaxLen = dtLabel.Rows[i]["FieldMaxLen"].ToString();

                    string labelname = Regex.Replace(Labels, @"\s", "");
                    string strinputname = servicetype + labelname;
                    string dateclass = Labels.ToLower().Contains("date") == true ? "commonDate" : "";
                    string dateplaceholder = Labels.ToLower().Contains("date") == true ? Labels + " (dd/mm/yyyy) " : Labels;

                    string putmaxlen = !string.IsNullOrEmpty(FieldMaxLen) && FieldMaxLen != "0" ? "maxlength='" + FieldMaxLen + "'" : string.Empty; ;

                    result.Append("<div class='col-md-2 " + nametype + "dynamicinput' data-" + nametype + "id='txt" + strinputname + "' data-labels='" + labelname + "'><div class='theme-payment-page-form-item form-group form-validation'><input type='text' class='form-control " + dateclass + "' id='txt" + strinputname + "' minlength='" + FieldMinLen + "' " + putmaxlen + " placeholder='" + dateplaceholder + "'/></div></div>");

                    labels.Append(GetLabelMinMaxNotes(Labels, FieldMinLen, FieldMaxLen) + "<br/>");

                    indexcount.Add(dtLabel.Rows[i]["Index"].ToString());
                }
            }

            labels.Append("</p></div>");

            labels.Append("<div class='col-md-12'><p style='color: #a5a4a4;'>" + GetLabelUpdationNotes(BillUpdation, string.Empty) + "</p></div>");

            resultStr.Add(result.ToString());
            resultStr.Add(labels.ToString());
        }
        catch (Exception)
        {

            throw;
        }

        return resultStr;
    }

    private static string GetLabelMinMaxNotes(string labelname, string minlen, string maxlen)
    {
        return "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Please enter your " + labelname + " from " + minlen + " to " + maxlen + " digit.";
    }

    private static string GetLabelUpdationNotes(string billupdation, string servicetype)
    {
        var result = "";

        if (billupdation == "T+0")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take one working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+1")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take two working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+2")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take three working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+3")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take four working days to consider bill paid in their accounts.";
        }
        return result;
    }

    #region [Mobile Section] 
    [WebMethod]
    public static List<string> MobileRecharge(string mobile, string opratorname, string oprator, string circle, string amount, string isbillfetch, string rchtype, string fetchid)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (rchtype.ToLower().Trim() == "prepaid")
                    {
                        string heading = string.Empty;
                        if (circle != "0")
                        {
                            heading = "prepaid Mobile Payment";
                        }
                        else
                        {
                            heading = "Postpaid Bill Payment";
                        }

                        result = OperatorPaymentEvent(heading, mobile, opratorname, oprator, circle, amount, null, fetchid);
                    }
                    else
                    {
                        string billFetch = BillFetch(UserId, mobile, oprator, circle);

                        result = BillFetchMessage("Postpaid Mobile Bill Detail", billFetch, "PostPaidMobile");
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BillFetch(string agentId, string number, string spKey, string circleID, List<string> optional = null)
    {
        HttpContext.Current.Session["CustomerName"] = null;
        return SMBPApiService.BBPSBillFetch(agentId, number, spKey, circleID, optional);
    }
    #endregion

    #region [DTH Section]
    [WebMethod]
    public static List<string> DTHPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("DTH Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Electricity Section]
    [WebMethod]
    public static List<string> ElectricityBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Electricity Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> ElectricityPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Electricity Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Landline Section]
    [WebMethod]
    public static List<string> LandlineBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Landline Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> LandlinePayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Landline Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Insurance Section]
    [WebMethod]
    public static List<string> InsuranceBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Insurance Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> InsurancePayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Insurance Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Gas Section]
    [WebMethod]
    public static List<string> GasBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Gas Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> GasPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Gas Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Broadband Section]
    [WebMethod]
    public static List<string> InternetBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Broadband Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> BroadbandPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Broadband Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Water Section]
    [WebMethod]
    public static List<string> WaterBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Water Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> WaterPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Water Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }

    #endregion

    private static List<string> BillFetchMessage(string heading, string fetchRespo, string actiontype)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(fetchRespo))
        {
            dynamic rech = JObject.Parse(fetchRespo);

            string responseCode = rech.ResponseCode;
            string responseMessage = rech.ResponseMessage;
            string dueamount = rech.dueamount;
            string duedate = rech.duedate != null ? rech.duedate : "NA";
            string customername = rech.customername != "NA" ? rech.customername : "- - -";
            string billnumber = rech.billnumber != "NA" ? rech.billnumber : "- - -";
            string billdate = rech.billdate != "NA" ? rech.billdate : "- - -";

            string acceptPartPay = rech.acceptPartPay;
            string BBPSCharges = rech.BBPSCharges;
            string BillUpdate = rech.BillUpdate;
            string RequestID = rech.RequestID;
            string ClientRefId = rech.ClientRefId;

            if (duedate.ToLower() != "na")
            {
                DateTime currDate = new DateTime();
                currDate = Convert.ToDateTime(duedate);
                duedate = currDate.ToString("dd MMM yyyy");
            }
            else
            {
                duedate = "- - -";
            }

            string finalamt = (Convert.ToDecimal(!string.IsNullOrEmpty(dueamount) ? dueamount : "0") + Convert.ToDecimal(!string.IsNullOrEmpty(BBPSCharges) ? BBPSCharges : "0")).ToString();

            result.Add(responseCode);
            if (responseCode == "000")
            {
                HttpContext.Current.Session["CustomerName"] = customername;

                string respo = "<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;" + heading + "</h5>"
                     + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                     + "<div class='modal-body'><div class='row'>"
                     + "<div class='col-sm-5'><span style='font-size: 15px;'>Request ID</span><p>" + RequestID + "</p></div>"
                     + "<div class='col-sm-7 text-right'><span>Customer Name</span><p>" + customername + "</p></div>"
                     + "</div><hr />"
                     + "<div class='row'>"
                     + "<div class='col-sm-6'><span style='font-size: 15px;'>Due Amount : ₹ " + dueamount + "</span><br /><span style='color: #8d8d8d;'>Charges : ₹ " + (!string.IsNullOrEmpty(BBPSCharges) ? BBPSCharges : "0") + "</span></div>"
                     + "<div class='col-sm-6 text-right'><s" +
                     "pan>Payment Due Date</span><p>" + duedate + "</p></div>"
                     + "</div><hr />"
                     + "<div class='row'>"
                     + "<div class='col-sm-4'><span style='font-size: 15px;'>Bill Number</span><p>" + billnumber + "</p></div>"
                     + "<div class='col-sm-8 text-right'><span>Billing Date</span><p>" + billdate + "</p></div>"
                     + "</div></div>";

                if (finalamt != "0")
                {
                    respo += "<div class='modal-footer'><button type='button' id='btn" + actiontype + "Payment' class='btn btn-success' onclick='" + actiontype + "PaymentSubmit();'>Bill Pay</button></div>"
                        + "<input type='hidden' id='hdnTotal" + actiontype + "PaidAmt' value='" + finalamt + "'/>";
                }
                result.Add(respo);
            }
            else
            {
                result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "</h5>"
                                              + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                              + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                                              + "<h5 class='text-danger'>" + responseMessage + "</h4>"
                                              + "</div></div>");
            }
        }
        else
        {
            result.Add("notavl");
            result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;Failed</h5>"
                              + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                              + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                              + "<h5>Error occurred, Please try again after sometime.</h4>"
                              + "</div></div>");
        }

        return result;
    }

    private static List<string> OperatorPaymentEvent(string heading, string number, string spkeytext, string spKey, string circleID, string amount, List<string> optionsvalue = null, string fetchId = null)
    {
        List<string> result = new List<string>();

        try
        {
            List<string> ledResult = Ledgerandcreditlimit_Transaction(amount, number, spkeytext, "Debit", "DR", "DEBIT NOTE");
            if (ledResult != null && ledResult.Count > 0)
            {
                if (ledResult[0] == "000")
                {
                    string clientRefId = ledResult[1];

                    string paymentRespo = DoOpratorPayment(clientRefId, spKey, circleID, number, amount, optionsvalue, fetchId);
                    //string paymentRespo = "{\"ResponseCode\":\"000\",\"ResponseMessage\":\"Transaction Successful\",\"TransactionId\":\"100062347\",\"AvailableBalance\":\"10487.54\",\"ClientRefId\":\"B2BAPI7F22A7EC60\",\"OperatorTransactionId\":\"150224477514A12C55D5\"}";

                    if (!string.IsNullOrEmpty(paymentRespo))
                    {
                        dynamic rech = JObject.Parse(paymentRespo);
                        string responseCode = rech.ResponseCode;
                        if (responseCode != "000" && responseCode != "999")
                        {
                            List<string> rufLedResult = Ledgerandcreditlimit_Transaction(amount, number, spkeytext, "Credit", "CR", "CREDIT NOTE");
                        }
                    }

                    result = DoOpratorPaymentMessage(heading, paymentRespo, amount, clientRefId, spkeytext);
                }
                else
                {
                    result = ledResult;
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string DoOpratorPayment(string clientRefId, string spKey, string circleID, string number, string amount, List<string> optionsvalue, string fetchId = null)
    {
        string result = string.Empty;

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            UserId = HttpContext.Current.Session["Validate"].ToString();
            string custName = HttpContext.Current.Session["CustomerName"] != null ? HttpContext.Current.Session["CustomerName"].ToString() : "- - -";

            result = SMBPApiService.BBPSPayment(UserId, clientRefId, number, spKey, circleID, amount, optionsvalue, custName, fetchId);
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    private static List<string> DoOpratorPaymentMessage(string heading, string reqstr, string amount, string clientRefId, string spkeytext)
    {
        List<string> result = new List<string>();

        if (reqstr != null && !string.IsNullOrEmpty(reqstr))
        {
            dynamic rech = JObject.Parse(reqstr);

            string responseCode = rech.ResponseCode;
            string responseMessage = rech.ResponseMessage;
            string transactionId = rech.TransactionId;
            string availableBalance = rech.AvailableBalance;
            string resClientRefId = rech.ClientRefId;
            string operatorTransactionId = rech.OperatorTransactionId;

            DateTime currDate = DateTime.Now;
            string strCurrDate = currDate.ToString("dd MMM yyyy, h:mm tt");

            result.Add(responseCode);

            if (responseCode == "000")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-success' id='rechheading'><i class='fa fa-check-circle'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else if (responseCode == "999")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-warning' id='rechheading'><i class='fa fa-clock'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
        }

        return result;
    }    

    private static List<string> GetBillFeatch(string modelheading, string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string number = string.Empty;
                    List<string> optional = new List<string>();

                    if (optionsvalue != null)
                    {
                        int counting = 1;
                        foreach (var item in optionsvalue)
                        {
                            if (counting == 1)
                            {
                                number = item;
                            }
                            else
                            {
                                optional.Add(item);
                                counting = counting + 1;
                            }
                        }
                    }

                    string billFetch = BillFetch(UserId, number, spkey, "0", optional);
                    //string billFetch = "{\"ResponseCode\":\"000\",\"ResponseMessage\":\"Transaction Successful\",\"dueamount\":420.0,\"duedate\":\"2020-08-08\",\"customername\":\"KRISHAN\",\"billnumber\":\"152686866\",\"billdate\":\"01 Jan 0001\",\"acceptPartPay\":\"N\",\"BBPSCharges\":\"\",\"BillUpdate\":\"T+1\",\"RequestID\":\"3320190805276240\",\"ClientRefId\":\"D2D3EF958EB04FE\"}";

                    result = BillFetchMessage(modelheading, billFetch, actiontype);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetBillTransactionHistory(string fromdate, string todate, string clintrefid, string transtype, string status, string defaultdate)
    {
        List<string> result = new List<string>();
        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            UserId = HttpContext.Current.Session["Validate"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (defaultdate == "today")
                {
                    DateTime currDate = DateTime.Now;
                    fromdate = currDate.ToString("dd/MM/yyyy");
                }

                DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, status);

                result.Add("success");
                result.Add(BindBillTransDetails(dtTrans));
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    private static string BindBillTransDetails(DataTable dtTrans)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string refund_html = "- - -";
                string status = dtTrans.Rows[i]["Status"].ToString();
                string alterstatus = status;

                bool isRefund = dtTrans.Rows[i]["Refund"].ToString().ToLower() == "true" ? true : false;

                if (status.ToLower().Trim() == "transaction under process" || status.ToLower().Trim().Contains("processed"))
                {
                    status = "<td class='text-warning'>UNDER PROCESS</td>";
                    alterstatus = "UNDER PROCESS";
                }
                else if (status.ToLower().Trim() == "transaction successful")
                {
                    status = "<td class='text-success'>SUCCESS</td>";
                    alterstatus = "SUCCESS";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>DUPLICATE</td>";
                    alterstatus = "DUPLICATE";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-paymentid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundBillFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else if (status.ToLower().Trim() == "transaction failed" || status.ToLower().Trim() == "failed from simulator" || status.ToLower().Trim().Contains("invalid"))
                {
                    status = "<td class='text-danger'>FAILED</td>";
                    alterstatus = "FAILED";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundBillFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else
                {
                    status = "<td>--</td>";
                    alterstatus = "FAILED";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REFUND</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "'  data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TransactionId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ClientRefId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["Number"].ToString() + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["AgentId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ServiceType"].ToString() + "</td>");
                sbTrans.Append(status);
                if (alterstatus.ToLower() == "under process")
                {
                    sbTrans.Append("<td class='text-center'><span class='btn btn-sm checkstatus' style='padding: 2px;color:#ff414d;' id='btnCallBackCheckStatus_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-transid='" + dtTrans.Rows[i]["TransactionId"].ToString() + "' data-status='" + alterstatus + "' data-refid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' onclick='CallBackCheckStatus(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ")'>Check Status</span></td>");
                }
                else
                {
                    sbTrans.Append("<td class='text-center'>- - -</td>");
                }
                sbTrans.Append("<td class='text-center'>" + refund_html + "</td>");
                string url = "/utilities/printbilltrans.aspx?transid=" + dtTrans.Rows[i]["TransactionId"].ToString() + "&agentid=" + dtTrans.Rows[i]["AgentId"].ToString() + "&trackid=" + dtTrans.Rows[i]["ClientRefId"].ToString();

                sbTrans.Append("<td class='text-center'><a style='color:#ff8d3c;font-weight: bold; ' href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            sbTrans.Append("<tr>");
            sbTrans.Append("<td colspan='11' class='text-danger text-center'>Record not found !</td>");
            sbTrans.Append("</tr>");
        }

        return sbTrans.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static List<string> ProcessToCallBackCheckStatus(string transid, string refid, string status, string billid)
    {
        List<string> result = new List<string>();

        try
        {
            string callBack = BBPSPaymentEnquiry(transid, refid, status);

            if (!string.IsNullOrEmpty(callBack))
            {
                dynamic dyResult = JObject.Parse(callBack);

                string responseCode = dyResult.ResponseCode;
                string responseMessage = dyResult.ResponseMessage;
                string transactionId = dyResult.TransactionId;
                string transactionStatus = dyResult.TransactionStatus;
                string clientRefId = dyResult.ClientRefId;
                string operatorTransactionId = dyResult.OperatorTransactionId;

                if (responseCode == "000")
                {
                    if (transactionStatus.ToLower() == "success")
                    {
                        result.Add(responseCode);
                        result.Add(responseMessage);
                        result.Add(transactionStatus);

                        UpdateBSSPPaymentByStatus(billid, "Transaction Successful");

                        DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, "", "", "", "", "");
                        result.Add(BindBillTransDetails(dtTrans));
                    }
                    else
                    {
                        result.Add(responseMessage);
                        result.Add(transactionStatus);
                    }
                }
                else if (responseCode == "999")
                {
                    result.Add(responseCode);
                    result.Add(responseMessage);
                    result.Add(transactionStatus);
                }
                else
                {
                    result.Add("Error Occurred !");
                    result.Add(responseMessage);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BBPSPaymentEnquiry(string transid, string refid, string status)
    {
        string result = string.Empty;

        if (!string.IsNullOrEmpty(transid) && !string.IsNullOrEmpty(refid) && !string.IsNullOrEmpty(status))
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    result = SMBPApiService.BBPSPaymentEnquiry(UserId, refid, transid, status);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }

        return result;
    }

    private static bool UpdateBSSPPaymentByStatus(string billid, string transStatus)
    {
        return SMBPApiService.UpdateBSSPPaymentByStatus(billid, transStatus);
    }
    #endregion

    #region [Payment Section]
    private static List<string> Ledgerandcreditlimit_Transaction(string amount, string number, string spkeytext, string transType, string transShortType, string transNotes)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
                    DataTable dtAgency = SMBPApiService.GetAgencyDetailById(UserId);

                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                        agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                        {
                            string narration = amount + "_Recharge_To_" + number + "_by_" + spkeytext;
                            List<string> isLedger = LedgerDebitCreditSection(amount, UserId, agencyName, transType, "Recharge To " + number + "", "Recharge", transShortType, narration, transNotes);
                            if (isLedger.Count > 0)
                            {
                                string avlBal = isLedger[1];
                                string trackid = isLedger[0];

                                result.Add("000");
                                result.Add(trackid);
                            }
                            else
                            {
                                result.Add("500");
                                result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                            }
                        }
                        else
                        {
                            result.Add("500");
                            result.Add("INSUFFICIENT CREDIT LIMIT IN YOUR WALLET !");
                        }
                    }
                    else
                    {
                        result.Add("500");
                        result.Add("AGENCY DOES NOT EXIST !");
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }

        return result;
    }

    private static List<string> LedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype)
    {
        List<string> result = new List<string>();

        try
        {
            double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
            double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

            if (Uploadtype.Trim().ToLower() == "cr")
            {
                Remark = "Utility Refund";
                shortRemark = "Utility Refund";
            }

            result = LedgerService.LedgerDebitCreditUtility(Convert.ToDouble(amount), agentid, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion

    public static string GetLocalIPAddress()
    {
        string result = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                result = ip.ToString();
            }
        }

        return result;
    }
}