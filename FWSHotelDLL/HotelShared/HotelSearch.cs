﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
namespace HotelShared
{
    public class HotelSearch
    {
        public string selectedvalue { get; set; }
        public string SearchCity { get; set; }
        public string RegionId { get; set; }
        public string ResultType { get; set; }
        public string Currency { get; set; }
        public string HTLCityList { get; set; }
        public string SearchCityCode { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string SearchType { get; set; }
        public string SearchID { get; set; }
        public string HotelCityCode { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string HotelName { get; set; }
        public string HtlCode { get; set; }
        public string HotelAddress { get; set; }
        public string StarRating { get; set; }
        public string ThumbImage { get; set; }
        public string HotelContactNo { get; set; }
        public int NoofRoom { get; set; }
        public int NoofNight { get; set; }
        public string GuestDetails { get; set; }
        public int TotAdt { get; set; }
        public int TotChd { get; set; }
        public string ExtraCot { get; set; }
        public DataSet MarkupDS  { get; set; }
        public string Sorting { get; set; }
        public string HtlType { get; set; }
        public string Provider { get; set; }
        public decimal CurrancyRate { get; set; }
        public decimal servicetax { get; set; }
        public int ExtraRoom { get; set; }
        public string ExtraRoomType { get; set; }
        public decimal TG_servicetax { get; set; }
        public decimal EX_servicetax { get; set; }
        public decimal ROOMXML_servicetax { get; set; }
        public decimal GTA_servicetax { get; set; }
        public decimal RZ_servicetax { get; set; }

        public ArrayList AdtPerRoom { get; set; }
        public ArrayList ChdPerRoom { get; set; }
        public int[,] ChdAge { get; set; }

        public string TGUrl { get; set; }
        public string TGUsername { get; set; }
        public string TGPassword { get; set; }
        public string TGPropertyId { get; set; }
        public string TGTrip { get; set; }

        public string GTAClintID { get; set; }
        public string GTAPassword { get; set; }
        public string GTAEmailAddress { get; set; }
        public string GTAURL { get; set; }
        public string GTATrip { get; set; }

        public string RoomXMLURL { get; set; }
        public string RoomXMLUserID { get; set; }
        public string RoomXMLPassword { get; set; }
        public string RoomXMLOrgID { get; set; }
        public string RoomXMLTrip { get; set; }

        public string RezNextUrl { get; set; }
        public string RezNextUserName { get; set; }
        public string RezNextPassword { get; set; }
        public string RezNextSystemId { get; set; }
        public string RezNextTrip { get; set; }

        public string EXURL { get; set; }
        public string EXAPIKEY { get; set; }
        public string EXCID { get; set; }
        public string EXSecretKey { get; set; }
        public string EXHtlInfoReq { get; set; }
        public string EXTrip { get; set; }

        public string TG_HotelSearchReq { get; set; }
        public string TG_HotelSearchRes { get; set; }
        public string GTA_HotelSearchReq { get; set; }
        public string GTA_HotelSearchRes { get; set; }
        public string GTAItemInformationReq { get; set; }
        public string GTAItemInformationRes { get; set; }
        public string RoomXML_HotelSearchReq { get; set; }
        public string RoomXML_HotelSearchRes { get; set; }
        public string EX_HotelSearchReq { get; set; }
        public string EX_HotelSearchRes { get; set; }
        public string RZ_HotelSearchReq { get; set; }
        public string RZ_HotelSearchRes { get; set; }
        public List<HotelResult> HotelList { get; set; }

        public string TGXmlHeader
        {
            get{return "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>";}
        }
        public string TGXmlFooter
        {
            get { return "</soap:Body></soap:Envelope>"; }
        }
        string _TGHotelSeachHeader = null;
        public string TGHotelSeachHeader
        {
            get
            {
                _TGHotelSeachHeader = "<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>";
                return _TGHotelSeachHeader += "<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>";
            }
        }
        public string TGHotelSeachFooter
        {
            //get { return "<Promotion Type='BOTH' Name='ALLPromotions' /></TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>"; }
            get { return "</TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>"; }
        }
    
        public string GTAXmlHeader
        {
            get { return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Source>"; }
        }
        public string GTARequestMode
        {
            get { return "<RequestMode>SYNCHRONOUS</RequestMode></RequestorPreferences></Source><RequestDetails>"; }
        }
        public string GTAXmlFooter
        {
            get { return "</RequestDetails></Request>"; }
        }
        public string RZXmlHeader
        {
            get { return "<Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">"; }
        }

        public string AgentID { get; set; }
        public string AdminMrkType { get; set; }
        public decimal AdminMrkPer { get; set; }
        public string AgentMrkType { get; set; }
        public decimal AgentMrkPer { get; set; }
        public string DistrMrkType { get; set; }
        public decimal DistrMrkPer { get; set; }
        public decimal MrkAmt { get; set; }
        public string HtlRoomCode { get; set; }

        public string ProBookingReq { get; set; }
        public string ProBookingRes { get; set; }
        public string BookingConfReq { get; set; }
        public string BookingConfRes { get; set; }
        public string BookingCancelReq { get; set; }
        public string BookingCancelRes { get; set; }
        public string GTAPolicyReq { get; set; }
        public string GTAPolicyResp { get; set; }

        public string Status { get; set; }
        public string LoginID { get; set; }
        public string Orderid { get; set; }
        public string ProBookingID { get; set; }
        public string BookingID { get; set; }
        public string ReferenceNo { get; set; }
        public string IPAddress { get; set; }

        public string PGTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PGAddress { get; set; }
        public string PGCity { get; set; }
        public string PGState { get; set; }
        public string PGCountry { get; set; }
        public string PGPin { get; set; }
        public string PGEmail { get; set; }
        public string PGContact { get; set; }
        public int NoofHotel { get; set; }
        public string ReviewRating { get; set; }

        //public string EXURL { get; set; }
        //public string EXAPIKEY { get; set; }
        //public string EXCID { get; set; }
        //public string EXHtlInfoReq { get; set; }
        //public string EXHtlAvailability { get; set; }
        //public string EXClintID { get; set; }
        //public string EXPassword { get; set; }
        //public string EXEmailAddress { get; set; }
        //public string EXRateKey { get; set; }
        //public string EXRateCode { get; set; }
        //public string EX_HotelSearchReq { get; set; }
        //public string EX_HotelSearchRes { get; set; }

       
    }
    
    
}
