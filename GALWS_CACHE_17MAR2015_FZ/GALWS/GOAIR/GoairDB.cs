﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace STD.Shared
{
 public   class GoairDB
    {
        public DataTable CreateSaveDataTable()
        {
            DataTable dtTemp = new DataTable("tblSave");
            DataColumn SerialNo = new DataColumn("SerialNo");
            SerialNo.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(SerialNo);
            DataColumn ConfirmationNo = new DataColumn("ConfirmationNo");
            ConfirmationNo.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(ConfirmationNo);
            DataColumn BookingAgent = new DataColumn("BookingAgent");
            BookingAgent.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(BookingAgent);
            DataColumn BookDate = new DataColumn("BookDate");
            BookDate.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(BookDate);
            DataColumn PassengerTitle = new DataColumn("PassengerTitle");
            PassengerTitle.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(PassengerTitle);
            DataColumn PassengerFName = new DataColumn("PassengerFName");
            PassengerFName.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(PassengerFName);
            DataColumn PassengerLName = new DataColumn("PassengerLName");
            PassengerLName.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(PassengerLName);
            DataColumn PassengerAge = new DataColumn("PassengerAge");
            PassengerAge.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(PassengerAge);
            DataColumn Cabin = new DataColumn("Cabin");
            Cabin.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Cabin);
            DataColumn Origin = new DataColumn("Origin");
            Origin.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Origin);
            DataColumn Destination = new DataColumn("Destination");
            Destination.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Destination);
            DataColumn Dept_Date = new DataColumn("Dept_Date");
            Dept_Date.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Dept_Date);
            DataColumn Dept_Time = new DataColumn("Dept_Time");
            Dept_Time.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Dept_Time);
            DataColumn Arr_Date = new DataColumn("Arr_Date");
            Arr_Date.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Arr_Date);
            DataColumn Arr_Time = new DataColumn("Arr_Time");
            Arr_Time.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Arr_Time);
            DataColumn ReservationBalance = new DataColumn("ReservationBalance");
            ReservationBalance.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(ReservationBalance);
            DataColumn Total_Fare = new DataColumn("Total_Fare");
            Total_Fare.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Total_Fare);
            DataColumn Fare_ID = new DataColumn("Fare_ID");
            Fare_ID.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Fare_ID);
            DataColumn Message = new DataColumn("Message");
            Message.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(Message);

            DataColumn BookReq = new DataColumn("BookReq");
            BookReq.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(BookReq);

            DataColumn BookRes = new DataColumn("BookRes");
            BookRes.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(BookRes);

            DataColumn AddPayReq = new DataColumn("AddPayReq");
            AddPayReq.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(AddPayReq);

            DataColumn AddPayRes = new DataColumn("AddPayRes");
            AddPayRes.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(AddPayRes);

            DataColumn ConfirmPayRes = new DataColumn("ConfirmPayRes");
            ConfirmPayRes.DataType = System.Type.GetType("System.String");
            dtTemp.Columns.Add(ConfirmPayRes);

            return dtTemp;


        }

    }
}
