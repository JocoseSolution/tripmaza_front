﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResListItem
/// </summary>
public class ResListItem : ICloneable
{

    public string OrgDestFrom { get; set; }
    public string OrgDestTo { get; set; }
    public int Adult { get; set; }
    public int Child { get; set; }
    public int Infant { get; set; }
    public int TotPax { get; set; }
    public string DepartureLocation { get; set; }
    public string DepartureCityName { get; set; }
    public string DepartureAirportName { get; set; }
    public string DepartureTerminal { get; set; }
    public string ArrivalLocation { get; set; }
    public string ArrivalCityName { get; set; }
    public string ArrivalAirportName { get; set; }
    public string ArrivalTerminal { get; set; }
    public string DepartureDate { get; set; }
    public string Departure_Date { get; set; }
    public string DepartureTime { get; set; }
    public string ArrivalDate { get; set; }
    public string Arrival_Date { get; set; }
    public string ArrivalTime { get; set; }
    public string MarketingCarrier { get; set; }
    public string FlightIdentification { get; set; }
    public string RBD { get; set; }
    public string AvailableSeats { get; set; }
    public string ValiDatingCarrier { get; set; }

    public float AdtFare { get; set; }
    public float AdtBfare { get; set; }
    public float AdtTax { get; set; }
    public float AdtFSur { get; set; }

    public float ChdFare { get; set; }
    public float ChdBfare { get; set; }
    public float ChdTax { get; set; }
    public float ChdFSur { get; set; }

    public float InfFare { get; set; }
    public float InfBfare { get; set; }
    public float InfTax { get; set; }
    public float InfFSur { get; set; }

    public float STax { get; set; }
    public float TFee { get; set; }
    public float DisCount { get; set; }

    public float ADTAdminMrk { get; set; }
    public float ADTDistMrk { get; set; }
    public float ADTAgentMrk { get; set; }
    public float CHDAdminMrk { get; set; }
    public float CHDDistMrk { get; set; }
    public float CHDAgentMrk { get; set; }

    public float TotalBfare { get; set; }
    public float TotalTax { get; set; }
    public float TotalFuelSur { get; set; }
    public float TotalFare { get; set; }

    public int LineNumber { get; set; }
    public int Leg { get; set; }
    public string Searchvalue { get; set; }
    public string Flight { get; set; }
    public string TotDur { get; set; }
    public string TripType { get; set; }
    public string EQ { get; set; }
    public string Stops { get; set; }
    public string AirLineName { get; set; }
    public string Trip { get; set; }
    public string Sector { get; set; }
    public string TripCnt { get; set; }
    public int IATAComm { get; set; }
    public string fareBasis { get; set; }
    public string FBPaxType { get; set; }
    public string sno { get; set; }
    public string depdatelcc { get; set; }
    public string arrdatelcc { get; set; }
    public float OriginalTF { get; set; }
    public float OriginalTT { get; set; }
    public string Track_id { get; set; }
    public string FType { get; set; }
    public string FareRule { get; set; }

    public object Clone()
    {
        return this.MemberwiseClone();
    }

}
