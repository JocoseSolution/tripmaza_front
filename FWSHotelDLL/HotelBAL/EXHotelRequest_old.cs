﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelShared;

namespace HotelBAL
{
   public class EXHotelRequest
   {
       public HotelSearch EXHotelSearchRequest(HotelSearch HtlSearchQuery)
       {
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               ReqXml.Append("&apiKey=" + HtlSearchQuery.EXAPIKEY + "&cid=" + HtlSearchQuery.EXCID + "&xml=");
               ReqXml.Append("<HotelListRequest>");
               if (HtlSearchQuery.HotelName == "")
               {
                   ReqXml.Append("<city>" + HtlSearchQuery.SearchCity + "</city>");
                   ReqXml.Append("<countryCode>" + HtlSearchQuery.CountryCode + "</countryCode>");
                   if (HtlSearchQuery.StarRating == "0")
                       ReqXml.Append("<minStarRating>" + Convert.ToString(HtlSearchQuery.StarRating) + "</minStarRating>");
               }
               else
                   ReqXml.Append("<hotelIdList>" + HtlSearchQuery.HotelName + "</hotelIdList>");

               string[] checkindate = HtlSearchQuery.CheckInDate.Split('-');
               string[] checkoutdate = HtlSearchQuery.CheckOutDate.Split('-');
               ReqXml.Append("<arrivalDate>" + checkindate[1] + "/" + checkindate[2] + "/" + checkindate[0] + "</arrivalDate>");
               ReqXml.Append("<departureDate>" + checkoutdate[1] + "/" + checkoutdate[2] + "/" + checkoutdate[0] + "</departureDate>");

               ReqXml.Append("<RoomGroup>");
               for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
               {
                   ReqXml.Append("<Room>");
                   ReqXml.Append("<numberOfAdults>" + HtlSearchQuery.AdtPerRoom[i].ToString() + "</numberOfAdults>");
                   if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                   {
                       ReqXml.Append("<numberOfChildren>" + HtlSearchQuery.ChdPerRoom[i].ToString() + "</numberOfChildren>");
                       for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                       {
                           ReqXml.Append("<childAges>" + HtlSearchQuery.ChdAge[i, j] + "</childAges>");
                       }
                   }
                   ReqXml.Append("</Room>");
               }
               ReqXml.Append("</RoomGroup>");

               ReqXml.Append("<includeDetails>true</includeDetails>");
               ReqXml.Append("</HotelListRequest>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXHotelSearchRequest");
           }
           HtlSearchQuery.EX_HotelSearchReq = ReqXml.ToString();
           return HtlSearchQuery;
       }

       public HotelSearch EXHotelInformation(HotelSearch HtlSearch)
       {
           StringBuilder ReqXml = new StringBuilder();
           ReqXml.Append("&apiKey=" + HtlSearch.EXAPIKEY + "&cid=" + HtlSearch.EXCID + "&xml=");
           ReqXml.Append("<HotelInformationRequest>");
           ReqXml.Append("<hotelId>" + HtlSearch.HtlCode + "</hotelId>");
           ReqXml.Append("</HotelInformationRequest>");
           HtlSearch.EXHtlInfoReq = ReqXml.ToString();
           return HtlSearch;
       }
    }
}
