﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using HotelShared;
using HotelDAL;
using System.Xml;
using System.Collections;

namespace HotelBAL
{
   public class EXHotelResponse
    {

       EXHotelRequest HtlReq = new EXHotelRequest();
        public HotelComposite EXHotels(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelsCombo = new HotelComposite();
            try
            {
                if (SearchDetails.EXURL != null)
                {
                    SearchDetails = HtlReq.EXHotelSearchRequest(SearchDetails);
                    SearchDetails.EX_HotelSearchRes = EXPostXml(SearchDetails.EXURL + SearchDetails.EX_HotelSearchReq);
                    obgHotelsCombo.Hotelresults = GetEXHotelsPrice(SearchDetails.EX_HotelSearchRes, SearchDetails);
                }
                else 
                {
                    SearchDetails.EX_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                    SearchDetails.EX_HotelSearchReq = "not allow Expedia";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.EX_HotelSearchRes });
                    obgHotelsCombo.Hotelresults = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "EXHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchRes, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.EX_HotelSearchRes = ex.Message;
            }
            obgHotelsCombo.HotelSearchDetail = SearchDetails;
            return obgHotelsCombo;
        }

        protected string EXPostXml(string url)
        {
            StringBuilder sbResult = new StringBuilder();
            string xml = "";
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    Http.Timeout = 56000;
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelDA objhtlDa = new HotelDA();
                HotelDA.InsertHotelErrorLog(WebEx, "Expedia Post methode");
                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "EX", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "EX", "HotelInsert");
                    sbResult.Append(WebEx.Message + "<Errors>");
                }
            }
            catch (Exception ex)
            {
                sbResult.Append(ex.Message + "<Errors>");
                HotelDA.InsertHotelErrorLog(ex, "Expedia Post methode");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "EX", "HotelInsert");
            }
            return sbResult.ToString();
        }
        protected List<HotelResult> GetEXHotelsPrice(string XmlRes, HotelSearch SearchDetails)
        {
            HotelDA objhtlDa = new HotelDA();             HotelMarkups objHtlMrk = new HotelMarkups();             MarkupList MarkList = new MarkupList();
            List<HotelResult> HResult = new List<HotelResult>();
            try
            {
                XmlDocument Xdoc = new XmlDocument();
                Xdoc.LoadXml(XmlRes);

                if (Xdoc.SelectSingleNode("//HotelList") != null)
                {
                    XmlNodeList hotelSummaryList = Xdoc.SelectNodes("//HotelList//HotelSummary");
                    int v_counter = 740;

                    foreach (XmlNode hotel in hotelSummaryList)
                    {

                        string RoomCode = "";
                        getMinRoomRate(hotel, hotel.SelectSingleNode("lowRate").InnerText, out RoomCode);

                        v_counter++;
                        string DisMsg = "", HotelAddon = "", HServices = "";
                        if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo").Attributes["promo"].InnerText == "true")
                        {
                            if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo//promoDescription") != null)
                            {
                                DisMsg = hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo//promoDescription").InnerText;
                            }
                        }
                        if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//ValueAdds") != null)
                        {
                            XmlNodeList ValueAddNodeList = hotel.SelectNodes("RoomRateDetailsList//RoomRateDetails//ValueAdds//ValueAdd");
                            foreach (XmlNode ValueAddNode in ValueAddNodeList)
                            {
                                HotelAddon += ValueAddNode.SelectSingleNode("description").InnerText + ",";
                            }
                        }
                        if (hotel.SelectSingleNode("amenityMask").InnerText != "0")
                        {
                            HServices = convertDecimalToBinary(hotel.SelectSingleNode("amenityMask").InnerText);
                        }
                        MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, hotel.SelectSingleNode("hotelRating").InnerText, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(hotel.SelectSingleNode("lowRate").InnerText), SearchDetails.EX_servicetax);


                        HResult.Add(new HotelResult
                        {

                            PopulerId = v_counter,
                            HotelCode = hotel.SelectSingleNode("hotelId").InnerText,
                            HotelCity = hotel.SelectSingleNode("city").InnerText,
                            HotelCityCode = SearchDetails.SearchCityCode,
                            ////CountryCode = hotel.SelectSingleNode("countryCode").InnerText,
                            HotelName = hotel.SelectSingleNode("name").InnerText,
                            StarRating = hotel.SelectSingleNode("hotelRating").InnerText,
                            HotelAddress = hotel.SelectSingleNode("address1").InnerText + "," + hotel.SelectSingleNode("city").InnerText + "-" + hotel.SelectSingleNode("postalCode").InnerText,
                            HotelThumbnailImg = "http://images.travelnow.com" + hotel.SelectSingleNode("thumbNailUrl").InnerText,
                            Location = hotel.SelectSingleNode("locationDescription").InnerText,
                            HotelDescription = "<strong>Description</strong>: " + (hotel.SelectSingleNode("shortDescription") != null ? hotel.SelectSingleNode("shortDescription").InnerText.Replace("&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;", "") : ""),
                            Lati_Longi = hotel.SelectSingleNode("latitude").InnerText + "##" + hotel.SelectSingleNode("longitude").InnerText,
                            Provider = "EX",
                            ReviewRating = (hotel.SelectSingleNode("tripAdvisorRating") != null ? hotel.SelectSingleNode("tripAdvisorRating").InnerText.Trim() : "") + "#" + (hotel.SelectSingleNode("tripAdvisorReviewCount") != null ? hotel.SelectSingleNode("tripAdvisorReviewCount").InnerText.Trim() : ""),

                            //// PropertyCategory = hotel.SelectSingleNode("propertyCategory").InnerText,
                            //// CurrencyCode = hotel.SelectSingleNode("rateCurrencyCode").InnerText,

                            // TripAdvisorRatingUrl = hotel.SelectSingleNode("tripAdvisorRatingUrl") != null ? hotel.SelectSingleNode("tripAdvisorRatingUrl").InnerText : "",

                            // RatePlanCode = hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//rateCode").InnerText,
                            hotelPrice = MarkList.TotelAmt,
                            AgtMrk = MarkList.AgentMrkAmt,
                            DiscountMsg = DisMsg,
                            hotelDiscoutAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(hotel.SelectSingleNode("highRate").InnerText) - Convert.ToDecimal(hotel.SelectSingleNode("lowRate").InnerText), SearchDetails.EX_servicetax),
                            inclusions = "",// HotelAddon.Length > 0 ? HotelAddon.Remove(HotelAddon.LastIndexOf(','), 1) : HotelAddon,
                            HotelServices = HServices,
                            RoomTypeCode = RoomCode

                        });
                    }
                }
                else
                { 
                    HResult.Add(new HotelResult {HotelName = "", hotelPrice =0, HtlError = "Hotel Not Found"});
                }
                return HResult; 
            }
            catch (Exception ex)
            {
                HResult.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                HotelDA.InsertHotelErrorLog(ex, "GetEXHotelsPrice");
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            return HResult;
        }
        

        public string EXCancellationPolicy(HotelSearch SearchDetails)
        {
            string Policy = "";
            try
            {
               // SearchDetails =  TGHotels(SearchDetails);
                if (SearchDetails.EX_HotelSearchRes.Contains("<RoomRateDetailsList>"))
                {
                    string responses = SearchDetails.EX_HotelSearchRes.Replace(@"xmlns:ns2=\u0022http://v3.hotel.wsapi.ean.com/\u0022", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    //var hotelPolicy = (from htlPolicy in xmlresp.Descendants("RoomStays").Descendants("RoomStay").Descendants("RatePlans").Descendants("RatePlan")
                    //                   where htlPolicy.Attribute("RatePlanCode").Value == SearchDetails.HtlRoomCode
                    //                   select new { RoomName = htlPolicy.Attribute("RatePlanName"), Cancellation = htlPolicy.Element("CancelPenalties").Element("CancelPenalty") }).ToList();

                    var hotelPolicy = xmlresp.Descendants("RoomRateDetails").Where(x => x.Element("rateCode").Value == SearchDetails.HtlRoomCode).Descendants("cancellationPolicy").Select(p => p.Value).FirstOrDefault();

                    if (hotelPolicy!=null)
                    {
                           Policy = "<Div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                       
                                Policy += "<li style='margin:4px 0 4px 0;'>" +hotelPolicy + "</li></Div>";
                           
                       
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "Expidia CancellationPolicy");
                return ex.Message;
            }
            return Policy;
        }
    
        private string EXPHotelInformation(HotelSearch SearchDetails, string ItemCode)
        {
            string RoomXMLResponce = "";
            try
            {
                SearchDetails = HtlReq.EXHotelInformation(SearchDetails);
                RoomXMLResponce = EXPostXml(SearchDetails.EXURL.Replace("list?","info?")+SearchDetails.EXHtlInfoReq);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, ItemCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTAItemInformationReq, RoomXMLResponce, "RoomXML", "HotelInsert");

            }
            return RoomXMLResponce;
        }

        public RoomComposite EXRoomDetals(HotelSearch SearchDetails)
        {
            RoomComposite objRoomDetals = new RoomComposite();
            List<RoomList> objRoomList = new List<RoomList>();
            SelectedHotel HotelDetail = new SelectedHotel();
            HotelMarkups objHtlMrk = new HotelMarkups();
            MarkupList MarkList = new MarkupList(); 
            MarkupList TaxMarkList = new MarkupList(); 
            MarkupList ExtraMarkList = new MarkupList();
            try
            {
                XDocument document = XDocument.Parse(SearchDetails.EX_HotelSearchRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                var RoomsDetails = (from Hotel in document.Descendants("HotelList").Elements("HotelSummary")
                                    where Hotel.Element("hotelId").Value == SearchDetails.HtlCode
                                    select new { RoomCategory = Hotel.Element("RoomRateDetailsList") }).ToList();

                if (RoomsDetails.Count > 0)
                {
                    #region Hotel Details
                  
                     HotelDetail = EXHotelInformation_Room(SearchDetails);
                    #endregion
                    #region Room Details
                    int j = 0;
                    foreach (var Rooms in RoomsDetails[0].RoomCategory.Descendants("RoomRateDetails"))
                    {
                        try
                        {
                            string smoking = "", discountmsg = "", Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "";
                            decimal Rate_Org = 0, DiscountRate = 0, MrkTotalPrice = 0, adminMrkAmt = 0, AgtMrkAmt = 0, MrkTaxes = 0, MrkExtraGuest = 0, SericeTaxAmt = 0, VSericeTaxAmt=0;
                            //Hotel Price
                            foreach (var Roomsrates in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate"))
                            {
                                Rate_Org += Convert.ToDecimal(Roomsrates.Attribute("rate").Value);
                                MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(Roomsrates.Attribute("rate").Value), SearchDetails.EX_servicetax);
                                MrkTotalPrice += MarkList.TotelAmt;
                                adminMrkAmt += MarkList.AdminMrkAmt;
                                AgtMrkAmt += MarkList.AgentMrkAmt;
                                SericeTaxAmt += MarkList.AgentServiceTaxAmt;
                                VSericeTaxAmt += MarkList.VenderServiceTaxAmt;
                                MrkRoomrateStr += MarkList.TotelAmt.ToString() + "/";
                                Org_RoomRateStr += Roomsrates.Attribute("rate").Value + "/";
                            }
                            if (Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges") != null)
                            {
                                foreach (var sercharge in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge"))
                                {
                                    if (sercharge.Attribute("type").Value == "ExtraPersonFee")
                                    {
                                        ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                        MrkTotalPrice += ExtraMarkList.TotelAmt;
                                        MrkExtraGuest += ExtraMarkList.TotelAmt;
                                        adminMrkAmt += ExtraMarkList.AdminMrkAmt;
                                        AgtMrkAmt += ExtraMarkList.AgentMrkAmt;
                                        SericeTaxAmt += ExtraMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += ExtraMarkList.VenderServiceTaxAmt;
                                    }
                                    else
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                        MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                    }
                                }
                            }
                            if (Rooms.Element("RateInfos").Element("RateInfo").Attribute("promo").Value == "true")
                                DiscountRate = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("averageBaseRate").Value), 0);
                            if (Rooms.Element("RateInfos").Element("promoDescription") != null)
                                discountmsg = Rooms.Element("RateInfos").Element("RateInfo").Element("promoDescription").Value;

                            //Display Hotel Inclution strat  
                            if (Rooms.Element("ValueAdds") != null)
                                foreach (var inclu in Rooms.Element("ValueAdds").Descendants("ValueAdd"))
                                {
                                    Inclusion += inclu.Element("description").Value + " ,";
                                }
                            //Display Hotel Inclution End
                            if (Rooms.Element("smokingPreferences") != null)
                                smoking = Rooms.Element("smokingPreferences").Value;


                            string RoomDiscription = "";
                            //Room Disciption
                            try
                            {
                                //if (j <= HotelDetail.RoomcatList.Count - 1)
                                //{
                                //    string[] roomdisArr = (string[])HotelDetail.RoomcatList[j];
                                //    roomdisArr.Select(x => x[0].ToString() == Rooms.Attribute("Id").Value);
                                //    RoomDiscription += roomdisArr[2];
                                //}
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "Expidia RoomcatList" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                            }
                            j++;

                            objRoomList.Add(new RoomList
                            {
                                HotelCode = SearchDetails.HtlCode,
                                RatePlanCode = Rooms.Element("rateCode").Value,
                                RoomTypeCode = Rooms.Element("roomTypeCode").Value,
                                RoomName = Rooms.Element("roomDescription").Value,
                                discountMsg = discountmsg,
                                DiscountAMT = (DiscountRate * SearchDetails.NoofNight * SearchDetails.NoofRoom) + MrkTaxes + MrkExtraGuest,
                                Total_Org_Roomrate = Rate_Org,
                                TotalRoomrate = MrkTotalPrice,
                                AdminMarkupPer = MarkList.AdminMrkPercent,
                                AdminMarkupAmt = adminMrkAmt,
                                AdminMarkupType = MarkList.AdminMrkType,
                                AgentMarkupPer = MarkList.AgentMrkPercent,
                                AgentMarkupAmt = AgtMrkAmt,
                                AgentMarkupType = MarkList.AgentMrkType,
                                AgentServiseTaxAmt = SericeTaxAmt,
                                V_ServiseTaxAmt = VSericeTaxAmt,
                                AmountBeforeTax = 0,
                                Taxes = 0,
                                MrkTaxes = MrkTaxes,
                                ExtraGuest_Charge = MrkExtraGuest,
                                Smoking = smoking,
                                inclusions = Inclusion,
                                CancelationPolicy = "",//SetHotelPolicyForRoom(Rooms.Element("ChargeConditions"), SearchDetails.CurrancyRate),
                                OrgRateBreakups = Org_RoomRateStr,
                                MrkRateBreakups = MrkRoomrateStr,
                                DiscRoomrateBreakups = "",
                                //EssentialInformation = EssentialInformation,
                                RoomDescription = RoomDiscription,
                                Provider = "EX",
                                RoomImage = ""
                            });
                        }
                        catch (Exception ex)
                        {
                            objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                            HotelDA.InsertHotelErrorLog(ex, "expidia Rooms adding" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                        }
                    }
                    #endregion
                    objRoomDetals.RoomDetails = objRoomList;
                    objRoomDetals.SelectedHotelDetail = HotelDetail;
                }
                else
                {
                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                HotelDA.InsertHotelErrorLog(ex, "EXRoomDetals" + SearchDetails.SearchCity);
            }
            return objRoomDetals;
        }
        public SelectedHotel EXHotelInformation_Room(HotelSearch hotelSearch)
        {
            string HotelCode = hotelSearch.HtlCode;
            string xmlresult = EXPHotelInformation(hotelSearch, "");
            SelectedHotel HotelInfo = new SelectedHotel();
            try
            {
               //// XDocument document = XDocument.Load(ConfigurationManager.AppSettings["EXPediaHotelDetail"] + "\\" + HotelCode + ".xml");

                XmlDocument document = new XmlDocument();
                document.LoadXml(xmlresult);
                string Addresss = "Address Not Found", Telephones = "";
                if (document.SelectSingleNode("//HotelSummary") != null)
                {
                    if (document.SelectSingleNode("//HotelSummary//address1") != null)
                    {
                        Addresss = document.SelectSingleNode("//HotelSummary//address1").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//address2") != null)
                    {
                        Addresss += "," + document.SelectSingleNode("//HotelSummary//address2").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//city") != null)
                    {
                        Addresss += "," + document.SelectSingleNode("//HotelSummary//city").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//postalCode") != null)
                    {
                        Addresss += "-" + document.SelectSingleNode("//HotelSummary//postalCode").InnerText;
                    }
                    HotelInfo.HotelAddress = Addresss;
                    XmlNodeList hotelNodes = document.SelectNodes("//RoomTypes//RoomType");
                    string hoteldiscription = "";
                        if(document.SelectSingleNode("//propertyDescription")!=null)
                        {
                             hoteldiscription = document.SelectSingleNode("//propertyDescription").InnerText;
                        }
                    HotelInfo.HotelDescription = "<strong>Description</strong>:<div class='check1'>" + hoteldiscription.Trim() + "</div>";
                    foreach (XmlNode Roomnode in hotelNodes)
                    {
                        string  facliStr = "";
                        HotelInfo.Attraction = ""; HotelInfo.RoomAmenities = "";
                       
                        if (Roomnode.SelectSingleNode("descriptionLong") != null)
                        {
                            hoteldiscription += Roomnode.SelectSingleNode("descriptionLong").InnerText;
                           
                        }
                        if (Roomnode.SelectSingleNode("roomAmenities//RoomAmenity") != null)
                            foreach (XmlNode flt in Roomnode.SelectNodes("roomAmenities//RoomAmenity"))
                            {
                                if (flt.SelectSingleNode("amenity") != null)
                                {
                                    facliStr += "<div class='check1'>" + flt.SelectSingleNode("amenity").InnerText + "</div>";
                                }
                            }
                        HotelInfo.HotelAmenities = facliStr;
                        
                    }
                    
                    if (document.SelectSingleNode("//HotelSummary//hotelRating") != null)
                    {
                        HotelInfo.StarRating = document.SelectSingleNode("//HotelSummary//hotelRating").InnerText;
                    }
                    HotelInfo.Lati_Longi = "";
                    if (document.SelectSingleNode("//HotelSummary//latitude") != null)
                    {
                        HotelInfo.Lati_Longi = document.SelectSingleNode("//HotelSummary//latitude").InnerText + "," + document.SelectSingleNode("//HotelSummary//longitude").InnerText;
                    }
                    string imgdiv = "";
                    ArrayList Images = new ArrayList();
                    try
                    {
                        int im = 0;

                        if (document.SelectNodes("//HotelImages") != null)
                        {
                            foreach (XmlNode img in document.SelectNodes("//HotelImages/HotelImage"))
                            {
                                im++;

                                imgdiv += "<img id='img" + im + "' src='" + img.SelectSingleNode("url").InnerText + "' onmouseover='return ShowHtlImg(this);' title='" + img.SelectSingleNode("caption").InnerText + "' alt='' class='imageHtlDetailsshow' />";
                                Images.Add(img.SelectSingleNode("//HotelImages//url").InnerText);
                                
                            }
                        }
                        else
                        {
                            Images.Add("Images/Hotel/NoImage.jpg");
                        }
                        HotelInfo.ThumbnailUrl = Images[0].ToString();
                        HotelInfo.HotelImage = imgdiv;
                    }
                    catch (Exception ex)
                    {
                        HotelDA.InsertHotelErrorLog(ex, HotelCode + "-ImageLinks");
                    }

                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.HotelAddress = "";
                    HotelInfo.HotelContactNo = "";
                    HotelInfo.Attraction = "";
                    HotelInfo.HotelDescription = "";
                    HotelInfo.RoomAmenities = "";
                    HotelInfo.HotelAmenities = "";
                    HotelInfo.Lati_Longi = "";
                    HotelInfo.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", HotelCode, document.ToString(), "", "HotelInsert");
                }
                ////var Hoteldtl = (from Hotel in document.Descendants("HotelSummary") select new { HotelDetails = Hotel }).ToList();
                ////if (Hoteldtl.Count == 0)
                ////    Hoteldtl = (from Hotel in document.Elements("RoomType").Elements("HotelAvailability").Descendants("Hotel") select new { HotelDetails = Hotel }).ToList();
                //if (Hoteldtl.Count > 0)
                //{
                //    foreach (var Htl in Hoteldtl)
                //    {
                        ////string Addresss = "Address Not Found", Telephones = "";
                        ////try
                        ////{
                        ////    Addresss = Htl.HotelDetails.Element("Address").Element("Address1").Value;
                        ////    if (Htl.HotelDetails.Element("Address").Element("Address2") != null)
                        ////        Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Address2").Value;
                        ////    if (Htl.HotelDetails.Element("Address").Element("City") != null)
                        ////        Addresss += ", " + Htl.HotelDetails.Element("Address").Element("City").Value;
                        ////    if (Htl.HotelDetails.Element("Address").Element("Country") != null)
                        ////        Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Country").Value;

                        ////    if (Htl.HotelDetails.Element("Address").Element("Tel") != null)
                        ////        Telephones = Htl.HotelDetails.Element("Address").Element("Tel").Value;
                        ////    HotelInfo.HotelContactNo = Telephones;
                        ////}
                        ////catch (Exception ex)
                        ////{
                        ////    HotelDA.InsertHotelErrorLog(ex, HotelCode + "-Addresss");
                        ////}
                       //// HotelInfo.HotelAddress = Addresss;

                        ////string hoteldiscription = "", facliStr = "";
                        ////HotelInfo.Attraction = ""; HotelInfo.RoomAmenities = "";
                        ////HotelInfo.HotelDescription = "<strong>Description</strong>: ";
                        ////if (Htl.HotelDetails.Element("Description") != null)
                        ////{
                        ////    foreach (var discrepsion in Htl.HotelDetails.Descendants("Description"))
                        ////    {
                        ////        if (discrepsion.Element("Type") != null)
                        ////        {
                        ////            hoteldiscription += "<div style='padding: 4px 0;'><span style='text-transform:uppercase; font-size:13px;font-weight: bold;'>" + discrepsion.Element("Type").Value + ": </span> " + discrepsion.Element("Text").Value + "</div><hr />";
                        ////        }
                        ////    }
                        ////}
                        ////HotelInfo.HotelDescription = hoteldiscription;
                        //Hotel facility Facilities
                        ////try
                        ////{
                        ////    ////if (Htl.HotelDetails.Element("Facilities") != null)
                        ////    ////    foreach (var flt in Htl.HotelDetails.Element("Facilities").Descendants("Facility"))
                        ////    ////    {
                        ////    ////        facliStr += "<div class='check1'>" + flt.Value.Trim() + "</div>";
                        ////    ////    }
                        ////    ////if (Htl.HotelDetails.Element("Amenity") != null)
                        ////    ////    foreach (var flt in Htl.HotelDetails.Descendants("Amenity"))
                        ////    ////    {
                        ////    ////        facliStr += "<div class='check1'>" + flt.Element("Text").Value.Trim() + "</div>"; ;
                        ////    ////    }
                        ////}
                        ////catch (Exception ex)
                        ////{
                        ////    HotelDA.InsertHotelErrorLog(ex, HotelCode + "-Facilities");
                        ////}
                        ////HotelInfo.HotelAmenities = facliStr;
                       // string imgdiv = "";
                        //Images Links ImageLinks
                        ////ArrayList Images = new ArrayList();
                        ////try
                        ////{
                        ////    int im = 0;
                        ////    if (Htl.HotelDetails.Element("Photo") != null)
                        ////    {
                        ////        foreach (var img in Htl.HotelDetails.Descendants("Photo"))
                        ////        {
                        ////            im++;
                        ////            if (Htl.HotelDetails.Element("Photo").Value.Contains("http:"))
                        ////            {
                        ////                imgdiv += "<img id='img" + im + "' src='" + img.Element("Url").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("PhotoType").Value + "' alt='' class='imageHtlDetailsshow' />";
                        ////                Images.Add(img.Element("Url").Value);
                        ////            }
                        ////            else
                        ////            {
                        ////                imgdiv += "<img id='img" + im + "' src='http://www.roomsxml.com" + img.Element("Url").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("PhotoType").Value + "' alt='' class='imageHtlDetailsshow' />";
                        ////                Images.Add("http://www.roomsxml.com" + img.Element("Url").Value);
                        ////            }
                        ////        }
                        ////    }
                        ////    else Images.Add("Images/Hotel/NoImage.jpg");
                        ////    HotelInfo.ThumbnailUrl = Images[0].ToString();
                        ////    HotelInfo.HotelImage = imgdiv;
                        ////}
                        ////catch (Exception ex)
                        ////{
                        ////    HotelDA.InsertHotelErrorLog(ex, HotelCode + "-ImageLinks");
                        ////}
                        ////HotelInfo.Lati_Longi = "";
                        ////if (Htl.HotelDetails.Element("GeneralInfo") != null)
                        ////    if (Htl.HotelDetails.Element("GeneralInfo").Element("Latitude") != null)
                        ////    {
                        ////        HotelInfo.Lati_Longi = Htl.HotelDetails.Element("GeneralInfo").Element("Latitude").Value + "," + Htl.HotelDetails.Element("GeneralInfo").Element("Longitude").Value;
                        ////    }
                        ////HotelInfo.StarRating = "0";
                        ////if (Htl.HotelDetails.Element("Stars") != null)
                        ////    HotelInfo.StarRating = Htl.HotelDetails.Element("Stars").Value;
                        ////else
                        ////    HotelInfo.StarRating = Htl.HotelDetails.Attribute("stars").Value;
                //    }
                //}
                //else
                //{
                //    HotelInfo.StarRating = "0";
                //    HotelInfo.HotelAddress = "";
                //    HotelInfo.HotelContactNo = "";
                //    HotelInfo.Attraction = "";
                //    HotelInfo.HotelDescription = "";
                //    HotelInfo.RoomAmenities = "";
                //    HotelInfo.HotelAmenities = "";
                //    HotelInfo.Lati_Longi = "";
                //    HotelInfo.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";
                //    HotelDA objhtlDa = new HotelDA();
                //    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", HotelCode, document.ToString(), "", "HotelInsert");
                //}
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, HotelCode + "-GetRoomXMLRoomHotelInformation");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", "", "", HotelCode, "HotelInsert");
            }
            return HotelInfo;
        }


        public  void getMinRoomRate(XmlNode HotelSummery, string minRate, out string Roomcode)
       {
           XElement ele = XElement.Parse(HotelSummery.OuterXml);
           var x = from item in ele.Descendants("NightlyRate").Where(r => r.Attribute("rate").Value == minRate)
                   select item;
           Roomcode = x.FirstOrDefault().Parent.Parent.Parent.Parent.Parent.Element("rateCode").Value;
       }
        public  string convertDecimalToBinary(string hService)
       {
           int amenities = Convert.ToInt32(hService);
           ArrayList arrary = new ArrayList();
           StringBuilder amenitiesval = new StringBuilder();
           int x = amenities;
           int y = 0;
           while (x > 0)
           {
               if (x % 2 == 1)
               {
                   y = x % 2;
                   amenitiesval.Append(y);
               }
               else
               {
                   y = x % 2;
                   amenitiesval.Append(y);
               }
               x = x / 2;
           }
           string v_amenity = amenitiesval.ToString();
           string v_amenityresult = "";
           double p = Math.Pow(2, 3);
           for (int i = 0; i < v_amenity.Length; i++)
           {
               if (v_amenity.Substring(i, 1) != "0")
               {
                   v_amenityresult += (Convert.ToInt16(v_amenity.Substring(i, 1)) * Convert.ToInt64(Math.Pow(2, i))) + "#";
               }
           }
           v_amenityresult = v_amenityresult.Remove(v_amenityresult.LastIndexOf('#'), 1);
           return v_amenityresult;
       }

        
    }
}
