﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMN_SHARED.Payment;

namespace COMN_SHARED.Flight
{

    public class FltPNR
    {
        public string GdsPNR { get; set; }
        public string AirlinePNR { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    

    }

    public class FltBookReqShrd
    {
        public string UserID { get; set; }
        public string ClientID { get; set; }
        public string OrderID { get; set; }
        public string OrderIDR { get; set; }
        public FltTicketSatus TicketStatus { get; set; }
        public PaymentReqShrd PaymentReq { get; set; }
        public string IP { get; set; }      

    }


    public class FltBookResShrd
    {
        public string UserID { get; set; }
        public string ClientID { get; set; }
        public string OrderID { get; set; }
        public string OrderIDR { get; set; }
        public FltTicketSatus TicketStatus { get; set; }
        public PaymentResShrd PaymentRes { get; set; }
        public FltPNR PNRO { get; set; }
        public FltPNR PNRR { get; set; }      
    }


    public enum FltTicketSatus
    {
        ToBeHold,
        ToBeTicketed

    }

    public enum FltPNRSatus
    {
        Confirm,
        ConfirmByAgent,
        PreConfirmByAgent,
        Failed



    }


}
