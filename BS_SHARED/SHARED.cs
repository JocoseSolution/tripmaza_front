﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace BS_SHARED
{
    public class SHARED : ICloneable
    {
        #region[Searching properties]
        public string src { get; set; }
        public string dest { get; set; }
        public string srcID { get; set; }
        public string destID { get; set; }
        public string journeyDate { get; set; }
        public string ReturnDate { get; set; }
        public string TripType { get; set; }
        public string NoOfPax { get; set; }
        
        public string SeatType { get; set; }
        public string BookingDate { get; set; }
        #endregion
        #region[Result Properties]
        public string travelerID { get; set; }
        public string arrTime { get; set; }
        public string departTime { get; set; }
        public string Dur_Time { get; set; }
        public string serviceName { get; set; }
        public string seatfare { get; set; }

        public decimal[] Arr_adcomm { get; set; }
        public decimal[] Arr_taTds { get; set; }
        public decimal[] Arr_serviceChrg { get; set; }
        public decimal[] Arr_taNetFare { get; set; }
        public decimal[] Arr_taTotFare { get; set; }
        public decimal[] Arr_totFare { get; set; }
        
        public string[] seat_Originalfare { get; set; }
        public string seatfarewithMarkp { get; set; }
        public string[] seat_farewithMarkp { get; set; }
        public string seatfarewithTaxes { get; set; }
        public string berthfare { get; set; }
        public string berthfarewithTaxes { get; set; }
        public int remainingSeat { get; set; }
        public string serviceNumber { get; set; }
        public string serviceType { get; set; }
        public string serviceID { get; set; }
        public string traveler { get; set; }
        public int totalSeat { get; set; }
        public string[] drPoint { get; set; }
        public string[] bdPoint { get; set; }
        public string busType { get; set; }
        public string[] canPolicy_AB { get; set; }
        public string canPolicy_RB { get; set; }
        public string tripId { get; set; }
        public string idproofReq { get; set; }
        public string partialCanAllowed { get; set; }
        public string provider_name { get; set; }
        public string startDate { get; set; }
        public string canPolicy { get; set; }
        public string[] TYcanPolicy { get; set; }
        public string AC_NONAC { get; set; }
        public string SEAT_TYPE { get; set; }
        public string WithTaxes { get; set; }
        public string totWithTaxes { get; set; }

        public string paymentmode { get; set; }
        //public string PassengerCategoryID { get; set; }
        #endregion
        #region[selected seat properties]
        public List<string> boardingId { get; set; }
        public List<string> boardinglocation { get; set; }
        public List<string> boardingtime { get; set; }
        public List<string> droppingId { get; set; }
        public List<string> droppinglocation { get; set; }
        public List<string> droppingtime { get; set; }
        public List<string> title { get; set; }
        public List<string> gender { get; set; }
        public List<string> paxage { get; set; }
        public List<string> paxname { get; set; }
        public List<string> paxseat { get; set; }
        public List<string> perFare { get; set; }
        public List<string> perComm { get; set; }
        public List<string> perTds { get; set; }
        public List<string> perTotfare { get; set; }
        public List<string> perTatotfare { get; set; }
        public List<string> perTaNetfare { get; set; }
        public List<string> peradMrkp { get; set; }
        public List<string> peragMrkp { get; set; }
        public List<string> perOriginalFare { get; set; }
        public string paxmob { get; set; }
        public string paxemail { get; set; }
        public string paxaddress { get; set; }
        public string idtype { get; set; }
        public string idnumber { get; set; }
        public string primaryPax { get; set; }
        public string Isprimary { get; set; }
        public ArrayList inventorylist { get; set; }
        public string boardpoint { get; set; }
        public string droppoint { get; set; }
        public string boardpointid { get; set; }
        public string droppointid { get; set; }
        public string agentID { get; set; }
        public string busoperator { get; set; }
        public string orderID { get; set; }
        public string seat { get; set; }
        public string fare { get; set; }
        public string originalfare { get; set; }
        public string ladiesSeat { get; set; }
        public string usertype { get; set; }
        #endregion
        #region[for tds , commision and markup calculation]
        public string commtype { get; set; }
        public decimal commission { get; set; }
        public decimal TA_tds { get; set; }
        public decimal adcomm { get; set; }
        public decimal taTds { get; set; }
        public decimal totFare { get; set; }
        public decimal taTotFare { get; set; }
        public decimal taNetFare { get; set; }
        public decimal serviceChrg { get; set; }
        public List<decimal> comlist { get; set; }
        public decimal admrkp { get; set; }
        public decimal agmrkp { get; set; }
        public string admrkptype { get; set; }
        public string agmrkptype { get; set; }
        public decimal mrkpFare { get; set; }
        #endregion
        #region ICloneable Members

        public object Clone()
        {
            throw new NotImplementedException();
        }

        #endregion
        #region[For Booking]
        public string bookreq { get; set; }
        public string bookres { get; set; }
        public string blockKey { get; set; }
        public string ticketno { get; set; }
        public string tin { get; set; }
        public string pnr { get; set; }
        public string status { get; set; }
        public string partialCancel { get; set; }
        public string TicketNoRes { get; set; }

        #endregion
        #region[For Cancel Ticket]
        public string cancelRecharge { get; set; }
        public string refundAmt { get; set; }
        public string canrequest { get; set; }
        public string canresponse { get; set; }
        public string Passengername { get; set; }
        public string cancellable { get; set; }
        public string canTime { get; set; }
        public string netfare { get; set; }
        #endregion
        #region[For Ledger]
        public decimal addAmt { get; set; }
        public decimal subAmt { get; set; }
        public double avalBal { get; set; }
        #endregion
        #region[Agency Details]
        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        #endregion
        
    }
    public class cityListService
    {
        public string SourceID { get; set; }
        public string Source { get; set; }
        public string SrcPlaceCode { get; set; }
        public string SrcStateCode { get; set; }
        public string SrcStateName { get; set; }
        public string DestinationID { get; set; }
        public string Destination { get; set; }
        public string DestPlaceCode { get; set; }
        public string DestStateCode { get; set; }
        public string DestStateName { get; set; }
        public string City_ID { get; set; }
        public string PROVIDER_NAME { get; set; }

    }
    public class ApiStatus
    {
        public bool success { get; set; }
        public string message { get; set; }
    }

    public class ApiAvailableBus
    {
        public int operatorId { get; set; }
        public double commPCT { get; set; }
        public string operatorName { get; set; }
        public string departureTime { get; set; }
        public bool mTicketAllowed { get; set; }
        public bool idProofRequired { get; set; }
        public string serviceId { get; set; }
        public string fare { get; set; }
        public string busType { get; set; }
        public string routeScheduleId { get; set; }
        public int availableSeats { get; set; }
        public bool partialCancellationAllowed { get; set; }
        public string arrivalTime { get; set; }
        public string cancellationPolicy { get; set; }
        public List<object> boardingPoints { get; set; }
        public List<object> droppingPoints { get; set; }
        public int inventoryType { get; set; }
    }
    public class Seat
    {
        public string available { get; set; }
        public string column { get; set; }
        public string fare { get; set; }
        public string ladiesSeat { get; set; }
        public string length { get; set; }
        public string name { get; set; }
        public string row { get; set; }
        public string width { get; set; }
        public string zIndex { get; set; }
        public string commission { get; set; }
        public string bookedBy { get; set; }
        public string ac { get; set; }
        public string sleeper { get; set; }
        public string id { get; set; }
        public decimal? totalFareWithTaxes { get; set; }
        public decimal? serviceTaxAmount { get; set; }
        public double serviceTaxPer { get; set; }
        public bool serviceTaxApplicable { get; set; }
        public string baseFare { get; set; }
        public string serviceTaxAbsolute { get; set; }
    }
    public class RootObject
    {
        public List<Seat> seats { get; set; }
        public ApiStatus apiStatus { get; set; }
        public List<ApiAvailableBus> apiAvailableBuses { get; set; }
      
    }
    public class RootObject2
    {
        public List<Seat> seats { get; set; }
        public ApiStatus apiStatus { get; set; }
        public List<ApiAvailableBus> apiAvailableBuses { get; set; }

    }
    public class RootObject1
    {
        public string sourceCity { get; set; }
        public string destinationCity { get; set; }
        public string doj { get; set; }
        public string routeScheduleId { get; set; }
        public BoardingPoint boardingPoint { get; set; }
        public string customerName { get; set; }
        public string customerLastName { get; set; }
        public string customerEmail { get; set; }
        public string customerPhone { get; set; }
        public string emergencyPhNumber { get; set; }
        public string customerAddress { get; set; }
        public List<BlockSeatPaxDetail> blockSeatPaxDetails { get; set; }
        public int inventoryType { get; set; }

    }
    public class BoardingPoint
    {
        public string id { get; set; }
        public string location { get; set; }
        public string time { get; set; }
    }

    public class BlockSeatPaxDetail
    {
        public string age { get; set; }
        public string name { get; set; }
        public string seatNbr { get; set; }
        public string sex { get; set; }
        public decimal fare { get; set; }
        public decimal totalFareWithTaxes { get; set; }
        public bool ladiesSeat { get; set; }
        public string lastName { get; set; }
        public string mobile { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
        public string nameOnId { get; set; }
        public bool primary { get; set; }
        public bool ac { get; set; }
        public bool sleeper { get; set; }
    }

}
