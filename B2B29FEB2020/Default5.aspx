﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default5.aspx.cs" Inherits="Default5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <style type="text/css">
        *{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
body{

.card-load{
  max-width: 350px;
  width: 100%;
  background: #fff;
  padding: 30px;
  border-radius: 10px;
  border: 1px solid #d9d9d9;
}
.card-load .header-load{
  display: flex;
  align-items: center;
  margin-bottom:10px;
}
.header-load .img{
  height: 30px;
  width: 30px;
  background: #d9d9d9;
  border-radius: 50%;
  position: relative;
  overflow: hidden;
}

.details span{
  display: block;
  background: #d9d9d9;
  border-radius: 10px;
  overflow: hidden;
  position: relative;
}
.details .name{
  height: 15px;
  width: 100px;
}
.details .about{
  height: 13px;
  width: 150px;
  margin-top: 4px;
}
.card-load .description{
  margin: 25px 0;
}
.description .line{
  background: #d9d9d9;
  border-radius: 10px;
  height: 13px;
  margin: 10px 0;
  overflow: hidden;
  position: relative;
}
.description .line-1{
  width: calc(100% - 15%);
}
.description .line-3{
  width: calc(100% - 40%);
}
.card-load .btns{
  display: flex;
}
.card-load .btns .btn{
  height: 45px;
  width: 18%;
  background: #d9d9d9;
  border-radius: 3px;
  position: relative;
  overflow: hidden;
}
.btns .btn-1{
  margin-right: -7px;
}
.btns .btn-2{
  margin-left: 8px;
}
.header-load .img::before,
.details span::before,
.description .line::before,
.btns .btn::before{
  position: absolute;
  content: "";
  height: 100%;
  width: 100%;
  background-image: linear-gradient(to right, #d9d9d9 0%, rgba(0,0,0,0.05) 20%, #d9d9d9 40%, #d9d9d9 100%);
  background-repeat: no-repeat;
  background-size: 450px 400px;
  animation: shimmer 1s linear infinite;
}
.header-load .img::before{
  background-size: 650px 600px;
}
.details span::before{
  animation-delay: 0.2s;
}
.btns .btn-2::before{
  animation-delay: 0.22s;
}
@keyframes shimmer {
  0%{
    background-position: -450px 0;
  }
  100%{
    background-position: 450px 0;
  }
}
    </style>
   
</head>
<body>
    
    <div class="card-load">
  <div class="header-load">
       <div class="details">
     
      <span class="about" style="margin-right: 20px;"></span>
    </div>
    <div class="img"></div>
   
  </div>

         <div class="header-load">
       <div class="details">
     
      <span class="about" style="margin-right: 20px;"></span>
    </div>
    <div class="img"></div>
   
  </div>

  <div class="description">
    <div class="line line-1"></div>
    <div class="line line-2"></div>
    <div class="line line-3"></div>
  </div>
  <div class="btns">
    <div class="btn btn-1"></div>
    <div class="btn btn-2"></div>
  </div>
</div>
    
</body>
</html>
