﻿using STD.BAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.IWEENAirAsia
{
    public class Iweenbook
    {
        string UserName;
        string Password;
        string ServiceUrl;
        string TargetCode;
        string SecurityGUID;


        public Iweenbook()
        {
            //UserName = username;
            //Password = password;
            //ServiceUrl = serviceUrl;
            //TargetCode = targetCode;
            //SecurityGUID = securityGUID;
        }
        public string BookingRequest(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, ref string bookingID, ref string airlinpnr)
        {
            string strRes = "";
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";
            string Status = "";
            string exep = "";
            string Bkgid = "";
            DataSet objDs = new DataSet();
            DataTable dtnew = new DataTable();
            dtnew = FltDT;
            dtnew.TableName = "TabelFDT";
            objDs.Tables.Add(dtnew);
            try
            {

                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");
                string[] sno = Convert.ToString(FltDT.Rows[0]["sno"]).Split(':');
                #region FareQuotel
                IweenPricing objAirPrice = new IweenPricing();



                #endregion
                #region FareQuote
                //DataSet dsCrd = Crd;

                ArrayList airPriceResp = new ArrayList();
                ArrayList FinalairPriceResp = new ArrayList();

                string SellReferenceId = "";
                string statusmessage = "";
                string statuscode = "";
                airPriceResp = objAirPrice.GetFareQuote(Crd, objDs, ref Log, ref exep, Convert.ToString(FltDT.Rows[0]["SearchId"]), ref statusmessage);

                float TotPrice = (float)Math.Ceiling((Convert.ToDecimal(airPriceResp[0])));

                float totfare = (float)Math.Ceiling((Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"])));
                statuscode = statusmessage.Split(':')[0];
                //decimal TotPrice = Math.Ceiling(Convert.ToDecimal(airPriceResp[0]));
                //float TotPrice = float.Parse(Convert.ToString(airPriceResp[0]));


                //decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]);
                //decimal totfare = Convert.ToDecimal(FltDT.Compute("SUM(OriginalTF)", string.Empty));

                if (totfare > 0 && statuscode != "200")
                {
                    goto FareMisMatch;
                }
                else
                {
                    #region
                    StringBuilder LowFareSearchReq = new StringBuilder();
                    LowFareSearchReq.Append("<?xml version='1.0' encoding='UTF-8'?>");
                    LowFareSearchReq.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                    LowFareSearchReq.Append("<soapenv:Header/>");
                    LowFareSearchReq.Append("<soapenv:Body>");
                    LowFareSearchReq.Append("<mer:BookFlight>");
                    LowFareSearchReq.Append("<bookingrequest>");
                    LowFareSearchReq.Append("<taxInfoStr>" + sno[0] + "</taxInfoStr>");
                    LowFareSearchReq.Append("<promocode />");
                    LowFareSearchReq.Append("<promovalue />");
                    LowFareSearchReq.Append("<leademail>anuragraj.now@gmail.com</leademail>");
                    LowFareSearchReq.Append("<leadmobile>919555640762</leadmobile>");
                    LowFareSearchReq.Append("<leadcountry>IN</leadcountry>");
                    LowFareSearchReq.Append("<leadcity>ghaziabad</leadcity>");
                    LowFareSearchReq.Append("<leadstate>MZ</leadstate>");
                    LowFareSearchReq.Append("<paymentmode>cashcard</paymentmode>");
                    LowFareSearchReq.Append("<totalinvoice />");
                    LowFareSearchReq.Append("<domint>domestic</domint>");
                    LowFareSearchReq.Append("<credentials>");
                    LowFareSearchReq.Append("<agentid>DM24614</agentid>");
                    LowFareSearchReq.Append("<officeid>DM24614</officeid>");
                    LowFareSearchReq.Append("<password>Test@321</password>");
                    LowFareSearchReq.Append("<username>testcustomer</username>");
                    LowFareSearchReq.Append("<timestamp>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "</timestamp>");//2019-04-18 10:35:00.688
                    LowFareSearchReq.Append("</credentials>");
                    LowFareSearchReq.Append("<numadults>" + ADTPax.Count() + "</numadults>");
                    LowFareSearchReq.Append("<numchildren>" + CHDPax.Count() + "</numchildren>");
                    LowFareSearchReq.Append("<numinfants>" + INFPax.Count() + "</numinfants>");
                    LowFareSearchReq.Append("<passengerList>");
                    LowFareSearchReq.Append("<title>" + Convert.ToString(PaxDs.Tables[0].Rows[0]["Title"]) + "</title>");
                    LowFareSearchReq.Append("<first_name>" + Convert.ToString(PaxDs.Tables[0].Rows[0]["FName"]) + "</first_name>");
                    LowFareSearchReq.Append("<last_name>" + Convert.ToString(PaxDs.Tables[0].Rows[0]["LName"]) + "</last_name>");
                    LowFareSearchReq.Append("<dob></dob>");
                    LowFareSearchReq.Append("<type>Adult</type>");
                    LowFareSearchReq.Append("<tour_code />");
                    LowFareSearchReq.Append("<deal_code />");
                    LowFareSearchReq.Append("<frequent_flyer_number />");
                    LowFareSearchReq.Append("<visa />");
                    LowFareSearchReq.Append("<agentid>DM24614</agentid>");
                    LowFareSearchReq.Append("<meal_preference />");
                    LowFareSearchReq.Append("<seat_preference />");
                    LowFareSearchReq.Append("<additional_segmentinfo />");
                    LowFareSearchReq.Append("<paxssrinfo />");
                    LowFareSearchReq.Append("<profileid />");
                    LowFareSearchReq.Append("</passengerList>");
                    LowFareSearchReq.Append("<tripid />");
                    LowFareSearchReq.Append("<wsmode>TEST</wsmode>");
                    LowFareSearchReq.Append("</bookingrequest>");
                    LowFareSearchReq.Append("</mer:BookFlight>");
                    LowFareSearchReq.Append("</soapenv:Body>");
                    LowFareSearchReq.Append("</soapenv:Envelope>");

                    Log.Add("BookReq", LowFareSearchReq.ToString());
                    // MULTIUtitlity.SaveFile(LowFareSearchReq.ToString(), "BookReq");
                    Log.Add("BookResp", strRes);
                    string randomkey = Utility.GetRndm();
                    strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", LowFareSearchReq.ToString());
                    IweenAirAsiaAuth.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "Book_Req" + randomkey);
                    IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Book_Res" + randomkey);
                    XDocument xd2 = XDocument.Parse(strRes);
                    string statuscodeval = "";
                    string statusmessagecode = "";
                    string txid = "";
                    xd2.Descendants("getitineraryrequest").ToList().ForEach(Each =>
                    {
                        statuscodeval = Each.Element("statuscode").Value;
                        statusmessagecode = Each.Element("statusmessage").Value;
                        txid = Each.Element("txid").Value;
                    });
                    if (statuscodeval == "200")
                    {
                        StringBuilder itinerary = new StringBuilder();
                        itinerary.Append("<?xml version='1.0' encoding='UTF-8'?>");
                        itinerary.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                        itinerary.Append("<soapenv:Header/>");
                        itinerary.Append("<soapenv:Body>");
                        //itinerary.Append("<mer:ValidateFlightPrice>");
                        itinerary.Append("<getitineraryrequest>");
                        itinerary.Append("<credentials>");
                        itinerary.Append("<officeid>DM24614</officeid>");
                        itinerary.Append("<password>Test@321</password>");
                        itinerary.Append("<username>testcustomer</username>");
                        itinerary.Append("</credentials>");
                        itinerary.Append("<txid>" + txid + "</txid>");
                        itinerary.Append("<wsmode>TEST</wsmode>");
                        itinerary.Append("</getitineraryrequest>");
                        //itinerary.Append("</mer:ValidateFlightPrice>");
                        itinerary.Append("</soapenv:Body>");
                        itinerary.Append("</soapenv:Envelope>");
                        string randomkey2 = Utility.GetRndm();
                        strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", itinerary.ToString());
                        IweenAirAsiaAuth.SaveXml(itinerary.ToString(), SecurityGUID, "Book_Req" + randomkey2);
                        IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Book_Res" + randomkey2);
                    
                    }
                    #endregion
                }



                #endregion
            FareMisMatch:
                exep = exep + "Fare Amount Diffrence new fare:" + TotPrice + " old Fare:" + totfare.ToString();

            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ";
            }
            finally
            {
                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                //objCommonBAL.InsertMULTIBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                //    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                //    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                //    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }
            return airlinpnr;

        }
    }
}
