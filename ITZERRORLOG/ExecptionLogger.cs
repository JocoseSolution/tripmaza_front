﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;
namespace ITZERRORLOG
{
   public class ExecptionLogger
    {
       public static string get_Error_Det(string Err_code)
       {
           string s = Error_Resource.ResourceManager.GetString(Err_code);
           return s;
       }
       public static void FileHandling(string Err_Service, string Err_Code, Exception Err_Msg, string Err_Module)
       {
               SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
               SqlCommand cmd = new SqlCommand("InsertErrorLog", constr);
               try
               {
                   constr.Open();
                   System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(Err_Msg, true);
                   string linenumber = "0", fileNames = "";
                   if (trace != null)
                   {
                       if (trace.FrameCount > 0 && trace.GetFrame(trace.FrameCount - 1).GetFileName() != null)
                       {
                           linenumber = (trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()).ToString();
                           fileNames = (trace.GetFrame((trace.FrameCount - 1)).GetFileName()).ToString();
                       }
                       else
                           fileNames = Err_Msg.StackTrace;
                   }
                  
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.AddWithValue("@PageName", fileNames);
                   cmd.Parameters.AddWithValue("@ErrorMessage", Err_Msg.Message);
                   cmd.Parameters.AddWithValue("@LineNumber", linenumber);
                   cmd.ExecuteNonQuery();
               }
               catch (Exception ex1)
               { }
               finally
               { cmd.Dispose();  constr.Close();}
           
           try
           {
               //Error Details
               System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(Err_Msg, true);
               string ErrorLineNo = (trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()).ToString();
               string Err_Source = (trace.GetFrame((trace.FrameCount - 1)).GetFileName()).ToString();
                string ErrorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                string activeDir = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                // Specify a "currently active folder" 
                //string activeDir = @"D:\ITZError_Folder_\" + DateTime.Now.Date.ToString("dd-MMM-yyyy");
               // Creating the folder
               DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
               if (!Directory.Exists(objDirectoryInfo.FullName))
               {
                   string newPath = "", newFileName = "";
                   try
                   {
                       // Create a new file name. This example generates 
                       Directory.CreateDirectory(activeDir);
                       newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                       // Combine the new file name with the path
                       newPath = Path.Combine(activeDir, newFileName);
                   }
                   catch (Exception ex)
                   {
                        //string activeDir2 = @"D:\ITZError_Folder_\" + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                        string activeDir2 = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                       DirectoryInfo objDirectoryInfo2 = new DirectoryInfo(activeDir2);
                       // Create a new file name. This example generates
                       Directory.CreateDirectory(activeDir2);
                       newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                       // Combine the new file name with the path
                       newPath = Path.Combine(activeDir2, newFileName);
                   }
                   //// Create a new file name. This example generates 
                   //string newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                   //// Combine the new file name with the path
                   //string newPath = Path.Combine(activeDir, newFileName);
                   FileStream fs = new FileStream(newPath, FileMode.Append, FileAccess.Write);
                   StreamWriter sw = new StreamWriter(fs);
                   sw.WriteLine("Time:" + DateTime.Now.ToString());
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error Code" + Err_Code);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Actual Error Message:" + Err_Msg.Message);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("User Friendly Message:" + get_Error_Det(Err_Code));
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Errror Source:" + Err_Source);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error Module:" + Err_Module);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error LineNo:" + ErrorLineNo);
                   sw.Write(sw.NewLine);
                   sw.Flush();
                   sw.Close();
                   fs.Close();
               }
               else
               {
                   string newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                   // Combine the new file name with the path
                   string newPath = Path.Combine(activeDir, newFileName);
                   FileStream fs = new FileStream(newPath, FileMode.Append, FileAccess.Write);
                   StreamWriter sw = new StreamWriter(fs);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Time:" + DateTime.Now.ToString());
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error Code" + Err_Code);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Actual Error Message:" + Err_Msg.Message);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("User Friendly Message:" + get_Error_Det(Err_Code));
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Errror Source:" + Err_Source);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error Module:" + Err_Module);
                   sw.Write(sw.NewLine);
                   sw.WriteLine("Error LineNo:" + ErrorLineNo);
                   sw.Write(sw.NewLine);
                   sw.Flush();
                   sw.Close();
                   fs.Close();
                   //}

               }
           }
           catch (Exception ex)
           {

           }
       }
    }
}
