﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />

    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">

            <div class="theme-hero-area-bg" style="background-image: url(https://bollyhollybaba.com/wp-content/uploads/2019/03/business-background-picture-wide-hd-cool-wallpaper-85481.jpg);"></div>
            <%--<div class="theme-hero-area-mask theme-hero-area-mask-half"></div>--%>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Discover Flyagain</h1>
                            <p class="theme-page-header-subtitle">The Story of Our Company</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">

            <p style="text-align: justify;">
                From humble origins as a travel arm of FLYAGAINONLINE, as an Travel Agency that is Specialized in corporate Travel service provider & consolidator of all Domestic and international Airlines, catering to corporate, B2B and B2C.
            </p>


            <h4>Talented people make the difference</h4>
            <p style="text-align: justify;">
                Most companies recognise that people are their greatest asset and at FLYAGAINONLINE we know that our talented people really do make a difference. Led by an experienced Management Team, employees have the knowledge, experience and skill sets which create a power that, we believe, sets our Business aside from the rest of the market.
            </p>
            <p style="text-align: justify;">
                By employing only the highest calibre people, we ensure that knowledge and expertise are the cornerstones of everything we do – from individual bookings to formulating your entire international business travel strategy and communicating it to all levels within your organisation to ensure its success.
            </p>

            <h4>Fully understanding your objectives
            </h4>
            <p style="text-align: justify;">
                our whole culture is built on listening and this is fundamental in ensuring that we fully understand your objectives. It also means that we listen to each other, sharing innovative thinking and best practice to the benefit of all our clients.
            </p>
            <p style="text-align: justify;">
                Those who choose us quickly discover that we believe actions speak louder than words, through the delivery of the best solutions from the industry’s leading experts – ensuring they are robust and flexible enough to meet both the needs of today and tomorrow.
            </p>


            <h4>Innovation through Technology
            </h4>
            <p style="text-align: justify;">
                The Business Technology team at FLYAGAINONLINE continues to invent, build and deliver products and services which better serve the corporate travel programme manager and their travellers. We achieve this by using feedback from our own client base, ensuring that new developments are made with their needs in mind. The technology is not only commercially viable, but is often endorsed by some of the good names in Pharmaceuticals, Banking, Telecommunications, Insurance and Incentive Houses.
            </p>
            <p style="text-align: justify;">
                Through our extensive knowledge of both corporate travel and related technologies, we have designed our proprietary technology platform to offer security, speed and flexibility. This also provides the independence needed for delivery of services that meet local needs.
            </p>


            <h4>Maximising the Benefits
            </h4>
            <p style="text-align: justify;">
                we have also used our experience in working with a diverse range of clients to create a comprehensive range of technology based products.
            </p>
            <p style="text-align: justify;">
                A complete understanding of your company and travel programme objectives enables us to identify which solution, or combination of solutions, is best placed to bring real business benefit.
            </p>

            <h4>Breadth of Service
            </h4>
            <p style="text-align: justify;">
                FLYAGAINONLINE. is recognised as the B2B Consolidator with over 1000 + working agents on Flyagainonline.com and corporate travel services company which offers a very real breadth and depth of services: from basic transactions, such as reservations and bookings for air, hotel, rail and car rental, through to data provision, warehousing and analytics, benchmarking and programme management. Our mission is to deliver outstanding value to clients from their total travel spend.
            </p>



            <h3>Key Features:
            </h3>
            <h4>Travel Management Company
            </h4>
            <ul>
                <li>Specialists in B2B Consolidation and Corporate Travel Solutions
                </li>
                <li>All Domestic Low cost carrier API integrated along with AIR ASIA, AIR ARBIA, AIR INDIA EXPRESS and FLY DUBAI.
                </li>
                <li>Provide an end to end seamless service to SME, Local clients.</li>
                <li>Supported by senior travel consultants, using the latest technology with unique travel management tools to effectively manage clients travel portfolio to deliver service excellence and cost benefits</li>

            </ul>


            <h3>Services
            </h3>
            <ul>
                <li>Air Reservations
                </li>
                <li>Online Booking tool
                </li>
                <li>Car Hire
                </li>
                <li>Management   Information
                </li>
                <li>Hotel booking and bill back service
                </li>
                <li>Meet and Assist
                </li>
                <li>Rail and ferry tickets
                </li>
                <li>Pre  and post trip reporting</li>
                <li>Passport and visa
                </li>
                <li>Travel tracker</li>
                <li>24 hour Out of hours service
                </li>
                <li>Account Management</li>

            </ul>


            <h4>Full Travel Service Delivery is supported by a range of FLYAGAINONLINE. Products which include:</h4>
            <ul>

                <li>MICE
                </li>
                <li>Corporate Leisure</li>
                <li>E-Statements</li>
                <li>Forex</li>
                <li>Management</li>
                <li>Director with 20+ years travel experience</li>
                <li>Supported by Operations Manager, Service Delivery Manager, Quality / SLA Manager, Sales Manager & Executives.  Finance Supervisor.</li>
                <li>Client /Business Management – Customer Relationship Managers available to manage clients.</li>

            </ul>



            <h3>What’s truly unique about FLYAGAINONLINE</h3>
            <ul>
                <li>Flexible and focused approach to managing National Accounts
                </li>
                <li>Agents can access a suite of quality performance tools</li>
                <li>Dedicated Key Account Managers</li>
                <li>Financials and billing capabilities
                </li>
                <li>Credit and Lodge card account
                </li>
                <li>Instant invoicing at the time of ticket issuance which is fully integrated from Online tool
                </li>
                <li>E-invoice & statements available
                </li>
                <li>Team of experienced account professionals
                </li>
                <li>Transparent Management / Transaction Fees
                </li>
                <li>Features - Online Booking Tool
                </li>
                <li>User-friendly web based interface
                </li>
                <li>Travel Policy Compliance</li>
                <li>Display of lowest air fare</li>
                <li>Hotel display closest to office location</li>
                <li>Access to ‘internet only’ content including global Low Cost Carriers (LCC)
                </li>
                <li>Real-time reporting and analysis
                </li>
                <li>Lost savings reports
                </li>
                <li>Automated multi-level approval workflows
                </li>
                <li>24/7 online travel booking anywhere</li>
                <li>Automated travel policy</li>
                <li>Direct access to clients’ negotiated rates for travel products and services
                </li>
                <li>Mobile application for Blackberry, I-Phone and Android.
                </li>
                <li>Unique - Service Centre Quality and SLA system
                </li>
                <li>Extensive Capabilities; Voice & Screen Recording, Live Monitoring, Innovative Playback Features
                </li>
                <li>Integrated coaching and feedback; Coaching Notes, Voice/Screen Comments, Audio Export
                </li>
                <li>Scalable system architecture that can be used on all leading industry servers and can interface with major Automatic Call Distributors (ACDs) and Diallers
                </li>
                <li>Improved consistency & quality of customer interactions across all channels – telephone, email & chat
                </li>

                <li>Automated reporting for tracking & trending</li>
                <li>Identification of agent training needs for better transaction quality
                </li>
                <li>Measured agent adherence to internal policies & procedures</li>

            </ul>


            <h3>Commercial proposal:</h3>
            <p style="text-align: justify;">Our pricing strategy is built around bringing savings in over all travel spent rather than focus on service fee.  Our approach is to delivery cost reduction and savings programs on behalf of our clients.</p>

            <p style="text-align: justify;">
                FLYAGAINONLINE believes in providing full Management program to our travel partners and hence, open to providing pricing options, to choose best as per our Client’s requirement.
            </p>


            <h3>What are your key travel management challenges?

            </h3>
            <p style="text-align: justify;">No matter what the budget, there’s always room to squeeze more from it. We’d love to have the opportunity to discuss the challenges facing your organization. We believe we have the people, technology and international connections to help.
</p>

            <p style="text-align: justify;">We’d be delighted to demonstrate our online travel management solutions, so you can see first-hand
</p>
            <p style="text-align: justify;">How you could introduce new efficiencies.
</p>


            <br />
            <br />

            <h5>Get in touch today to see how FLYAGAINONLINE can help your business fly.     
</h5>


        </div>
    </div>

</asp:Content>

